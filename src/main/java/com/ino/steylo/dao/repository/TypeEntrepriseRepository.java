package com.ino.steylo.dao.repository;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.springframework.dao.DataAccessException;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.ino.steylo.helper.dto.*;
import com.ino.steylo.helper.contrat.*;
import com.ino.steylo.dao.entity.*;

/**
 * Repository : TypeEntreprise.
 */
@Repository
public interface TypeEntrepriseRepository extends JpaRepository<TypeEntreprise, Integer> {
	/**
	 * Finds TypeEntreprise by using id as a search criteria.
	 * 
	 * @param id
	 * @return An Object TypeEntreprise whose id is equals to the given id. If
	 *         no TypeEntreprise is found, this method returns null.
	 */
	@Query("select e from TypeEntreprise e where e.id = :id")
	TypeEntreprise findById(@Param("id")Integer id);

	/**
	 * Finds TypeEntreprise by using libelle as a search criteria.
	 * 
	 * @param libelle
	 * @return An Object TypeEntreprise whose libelle is equals to the given libelle. If
	 *         no TypeEntreprise is found, this method returns null.
	 */
	@Query("select e from TypeEntreprise e where e.libelle = :libelle")
	List<TypeEntreprise> findByLibelle(@Param("libelle")String libelle);

	/**
	 * Finds List of TypeEntreprise by using typeEntrepriseDto as a search criteria.
	 * 
	 * @param request, em
	 * @return A List of TypeEntreprise
	 * @throws DataAccessException,ParseException
	 */
	public default List<TypeEntreprise> getByCriteria(Request request, EntityManager em) throws DataAccessException, ParseException {
		String req = "select e from TypeEntreprise e where e IS NOT NULL";
		HashMap<String, Object> param = new HashMap<>();

		TypeEntrepriseDto dto = request.getDataTypeEntreprise();
		
		req = generateCriteria(dto, req, param);
		req += " order by  e.id desc";

		TypedQuery<TypeEntreprise> query = em.createQuery(req, TypeEntreprise.class);

		for (Map.Entry<String, Object> entry : param.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}

		if (request.getIndex() != null && request.getSize() != null) {
			query.setFirstResult(request.getIndex() * request.getSize());
			query.setMaxResults(request.getSize());
		}

		return query.getResultList();
	}

	/**
	 * Finds count of TypeEntreprise by using typeEntrepriseDto as a search criteria.
	 * 
	 * @param request,em
	 * @return Number of TypeEntreprise
	 * 
	 */
	public default Long count(Request request, EntityManager em) throws DataAccessException, ParseException  {
		String req = "select new com.ino.steylo.helper.contrat.NumberDto(count(e.id)) from TypeEntreprise e where e IS NOT NULL";
		HashMap<String, Object> param = new HashMap<>();

		TypeEntrepriseDto dto = request.getDataTypeEntreprise();

		req = generateCriteria(dto, req, param);

		javax.persistence.Query query = em.createQuery(req);

		for (Map.Entry<String, Object> entry : param.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}

		NumberDto number = (NumberDto) query.getResultList().get(0);
		return number.getNombre();
	}

	default String generateCriteria(TypeEntrepriseDto dto, String req, HashMap<String, Object> param) throws ParseException{
		if (dto != null) {
			if (dto.getId()!= null && dto.getId() > 0) {
				req += " AND e.id = :id";
				param.put("id", dto.getId());
			}
			if (dto.getLibelle()!= null && !dto.getLibelle().isEmpty()) {
				req += " AND e.libelle = :libelle";
				param.put("libelle", dto.getLibelle());
			}
		}
		
		return req;
	}
}
