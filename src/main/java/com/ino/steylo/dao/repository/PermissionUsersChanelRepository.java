package com.ino.steylo.dao.repository;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.springframework.dao.DataAccessException;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.ino.steylo.helper.dto.*;
import com.ino.steylo.helper.contrat.*;
import com.ino.steylo.dao.entity.*;

/**
 * Repository : PermissionUsersChanel.
 */
@Repository
public interface PermissionUsersChanelRepository extends JpaRepository<PermissionUsersChanel, Integer> {
	/**
	 * Finds PermissionUsersChanel by using id as a search criteria.
	 * 
	 * @param id
	 * @return An Object PermissionUsersChanel whose id is equals to the given id. If
	 *         no PermissionUsersChanel is found, this method returns null.
	 */
	@Query("select e from PermissionUsersChanel e where e.id = :id")
	PermissionUsersChanel findById(@Param("id")Integer id);

	/**
	 * Finds PermissionUsersChanel by using idPermission as a search criteria.
	 * 
	 * @param idPermission
	 * @return An Object PermissionUsersChanel whose idPermission is equals to the given idPermission. If
	 *         no PermissionUsersChanel is found, this method returns null.
	 */
	@Query("select e from PermissionUsersChanel e where e.idPermission = :idPermission")
	List<PermissionUsersChanel> findByIdPermission(@Param("idPermission")Integer idPermission);
	/**
	 * Finds PermissionUsersChanel by using idChanelUser as a search criteria.
	 * 
	 * @param idChanelUser
	 * @return An Object PermissionUsersChanel whose idChanelUser is equals to the given idChanelUser. If
	 *         no PermissionUsersChanel is found, this method returns null.
	 */
	@Query("select e from PermissionUsersChanel e where e.idChanelUser = :idChanelUser")
	List<PermissionUsersChanel> findByIdChanelUser(@Param("idChanelUser")Integer idChanelUser);

	/**
	 * Finds List of PermissionUsersChanel by using permissionUsersChanelDto as a search criteria.
	 * 
	 * @param request, em
	 * @return A List of PermissionUsersChanel
	 * @throws DataAccessException,ParseException
	 */
	public default List<PermissionUsersChanel> getByCriteria(Request request, EntityManager em) throws DataAccessException, ParseException {
		String req = "select e from PermissionUsersChanel e where e IS NOT NULL";
		HashMap<String, Object> param = new HashMap<>();

		PermissionUsersChanelDto dto = request.getDataPermissionUsersChanel();
		
		req = generateCriteria(dto, req, param);
		req += " order by  e.id desc";

		TypedQuery<PermissionUsersChanel> query = em.createQuery(req, PermissionUsersChanel.class);

		for (Map.Entry<String, Object> entry : param.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}

		if (request.getIndex() != null && request.getSize() != null) {
			query.setFirstResult(request.getIndex() * request.getSize());
			query.setMaxResults(request.getSize());
		}

		return query.getResultList();
	}

	/**
	 * Finds count of PermissionUsersChanel by using permissionUsersChanelDto as a search criteria.
	 * 
	 * @param request,em
	 * @return Number of PermissionUsersChanel
	 * 
	 */
	public default Long count(Request request, EntityManager em) throws DataAccessException, ParseException  {
		String req = "select new com.ino.steylo.helper.contrat.NumberDto(count(e.id)) from PermissionUsersChanel e where e IS NOT NULL";
		HashMap<String, Object> param = new HashMap<>();

		PermissionUsersChanelDto dto = request.getDataPermissionUsersChanel();

		req = generateCriteria(dto, req, param);

		javax.persistence.Query query = em.createQuery(req);

		for (Map.Entry<String, Object> entry : param.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}

		NumberDto number = (NumberDto) query.getResultList().get(0);
		return number.getNombre();
	}

	default String generateCriteria(PermissionUsersChanelDto dto, String req, HashMap<String, Object> param) throws ParseException{
		if (dto != null) {
			if (dto.getId()!= null && dto.getId() > 0) {
				req += " AND e.id = :id";
				param.put("id", dto.getId());
			}
			if (dto.getIdPermission()!= null && dto.getIdPermission() > 0) {
				req += " AND e.idPermission = :idPermission";
				param.put("idPermission", dto.getIdPermission());
			}
			if (dto.getIdChanelUser()!= null && dto.getIdChanelUser() > 0) {
				req += " AND e.idChanelUser = :idChanelUser";
				param.put("idChanelUser", dto.getIdChanelUser());
			}
		}
		
		return req;
	}
}
