package com.ino.steylo.dao.repository;

import java.util.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.springframework.dao.DataAccessException;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.ino.steylo.helper.dto.*;
import com.ino.steylo.helper.contrat.*;
import com.ino.steylo.dao.entity.*;

/**
 * Repository : AbonneDemande.
 */
@Repository
public interface AbonneDemandeRepository extends JpaRepository<AbonneDemande, Integer> {
	/**
	 * Finds AbonneDemande by using id as a search criteria.
	 * 
	 * @param id
	 * @return An Object AbonneDemande whose id is equals to the given id. If
	 *         no AbonneDemande is found, this method returns null.
	 */
	@Query("select e from AbonneDemande e where e.id = :id")
	AbonneDemande findById(@Param("id")Integer id);

	/**
	 * Finds AbonneDemande by using dateDemande as a search criteria.
	 * 
	 * @param dateDemande
	 * @return An Object AbonneDemande whose dateDemande is equals to the given dateDemande. If
	 *         no AbonneDemande is found, this method returns null.
	 */
	@Query("select e from AbonneDemande e where e.dateDemande = :dateDemande")
	List<AbonneDemande> findByDateDemande(@Param("dateDemande")Date dateDemande);
	/**
	 * Finds AbonneDemande by using statut as a search criteria.
	 * 
	 * @param statut
	 * @return An Object AbonneDemande whose statut is equals to the given statut. If
	 *         no AbonneDemande is found, this method returns null.
	 */
	@Query("select e from AbonneDemande e where e.statut = :statut")
	List<AbonneDemande> findByStatut(@Param("statut")Integer statut);
	/**
	 * Finds AbonneDemande by using commentaire as a search criteria.
	 * 
	 * @param commentaire
	 * @return An Object AbonneDemande whose commentaire is equals to the given commentaire. If
	 *         no AbonneDemande is found, this method returns null.
	 */
	@Query("select e from AbonneDemande e where e.commentaire = :commentaire")
	List<AbonneDemande> findByCommentaire(@Param("commentaire")String commentaire);
	/**
	 * Finds AbonneDemande by using dateDecision as a search criteria.
	 * 
	 * @param dateDecision
	 * @return An Object AbonneDemande whose dateDecision is equals to the given dateDecision. If
	 *         no AbonneDemande is found, this method returns null.
	 */
	@Query("select e from AbonneDemande e where e.dateDecision = :dateDecision")
	List<AbonneDemande> findByDateDecision(@Param("dateDecision")Date dateDecision);
	/**
	 * Finds AbonneDemande by using userDecisionId as a search criteria.
	 * 
	 * @param userDecisionId
	 * @return An Object AbonneDemande whose userDecisionId is equals to the given userDecisionId. If
	 *         no AbonneDemande is found, this method returns null.
	 */
	@Query("select e from AbonneDemande e where e.userDecisionId = :userDecisionId")
	List<AbonneDemande> findByUserDecisionId(@Param("userDecisionId")Integer userDecisionId);
	/**
	 * Finds AbonneDemande by using demandeState as a search criteria.
	 * 
	 * @param demandeState
	 * @return An Object AbonneDemande whose demandeState is equals to the given demandeState. If
	 *         no AbonneDemande is found, this method returns null.
	 */
	@Query("select e from AbonneDemande e where e.demandeState = :demandeState")
	List<AbonneDemande> findByDemandeState(@Param("demandeState")Integer demandeState);
	/**
	 * Finds AbonneDemande by using aDesouscrire as a search criteria.
	 * 
	 * @param aDesouscrire
	 * @return An Object AbonneDemande whose aDesouscrire is equals to the given aDesouscrire. If
	 *         no AbonneDemande is found, this method returns null.
	 */
	@Query("select e from AbonneDemande e where e.aDesouscrire = :aDesouscrire")
	List<AbonneDemande> findByADesouscrire(@Param("aDesouscrire")Boolean aDesouscrire);
	/**
	 * Finds AbonneDemande by using dateDesouscription as a search criteria.
	 * 
	 * @param dateDesouscription
	 * @return An Object AbonneDemande whose dateDesouscription is equals to the given dateDesouscription. If
	 *         no AbonneDemande is found, this method returns null.
	 */
	@Query("select e from AbonneDemande e where e.dateDesouscription = :dateDesouscription")
	List<AbonneDemande> findByDateDesouscription(@Param("dateDesouscription")Date dateDesouscription);
	/**
	 * Finds AbonneDemande by using motifRejet as a search criteria.
	 * 
	 * @param motifRejet
	 * @return An Object AbonneDemande whose motifRejet is equals to the given motifRejet. If
	 *         no AbonneDemande is found, this method returns null.
	 */
	@Query("select e from AbonneDemande e where e.motifRejet = :motifRejet")
	List<AbonneDemande> findByMotifRejet(@Param("motifRejet")String motifRejet);

	/**
	 * Finds AbonneDemande by using idAbonne as a search criteria.
	 * 
	 * @param idAbonne
	 * @return An Object AbonneDemande whose idAbonne is equals to the given idAbonne. If
	 *         no AbonneDemande is found, this method returns null.
	 */
	@Query("select e from AbonneDemande e where e.abonne.id = :idAbonne")
	List<AbonneDemande> findByIdAbonne(@Param("idAbonne")Long idAbonne);

	/**
	 * Finds AbonneDemande by using idChannel as a search criteria.
	 * 
	 * @param idChannel
	 * @return An Object AbonneDemande whose idChannel is equals to the given idChannel. If
	 *         no AbonneDemande is found, this method returns null.
	 */
	@Query("select e from AbonneDemande e where e.chanel.id = :idChannel")
	List<AbonneDemande> findByIdChannel(@Param("idChannel")Integer idChannel);

	/**
	 * Finds List of AbonneDemande by using abonneDemandeDto as a search criteria.
	 * 
	 * @param request, em
	 * @return A List of AbonneDemande
	 * @throws DataAccessException,ParseException
	 */
	public default List<AbonneDemande> getByCriteria(Request request, EntityManager em) throws DataAccessException, ParseException {
		String req = "select e from AbonneDemande e where e IS NOT NULL";
		HashMap<String, Object> param = new HashMap<>();

		AbonneDemandeDto dto = request.getDataAbonneDemande();

		if (dto.getNomAbonne() != null && !dto.getNomAbonne().isEmpty()) {
			req += " AND e.abonne.nom LIKE :nomAbonne";
			param.put("nomAbonne", "%"+dto.getNomAbonne()+"%");
		}
		if (dto.getPrenomAbonne() != null && !dto.getPrenomAbonne().isEmpty()) {
			req += " AND e.abonne.prenoms LIKE :prenomAbonne";
			param.put("prenomAbonne", "%"+dto.getPrenomAbonne()+"%");
		}
		
		req = generateCriteria(dto, req, param);
		req += " order by  e.id desc";

		TypedQuery<AbonneDemande> query = em.createQuery(req, AbonneDemande.class);

		for (Map.Entry<String, Object> entry : param.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}

		if (request.getIndex() != null && request.getSize() != null) {
			query.setFirstResult(request.getIndex() * request.getSize());
			query.setMaxResults(request.getSize());
		}

		return query.getResultList();
	}

	/**
	 * Finds count of AbonneDemande by using abonneDemandeDto as a search criteria.
	 * 
	 * @param request,em
	 * @return Number of AbonneDemande
	 * 
	 */
	public default Long count(Request request, EntityManager em) throws DataAccessException, ParseException  {
		String req = "select new com.ino.steylo.helper.contrat.NumberDto(count(e.id)) from AbonneDemande e where e IS NOT NULL";
		HashMap<String, Object> param = new HashMap<>();

		AbonneDemandeDto dto = request.getDataAbonneDemande();

		if (dto.getNomAbonne() != null && !dto.getNomAbonne().isEmpty()) {
			req += " AND e.abonne.nom LIKE :nomAbonne";
			param.put("nomAbonne", "%"+dto.getNomAbonne()+"%");
		}
		if (dto.getPrenomAbonne() != null && !dto.getPrenomAbonne().isEmpty()) {
			req += " AND e.abonne.prenoms LIKE :prenomAbonne";
			param.put("prenomAbonne", "%"+dto.getPrenomAbonne()+"%");
		}

		req = generateCriteria(dto, req, param);

		javax.persistence.Query query = em.createQuery(req);

		for (Map.Entry<String, Object> entry : param.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}

		NumberDto number = (NumberDto) query.getResultList().get(0);
		return number.getNombre();
	}

	default String generateCriteria(AbonneDemandeDto dto, String req, HashMap<String, Object> param) throws ParseException{
		if (dto != null) {
			if (dto.getId()!= null && dto.getId() > 0) {
				req += " AND e.id = :id";
				param.put("id", dto.getId());
			}
			if (dto.getDateDemande()!= null && !dto.getDateDemande().isEmpty()) {
				req += " AND FUNCTION('DAY', e.dateDemande) = FUNCTION('DAY', :dateDemande) AND FUNCTION('MONTH', e.dateDemande) = FUNCTION('MONTH', :dateDemande) AND FUNCTION('YEAR', e.dateDemande) = FUNCTION('YEAR', :dateDemande) ";
				param.put("dateDemande",  new SimpleDateFormat("dd/MM/yyyy").parse(dto.getDateDemande()));
			}
			if (dto.getStatut()!= null && dto.getStatut() >= 0) {
				req += " AND e.statut = :statut";
				param.put("statut", dto.getStatut());
			}
			if (dto.getCommentaire()!= null && !dto.getCommentaire().isEmpty()) {
				req += " AND e.commentaire = :commentaire";
				param.put("commentaire", dto.getCommentaire());
			}
			if (dto.getDateDecision()!= null && !dto.getDateDecision().isEmpty()) {
				req += " AND FUNCTION('DAY', e.dateDecision) = FUNCTION('DAY', :dateDecision) AND FUNCTION('MONTH', e.dateDecision) = FUNCTION('MONTH', :dateDecision) AND FUNCTION('YEAR', e.dateDecision) = FUNCTION('YEAR', :dateDecision) ";
				param.put("dateDecision",  new SimpleDateFormat("dd/MM/yyyy").parse(dto.getDateDecision()));
			}
			if (dto.getUserDecisionId()!= null && dto.getUserDecisionId() > 0) {
				req += " AND e.userDecisionId = :userDecisionId";
				param.put("userDecisionId", dto.getUserDecisionId());
			}
			if (dto.getDemandeState()!= null && dto.getDemandeState() >= 0) {
				req += " AND e.demandeState = :demandeState";
				param.put("demandeState", dto.getDemandeState());
			}
			if (dto.getADesouscrire()!= null) {
				req += " AND e.aDesouscrire = :aDesouscrire";
				param.put("aDesouscrire", dto.getADesouscrire());
			}
			if (dto.getDateDesouscription()!= null && !dto.getDateDesouscription().isEmpty()) {
				req += " AND FUNCTION('DAY', e.dateDesouscription) = FUNCTION('DAY', :dateDesouscription) AND FUNCTION('MONTH', e.dateDesouscription) = FUNCTION('MONTH', :dateDesouscription) AND FUNCTION('YEAR', e.dateDesouscription) = FUNCTION('YEAR', :dateDesouscription) ";
				param.put("dateDesouscription",  new SimpleDateFormat("dd/MM/yyyy").parse(dto.getDateDesouscription()));
			}
			if (dto.getMotifRejet()!= null && !dto.getMotifRejet().isEmpty()) {
				req += " AND e.motifRejet = :motifRejet";
				param.put("motifRejet", dto.getMotifRejet());
			}
			if (dto.getIdAbonne()!= null && dto.getIdAbonne() > 0) {
				req += " AND e.abonne.id = :idAbonne";
				param.put("idAbonne", dto.getIdAbonne());
			}
			if (dto.getIdChannel()!= null && dto.getIdChannel() > 0) {
				req += " AND e.chanel.id = :idChannel";
				param.put("idChannel", dto.getIdChannel());
			}
		}
		
		return req;
	}
}
