package com.ino.steylo.dao.repository;

import java.util.List;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.springframework.dao.DataAccessException;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.ino.steylo.helper.dto.*;
import com.ino.steylo.helper.contrat.*;
import com.ino.steylo.dao.entity.*;

/**
 * Repository : CategorieProduit.
 */
@Repository
public interface CategorieProduitRepository extends JpaRepository<CategorieProduit, Integer> {
	/**
	 * Finds CategorieProduit by using id as a search criteria.
	 * 
	 * @param id
	 * @return An Object CategorieProduit whose id is equals to the given id. If
	 *         no CategorieProduit is found, this method returns null.
	 */
	@Query("select e from CategorieProduit e where e.id = :id")
	CategorieProduit findById(@Param("id")Integer id);

	/**
	 * Finds CategorieProduit by using libelle as a search criteria.
	 * 
	 * @param libelle
	 * @return An Object CategorieProduit whose libelle is equals to the given libelle. If
	 *         no CategorieProduit is found, this method returns null.
	 */
	@Query("select e from CategorieProduit e where e.libelle = :libelle")
	List<CategorieProduit> findByLibelle(@Param("libelle")String libelle);

	/**
	 * Finds CategorieProduit by using idMarque as a search criteria.
	 *
	 * @param idMarque
	 * @return An Object CategorieProduit whose idMarque is equals to the given idMarque. If
	 *         no CategorieProduit is found, this method returns null.
	 */
	@Query(value = "SELECT DISTINCT cp.* FROM produit p, categorie_produit cp WHERE p.ID_CATEGORIE = cp.id AND p.ID_MARQUE = :idMarque", nativeQuery = true)
	List<CategorieProduit> findByIdMarque(@Param("idMarque")Integer idMarque);

	/**
	 * Finds List of CategorieProduit by using categorieProduitDto as a search criteria.
	 * 
	 * @param request, em
	 * @return A List of CategorieProduit
	 * @throws DataAccessException,ParseException
	 */
	public default List<CategorieProduit> getByCriteria(Request request, EntityManager em) throws DataAccessException, ParseException {
		String req = "select e from CategorieProduit e where e IS NOT NULL";
		HashMap<String, Object> param = new HashMap<>();

		CategorieProduitDto dto = request.getDataCategorieProduit();
		
		req = generateCriteria(dto, req, param);
		req += " order by  e.id desc";

		TypedQuery<CategorieProduit> query = em.createQuery(req, CategorieProduit.class);

		for (Map.Entry<String, Object> entry : param.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}

		if (request.getIndex() != null && request.getSize() != null) {
			query.setFirstResult(request.getIndex() * request.getSize());
			query.setMaxResults(request.getSize());
		}

		return query.getResultList();
	}

	/**
	 * Finds count of CategorieProduit by using categorieProduitDto as a search criteria.
	 * 
	 * @param request,em
	 * @return Number of CategorieProduit
	 * 
	 */
	public default Long count(Request request, EntityManager em) throws DataAccessException, ParseException  {
		String req = "select new com.ino.steylo.helper.contrat.NumberDto(count(e.id)) from CategorieProduit e where e IS NOT NULL";
		HashMap<String, Object> param = new HashMap<>();

		CategorieProduitDto dto = request.getDataCategorieProduit();

		req = generateCriteria(dto, req, param);

		javax.persistence.Query query = em.createQuery(req);

		for (Map.Entry<String, Object> entry : param.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}

		NumberDto number = (NumberDto) query.getResultList().get(0);
		return number.getNombre();
	}

	default String generateCriteria(CategorieProduitDto dto, String req, HashMap<String, Object> param) throws ParseException{
		if (dto != null) {
			if (dto.getId()!= null && dto.getId() > 0) {
				req += " AND e.id = :id";
				param.put("id", dto.getId());
			}
			if (dto.getLibelle()!= null && !dto.getLibelle().isEmpty()) {
				req += " AND e.libelle = :libelle";
				param.put("libelle", dto.getLibelle());
			}
		}
		
		return req;
	}
}
