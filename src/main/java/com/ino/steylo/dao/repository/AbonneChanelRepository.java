package com.ino.steylo.dao.repository;

import java.util.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.springframework.dao.DataAccessException;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.ino.steylo.helper.dto.*;
import com.ino.steylo.helper.contrat.*;
import com.ino.steylo.dao.entity.*;

/**
 * Repository : AbonneChanel.
 */
@Repository
public interface AbonneChanelRepository extends JpaRepository<AbonneChanel, Long> {
	/**
	 * Finds AbonneChanel by using id as a search criteria.
	 * 
	 * @param id
	 * @return An Object AbonneChanel whose id is equals to the given id. If
	 *         no AbonneChanel is found, this method returns null.
	 */
	@Query("select e from AbonneChanel e where e.id = :id")
	AbonneChanel findById(@Param("id")Long id);

	/**
	 * Finds AbonneChanel by using idChanel as a search criteria.
	 * 
	 * @param idChanel
	 * @return An Object AbonneChanel whose idChanel is equals to the given idChanel. If
	 *         no AbonneChanel is found, this method returns null.
	 */
	@Query("select e from AbonneChanel e where e.idChanel = :idChanel")
	List<AbonneChanel> findByIdChanel(@Param("idChanel")Integer idChanel);
	/**
	 * Finds AbonneChanel by using idAbonne as a search criteria.
	 * 
	 * @param idAbonne
	 * @return An Object AbonneChanel whose idAbonne is equals to the given idAbonne. If
	 *         no AbonneChanel is found, this method returns null.
	 */
	@Query("select e from AbonneChanel e where e.idAbonne = :idAbonne")
	List<AbonneChanel> findByIdAbonne(@Param("idAbonne")Integer idAbonne);
	/**
	 * Finds AbonneChanel by using dateDesouscription as a search criteria.
	 * 
	 * @param dateDesouscription
	 * @return An Object AbonneChanel whose dateDesouscription is equals to the given dateDesouscription. If
	 *         no AbonneChanel is found, this method returns null.
	 */
	@Query("select e from AbonneChanel e where e.dateDesouscription = :dateDesouscription")
	List<AbonneChanel> findByDateDesouscription(@Param("dateDesouscription")Date dateDesouscription);
	/**
	 * Finds AbonneChanel by using dateSouscription as a search criteria.
	 * 
	 * @param dateSouscription
	 * @return An Object AbonneChanel whose dateSouscription is equals to the given dateSouscription. If
	 *         no AbonneChanel is found, this method returns null.
	 */
	@Query("select e from AbonneChanel e where e.dateSouscription = :dateSouscription")
	List<AbonneChanel> findByDateSouscription(@Param("dateSouscription")Date dateSouscription);
	/**
	 * Finds AbonneChanel by using metadata as a search criteria.
	 * 
	 * @param metadata
	 * @return An Object AbonneChanel whose metadata is equals to the given metadata. If
	 *         no AbonneChanel is found, this method returns null.
	 */
	@Query("select e from AbonneChanel e where e.metadata = :metadata")
	List<AbonneChanel> findByMetadata(@Param("metadata")String metadata);
	/**
	 * Finds AbonneChanel by using isActive as a search criteria.
	 * 
	 * @param isActive
	 * @return An Object AbonneChanel whose isActive is equals to the given isActive. If
	 *         no AbonneChanel is found, this method returns null.
	 */
	@Query("select e from AbonneChanel e where e.isActive = :isActive")
	List<AbonneChanel> findByIsActive(@Param("isActive")Boolean isActive);
	/**
	 * Finds AbonneChanel by using isSeller as a search criteria.
	 * 
	 * @param isSeller
	 * @return An Object AbonneChanel whose isSeller is equals to the given isSeller. If
	 *         no AbonneChanel is found, this method returns null.
	 */
	@Query("select e from AbonneChanel e where e.isSeller = :isSeller")
	List<AbonneChanel> findByIsSeller(@Param("isSeller")Boolean isSeller);

	/**
	 * Finds List of AbonneChanel by using abonneChanelDto as a search criteria.
	 * 
	 * @param request, em
	 * @return A List of AbonneChanel
	 * @throws DataAccessException,ParseException
	 */
	public default List<AbonneChanel> getByCriteria(Request request, EntityManager em) throws DataAccessException, ParseException {
		String req = "select e from AbonneChanel e where e IS NOT NULL";
		HashMap<String, Object> param = new HashMap<>();

		AbonneChanelDto dto = request.getDataAbonneChanel();
		
		req = generateCriteria(dto, req, param);
		req += " order by  e.id desc";

		TypedQuery<AbonneChanel> query = em.createQuery(req, AbonneChanel.class);

		for (Map.Entry<String, Object> entry : param.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}

		if (request.getIndex() != null && request.getSize() != null) {
			query.setFirstResult(request.getIndex() * request.getSize());
			query.setMaxResults(request.getSize());
		}

		return query.getResultList();
	}

	/**
	 * Finds count of AbonneChanel by using abonneChanelDto as a search criteria.
	 * 
	 * @param request,em
	 * @return Number of AbonneChanel
	 * 
	 */
	public default Long count(Request request, EntityManager em) throws DataAccessException, ParseException  {
		String req = "select new com.ino.steylo.helper.contrat.NumberDto(count(e.id)) from AbonneChanel e where e IS NOT NULL";
		HashMap<String, Object> param = new HashMap<>();

		AbonneChanelDto dto = request.getDataAbonneChanel();

		req = generateCriteria(dto, req, param);

		javax.persistence.Query query = em.createQuery(req);

		for (Map.Entry<String, Object> entry : param.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}

		NumberDto number = (NumberDto) query.getResultList().get(0);
		return number.getNombre();
	}

	default String generateCriteria(AbonneChanelDto dto, String req, HashMap<String, Object> param) throws ParseException{
		if (dto != null) {
			if (dto.getId()!= null && dto.getId() > 0) {
				req += " AND e.id = :id";
				param.put("id", dto.getId());
			}
			if (dto.getIdChanel()!= null && dto.getIdChanel() > 0) {
				req += " AND e.idChanel = :idChanel";
				param.put("idChanel", dto.getIdChanel());
			}
			if (dto.getIdAbonne()!= null && dto.getIdAbonne() > 0) {
				req += " AND e.idAbonne = :idAbonne";
				param.put("idAbonne", dto.getIdAbonne());
			}
			if (dto.getDateDesouscription()!= null && !dto.getDateDesouscription().isEmpty()) {
				req += " AND FUNCTION('DAY', e.dateDesouscription) = FUNCTION('DAY', :dateDesouscription) AND FUNCTION('MONTH', e.dateDesouscription) = FUNCTION('MONTH', :dateDesouscription) AND FUNCTION('YEAR', e.dateDesouscription) = FUNCTION('YEAR', :dateDesouscription) ";
				param.put("dateDesouscription",  new SimpleDateFormat("dd/MM/yyyy").parse(dto.getDateDesouscription()));
			}
			if (dto.getDateSouscription()!= null && !dto.getDateSouscription().isEmpty()) {
				req += " AND FUNCTION('DAY', e.dateSouscription) = FUNCTION('DAY', :dateSouscription) AND FUNCTION('MONTH', e.dateSouscription) = FUNCTION('MONTH', :dateSouscription) AND FUNCTION('YEAR', e.dateSouscription) = FUNCTION('YEAR', :dateSouscription) ";
				param.put("dateSouscription",  new SimpleDateFormat("dd/MM/yyyy").parse(dto.getDateSouscription()));
			}
			if (dto.getMetadata()!= null && !dto.getMetadata().isEmpty()) {
				req += " AND e.metadata = :metadata";
				param.put("metadata", dto.getMetadata());
			}
			if (dto.getIsActive()!= null) {
				req += " AND e.isActive = :isActive";
				param.put("isActive", dto.getIsActive());
			}
			if (dto.getIsSeller()!= null) {
				req += " AND e.isSeller = :isSeller";
				param.put("isSeller", dto.getIsSeller());
			}
		}
		
		return req;
	}
}
