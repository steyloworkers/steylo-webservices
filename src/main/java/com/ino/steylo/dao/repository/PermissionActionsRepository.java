package com.ino.steylo.dao.repository;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.springframework.dao.DataAccessException;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.ino.steylo.helper.dto.*;
import com.ino.steylo.helper.contrat.*;
import com.ino.steylo.dao.entity.*;

/**
 * Repository : PermissionActions.
 */
@Repository
public interface PermissionActionsRepository extends JpaRepository<PermissionActions, Integer> {
	/**
	 * Finds PermissionActions by using id as a search criteria.
	 * 
	 * @param id
	 * @return An Object PermissionActions whose id is equals to the given id. If
	 *         no PermissionActions is found, this method returns null.
	 */
	@Query("select e from PermissionActions e where e.id = :id")
	PermissionActions findById(@Param("id")Integer id);

	/**
	 * Finds PermissionActions by using idActionProduit as a search criteria.
	 * 
	 * @param idActionProduit
	 * @return An Object PermissionActions whose idActionProduit is equals to the given idActionProduit. If
	 *         no PermissionActions is found, this method returns null.
	 */
	@Query("select e from PermissionActions e where e.idActionProduit = :idActionProduit")
	List<PermissionActions> findByIdActionProduit(@Param("idActionProduit")Integer idActionProduit);
	/**
	 * Finds PermissionActions by using idActionAbonne as a search criteria.
	 * 
	 * @param idActionAbonne
	 * @return An Object PermissionActions whose idActionAbonne is equals to the given idActionAbonne. If
	 *         no PermissionActions is found, this method returns null.
	 */
	@Query("select e from PermissionActions e where e.idActionAbonne = :idActionAbonne")
	List<PermissionActions> findByIdActionAbonne(@Param("idActionAbonne")Integer idActionAbonne);
	/**
	 * Finds PermissionActions by using idActionParam as a search criteria.
	 * 
	 * @param idActionParam
	 * @return An Object PermissionActions whose idActionParam is equals to the given idActionParam. If
	 *         no PermissionActions is found, this method returns null.
	 */
	@Query("select e from PermissionActions e where e.idActionParam = :idActionParam")
	List<PermissionActions> findByIdActionParam(@Param("idActionParam")Integer idActionParam);

	/**
	 * Finds PermissionActions by using idPermission as a search criteria.
	 * 
	 * @param idPermission
	 * @return An Object PermissionActions whose idPermission is equals to the given idPermission. If
	 *         no PermissionActions is found, this method returns null.
	 */
	@Query("select e from PermissionActions e where e.chanelPermissions.id = :idPermission")
	List<PermissionActions> findByIdPermission(@Param("idPermission")Integer idPermission);

	/**
	 * Finds List of PermissionActions by using permissionActionsDto as a search criteria.
	 * 
	 * @param request, em
	 * @return A List of PermissionActions
	 * @throws DataAccessException,ParseException
	 */
	public default List<PermissionActions> getByCriteria(Request request, EntityManager em) throws DataAccessException, ParseException {
		String req = "select e from PermissionActions e where e IS NOT NULL";
		HashMap<String, Object> param = new HashMap<>();

		PermissionActionsDto dto = request.getDataPermissionActions();
		
		req = generateCriteria(dto, req, param);
		req += " order by  e.id desc";

		TypedQuery<PermissionActions> query = em.createQuery(req, PermissionActions.class);

		for (Map.Entry<String, Object> entry : param.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}

		if (request.getIndex() != null && request.getSize() != null) {
			query.setFirstResult(request.getIndex() * request.getSize());
			query.setMaxResults(request.getSize());
		}

		return query.getResultList();
	}

	/**
	 * Finds count of PermissionActions by using permissionActionsDto as a search criteria.
	 * 
	 * @param request,em
	 * @return Number of PermissionActions
	 * 
	 */
	public default Long count(Request request, EntityManager em) throws DataAccessException, ParseException  {
		String req = "select new com.ino.steylo.helper.contrat.NumberDto(count(e.id)) from PermissionActions e where e IS NOT NULL";
		HashMap<String, Object> param = new HashMap<>();

		PermissionActionsDto dto = request.getDataPermissionActions();

		req = generateCriteria(dto, req, param);

		javax.persistence.Query query = em.createQuery(req);

		for (Map.Entry<String, Object> entry : param.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}

		NumberDto number = (NumberDto) query.getResultList().get(0);
		return number.getNombre();
	}

	default String generateCriteria(PermissionActionsDto dto, String req, HashMap<String, Object> param) throws ParseException{
		if (dto != null) {
			if (dto.getId()!= null && dto.getId() > 0) {
				req += " AND e.id = :id";
				param.put("id", dto.getId());
			}
			if (dto.getIdActionProduit()!= null && dto.getIdActionProduit() > 0) {
				req += " AND e.idActionProduit = :idActionProduit";
				param.put("idActionProduit", dto.getIdActionProduit());
			}
			if (dto.getIdActionAbonne()!= null && dto.getIdActionAbonne() > 0) {
				req += " AND e.idActionAbonne = :idActionAbonne";
				param.put("idActionAbonne", dto.getIdActionAbonne());
			}
			if (dto.getIdActionParam()!= null && dto.getIdActionParam() > 0) {
				req += " AND e.idActionParam = :idActionParam";
				param.put("idActionParam", dto.getIdActionParam());
			}
			if (dto.getIdPermission()!= null && dto.getIdPermission() > 0) {
				req += " AND e.chanelPermissions.id = :idPermission";
				param.put("idPermission", dto.getIdPermission());
			}
		}
		
		return req;
	}
}
