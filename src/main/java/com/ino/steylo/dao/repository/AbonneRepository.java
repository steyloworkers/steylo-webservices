package com.ino.steylo.dao.repository;

import java.util.Date;
import java.util.List;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.springframework.dao.DataAccessException;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.ino.steylo.helper.dto.*;
import com.ino.steylo.helper.contrat.*;
import com.ino.steylo.dao.entity.*;

/**
 * Repository : Abonne.
 */
@Repository
public interface AbonneRepository extends JpaRepository<Abonne, Long> {
	/**
	 * Finds Abonne by using id as a search criteria.
	 * 
	 * @param id
	 * @return An Object Abonne whose id is equals to the given id. If
	 *         no Abonne is found, this method returns null.
	 */
	@Query("select e from Abonne e where e.id = :id")
	Abonne findById(@Param("id")Long id);

	/**
	 * Finds Abonne by using dateEnreg as a search criteria.
	 * 
	 * @param dateEnreg
	 * @return An Object Abonne whose dateEnreg is equals to the given dateEnreg. If
	 *         no Abonne is found, this method returns null.
	 */
	@Query("select e from Abonne e where e.dateEnreg = :dateEnreg")
	List<Abonne> findByDateEnreg(@Param("dateEnreg")Date dateEnreg);
	/**
	 * Finds Abonne by using nom as a search criteria.
	 * 
	 * @param nom
	 * @return An Object Abonne whose nom is equals to the given nom. If
	 *         no Abonne is found, this method returns null.
	 */
	@Query("select e from Abonne e where e.nom = :nom")
	List<Abonne> findByNom(@Param("nom")String nom);
	/**
	 * Finds Abonne by using prenoms as a search criteria.
	 * 
	 * @param prenoms
	 * @return An Object Abonne whose prenoms is equals to the given prenoms. If
	 *         no Abonne is found, this method returns null.
	 */
	@Query("select e from Abonne e where e.prenoms = :prenoms")
	List<Abonne> findByPrenoms(@Param("prenoms")String prenoms);
	/**
	 * Finds Abonne by using sexe as a search criteria.
	 * 
	 * @param sexe
	 * @return An Object Abonne whose sexe is equals to the given sexe. If
	 *         no Abonne is found, this method returns null.
	 */
	@Query("select e from Abonne e where e.sexe = :sexe")
	List<Abonne> findBySexe(@Param("sexe")String sexe);
	/**
	 * Finds Abonne by using dateNaissance as a search criteria.
	 * 
	 * @param dateNaissance
	 * @return An Object Abonne whose dateNaissance is equals to the given dateNaissance. If
	 *         no Abonne is found, this method returns null.
	 */
	@Query("select e from Abonne e where e.dateNaissance = :dateNaissance")
	List<Abonne> findByDateNaissance(@Param("dateNaissance")String dateNaissance);
	/**
	 * Finds Abonne by using profession as a search criteria.
	 * 
	 * @param profession
	 * @return An Object Abonne whose profession is equals to the given profession. If
	 *         no Abonne is found, this method returns null.
	 */
	@Query("select e from Abonne e where e.profession = :profession")
	List<Abonne> findByProfession(@Param("profession")String profession);
	/**
	 * Finds Abonne by using mobileNumber as a search criteria.
	 * 
	 * @param mobileNumber
	 * @return An Object Abonne whose mobileNumber is equals to the given mobileNumber. If
	 *         no Abonne is found, this method returns null.
	 */
	@Query("select e from Abonne e where e.mobileNumber = :mobileNumber")
	List<Abonne> findByMobileNumber(@Param("mobileNumber")String mobileNumber);
	/**
	 * Finds Abonne by using address as a search criteria.
	 * 
	 * @param address
	 * @return An Object Abonne whose address is equals to the given address. If
	 *         no Abonne is found, this method returns null.
	 */
	@Query("select e from Abonne e where e.address = :address")
	List<Abonne> findByAddress(@Param("address")String address);
	/**
	 * Finds Abonne by using ville as a search criteria.
	 * 
	 * @param ville
	 * @return An Object Abonne whose ville is equals to the given ville. If
	 *         no Abonne is found, this method returns null.
	 */
	@Query("select e from Abonne e where e.ville = :ville")
	List<Abonne> findByVille(@Param("ville")String ville);
	/**
	 * Finds Abonne by using nationalite as a search criteria.
	 * 
	 * @param nationalite
	 * @return An Object Abonne whose nationalite is equals to the given nationalite. If
	 *         no Abonne is found, this method returns null.
	 */
	@Query("select e from Abonne e where e.nationalite = :nationalite")
	List<Abonne> findByNationalite(@Param("nationalite")String nationalite);
	/**
	 * Finds Abonne by using idTypeUtilisateur as a search criteria.
	 * 
	 * @param idTypeUtilisateur
	 * @return An Object Abonne whose idTypeUtilisateur is equals to the given idTypeUtilisateur. If
	 *         no Abonne is found, this method returns null.
	 */
	@Query("select e from Abonne e where e.idTypeUtilisateur = :idTypeUtilisateur")
	List<Abonne> findByIdTypeUtilisateur(@Param("idTypeUtilisateur")Integer idTypeUtilisateur);
	/**
	 * Finds Abonne by using metadata as a search criteria.
	 * 
	 * @param metadata
	 * @return An Object Abonne whose metadata is equals to the given metadata. If
	 *         no Abonne is found, this method returns null.
	 */
	@Query("select e from Abonne e where e.metadata = :metadata")
	List<Abonne> findByMetadata(@Param("metadata")String metadata);
	/**
	 * Finds Abonne by using login as a search criteria.
	 * 
	 * @param login
	 * @return An Object Abonne whose login is equals to the given login. If
	 *         no Abonne is found, this method returns null.
	 */
	@Query("select e from Abonne e where e.login = :login")
	List<Abonne> findByLogin(@Param("login")String login);
	/**
	 * Finds Abonne by using password as a search criteria.
	 * 
	 * @param password
	 * @return An Object Abonne whose password is equals to the given password. If
	 *         no Abonne is found, this method returns null.
	 */
	@Query("select e from Abonne e where e.password = :password")
	List<Abonne> findByPassword(@Param("password")String password);
	/**
	 * Finds Abonne by using email as a search criteria.
	 * 
	 * @param email
	 * @return An Object Abonne whose email is equals to the given email. If
	 *         no Abonne is found, this method returns null.
	 */
	@Query("select e from Abonne e where e.email = :email")
	List<Abonne> findByEmail(@Param("email")String email);
	/**
	 * Finds Abonne by using isActive as a search criteria.
	 * 
	 * @param isActive
	 * @return An Object Abonne whose isActive is equals to the given isActive. If
	 *         no Abonne is found, this method returns null.
	 */
	@Query("select e from Abonne e where e.isActive = :isActive")
	List<Abonne> findByIsActive(@Param("isActive")Boolean isActive);
	/**
	 * Finds Abonne by using longitude as a search criteria.
	 * 
	 * @param longitude
	 * @return An Object Abonne whose longitude is equals to the given longitude. If
	 *         no Abonne is found, this method returns null.
	 */
	@Query("select e from Abonne e where e.longitude = :longitude")
	List<Abonne> findByLongitude(@Param("longitude")Float longitude);
	/**
	 * Finds Abonne by using latitude as a search criteria.
	 * 
	 * @param latitude
	 * @return An Object Abonne whose latitude is equals to the given latitude. If
	 *         no Abonne is found, this method returns null.
	 */
	@Query("select e from Abonne e where e.latitude = :latitude")
	List<Abonne> findByLatitude(@Param("latitude")Float latitude);
	/**
	 * Finds Abonne by using photoIdentite as a search criteria.
	 * 
	 * @param photoIdentite
	 * @return An Object Abonne whose photoIdentite is equals to the given photoIdentite. If
	 *         no Abonne is found, this method returns null.
	 */
	@Query("select e from Abonne e where e.photoIdentite = :photoIdentite")
	List<Abonne> findByPhotoIdentite(@Param("photoIdentite")String photoIdentite);
	/**
	 * Finds Abonne by using photoPieceRecto as a search criteria.
	 * 
	 * @param photoPieceRecto
	 * @return An Object Abonne whose photoPieceRecto is equals to the given photoPieceRecto. If
	 *         no Abonne is found, this method returns null.
	 */
	@Query("select e from Abonne e where e.photoPieceRecto = :photoPieceRecto")
	List<Abonne> findByPhotoPieceRecto(@Param("photoPieceRecto")String photoPieceRecto);
	/**
	 * Finds Abonne by using photoPieceVerso as a search criteria.
	 * 
	 * @param photoPieceVerso
	 * @return An Object Abonne whose photoPieceVerso is equals to the given photoPieceVerso. If
	 *         no Abonne is found, this method returns null.
	 */
	@Query("select e from Abonne e where e.photoPieceVerso = :photoPieceVerso")
	List<Abonne> findByPhotoPieceVerso(@Param("photoPieceVerso")String photoPieceVerso);
	/**
	 * Finds Abonne by using communaute as a search criteria.
	 * 
	 * @param communaute
	 * @return An Object Abonne whose communaute is equals to the given communaute. If
	 *         no Abonne is found, this method returns null.
	 */
	@Query("select e from Abonne e where e.communaute = :communaute")
	List<Abonne> findByCommunaute(@Param("communaute")Integer communaute);
	/**
	 * Finds Abonne by using extPhotoIdentite as a search criteria.
	 * 
	 * @param extPhotoIdentite
	 * @return An Object Abonne whose extPhotoIdentite is equals to the given extPhotoIdentite. If
	 *         no Abonne is found, this method returns null.
	 */
	@Query("select e from Abonne e where e.extPhotoIdentite = :extPhotoIdentite")
	List<Abonne> findByExtPhotoIdentite(@Param("extPhotoIdentite")String extPhotoIdentite);
	/**
	 * Finds Abonne by using extPhotoRecto as a search criteria.
	 * 
	 * @param extPhotoRecto
	 * @return An Object Abonne whose extPhotoRecto is equals to the given extPhotoRecto. If
	 *         no Abonne is found, this method returns null.
	 */
	@Query("select e from Abonne e where e.extPhotoRecto = :extPhotoRecto")
	List<Abonne> findByExtPhotoRecto(@Param("extPhotoRecto")String extPhotoRecto);
	/**
	 * Finds Abonne by using extPhotoVerso as a search criteria.
	 * 
	 * @param extPhotoVerso
	 * @return An Object Abonne whose extPhotoVerso is equals to the given extPhotoVerso. If
	 *         no Abonne is found, this method returns null.
	 */
	@Query("select e from Abonne e where e.extPhotoVerso = :extPhotoVerso")
	List<Abonne> findByExtPhotoVerso(@Param("extPhotoVerso")String extPhotoVerso);
	/**
	 * Finds Abonne by using verifKey as a search criteria.
	 * 
	 * @param verifKey
	 * @return An Object Abonne whose verifKey is equals to the given verifKey. If
	 *         no Abonne is found, this method returns null.
	 */
	@Query("select e from Abonne e where e.verifKey = :verifKey")
	List<Abonne> findByVerifKey(@Param("verifKey")String verifKey);
	/**
	 * Finds Abonne by using seen as a search criteria.
	 * 
	 * @param seen
	 * @return An Object Abonne whose seen is equals to the given seen. If
	 *         no Abonne is found, this method returns null.
	 */
	@Query("select e from Abonne e where e.seen = :seen")
	List<Abonne> findBySeen(@Param("seen")Date seen);

	/**
	 * Finds Abonne by using pays as a search criteria.
	 * 
	 * @param pays
	 * @return An Object Abonne whose pays is equals to the given pays. If
	 *         no Abonne is found, this method returns null.
	 */
	@Query("select e from Abonne e where e.country.id = :pays")
	List<Abonne> findByPays(@Param("pays")String pays);

	/**
	 * Finds List of Abonne by using abonneDto as a search criteria.
	 * 
	 * @param request, em
	 * @return A List of Abonne
	 * @throws DataAccessException,ParseException
	 */
	public default List<Abonne> getByCriteria(Request request, EntityManager em) throws DataAccessException, ParseException {
		String req = "select e from Abonne e where e IS NOT NULL";
		HashMap<String, Object> param = new HashMap<>();

		AbonneDto dto = request.getDataAbonne();
		
		req = generateCriteria(dto, req, param);
		req += " order by  e.id desc";

		TypedQuery<Abonne> query = em.createQuery(req, Abonne.class);

		for (Map.Entry<String, Object> entry : param.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}

		if (request.getIndex() != null && request.getSize() != null) {
			query.setFirstResult(request.getIndex() * request.getSize());
			query.setMaxResults(request.getSize());
		}

		return query.getResultList();
	}

	/**
	 * Finds count of Abonne by using abonneDto as a search criteria.
	 * 
	 * @param request,em
	 * @return Number of Abonne
	 * 
	 */
	public default Long count(Request request, EntityManager em) throws DataAccessException, ParseException  {
		String req = "select new com.ino.steylo.helper.contrat.NumberDto(count(e.id)) from Abonne e where e IS NOT NULL";
		HashMap<String, Object> param = new HashMap<>();

		AbonneDto dto = request.getDataAbonne();

		req = generateCriteria(dto, req, param);

		javax.persistence.Query query = em.createQuery(req);

		for (Map.Entry<String, Object> entry : param.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}

		NumberDto number = (NumberDto) query.getResultList().get(0);
		return number.getNombre();
	}

	default String generateCriteria(AbonneDto dto, String req, HashMap<String, Object> param) throws ParseException{
		if (dto != null) {
			if (dto.getId()!= null && dto.getId() > 0) {
				req += " AND e.id = :id";
				param.put("id", dto.getId());
			}
			if (dto.getDateEnreg()!= null && !dto.getDateEnreg().isEmpty()) {
				req += " AND FUNCTION('DAY', e.dateEnreg) = FUNCTION('DAY', :dateEnreg) AND FUNCTION('MONTH', e.dateEnreg) = FUNCTION('MONTH', :dateEnreg) AND FUNCTION('YEAR', e.dateEnreg) = FUNCTION('YEAR', :dateEnreg) ";
				param.put("dateEnreg",  new SimpleDateFormat("dd/MM/yyyy").parse(dto.getDateEnreg()));
			}
			if (dto.getNom()!= null && !dto.getNom().isEmpty()) {
				req += " AND e.nom = :nom";
				param.put("nom", dto.getNom());
			}
			if (dto.getPrenoms()!= null && !dto.getPrenoms().isEmpty()) {
				req += " AND e.prenoms = :prenoms";
				param.put("prenoms", dto.getPrenoms());
			}
			if (dto.getSexe()!= null && !dto.getSexe().isEmpty()) {
				req += " AND e.sexe = :sexe";
				param.put("sexe", dto.getSexe());
			}
			if (dto.getDateNaissance()!= null && !dto.getDateNaissance().isEmpty()) {
				req += " AND e.dateNaissance = :dateNaissance";
				param.put("dateNaissance", dto.getDateNaissance());
			}
			if (dto.getProfession()!= null && !dto.getProfession().isEmpty()) {
				req += " AND e.profession = :profession";
				param.put("profession", dto.getProfession());
			}
			if (dto.getMobileNumber()!= null && !dto.getMobileNumber().isEmpty()) {
				req += " AND e.mobileNumber = :mobileNumber";
				param.put("mobileNumber", dto.getMobileNumber());
			}
			if (dto.getAddress()!= null && !dto.getAddress().isEmpty()) {
				req += " AND e.address = :address";
				param.put("address", dto.getAddress());
			}
			if (dto.getVille()!= null && !dto.getVille().isEmpty()) {
				req += " AND e.ville = :ville";
				param.put("ville", dto.getVille());
			}
			if (dto.getNationalite()!= null && !dto.getNationalite().isEmpty()) {
				req += " AND e.nationalite = :nationalite";
				param.put("nationalite", dto.getNationalite());
			}
			if (dto.getIdTypeUtilisateur()!= null && dto.getIdTypeUtilisateur() > 0) {
				req += " AND e.idTypeUtilisateur = :idTypeUtilisateur";
				param.put("idTypeUtilisateur", dto.getIdTypeUtilisateur());
			}
			if (dto.getMetadata()!= null && !dto.getMetadata().isEmpty()) {
				req += " AND e.metadata = :metadata";
				param.put("metadata", dto.getMetadata());
			}
			if (dto.getLogin()!= null && !dto.getLogin().isEmpty()) {
				req += " AND e.login = :login";
				param.put("login", dto.getLogin());
			}
			if (dto.getPassword()!= null && !dto.getPassword().isEmpty()) {
				req += " AND e.password = :password";
				param.put("password", dto.getPassword());
			}
			if (dto.getEmail()!= null && !dto.getEmail().isEmpty()) {
				req += " AND e.email = :email";
				param.put("email", dto.getEmail());
			}
			if (dto.getIsActive()!= null) {
				req += " AND e.isActive = :isActive";
				param.put("isActive", dto.getIsActive());
			}
			if (dto.getLongitude()!= null && dto.getLongitude() > 0) {
				req += " AND e.longitude = :longitude";
				param.put("longitude", dto.getLongitude());
			}
			if (dto.getLatitude()!= null && dto.getLatitude() > 0) {
				req += " AND e.latitude = :latitude";
				param.put("latitude", dto.getLatitude());
			}
			if (dto.getPhotoIdentite()!= null && !dto.getPhotoIdentite().isEmpty()) {
				req += " AND e.photoIdentite = :photoIdentite";
				param.put("photoIdentite", dto.getPhotoIdentite());
			}
			if (dto.getPhotoPieceRecto()!= null && !dto.getPhotoPieceRecto().isEmpty()) {
				req += " AND e.photoPieceRecto = :photoPieceRecto";
				param.put("photoPieceRecto", dto.getPhotoPieceRecto());
			}
			if (dto.getPhotoPieceVerso()!= null && !dto.getPhotoPieceVerso().isEmpty()) {
				req += " AND e.photoPieceVerso = :photoPieceVerso";
				param.put("photoPieceVerso", dto.getPhotoPieceVerso());
			}
			if (dto.getCommunaute()!= null && dto.getCommunaute() > 0) {
				req += " AND e.communaute = :communaute";
				param.put("communaute", dto.getCommunaute());
			}
			if (dto.getExtPhotoIdentite()!= null && !dto.getExtPhotoIdentite().isEmpty()) {
				req += " AND e.extPhotoIdentite = :extPhotoIdentite";
				param.put("extPhotoIdentite", dto.getExtPhotoIdentite());
			}
			if (dto.getExtPhotoRecto()!= null && !dto.getExtPhotoRecto().isEmpty()) {
				req += " AND e.extPhotoRecto = :extPhotoRecto";
				param.put("extPhotoRecto", dto.getExtPhotoRecto());
			}
			if (dto.getExtPhotoVerso()!= null && !dto.getExtPhotoVerso().isEmpty()) {
				req += " AND e.extPhotoVerso = :extPhotoVerso";
				param.put("extPhotoVerso", dto.getExtPhotoVerso());
			}
			if (dto.getVerifKey()!= null && !dto.getVerifKey().isEmpty()) {
				req += " AND e.verifKey = :verifKey";
				param.put("verifKey", dto.getVerifKey());
			}
			if (dto.getSeen()!= null && !dto.getSeen().isEmpty()) {
				req += " AND FUNCTION('DAY', e.seen) = FUNCTION('DAY', :seen) AND FUNCTION('MONTH', e.seen) = FUNCTION('MONTH', :seen) AND FUNCTION('YEAR', e.seen) = FUNCTION('YEAR', :seen) ";
				param.put("seen",  new SimpleDateFormat("dd/MM/yyyy").parse(dto.getSeen()));
			}
		}
		
		return req;
	}
}
