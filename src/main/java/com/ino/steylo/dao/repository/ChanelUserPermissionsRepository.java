package com.ino.steylo.dao.repository;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.springframework.dao.DataAccessException;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.ino.steylo.helper.dto.*;
import com.ino.steylo.helper.contrat.*;
import com.ino.steylo.dao.entity.*;

/**
 * Repository : ChanelUserPermissions.
 */
@Repository
public interface ChanelUserPermissionsRepository extends JpaRepository<ChanelUserPermissions, Integer> {
	/**
	 * Finds ChanelUserPermissions by using id as a search criteria.
	 * 
	 * @param id
	 * @return An Object ChanelUserPermissions whose id is equals to the given id. If
	 *         no ChanelUserPermissions is found, this method returns null.
	 */
	@Query("select e from ChanelUserPermissions e where e.id = :id")
	ChanelUserPermissions findById(@Param("id")Integer id);


	/**
	 * Finds ChanelUserPermissions by using idPermission as a search criteria.
	 * 
	 * @param idPermission
	 * @return An Object ChanelUserPermissions whose idPermission is equals to the given idPermission. If
	 *         no ChanelUserPermissions is found, this method returns null.
	 */
	@Query("select e from ChanelUserPermissions e where e.chanelPermissions.id = :idPermission")
	List<ChanelUserPermissions> findByIdPermission(@Param("idPermission")Integer idPermission);

	/**
	 * Finds ChanelUserPermissions by using idUser as a search criteria.
	 * 
	 * @param idUser
	 * @return An Object ChanelUserPermissions whose idUser is equals to the given idUser. If
	 *         no ChanelUserPermissions is found, this method returns null.
	 */
	@Query("select e from ChanelUserPermissions e where e.chanelUsers.id = :idUser")
	List<ChanelUserPermissions> findByIdUser(@Param("idUser")Integer idUser);

	/**
	 * Finds List of ChanelUserPermissions by using chanelUserPermissionsDto as a search criteria.
	 * 
	 * @param request, em
	 * @return A List of ChanelUserPermissions
	 * @throws DataAccessException,ParseException
	 */
	public default List<ChanelUserPermissions> getByCriteria(Request request, EntityManager em) throws DataAccessException, ParseException {
		String req = "select e from ChanelUserPermissions e where e IS NOT NULL";
		HashMap<String, Object> param = new HashMap<>();

		ChanelUserPermissionsDto dto = request.getDataChanelUserPermissions();
		
		req = generateCriteria(dto, req, param);
		req += " order by  e.id desc";

		TypedQuery<ChanelUserPermissions> query = em.createQuery(req, ChanelUserPermissions.class);

		for (Map.Entry<String, Object> entry : param.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}

		if (request.getIndex() != null && request.getSize() != null) {
			query.setFirstResult(request.getIndex() * request.getSize());
			query.setMaxResults(request.getSize());
		}

		return query.getResultList();
	}

	/**
	 * Finds count of ChanelUserPermissions by using chanelUserPermissionsDto as a search criteria.
	 * 
	 * @param request,em
	 * @return Number of ChanelUserPermissions
	 * 
	 */
	public default Long count(Request request, EntityManager em) throws DataAccessException, ParseException  {
		String req = "select new com.ino.steylo.helper.contrat.NumberDto(count(e.id)) from ChanelUserPermissions e where e IS NOT NULL";
		HashMap<String, Object> param = new HashMap<>();

		ChanelUserPermissionsDto dto = request.getDataChanelUserPermissions();

		req = generateCriteria(dto, req, param);

		javax.persistence.Query query = em.createQuery(req);

		for (Map.Entry<String, Object> entry : param.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}

		NumberDto number = (NumberDto) query.getResultList().get(0);
		return number.getNombre();
	}

	default String generateCriteria(ChanelUserPermissionsDto dto, String req, HashMap<String, Object> param) throws ParseException{
		if (dto != null) {
			if (dto.getId()!= null && dto.getId() > 0) {
				req += " AND e.id = :id";
				param.put("id", dto.getId());
			}
			if (dto.getIdPermission()!= null && dto.getIdPermission() > 0) {
				req += " AND e.chanelPermissions.id = :idPermission";
				param.put("idPermission", dto.getIdPermission());
			}
			if (dto.getIdUser()!= null && dto.getIdUser() > 0) {
				req += " AND e.chanelUsers.id = :idUser";
				param.put("idUser", dto.getIdUser());
			}
		}
		
		return req;
	}
}
