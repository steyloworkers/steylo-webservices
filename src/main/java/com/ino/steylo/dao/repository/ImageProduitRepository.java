package com.ino.steylo.dao.repository;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.springframework.dao.DataAccessException;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.ino.steylo.helper.dto.*;
import com.ino.steylo.helper.contrat.*;
import com.ino.steylo.dao.entity.*;

/**
 * Repository : ImageProduit.
 */
@Repository
public interface ImageProduitRepository extends JpaRepository<ImageProduit, Integer> {
	/**
	 * Finds ImageProduit by using id as a search criteria.
	 * 
	 * @param id
	 * @return An Object ImageProduit whose id is equals to the given id. If
	 *         no ImageProduit is found, this method returns null.
	 */
	@Query("select e from ImageProduit e where e.id = :id")
	ImageProduit findById(@Param("id")Integer id);

	/**
	 * Finds ImageProduit by using image as a search criteria.
	 * 
	 * @param image
	 * @return An Object ImageProduit whose image is equals to the given image. If
	 *         no ImageProduit is found, this method returns null.
	 */
	@Query("select e from ImageProduit e where e.image = :image")
	List<ImageProduit> findByImage(@Param("image")String image);
	/**
	 * Finds ImageProduit by using type as a search criteria.
	 * 
	 * @param type
	 * @return An Object ImageProduit whose type is equals to the given type. If
	 *         no ImageProduit is found, this method returns null.
	 */
	@Query("select e from ImageProduit e where e.type = :type")
	List<ImageProduit> findByType(@Param("type")String type);
	/**
	 * Finds ImageProduit by using extImg as a search criteria.
	 * 
	 * @param extImg
	 * @return An Object ImageProduit whose extImg is equals to the given extImg. If
	 *         no ImageProduit is found, this method returns null.
	 */
	@Query("select e from ImageProduit e where e.extImg = :extImg")
	List<ImageProduit> findByExtImg(@Param("extImg")String extImg);

	/**
	 * Finds ImageProduit by using idProduit as a search criteria.
	 * 
	 * @param idProduit
	 * @return An Object ImageProduit whose idProduit is equals to the given idProduit. If
	 *         no ImageProduit is found, this method returns null.
	 */
	@Query("select e from ImageProduit e where e.produit.id = :idProduit")
	List<ImageProduit> findByIdProduit(@Param("idProduit")Integer idProduit);

	/**
	 * Finds List of ImageProduit by using imageProduitDto as a search criteria.
	 * 
	 * @param request, em
	 * @return A List of ImageProduit
	 * @throws DataAccessException,ParseException
	 */
	public default List<ImageProduit> getByCriteria(Request request, EntityManager em) throws DataAccessException, ParseException {
		String req = "select e from ImageProduit e where e IS NOT NULL";
		HashMap<String, Object> param = new HashMap<>();

		ImageProduitDto dto = request.getDataImageProduit();
		
		req = generateCriteria(dto, req, param);
		req += " order by  e.id desc";

		TypedQuery<ImageProduit> query = em.createQuery(req, ImageProduit.class);

		for (Map.Entry<String, Object> entry : param.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}

		if (request.getIndex() != null && request.getSize() != null) {
			query.setFirstResult(request.getIndex() * request.getSize());
			query.setMaxResults(request.getSize());
		}

		return query.getResultList();
	}

	/**
	 * Finds count of ImageProduit by using imageProduitDto as a search criteria.
	 * 
	 * @param request,em
	 * @return Number of ImageProduit
	 * 
	 */
	public default Long count(Request request, EntityManager em) throws DataAccessException, ParseException  {
		String req = "select new com.ino.steylo.helper.contrat.NumberDto(count(e.id)) from ImageProduit e where e IS NOT NULL";
		HashMap<String, Object> param = new HashMap<>();

		ImageProduitDto dto = request.getDataImageProduit();

		req = generateCriteria(dto, req, param);

		javax.persistence.Query query = em.createQuery(req);

		for (Map.Entry<String, Object> entry : param.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}

		NumberDto number = (NumberDto) query.getResultList().get(0);
		return number.getNombre();
	}

	default String generateCriteria(ImageProduitDto dto, String req, HashMap<String, Object> param) throws ParseException{
		if (dto != null) {
			if (dto.getId()!= null && dto.getId() > 0) {
				req += " AND e.id = :id";
				param.put("id", dto.getId());
			}
			if (dto.getImage()!= null && !dto.getImage().isEmpty()) {
				req += " AND e.image = :image";
				param.put("image", dto.getImage());
			}
			if (dto.getType()!= null && !dto.getType().isEmpty()) {
				req += " AND e.type = :type";
				param.put("type", dto.getType());
			}
			if (dto.getExtImg()!= null && !dto.getExtImg().isEmpty()) {
				req += " AND e.extImg = :extImg";
				param.put("extImg", dto.getExtImg());
			}
			if (dto.getIdProduit()!= null && dto.getIdProduit() > 0) {
				req += " AND e.produit.id = :idProduit";
				param.put("idProduit", dto.getIdProduit());
			}
		}
		
		return req;
	}
}
