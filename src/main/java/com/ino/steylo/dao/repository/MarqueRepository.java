package com.ino.steylo.dao.repository;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.springframework.dao.DataAccessException;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.ino.steylo.helper.dto.*;
import com.ino.steylo.helper.contrat.*;
import com.ino.steylo.dao.entity.*;

/**
 * Repository : Marque.
 */
@Repository
public interface MarqueRepository extends JpaRepository<Marque, Integer> {
	/**
	 * Finds Marque by using id as a search criteria.
	 * 
	 * @param id
	 * @return An Object Marque whose id is equals to the given id. If
	 *         no Marque is found, this method returns null.
	 */
	@Query("select e from Marque e where e.id = :id")
	Marque findById(@Param("id")Integer id);

	/**
	 * Finds Marque by using libelle as a search criteria.
	 * 
	 * @param libelle
	 * @return An Object Marque whose libelle is equals to the given libelle. If
	 *         no Marque is found, this method returns null.
	 */
	@Query("select e from Marque e where e.libelle = :libelle")
	List<Marque> findByLibelle(@Param("libelle")String libelle);
	/**
	 * Finds Marque by using slogan as a search criteria.
	 * 
	 * @param slogan
	 * @return An Object Marque whose slogan is equals to the given slogan. If
	 *         no Marque is found, this method returns null.
	 */
	@Query("select e from Marque e where e.slogan = :slogan")
	List<Marque> findBySlogan(@Param("slogan")String slogan);
	/**
	 * Finds Marque by using statut as a search criteria.
	 * 
	 * @param statut
	 * @return An Object Marque whose statut is equals to the given statut. If
	 *         no Marque is found, this method returns null.
	 */
	@Query("select e from Marque e where e.statut = :statut")
	List<Marque> findByStatut(@Param("statut")Boolean statut);

	/**
	 * Finds List of Marque by using marqueDto as a search criteria.
	 * 
	 * @param request, em
	 * @return A List of Marque
	 * @throws DataAccessException,ParseException
	 */
	public default List<Marque> getByCriteria(Request request, EntityManager em) throws DataAccessException, ParseException {
		String req = "select e from Marque e where e IS NOT NULL";
		HashMap<String, Object> param = new HashMap<>();

		MarqueDto dto = request.getDataMarque();
		
		req = generateCriteria(dto, req, param);
		req += " order by  e.id desc";

		TypedQuery<Marque> query = em.createQuery(req, Marque.class);

		for (Map.Entry<String, Object> entry : param.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}

		if (request.getIndex() != null && request.getSize() != null) {
			query.setFirstResult(request.getIndex() * request.getSize());
			query.setMaxResults(request.getSize());
		}

		return query.getResultList();
	}

	/**
	 * Finds count of Marque by using marqueDto as a search criteria.
	 * 
	 * @param request,em
	 * @return Number of Marque
	 * 
	 */
	public default Long count(Request request, EntityManager em) throws DataAccessException, ParseException  {
		String req = "select new com.ino.steylo.helper.contrat.NumberDto(count(e.id)) from Marque e where e IS NOT NULL";
		HashMap<String, Object> param = new HashMap<>();

		MarqueDto dto = request.getDataMarque();

		req = generateCriteria(dto, req, param);

		javax.persistence.Query query = em.createQuery(req);

		for (Map.Entry<String, Object> entry : param.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}

		NumberDto number = (NumberDto) query.getResultList().get(0);
		return number.getNombre();
	}

	default String generateCriteria(MarqueDto dto, String req, HashMap<String, Object> param) throws ParseException{
		if (dto != null) {
			if (dto.getId()!= null && dto.getId() > 0) {
				req += " AND e.id = :id";
				param.put("id", dto.getId());
			}
			if (dto.getLibelle()!= null && !dto.getLibelle().isEmpty()) {
				req += " AND e.libelle = :libelle";
				param.put("libelle", dto.getLibelle());
			}
			if (dto.getSlogan()!= null && !dto.getSlogan().isEmpty()) {
				req += " AND e.slogan = :slogan";
				param.put("slogan", dto.getSlogan());
			}
			if (dto.getStatut()!= null) {
				req += " AND e.statut = :statut";
				param.put("statut", dto.getStatut());
			}
		}
		
		return req;
	}
}
