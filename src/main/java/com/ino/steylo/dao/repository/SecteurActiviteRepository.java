package com.ino.steylo.dao.repository;

import java.util.List;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.springframework.dao.DataAccessException;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.ino.steylo.helper.dto.*;
import com.ino.steylo.helper.contrat.*;
import com.ino.steylo.dao.entity.*;

/**
 * Repository : SecteurActivite.
 */
@Repository
public interface SecteurActiviteRepository extends JpaRepository<SecteurActivite, Integer> {
	/**
	 * Finds SecteurActivite by using id as a search criteria.
	 * 
	 * @param id
	 * @return An Object SecteurActivite whose id is equals to the given id. If
	 *         no SecteurActivite is found, this method returns null.
	 */
	@Query("select e from SecteurActivite e where e.id = :id")
	SecteurActivite findById(@Param("id")Integer id);

	/**
	 * Finds SecteurActivite by using libelle as a search criteria.
	 * 
	 * @param libelle
	 * @return An Object SecteurActivite whose libelle is equals to the given libelle. If
	 *         no SecteurActivite is found, this method returns null.
	 */
	@Query("select e from SecteurActivite e where e.libelle = :libelle")
	List<SecteurActivite> findByLibelle(@Param("libelle")String libelle);

	/**
	 * Finds List of SecteurActivite by using secteurActiviteDto as a search criteria.
	 * 
	 * @param request, em
	 * @return A List of SecteurActivite
	 * @throws DataAccessException,ParseException
	 */
	public default List<SecteurActivite> getByCriteria(Request request, EntityManager em) throws DataAccessException, ParseException {
		String req = "select e from SecteurActivite e where e IS NOT NULL";
		HashMap<String, Object> param = new HashMap<>();

		SecteurActiviteDto dto = request.getDataSecteurActivite();
		
		req = generateCriteria(dto, req, param);
		req += " order by e.libelle asc";

		TypedQuery<SecteurActivite> query = em.createQuery(req, SecteurActivite.class);

		for (Map.Entry<String, Object> entry : param.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}

		if (request.getIndex() != null && request.getSize() != null) {
			query.setFirstResult(request.getIndex() * request.getSize());
			query.setMaxResults(request.getSize());
		}

		return query.getResultList();
	}

	/**
	 * Finds count of SecteurActivite by using secteurActiviteDto as a search criteria.
	 * 
	 * @param request,em
	 * @return Number of SecteurActivite
	 * 
	 */
	public default Long count(Request request, EntityManager em) throws DataAccessException, ParseException  {
		String req = "select new com.ino.steylo.helper.contrat.NumberDto(count(e.id)) from SecteurActivite e where e IS NOT NULL";
		HashMap<String, Object> param = new HashMap<>();

		SecteurActiviteDto dto = request.getDataSecteurActivite();

		req = generateCriteria(dto, req, param);

		javax.persistence.Query query = em.createQuery(req);

		for (Map.Entry<String, Object> entry : param.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}

		NumberDto number = (NumberDto) query.getResultList().get(0);
		return number.getNombre();
	}

	default String generateCriteria(SecteurActiviteDto dto, String req, HashMap<String, Object> param) throws ParseException{
		if (dto != null) {
			if (dto.getId()!= null && dto.getId() > 0) {
				req += " AND e.id = :id";
				param.put("id", dto.getId());
			}
			if (dto.getLibelle()!= null && !dto.getLibelle().isEmpty()) {
				req += " AND e.libelle = :libelle";
				param.put("libelle", dto.getLibelle());
			}
		}
		
		return req;
	}
}
