package com.ino.steylo.dao.repository;

import java.util.Date;
import java.util.List;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.springframework.dao.DataAccessException;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.ino.steylo.helper.dto.*;
import com.ino.steylo.helper.contrat.*;
import com.ino.steylo.dao.entity.*;

/**
 * Repository : Chanel.
 */
@Repository
public interface ChanelRepository extends JpaRepository<Chanel, Integer> {
	/**
	 * Finds Chanel by using id as a search criteria.
	 * 
	 * @param id
	 * @return An Object Chanel whose id is equals to the given id. If
	 *         no Chanel is found, this method returns null.
	 */
	@Query("select e from Chanel e where e.id = :id")
	Chanel findById(@Param("id")Integer id);

	/**
	 * Finds Chanel by using formeJuridique as a search criteria.
	 * 
	 * @param formeJuridique
	 * @return An Object Chanel whose formeJuridique is equals to the given formeJuridique. If
	 *         no Chanel is found, this method returns null.
	 */
	@Query("select e from Chanel e where e.formeJuridique = :formeJuridique")
	List<Chanel> findByFormeJuridique(@Param("formeJuridique")String formeJuridique);
	/**
	 * Finds Chanel by using nomChanel as a search criteria.
	 * 
	 * @param nomChanel
	 * @return An Object Chanel whose nomChanel is equals to the given nomChanel. If
	 *         no Chanel is found, this method returns null.
	 */
	@Query("select e from Chanel e where e.nomChanel = :nomChanel")
	List<Chanel> findByNomChanel(@Param("nomChanel")String nomChanel);
	/**
	 * Finds Chanel by using sigle as a search criteria.
	 * 
	 * @param sigle
	 * @return An Object Chanel whose sigle is equals to the given sigle. If
	 *         no Chanel is found, this method returns null.
	 */
	@Query("select e from Chanel e where e.sigle = :sigle")
	List<Chanel> findBySigle(@Param("sigle")String sigle);
	/**
	 * Finds Chanel by using numeroRegistre as a search criteria.
	 * 
	 * @param numeroRegistre
	 * @return An Object Chanel whose numeroRegistre is equals to the given numeroRegistre. If
	 *         no Chanel is found, this method returns null.
	 */
	@Query("select e from Chanel e where e.numeroRegistre = :numeroRegistre")
    Chanel findByNumeroRegistre(@Param("numeroRegistre")String numeroRegistre);
	/**
	 * Finds Chanel by using numeroImmatriculation as a search criteria.
	 * 
	 * @param numeroImmatriculation
	 * @return An Object Chanel whose numeroImmatriculation is equals to the given numeroImmatriculation. If
	 *         no Chanel is found, this method returns null.
	 */
	@Query("select e from Chanel e where e.numeroImmatriculation = :numeroImmatriculation")
	Chanel findByNumeroImmatriculation(@Param("numeroImmatriculation")String numeroImmatriculation);
	/**
	 * Finds Chanel by using descriptionSecteur as a search criteria.
	 * 
	 * @param descriptionSecteur
	 * @return An Object Chanel whose descriptionSecteur is equals to the given descriptionSecteur. If
	 *         no Chanel is found, this method returns null.
	 */
	@Query("select e from Chanel e where e.descriptionSecteur = :descriptionSecteur")
	List<Chanel> findByDescriptionSecteur(@Param("descriptionSecteur")String descriptionSecteur);
	/**
	 * Finds Chanel by using siteWeb as a search criteria.
	 * 
	 * @param siteWeb
	 * @return An Object Chanel whose siteWeb is equals to the given siteWeb. If
	 *         no Chanel is found, this method returns null.
	 */
	@Query("select e from Chanel e where e.siteWeb = :siteWeb")
	List<Chanel> findBySiteWeb(@Param("siteWeb")String siteWeb);
	/**
	 * Finds Chanel by using email as a search criteria.
	 * 
	 * @param email
	 * @return An Object Chanel whose email is equals to the given email. If
	 *         no Chanel is found, this method returns null.
	 */
	@Query("select e from Chanel e where e.email = :email")
	List<Chanel> findByEmail(@Param("email")String email);
	/**
	 * Finds Chanel by using phoneNumber as a search criteria.
	 * 
	 * @param phoneNumber
	 * @return An Object Chanel whose phoneNumber is equals to the given phoneNumber. If
	 *         no Chanel is found, this method returns null.
	 */
	@Query("select e from Chanel e where e.phoneNumber = :phoneNumber")
	List<Chanel> findByPhoneNumber(@Param("phoneNumber")String phoneNumber);
	/**
	 * Finds Chanel by using fax as a search criteria.
	 * 
	 * @param fax
	 * @return An Object Chanel whose fax is equals to the given fax. If
	 *         no Chanel is found, this method returns null.
	 */
	@Query("select e from Chanel e where e.fax = :fax")
	List<Chanel> findByFax(@Param("fax")String fax);
	/**
	 * Finds Chanel by using etat as a search criteria.
	 * 
	 * @param etat
	 * @return An Object Chanel whose etat is equals to the given etat. If
	 *         no Chanel is found, this method returns null.
	 */
	@Query("select e from Chanel e where e.etat = :etat")
	List<Chanel> findByEtat(@Param("etat")String etat);
	/**
	 * Finds Chanel by using ville as a search criteria.
	 * 
	 * @param ville
	 * @return An Object Chanel whose ville is equals to the given ville. If
	 *         no Chanel is found, this method returns null.
	 */
	@Query("select e from Chanel e where e.ville = :ville")
	List<Chanel> findByVille(@Param("ville")String ville);
	/**
	 * Finds Chanel by using codePostal as a search criteria.
	 * 
	 * @param codePostal
	 * @return An Object Chanel whose codePostal is equals to the given codePostal. If
	 *         no Chanel is found, this method returns null.
	 */
	@Query("select e from Chanel e where e.codePostal = :codePostal")
	List<Chanel> findByCodePostal(@Param("codePostal")String codePostal);
	/**
	 * Finds Chanel by using address as a search criteria.
	 * 
	 * @param address
	 * @return An Object Chanel whose address is equals to the given address. If
	 *         no Chanel is found, this method returns null.
	 */
	@Query("select e from Chanel e where e.address = :address")
	List<Chanel> findByAddress(@Param("address")String address);
	/**
	 * Finds Chanel by using logo as a search criteria.
	 * 
	 * @param logo
	 * @return An Object Chanel whose logo is equals to the given logo. If
	 *         no Chanel is found, this method returns null.
	 */
	@Query("select e from Chanel e where e.logo = :logo")
	List<Chanel> findByLogo(@Param("logo")String logo);
	/**
	 * Finds Chanel by using extLogo as a search criteria.
	 * 
	 * @param extLogo
	 * @return An Object Chanel whose extLogo is equals to the given extLogo. If
	 *         no Chanel is found, this method returns null.
	 */
	@Query("select e from Chanel e where e.extLogo = :extLogo")
	List<Chanel> findByExtLogo(@Param("extLogo")String extLogo);
	/**
	 * Finds Chanel by using codeGuichet as a search criteria.
	 * 
	 * @param codeGuichet
	 * @return An Object Chanel whose codeGuichet is equals to the given codeGuichet. If
	 *         no Chanel is found, this method returns null.
	 */
	@Query("select e from Chanel e where e.codeGuichet = :codeGuichet")
	List<Chanel> findByCodeGuichet(@Param("codeGuichet")String codeGuichet);
	/**
	 * Finds Chanel by using codeBanque as a search criteria.
	 * 
	 * @param codeBanque
	 * @return An Object Chanel whose codeBanque is equals to the given codeBanque. If
	 *         no Chanel is found, this method returns null.
	 */
	@Query("select e from Chanel e where e.codeBanque = :codeBanque")
	List<Chanel> findByCodeBanque(@Param("codeBanque")String codeBanque);
	/**
	 * Finds Chanel by using numeroCompte as a search criteria.
	 * 
	 * @param numeroCompte
	 * @return An Object Chanel whose numeroCompte is equals to the given numeroCompte. If
	 *         no Chanel is found, this method returns null.
	 */
	@Query("select e from Chanel e where e.numeroCompte = :numeroCompte")
	List<Chanel> findByNumeroCompte(@Param("numeroCompte")String numeroCompte);
	/**
	 * Finds Chanel by using swift as a search criteria.
	 * 
	 * @param swift
	 * @return An Object Chanel whose swift is equals to the given swift. If
	 *         no Chanel is found, this method returns null.
	 */
	@Query("select e from Chanel e where e.swift = :swift")
	List<Chanel> findBySwift(@Param("swift")String swift);
	/**
	 * Finds Chanel by using cleRib as a search criteria.
	 * 
	 * @param cleRib
	 * @return An Object Chanel whose cleRib is equals to the given cleRib. If
	 *         no Chanel is found, this method returns null.
	 */
	@Query("select e from Chanel e where e.cleRib = :cleRib")
	List<Chanel> findByCleRib(@Param("cleRib")String cleRib);
	/**
	 * Finds Chanel by using rib as a search criteria.
	 * 
	 * @param rib
	 * @return An Object Chanel whose rib is equals to the given rib. If
	 *         no Chanel is found, this method returns null.
	 */
	@Query("select e from Chanel e where e.rib = :rib")
	List<Chanel> findByRib(@Param("rib")String rib);
	/**
	 * Finds Chanel by using iban as a search criteria.
	 * 
	 * @param iban
	 * @return An Object Chanel whose iban is equals to the given iban. If
	 *         no Chanel is found, this method returns null.
	 */
	@Query("select e from Chanel e where e.iban = :iban")
	List<Chanel> findByIban(@Param("iban")String iban);
	/**
	 * Finds Chanel by using rowDateUpdate as a search criteria.
	 * 
	 * @param rowDateUpdate
	 * @return An Object Chanel whose rowDateUpdate is equals to the given rowDateUpdate. If
	 *         no Chanel is found, this method returns null.
	 */
	@Query("select e from Chanel e where e.rowDateUpdate = :rowDateUpdate")
	List<Chanel> findByRowDateUpdate(@Param("rowDateUpdate")Date rowDateUpdate);
	/**
	 * Finds Chanel by using logoDateUpdate as a search criteria.
	 * 
	 * @param logoDateUpdate
	 * @return An Object Chanel whose logoDateUpdate is equals to the given logoDateUpdate. If
	 *         no Chanel is found, this method returns null.
	 */
	@Query("select e from Chanel e where e.logoDateUpdate = :logoDateUpdate")
	List<Chanel> findByLogoDateUpdate(@Param("logoDateUpdate")Date logoDateUpdate);

	/**
	 * Finds Chanel by using idPays as a search criteria.
	 * 
	 * @param idPays
	 * @return An Object Chanel whose idPays is equals to the given idPays. If
	 *         no Chanel is found, this method returns null.
	 */
	@Query("select e from Chanel e where e.country.id = :idPays")
	List<Chanel> findByIdPays(@Param("idPays")Integer idPays);

	/**
	 * Finds Chanel by using idSecteurActivite as a search criteria.
	 * 
	 * @param idSecteurActivite
	 * @return An Object Chanel whose idSecteurActivite is equals to the given idSecteurActivite. If
	 *         no Chanel is found, this method returns null.
	 */
	@Query("select e from Chanel e where e.secteurActivite.id = :idSecteurActivite")
	List<Chanel> findByIdSecteurActivite(@Param("idSecteurActivite")Integer idSecteurActivite);

	/**
	 * Finds List of Chanel by using chanelDto as a search criteria.
	 * 
	 * @param request, em
	 * @return A List of Chanel
	 * @throws DataAccessException,ParseException
	 */
	public default List<Chanel> getByCriteria(Request request, EntityManager em) throws DataAccessException, ParseException {
		String req = "select e from Chanel e where e IS NOT NULL";
		HashMap<String, Object> param = new HashMap<>();

		ChanelDto dto = request.getDataChanel();
		
		req = generateCriteria(dto, req, param);
		req += " order by  e.id desc";

		TypedQuery<Chanel> query = em.createQuery(req, Chanel.class);

		for (Map.Entry<String, Object> entry : param.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}

		if (request.getIndex() != null && request.getSize() != null) {
			query.setFirstResult(request.getIndex() * request.getSize());
			query.setMaxResults(request.getSize());
		}

		return query.getResultList();
	}

	/**
	 * Finds count of Chanel by using chanelDto as a search criteria.
	 * 
	 * @param request,em
	 * @return Number of Chanel
	 * 
	 */
	public default Long count(Request request, EntityManager em) throws DataAccessException, ParseException  {
		String req = "select new com.ino.steylo.helper.contrat.NumberDto(count(e.id)) from Chanel e where e IS NOT NULL";
		HashMap<String, Object> param = new HashMap<>();

		ChanelDto dto = request.getDataChanel();

		req = generateCriteria(dto, req, param);

		javax.persistence.Query query = em.createQuery(req);

		for (Map.Entry<String, Object> entry : param.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}

		NumberDto number = (NumberDto) query.getResultList().get(0);
		return number.getNombre();
	}

	default String generateCriteria(ChanelDto dto, String req, HashMap<String, Object> param) throws ParseException{
		if (dto != null) {
			if (dto.getId()!= null && dto.getId() > 0) {
				req += " AND e.id = :id";
				param.put("id", dto.getId());
			}
			if (dto.getFormeJuridique()!= null && !dto.getFormeJuridique().isEmpty()) {
				req += " AND e.formeJuridique = :formeJuridique";
				param.put("formeJuridique", dto.getFormeJuridique());
			}
			if (dto.getNomChanel()!= null && !dto.getNomChanel().isEmpty()) {
				req += " AND e.nomChanel = :nomChanel";
				param.put("nomChanel", dto.getNomChanel());
			}
			if (dto.getSigle()!= null && !dto.getSigle().isEmpty()) {
				req += " AND e.sigle = :sigle";
				param.put("sigle", dto.getSigle());
			}
			if (dto.getNumeroRegistre()!= null && !dto.getNumeroRegistre().isEmpty()) {
				req += " AND e.numeroRegistre = :numeroRegistre";
				param.put("numeroRegistre", dto.getNumeroRegistre());
			}
			if (dto.getNumeroImmatriculation()!= null && !dto.getNumeroImmatriculation().isEmpty()) {
				req += " AND e.numeroImmatriculation = :numeroImmatriculation";
				param.put("numeroImmatriculation", dto.getNumeroImmatriculation());
			}
			if (dto.getDescriptionSecteur()!= null && !dto.getDescriptionSecteur().isEmpty()) {
				req += " AND e.descriptionSecteur = :descriptionSecteur";
				param.put("descriptionSecteur", dto.getDescriptionSecteur());
			}
			if (dto.getSiteWeb()!= null && !dto.getSiteWeb().isEmpty()) {
				req += " AND e.siteWeb = :siteWeb";
				param.put("siteWeb", dto.getSiteWeb());
			}
			if (dto.getEmail()!= null && !dto.getEmail().isEmpty()) {
				req += " AND e.email = :email";
				param.put("email", dto.getEmail());
			}
			if (dto.getPhoneNumber()!= null && !dto.getPhoneNumber().isEmpty()) {
				req += " AND e.phoneNumber = :phoneNumber";
				param.put("phoneNumber", dto.getPhoneNumber());
			}
			if (dto.getFax()!= null && !dto.getFax().isEmpty()) {
				req += " AND e.fax = :fax";
				param.put("fax", dto.getFax());
			}
			if (dto.getEtat()!= null && !dto.getEtat().isEmpty()) {
				req += " AND e.etat = :etat";
				param.put("etat", dto.getEtat());
			}
			if (dto.getVille()!= null && !dto.getVille().isEmpty()) {
				req += " AND e.ville = :ville";
				param.put("ville", dto.getVille());
			}
			if (dto.getCodePostal()!= null && !dto.getCodePostal().isEmpty()) {
				req += " AND e.codePostal = :codePostal";
				param.put("codePostal", dto.getCodePostal());
			}
			if (dto.getAddress()!= null && !dto.getAddress().isEmpty()) {
				req += " AND e.address = :address";
				param.put("address", dto.getAddress());
			}
			if (dto.getLogo()!= null && !dto.getLogo().isEmpty()) {
				req += " AND e.logo = :logo";
				param.put("logo", dto.getLogo());
			}
			if (dto.getExtLogo()!= null && !dto.getExtLogo().isEmpty()) {
				req += " AND e.extLogo = :extLogo";
				param.put("extLogo", dto.getExtLogo());
			}
			if (dto.getCodeGuichet()!= null && !dto.getCodeGuichet().isEmpty()) {
				req += " AND e.codeGuichet = :codeGuichet";
				param.put("codeGuichet", dto.getCodeGuichet());
			}
			if (dto.getCodeBanque()!= null && !dto.getCodeBanque().isEmpty()) {
				req += " AND e.codeBanque = :codeBanque";
				param.put("codeBanque", dto.getCodeBanque());
			}
			if (dto.getNumeroCompte()!= null && !dto.getNumeroCompte().isEmpty()) {
				req += " AND e.numeroCompte = :numeroCompte";
				param.put("numeroCompte", dto.getNumeroCompte());
			}
			if (dto.getSwift()!= null && !dto.getSwift().isEmpty()) {
				req += " AND e.swift = :swift";
				param.put("swift", dto.getSwift());
			}
			if (dto.getCleRib()!= null && !dto.getCleRib().isEmpty()) {
				req += " AND e.cleRib = :cleRib";
				param.put("cleRib", dto.getCleRib());
			}
			if (dto.getRib()!= null && !dto.getRib().isEmpty()) {
				req += " AND e.rib = :rib";
				param.put("rib", dto.getRib());
			}
			if (dto.getIban()!= null && !dto.getIban().isEmpty()) {
				req += " AND e.iban = :iban";
				param.put("iban", dto.getIban());
			}
			if (dto.getRowDateUpdate()!= null && !dto.getRowDateUpdate().isEmpty()) {
				req += " AND FUNCTION('DAY', e.rowDateUpdate) = FUNCTION('DAY', :rowDateUpdate) AND FUNCTION('MONTH', e.rowDateUpdate) = FUNCTION('MONTH', :rowDateUpdate) AND FUNCTION('YEAR', e.rowDateUpdate) = FUNCTION('YEAR', :rowDateUpdate) ";
				param.put("rowDateUpdate",  new SimpleDateFormat("dd/MM/yyyy").parse(dto.getRowDateUpdate()));
			}
			if (dto.getLogoDateUpdate()!= null && !dto.getLogoDateUpdate().isEmpty()) {
				req += " AND FUNCTION('DAY', e.logoDateUpdate) = FUNCTION('DAY', :logoDateUpdate) AND FUNCTION('MONTH', e.logoDateUpdate) = FUNCTION('MONTH', :logoDateUpdate) AND FUNCTION('YEAR', e.logoDateUpdate) = FUNCTION('YEAR', :logoDateUpdate) ";
				param.put("logoDateUpdate",  new SimpleDateFormat("dd/MM/yyyy").parse(dto.getLogoDateUpdate()));
			}
			if (dto.getIdPays()!= null && dto.getIdPays() > 0) {
				req += " AND e.country.id = :idPays";
				param.put("idPays", dto.getIdPays());
			}
			if (dto.getIdSecteurActivite()!= null && dto.getIdSecteurActivite() > 0) {
				req += " AND e.secteurActivite.id = :idSecteurActivite";
				param.put("idSecteurActivite", dto.getIdSecteurActivite());
			}
		}
		
		return req;
	}
}
