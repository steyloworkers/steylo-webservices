package com.ino.steylo.dao.repository;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.springframework.dao.DataAccessException;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.ino.steylo.helper.dto.*;
import com.ino.steylo.helper.contrat.*;
import com.ino.steylo.dao.entity.*;

/**
 * Repository : SousCategorieProduit.
 */
@Repository
public interface SousCategorieProduitRepository extends JpaRepository<SousCategorieProduit, Integer> {
	/**
	 * Finds SousCategorieProduit by using id as a search criteria.
	 * 
	 * @param id
	 * @return An Object SousCategorieProduit whose id is equals to the given id. If
	 *         no SousCategorieProduit is found, this method returns null.
	 */
	@Query("select e from SousCategorieProduit e where e.id = :id")
	SousCategorieProduit findById(@Param("id")Integer id);

	/**
	 * Finds SousCategorieProduit by using libelle as a search criteria.
	 * 
	 * @param libelle
	 * @return An Object SousCategorieProduit whose libelle is equals to the given libelle. If
	 *         no SousCategorieProduit is found, this method returns null.
	 */
	@Query("select e from SousCategorieProduit e where e.libelle = :libelle")
	List<SousCategorieProduit> findByLibelle(@Param("libelle")String libelle);

	/**
	 * Finds SousCategorieProduit by using idCategorie as a search criteria.
	 * 
	 * @param idCategorie
	 * @return An Object SousCategorieProduit whose idCategorie is equals to the given idCategorie. If
	 *         no SousCategorieProduit is found, this method returns null.
	 */
	@Query("select e from SousCategorieProduit e where e.categorieProduit.id = :idCategorie")
	List<SousCategorieProduit> findByIdCategorie(@Param("idCategorie")Integer idCategorie);

	/**
	 * Finds List of SousCategorieProduit by using sousCategorieProduitDto as a search criteria.
	 * 
	 * @param request, em
	 * @return A List of SousCategorieProduit
	 * @throws DataAccessException,ParseException
	 */
	public default List<SousCategorieProduit> getByCriteria(Request request, EntityManager em) throws DataAccessException, ParseException {
		String req = "select e from SousCategorieProduit e where e IS NOT NULL";
		HashMap<String, Object> param = new HashMap<>();

		SousCategorieProduitDto dto = request.getDataSousCategorieProduit();
		
		req = generateCriteria(dto, req, param);
		req += " order by  e.id desc";

		TypedQuery<SousCategorieProduit> query = em.createQuery(req, SousCategorieProduit.class);

		for (Map.Entry<String, Object> entry : param.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}

		if (request.getIndex() != null && request.getSize() != null) {
			query.setFirstResult(request.getIndex() * request.getSize());
			query.setMaxResults(request.getSize());
		}

		return query.getResultList();
	}

	/**
	 * Finds count of SousCategorieProduit by using sousCategorieProduitDto as a search criteria.
	 * 
	 * @param request,em
	 * @return Number of SousCategorieProduit
	 * 
	 */
	public default Long count(Request request, EntityManager em) throws DataAccessException, ParseException  {
		String req = "select new com.ino.steylo.helper.contrat.NumberDto(count(e.id)) from SousCategorieProduit e where e IS NOT NULL";
		HashMap<String, Object> param = new HashMap<>();

		SousCategorieProduitDto dto = request.getDataSousCategorieProduit();

		req = generateCriteria(dto, req, param);

		javax.persistence.Query query = em.createQuery(req);

		for (Map.Entry<String, Object> entry : param.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}

		NumberDto number = (NumberDto) query.getResultList().get(0);
		return number.getNombre();
	}

	default String generateCriteria(SousCategorieProduitDto dto, String req, HashMap<String, Object> param) throws ParseException{
		if (dto != null) {
			if (dto.getId()!= null && dto.getId() > 0) {
				req += " AND e.id = :id";
				param.put("id", dto.getId());
			}
			if (dto.getLibelle()!= null && !dto.getLibelle().isEmpty()) {
				req += " AND e.libelle = :libelle";
				param.put("libelle", dto.getLibelle());
			}
			if (dto.getIdCategorie()!= null && dto.getIdCategorie() > 0) {
				req += " AND e.categorieProduit.id = :idCategorie";
				param.put("idCategorie", dto.getIdCategorie());
			}
		}
		
		return req;
	}
}
