package com.ino.steylo.dao.repository;

import java.util.Date;
import java.util.List;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.springframework.dao.DataAccessException;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.ino.steylo.helper.dto.*;
import com.ino.steylo.helper.contrat.*;
import com.ino.steylo.dao.entity.*;

/**
 * Repository : ChanelUsers.
 */
@Repository
public interface ChanelUsersRepository extends JpaRepository<ChanelUsers, Integer> {
	/**
	 * Finds ChanelUsers by using id as a search criteria.
	 * 
	 * @param id
	 * @return An Object ChanelUsers whose id is equals to the given id. If
	 *         no ChanelUsers is found, this method returns null.
	 */
	@Query("select e from ChanelUsers e where e.id = :id")
	ChanelUsers findById(@Param("id")Integer id);

	/**
	 * Finds ChanelUsers by using nom as a search criteria.
	 * 
	 * @param nom
	 * @return An Object ChanelUsers whose nom is equals to the given nom. If
	 *         no ChanelUsers is found, this method returns null.
	 */
	@Query("select e from ChanelUsers e where e.nom = :nom")
	List<ChanelUsers> findByNom(@Param("nom")String nom);
	/**
	 * Finds ChanelUsers by using prenoms as a search criteria.
	 * 
	 * @param prenoms
	 * @return An Object ChanelUsers whose prenoms is equals to the given prenoms. If
	 *         no ChanelUsers is found, this method returns null.
	 */
	@Query("select e from ChanelUsers e where e.prenoms = :prenoms")
	List<ChanelUsers> findByPrenoms(@Param("prenoms")String prenoms);
	/**
	 * Finds ChanelUsers by using login as a search criteria.
	 * 
	 * @param login
	 * @return An Object ChanelUsers whose login is equals to the given login. If
	 *         no ChanelUsers is found, this method returns null.
	 */
	@Query("select e from ChanelUsers e where e.login = :login")
    ChanelUsers findByLogin(@Param("login")String login);
	/**
	 * Finds ChanelUsers by using password as a search criteria.
	 * 
	 * @param password
	 * @return An Object ChanelUsers whose password is equals to the given password. If
	 *         no ChanelUsers is found, this method returns null.
	 */
	@Query("select e from ChanelUsers e where e.password = :password")
	List<ChanelUsers> findByPassword(@Param("password")String password);
	/**
	 * Finds ChanelUsers by using email as a search criteria.
	 * 
	 * @param email
	 * @return An Object ChanelUsers whose email is equals to the given email. If
	 *         no ChanelUsers is found, this method returns null.
	 */
	@Query("select e from ChanelUsers e where e.email = :email")
	List<ChanelUsers> findByEmail(@Param("email")String email);
	/**
	 * Finds ChanelUsers by using statut as a search criteria.
	 * 
	 * @param statut
	 * @return An Object ChanelUsers whose statut is equals to the given statut. If
	 *         no ChanelUsers is found, this method returns null.
	 */
	@Query("select e from ChanelUsers e where e.statut = :statut")
	List<ChanelUsers> findByStatut(@Param("statut")Integer statut);
	/**
	 * Finds ChanelUsers by using dateCreation as a search criteria.
	 * 
	 * @param dateCreation
	 * @return An Object ChanelUsers whose dateCreation is equals to the given dateCreation. If
	 *         no ChanelUsers is found, this method returns null.
	 */
	@Query("select e from ChanelUsers e where e.dateCreation = :dateCreation")
	List<ChanelUsers> findByDateCreation(@Param("dateCreation")Date dateCreation);
	/**
	 * Finds ChanelUsers by using updatedAt as a search criteria.
	 * 
	 * @param updatedAt
	 * @return An Object ChanelUsers whose updatedAt is equals to the given updatedAt. If
	 *         no ChanelUsers is found, this method returns null.
	 */
	@Query("select e from ChanelUsers e where e.updatedAt = :updatedAt")
	List<ChanelUsers> findByUpdatedAt(@Param("updatedAt")Date updatedAt);
	/**
	 * Finds ChanelUsers by using deleteAt as a search criteria.
	 * 
	 * @param deleteAt
	 * @return An Object ChanelUsers whose deleteAt is equals to the given deleteAt. If
	 *         no ChanelUsers is found, this method returns null.
	 */
	@Query("select e from ChanelUsers e where e.deleteAt = :deleteAt")
	List<ChanelUsers> findByDeleteAt(@Param("deleteAt")Date deleteAt);

	/**
	 * Finds ChanelUsers by using updatedBy as a search criteria.
	 * 
	 * @param updatedBy
	 * @return An Object ChanelUsers whose updatedBy is equals to the given updatedBy. If
	 *         no ChanelUsers is found, this method returns null.
	 */
	@Query("select e from ChanelUsers e where e.chanelUsers3.id = :updatedBy")
	List<ChanelUsers> findByUpdatedBy(@Param("updatedBy")Integer updatedBy);

	/**
	 * Finds ChanelUsers by using deleteBy as a search criteria.
	 * 
	 * @param deleteBy
	 * @return An Object ChanelUsers whose deleteBy is equals to the given deleteBy. If
	 *         no ChanelUsers is found, this method returns null.
	 */
	@Query("select e from ChanelUsers e where e.chanelUsers2.id = :deleteBy")
	List<ChanelUsers> findByDeleteBy(@Param("deleteBy")Integer deleteBy);

	/**
	 * Finds ChanelUsers by using idChanel as a search criteria.
	 * 
	 * @param idChanel
	 * @return An Object ChanelUsers whose idChanel is equals to the given idChanel. If
	 *         no ChanelUsers is found, this method returns null.
	 */
	@Query("select e from ChanelUsers e where e.chanel.id = :idChanel")
	List<ChanelUsers> findByIdChanel(@Param("idChanel")Integer idChanel);

	/**
	 * Finds ChanelUsers by using createdBy as a search criteria.
	 *
	 * @param createdBy
	 * @return An Object ChanelUsers whose createdBy is equals to the given createdBy. If
	 *         no ChanelUsers is found, this method returns null.
	 */
	@Query("select e from ChanelUsers e where e.chanelUsers.id = :createdBy")
	List<ChanelUsers> findByCreatedBy(@Param("createdBy")Integer createdBy);

	ChanelUsers findChanelUsersByLoginAndPassword(@Param("login")String login, @Param("password")String password);

	ChanelUsers findChanelUsersByLoginAndChanel_Id(@Param("login")String login, @Param("idChanel")Integer idChanel);

	/**
	 * Finds List of ChanelUsers by using chanelUsersDto as a search criteria.
	 * 
	 * @param request, em
	 * @return A List of ChanelUsers
	 * @throws DataAccessException,ParseException
	 */
	public default List<ChanelUsers> getByCriteria(Request request, EntityManager em) throws DataAccessException, ParseException {
		String req = "select e from ChanelUsers e where e IS NOT NULL";
		HashMap<String, Object> param = new HashMap<>();

		ChanelUsersDto dto = request.getDataChanelUsers();
		
		req = generateCriteria(dto, req, param);
		req += " order by  e.id desc";

		TypedQuery<ChanelUsers> query = em.createQuery(req, ChanelUsers.class);

		for (Map.Entry<String, Object> entry : param.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}

		if (request.getIndex() != null && request.getSize() != null) {
			query.setFirstResult(request.getIndex() * request.getSize());
			query.setMaxResults(request.getSize());
		}

		return query.getResultList();
	}

	/**
	 * Finds count of ChanelUsers by using chanelUsersDto as a search criteria.
	 * 
	 * @param request,em
	 * @return Number of ChanelUsers
	 * 
	 */
	public default Long count(Request request, EntityManager em) throws DataAccessException, ParseException  {
		String req = "select new com.ino.steylo.helper.contrat.NumberDto(count(e.id)) from ChanelUsers e where e IS NOT NULL";
		HashMap<String, Object> param = new HashMap<>();

		ChanelUsersDto dto = request.getDataChanelUsers();

		req = generateCriteria(dto, req, param);

		javax.persistence.Query query = em.createQuery(req);

		for (Map.Entry<String, Object> entry : param.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}

		NumberDto number = (NumberDto) query.getResultList().get(0);
		return number.getNombre();
	}

	default String generateCriteria(ChanelUsersDto dto, String req, HashMap<String, Object> param) throws ParseException{
		if (dto != null) {
			if (dto.getId()!= null && dto.getId() > 0) {
				req += " AND e.id = :id";
				param.put("id", dto.getId());
			}
			if (dto.getNom()!= null && !dto.getNom().isEmpty()) {
				req += " AND e.nom = :nom";
				param.put("nom", dto.getNom());
			}
			if (dto.getPrenoms()!= null && !dto.getPrenoms().isEmpty()) {
				req += " AND e.prenoms = :prenoms";
				param.put("prenoms", dto.getPrenoms());
			}
			if (dto.getLogin()!= null && !dto.getLogin().isEmpty()) {
				req += " AND e.login = :login";
				param.put("login", dto.getLogin());
			}
			if (dto.getPassword()!= null && !dto.getPassword().isEmpty()) {
				req += " AND e.password = :password";
				param.put("password", dto.getPassword());
			}
			if (dto.getEmail()!= null && !dto.getEmail().isEmpty()) {
				req += " AND e.email = :email";
				param.put("email", dto.getEmail());
			}
			if (dto.getStatut()!= null && dto.getStatut() > 0) {
				req += " AND e.statut = :statut";
				param.put("statut", dto.getStatut());
			}
			if (dto.getDateCreation()!= null && !dto.getDateCreation().isEmpty()) {
				req += " AND FUNCTION('DAY', e.dateCreation) = FUNCTION('DAY', :dateCreation) AND FUNCTION('MONTH', e.dateCreation) = FUNCTION('MONTH', :dateCreation) AND FUNCTION('YEAR', e.dateCreation) = FUNCTION('YEAR', :dateCreation) ";
				param.put("dateCreation",  new SimpleDateFormat("dd/MM/yyyy").parse(dto.getDateCreation()));
			}
			if (dto.getUpdatedAt()!= null && !dto.getUpdatedAt().isEmpty()) {
				req += " AND FUNCTION('DAY', e.updatedAt) = FUNCTION('DAY', :updatedAt) AND FUNCTION('MONTH', e.updatedAt) = FUNCTION('MONTH', :updatedAt) AND FUNCTION('YEAR', e.updatedAt) = FUNCTION('YEAR', :updatedAt) ";
				param.put("updatedAt",  new SimpleDateFormat("dd/MM/yyyy").parse(dto.getUpdatedAt()));
			}
			if (dto.getDeleteAt()!= null && !dto.getDeleteAt().isEmpty()) {
				req += " AND FUNCTION('DAY', e.deleteAt) = FUNCTION('DAY', :deleteAt) AND FUNCTION('MONTH', e.deleteAt) = FUNCTION('MONTH', :deleteAt) AND FUNCTION('YEAR', e.deleteAt) = FUNCTION('YEAR', :deleteAt) ";
				param.put("deleteAt",  new SimpleDateFormat("dd/MM/yyyy").parse(dto.getDeleteAt()));
			}
			if (dto.getUpdatedBy()!= null && dto.getUpdatedBy() > 0) {
				req += " AND e.chanelUsers3.id = :updatedBy";
				param.put("updatedBy", dto.getUpdatedBy());
			}
			if (dto.getDeleteBy()!= null && dto.getDeleteBy() > 0) {
				req += " AND e.chanelUsers2.id = :deleteBy";
				param.put("deleteBy", dto.getDeleteBy());
			}
			if (dto.getIdChanel()!= null && dto.getIdChanel() > 0) {
				req += " AND e.chanel.id = :idChanel";
				param.put("idChanel", dto.getIdChanel());
			}
			if (dto.getCreatedBy()!= null && dto.getCreatedBy() > 0) {
				req += " AND e.chanelUsers.id = :createdBy";
				param.put("createdBy", dto.getCreatedBy());
			}
		}
		
		return req;
	}
}
