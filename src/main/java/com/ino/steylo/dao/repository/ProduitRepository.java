package com.ino.steylo.dao.repository;

import java.util.Date;
import java.util.List;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.springframework.dao.DataAccessException;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.ino.steylo.helper.dto.*;
import com.ino.steylo.helper.contrat.*;
import com.ino.steylo.dao.entity.*;

/**
 * Repository : Produit.
 */
@Repository
public interface ProduitRepository extends JpaRepository<Produit, Integer> {
	/**
	 * Finds Produit by using id as a search criteria.
	 * 
	 * @param id
	 * @return An Object Produit whose id is equals to the given id. If
	 *         no Produit is found, this method returns null.
	 */
	@Query("select e from Produit e where e.id = :id")
	Produit findById(@Param("id")Integer id);

	/**
	 * Finds Produit by using libelle as a search criteria.
	 * 
	 * @param libelle
	 * @return An Object Produit whose libelle is equals to the given libelle. If
	 *         no Produit is found, this method returns null.
	 */
	@Query("select e from Produit e where e.libelle = :libelle")
	List<Produit> findByLibelle(@Param("libelle")String libelle);
	/**
	 * Finds Produit by using referenceProduit as a search criteria.
	 * 
	 * @param referenceProduit
	 * @return An Object Produit whose referenceProduit is equals to the given referenceProduit. If
	 *         no Produit is found, this method returns null.
	 */
	@Query("select e from Produit e where e.referenceProduit = :referenceProduit")
    Produit findByReferenceProduit(@Param("referenceProduit")String referenceProduit);
	/**
	 * Finds Produit by using description as a search criteria.
	 * 
	 * @param description
	 * @return An Object Produit whose description is equals to the given description. If
	 *         no Produit is found, this method returns null.
	 */
	@Query("select e from Produit e where e.description = :description")
	List<Produit> findByDescription(@Param("description")String description);
	/**
	 * Finds Produit by using prixMin as a search criteria.
	 * 
	 * @param prixMin
	 * @return An Object Produit whose prixMin is equals to the given prixMin. If
	 *         no Produit is found, this method returns null.
	 */
	@Query("select e from Produit e where e.prixMin = :prixMin")
	List<Produit> findByPrixMin(@Param("prixMin")Integer prixMin);
	/**
	 * Finds Produit by using prixMax as a search criteria.
	 * 
	 * @param prixMax
	 * @return An Object Produit whose prixMax is equals to the given prixMax. If
	 *         no Produit is found, this method returns null.
	 */
	@Query("select e from Produit e where e.prixMax = :prixMax")
	List<Produit> findByPrixMax(@Param("prixMax")Integer prixMax);
	/**
	 * Finds Produit by using prix as a search criteria.
	 * 
	 * @param prix
	 * @return An Object Produit whose prix is equals to the given prix. If
	 *         no Produit is found, this method returns null.
	 */
	@Query("select e from Produit e where e.prix = :prix")
	List<Produit> findByPrix(@Param("prix")Integer prix);
	/**
	 * Finds Produit by using quantite as a search criteria.
	 * 
	 * @param quantite
	 * @return An Object Produit whose quantite is equals to the given quantite. If
	 *         no Produit is found, this method returns null.
	 */
	@Query("select e from Produit e where e.quantite = :quantite")
	List<Produit> findByQuantite(@Param("quantite")Integer quantite);
	/**
	 * Finds Produit by using dateCreation as a search criteria.
	 * 
	 * @param dateCreation
	 * @return An Object Produit whose dateCreation is equals to the given dateCreation. If
	 *         no Produit is found, this method returns null.
	 */
	@Query("select e from Produit e where e.dateCreation = :dateCreation")
	List<Produit> findByDateCreation(@Param("dateCreation")Date dateCreation);
	/**
	 * Finds Produit by using status as a search criteria.
	 * 
	 * @param status
	 * @return An Object Produit whose status is equals to the given status. If
	 *         no Produit is found, this method returns null.
	 */
	@Query("select e from Produit e where e.status = :status")
	List<Produit> findByStatus(@Param("status")Integer status);
	/**
	 * Finds Produit by using trash as a search criteria.
	 * 
	 * @param trash
	 * @return An Object Produit whose trash is equals to the given trash. If
	 *         no Produit is found, this method returns null.
	 */
	@Query("select e from Produit e where e.trash = :trash")
	List<Produit> findByTrash(@Param("trash")Integer trash);
	/**
	 * Finds Produit by using couleur as a search criteria.
	 * 
	 * @param couleur
	 * @return An Object Produit whose couleur is equals to the given couleur. If
	 *         no Produit is found, this method returns null.
	 */
	@Query("select e from Produit e where e.couleur = :couleur")
	List<Produit> findByCouleur(@Param("couleur")String couleur);
	/**
	 * Finds Produit by using poids as a search criteria.
	 * 
	 * @param poids
	 * @return An Object Produit whose poids is equals to the given poids. If
	 *         no Produit is found, this method returns null.
	 */
	@Query("select e from Produit e where e.poids = :poids")
	List<Produit> findByPoids(@Param("poids")String poids);
	/**
	 * Finds Produit by using size as a search criteria.
	 * 
	 * @param size
	 * @return An Object Produit whose size is equals to the given size. If
	 *         no Produit is found, this method returns null.
	 */
	@Query("select e from Produit e where e.size = :size")
	List<Produit> findBySize(@Param("size")String size);
	/**
	 * Finds Produit by using idSousCategorie as a search criteria.
	 * 
	 * @param idSousCategorie
	 * @return An Object Produit whose idSousCategorie is equals to the given idSousCategorie. If
	 *         no Produit is found, this method returns null.
	 */
	@Query("select e from Produit e where e.idSousCategorie = :idSousCategorie")
	List<Produit> findByIdSousCategorie(@Param("idSousCategorie")Integer idSousCategorie);
	/**
	 * Finds Produit by using lastUpdate as a search criteria.
	 * 
	 * @param lastUpdate
	 * @return An Object Produit whose lastUpdate is equals to the given lastUpdate. If
	 *         no Produit is found, this method returns null.
	 */
	@Query("select e from Produit e where e.lastUpdate = :lastUpdate")
	List<Produit> findByLastUpdate(@Param("lastUpdate")Date lastUpdate);
	/**
	 * Finds Produit by using idMarque as a search criteria.
	 * 
	 * @param idMarque
	 * @return An Object Produit whose idMarque is equals to the given idMarque. If
	 *         no Produit is found, this method returns null.
	 */
	@Query("select e from Produit e where e.idMarque = :idMarque")
	List<Produit> findByIdMarque(@Param("idMarque")Integer idMarque);

	/**
	 * Finds Produit by using idChanel as a search criteria.
	 * 
	 * @param idChanel
	 * @return An Object Produit whose idChanel is equals to the given idChanel. If
	 *         no Produit is found, this method returns null.
	 */
	@Query("select e from Produit e where e.chanel.id = :idChanel")
	List<Produit> findByIdChanel(@Param("idChanel")Integer idChanel);

	/**
	 * Finds Produit by using idCategorie as a search criteria.
	 * 
	 * @param idCategorie
	 * @return An Object Produit whose idCategorie is equals to the given idCategorie. If
	 *         no Produit is found, this method returns null.
	 */
	@Query("select e from Produit e where e.categorieProduit.id = :idCategorie")
	List<Produit> findByIdCategorie(@Param("idCategorie")Integer idCategorie);

	/**
	 * Finds List of Produit by using produitDto as a search criteria.
	 * 
	 * @param request, em
	 * @return A List of Produit
	 * @throws DataAccessException,ParseException
	 */
	public default List<Produit> getByCriteria(Request request, EntityManager em) throws DataAccessException, ParseException {
		String req = "select e from Produit e where e IS NOT NULL";
		HashMap<String, Object> param = new HashMap<>();

		ProduitDto dto = request.getDataProduit();
		
		req = generateCriteria(dto, req, param);
		req += " order by e.id desc";

		TypedQuery<Produit> query = em.createQuery(req, Produit.class);

		for (Map.Entry<String, Object> entry : param.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}

		if (request.getIndex() != null && request.getSize() != null) {
			query.setFirstResult(request.getIndex() * request.getSize());
			query.setMaxResults(request.getSize());
		}

		return query.getResultList();
	}

	/**
	 * Finds count of Produit by using produitDto as a search criteria.
	 * 
	 * @param request,em
	 * @return Number of Produit
	 * 
	 */
	public default Long count(Request request, EntityManager em) throws DataAccessException, ParseException  {
		String req = "select new com.ino.steylo.helper.contrat.NumberDto(count(e.id)) from Produit e where e IS NOT NULL";
		HashMap<String, Object> param = new HashMap<>();

		ProduitDto dto = request.getDataProduit();

		req = generateCriteria(dto, req, param);

		javax.persistence.Query query = em.createQuery(req);

		for (Map.Entry<String, Object> entry : param.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}

		NumberDto number = (NumberDto) query.getResultList().get(0);
		return number.getNombre();
	}

	default String generateCriteria(ProduitDto dto, String req, HashMap<String, Object> param) throws ParseException{
		if (dto != null) {
			if (dto.getId()!= null && dto.getId() > 0) {
				req += " AND e.id = :id";
				param.put("id", dto.getId());
			}
			if (dto.getLibelle()!= null && !dto.getLibelle().isEmpty()) {
				req += " AND e.libelle LIKE :libelle";
				param.put("libelle", "%"+dto.getLibelle()+"%");
			}
			if (dto.getReferenceProduit()!= null && !dto.getReferenceProduit().isEmpty()) {
				req += " AND e.referenceProduit LIKE :referenceProduit";
				param.put("referenceProduit", "%"+dto.getReferenceProduit()+"%");
			}
			if (dto.getDescription()!= null && !dto.getDescription().isEmpty()) {
				req += " AND e.description = :description";
				param.put("description", dto.getDescription());
			}
			if (dto.getPrixMin()!= null && dto.getPrixMin() > 0) {
				req += " AND e.prixMin = :prixMin";
				param.put("prixMin", dto.getPrixMin());
			}
			if (dto.getPrixMax()!= null && dto.getPrixMax() > 0) {
				req += " AND e.prixMax = :prixMax";
				param.put("prixMax", dto.getPrixMax());
			}
			if (dto.getPrix()!= null && dto.getPrix() > 0) {
				req += " AND e.prix = :prix";
				param.put("prix", dto.getPrix());
			}
			if (dto.getQuantite()!= null && dto.getQuantite() > 0) {
				req += " AND e.quantite = :quantite";
				param.put("quantite", dto.getQuantite());
			}
			if (dto.getDateCreation()!= null && !dto.getDateCreation().isEmpty()) {
				req += " AND FUNCTION('DAY', e.dateCreation) = FUNCTION('DAY', :dateCreation) AND FUNCTION('MONTH', e.dateCreation) = FUNCTION('MONTH', :dateCreation) AND FUNCTION('YEAR', e.dateCreation) = FUNCTION('YEAR', :dateCreation) ";
				param.put("dateCreation",  new SimpleDateFormat("dd/MM/yyyy").parse(dto.getDateCreation()));
			}
			if (dto.getStatus()!= null && dto.getStatus() >= 0) {
				req += " AND e.status = :status";
				param.put("status", dto.getStatus());
			}
			if (dto.getTrash()!= null && dto.getTrash() >= 0) {
				req += " AND e.trash = :trash";
				param.put("trash", dto.getTrash());
			}
			if (dto.getCouleur()!= null && !dto.getCouleur().isEmpty()) {
				req += " AND e.couleur LIKE :couleur";
				param.put("couleur", "%"+dto.getCouleur()+"%");
			}
			if (dto.getPoids()!= null && !dto.getPoids().isEmpty()) {
				req += " AND e.poids = :poids";
				param.put("poids", dto.getPoids());
			}
			if (dto.getSize()!= null && !dto.getSize().isEmpty()) {
				req += " AND e.size = :size";
				param.put("size", dto.getSize());
			}
			if (dto.getIdSousCategorie()!= null && dto.getIdSousCategorie() > 0) {
				req += " AND e.idSousCategorie = :idSousCategorie";
				param.put("idSousCategorie", dto.getIdSousCategorie());
			}
			if (dto.getLastUpdate()!= null && !dto.getLastUpdate().isEmpty()) {
				req += " AND FUNCTION('DAY', e.lastUpdate) = FUNCTION('DAY', :lastUpdate) AND FUNCTION('MONTH', e.lastUpdate) = FUNCTION('MONTH', :lastUpdate) AND FUNCTION('YEAR', e.lastUpdate) = FUNCTION('YEAR', :lastUpdate) ";
				param.put("lastUpdate",  new SimpleDateFormat("dd/MM/yyyy").parse(dto.getLastUpdate()));
			}
			if (dto.getIdMarque()!= null && dto.getIdMarque() > 0) {
				req += " AND e.idMarque = :idMarque";
				param.put("idMarque", dto.getIdMarque());
			}
			if (dto.getIdChanel()!= null && dto.getIdChanel() > 0) {
				req += " AND e.chanel.id = :idChanel";
				param.put("idChanel", dto.getIdChanel());
			}
			if (dto.getIdCategorie()!= null && dto.getIdCategorie() > 0) {
				req += " AND e.categorieProduit.id = :idCategorie";
				param.put("idCategorie", dto.getIdCategorie());
			}
		}
		
		return req;
	}
}
