package com.ino.steylo.dao.repository;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.springframework.dao.DataAccessException;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.ino.steylo.helper.dto.*;
import com.ino.steylo.helper.contrat.*;
import com.ino.steylo.dao.entity.*;

/**
 * Repository : PermissionProfil.
 */
@Repository
public interface PermissionProfilRepository extends JpaRepository<PermissionProfil, Integer> {
	/**
	 * Finds PermissionProfil by using id as a search criteria.
	 * 
	 * @param id
	 * @return An Object PermissionProfil whose id is equals to the given id. If
	 *         no PermissionProfil is found, this method returns null.
	 */
	@Query("select e from PermissionProfil e where e.id = :id")
	PermissionProfil findById(@Param("id")Integer id);

	/**
	 * Finds PermissionProfil by using libelle as a search criteria.
	 * 
	 * @param libelle
	 * @return An Object PermissionProfil whose libelle is equals to the given libelle. If
	 *         no PermissionProfil is found, this method returns null.
	 */
	@Query("select e from PermissionProfil e where e.libelle = :libelle")
	List<PermissionProfil> findByLibelle(@Param("libelle")String libelle);

	/**
	 * Finds PermissionProfil by using idChanel as a search criteria.
	 * 
	 * @param idChanel
	 * @return An Object PermissionProfil whose idChanel is equals to the given idChanel. If
	 *         no PermissionProfil is found, this method returns null.
	 */
	@Query("select e from PermissionProfil e where e.chanel.id = :idChanel")
	List<PermissionProfil> findByIdChanel(@Param("idChanel")Integer idChanel);

	/**
	 * Finds List of PermissionProfil by using permissionProfilDto as a search criteria.
	 * 
	 * @param request, em
	 * @return A List of PermissionProfil
	 * @throws DataAccessException,ParseException
	 */
	public default List<PermissionProfil> getByCriteria(Request request, EntityManager em) throws DataAccessException, ParseException {
		String req = "select e from PermissionProfil e where e IS NOT NULL";
		HashMap<String, Object> param = new HashMap<>();

		PermissionProfilDto dto = request.getDataPermissionProfil();
		
		req = generateCriteria(dto, req, param);
		req += " order by  e.id desc";

		TypedQuery<PermissionProfil> query = em.createQuery(req, PermissionProfil.class);

		for (Map.Entry<String, Object> entry : param.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}

		if (request.getIndex() != null && request.getSize() != null) {
			query.setFirstResult(request.getIndex() * request.getSize());
			query.setMaxResults(request.getSize());
		}

		return query.getResultList();
	}

	/**
	 * Finds count of PermissionProfil by using permissionProfilDto as a search criteria.
	 * 
	 * @param request,em
	 * @return Number of PermissionProfil
	 * 
	 */
	public default Long count(Request request, EntityManager em) throws DataAccessException, ParseException  {
		String req = "select new com.ino.steylo.helper.contrat.NumberDto(count(e.id)) from PermissionProfil e where e IS NOT NULL";
		HashMap<String, Object> param = new HashMap<>();

		PermissionProfilDto dto = request.getDataPermissionProfil();

		req = generateCriteria(dto, req, param);

		javax.persistence.Query query = em.createQuery(req);

		for (Map.Entry<String, Object> entry : param.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}

		NumberDto number = (NumberDto) query.getResultList().get(0);
		return number.getNombre();
	}

	default String generateCriteria(PermissionProfilDto dto, String req, HashMap<String, Object> param) throws ParseException{
		if (dto != null) {
			if (dto.getId()!= null && dto.getId() > 0) {
				req += " AND e.id = :id";
				param.put("id", dto.getId());
			}
			if (dto.getLibelle()!= null && !dto.getLibelle().isEmpty()) {
				req += " AND e.libelle = :libelle";
				param.put("libelle", dto.getLibelle());
			}
			if (dto.getIdChanel()!= null && dto.getIdChanel() > 0) {
				req += " AND e.chanel.id = :idChanel";
				param.put("idChanel", dto.getIdChanel());
			}
		}
		
		return req;
	}
}
