package com.ino.steylo.dao.repository;

import java.util.List;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.springframework.dao.DataAccessException;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.ino.steylo.helper.dto.*;
import com.ino.steylo.helper.contrat.*;
import com.ino.steylo.dao.entity.*;

/**
 * Repository : ChanelPermissions.
 */
@Repository
public interface ChanelPermissionsRepository extends JpaRepository<ChanelPermissions, Integer> {
	/**
	 * Finds ChanelPermissions by using id as a search criteria.
	 * 
	 * @param id
	 * @return An Object ChanelPermissions whose id is equals to the given id. If
	 *         no ChanelPermissions is found, this method returns null.
	 */
	@Query("select e from ChanelPermissions e where e.id = :id")
	ChanelPermissions findById(@Param("id")Integer id);

	/**
	 * Finds ChanelPermissions by using code as a search criteria.
	 * 
	 * @param code
	 * @return An Object ChanelPermissions whose code is equals to the given code. If
	 *         no ChanelPermissions is found, this method returns null.
	 */
	@Query("select e from ChanelPermissions e where e.code = :code")
	ChanelPermissions findByCode(@Param("code")Integer code);
	/**
	 * Finds ChanelPermissions by using libelle as a search criteria.
	 * 
	 * @param libelle
	 * @return An Object ChanelPermissions whose libelle is equals to the given libelle. If
	 *         no ChanelPermissions is found, this method returns null.
	 */
	@Query("select e from ChanelPermissions e where e.libelle = :libelle")
	List<ChanelPermissions> findByLibelle(@Param("libelle")String libelle);

	/**
	 * Finds List of ChanelPermissions by using chanelPermissionsDto as a search criteria.
	 * 
	 * @param request, em
	 * @return A List of ChanelPermissions
	 * @throws DataAccessException,ParseException
	 */
	public default List<ChanelPermissions> getByCriteria(Request request, EntityManager em) throws DataAccessException, ParseException {
		String req = "select e from ChanelPermissions e where e IS NOT NULL";
		HashMap<String, Object> param = new HashMap<>();

		ChanelPermissionsDto dto = request.getDataChanelPermissions();
		
		req = generateCriteria(dto, req, param);
		req += " order by  e.id desc";

		TypedQuery<ChanelPermissions> query = em.createQuery(req, ChanelPermissions.class);

		for (Map.Entry<String, Object> entry : param.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}

		if (request.getIndex() != null && request.getSize() != null) {
			query.setFirstResult(request.getIndex() * request.getSize());
			query.setMaxResults(request.getSize());
		}

		return query.getResultList();
	}

	/**
	 * Finds count of ChanelPermissions by using chanelPermissionsDto as a search criteria.
	 * 
	 * @param request,em
	 * @return Number of ChanelPermissions
	 * 
	 */
	public default Long count(Request request, EntityManager em) throws DataAccessException, ParseException  {
		String req = "select new com.ino.steylo.helper.contrat.NumberDto(count(e.id)) from ChanelPermissions e where e IS NOT NULL";
		HashMap<String, Object> param = new HashMap<>();

		ChanelPermissionsDto dto = request.getDataChanelPermissions();

		req = generateCriteria(dto, req, param);

		javax.persistence.Query query = em.createQuery(req);

		for (Map.Entry<String, Object> entry : param.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}

		NumberDto number = (NumberDto) query.getResultList().get(0);
		return number.getNombre();
	}

	default String generateCriteria(ChanelPermissionsDto dto, String req, HashMap<String, Object> param) throws ParseException{
		if (dto != null) {
			if (dto.getId()!= null && dto.getId() > 0) {
				req += " AND e.id = :id";
				param.put("id", dto.getId());
			}
			if (dto.getCode()!= null && dto.getCode() > 0) {
				req += " AND e.code = :code";
				param.put("code", dto.getCode());
			}
			if (dto.getLibelle()!= null && !dto.getLibelle().isEmpty()) {
				req += " AND e.libelle = :libelle";
				param.put("libelle", dto.getLibelle());
			}
		}
		
		return req;
	}
}
