package com.ino.steylo.dao.repository;

import java.util.List;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.springframework.dao.DataAccessException;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.ino.steylo.helper.dto.*;
import com.ino.steylo.helper.contrat.*;
import com.ino.steylo.dao.entity.*;

/**
 * Repository : Country.
 */
@Repository
public interface CountryRepository extends JpaRepository<Country, Integer> {
	/**
	 * Finds Country by using id as a search criteria.
	 * 
	 * @param id
	 * @return An Object Country whose id is equals to the given id. If
	 *         no Country is found, this method returns null.
	 */
	@Query("select e from Country e where e.id = :id")
	Country findById(@Param("id")Integer id);

	/**
	 * Finds Country by using countryIso as a search criteria.
	 * 
	 * @param countryIso
	 * @return An Object Country whose countryIso is equals to the given countryIso. If
	 *         no Country is found, this method returns null.
	 */
	@Query("select e from Country e where e.countryIso = :countryIso")
	List<Country> findByCountryIso(@Param("countryIso")String countryIso);
	/**
	 * Finds Country by using countryName as a search criteria.
	 * 
	 * @param countryName
	 * @return An Object Country whose countryName is equals to the given countryName. If
	 *         no Country is found, this method returns null.
	 */
	@Query("select e from Country e where e.countryName = :countryName")
	List<Country> findByCountryName(@Param("countryName")String countryName);
	/**
	 * Finds Country by using countryNicename as a search criteria.
	 * 
	 * @param countryNicename
	 * @return An Object Country whose countryNicename is equals to the given countryNicename. If
	 *         no Country is found, this method returns null.
	 */
	@Query("select e from Country e where e.countryNicename = :countryNicename")
	List<Country> findByCountryNicename(@Param("countryNicename")String countryNicename);
	/**
	 * Finds Country by using countryIso3 as a search criteria.
	 * 
	 * @param countryIso3
	 * @return An Object Country whose countryIso3 is equals to the given countryIso3. If
	 *         no Country is found, this method returns null.
	 */
	@Query("select e from Country e where e.countryIso3 = :countryIso3")
	List<Country> findByCountryIso3(@Param("countryIso3")String countryIso3);
	/**
	 * Finds Country by using countryNumcode as a search criteria.
	 * 
	 * @param countryNumcode
	 * @return An Object Country whose countryNumcode is equals to the given countryNumcode. If
	 *         no Country is found, this method returns null.
	 */
	@Query("select e from Country e where e.countryNumcode = :countryNumcode")
	List<Country> findByCountryNumcode(@Param("countryNumcode")Short countryNumcode);
	/**
	 * Finds Country by using countryPhonecode as a search criteria.
	 * 
	 * @param countryPhonecode
	 * @return An Object Country whose countryPhonecode is equals to the given countryPhonecode. If
	 *         no Country is found, this method returns null.
	 */
	@Query("select e from Country e where e.countryPhonecode = :countryPhonecode")
	List<Country> findByCountryPhonecode(@Param("countryPhonecode")Integer countryPhonecode);

	/**
	 * Finds List of Country by using countryDto as a search criteria.
	 * 
	 * @param request, em
	 * @return A List of Country
	 * @throws DataAccessException,ParseException
	 */
	public default List<Country> getByCriteria(Request request, EntityManager em) throws DataAccessException, ParseException {
		String req = "select e from Country e where e IS NOT NULL";
		HashMap<String, Object> param = new HashMap<>();

		CountryDto dto = request.getDataCountry();
		
		req = generateCriteria(dto, req, param);
		req += " order by e.countryNicename asc";

		TypedQuery<Country> query = em.createQuery(req, Country.class);

		for (Map.Entry<String, Object> entry : param.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}

		if (request.getIndex() != null && request.getSize() != null) {
			query.setFirstResult(request.getIndex() * request.getSize());
			query.setMaxResults(request.getSize());
		}

		return query.getResultList();
	}

	/**
	 * Finds count of Country by using countryDto as a search criteria.
	 * 
	 * @param request,em
	 * @return Number of Country
	 * 
	 */
	public default Long count(Request request, EntityManager em) throws DataAccessException, ParseException  {
		String req = "select new com.ino.steylo.helper.contrat.NumberDto(count(e.id)) from Country e where e IS NOT NULL";
		HashMap<String, Object> param = new HashMap<>();

		CountryDto dto = request.getDataCountry();

		req = generateCriteria(dto, req, param);

		javax.persistence.Query query = em.createQuery(req);

		for (Map.Entry<String, Object> entry : param.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}

		NumberDto number = (NumberDto) query.getResultList().get(0);
		return number.getNombre();
	}

	default String generateCriteria(CountryDto dto, String req, HashMap<String, Object> param) throws ParseException{
		if (dto != null) {
			if (dto.getId()!= null && dto.getId() > 0) {
				req += " AND e.id = :id";
				param.put("id", dto.getId());
			}
			if (dto.getCountryIso()!= null && !dto.getCountryIso().isEmpty()) {
				req += " AND e.countryIso = :countryIso";
				param.put("countryIso", dto.getCountryIso());
			}
			if (dto.getCountryName()!= null && !dto.getCountryName().isEmpty()) {
				req += " AND e.countryName = :countryName";
				param.put("countryName", dto.getCountryName());
			}
			if (dto.getCountryNicename()!= null && !dto.getCountryNicename().isEmpty()) {
				req += " AND e.countryNicename = :countryNicename";
				param.put("countryNicename", dto.getCountryNicename());
			}
			if (dto.getCountryIso3()!= null && !dto.getCountryIso3().isEmpty()) {
				req += " AND e.countryIso3 = :countryIso3";
				param.put("countryIso3", dto.getCountryIso3());
			}
			if (dto.getCountryNumcode()!= null && dto.getCountryNumcode() > 0) {
				req += " AND e.countryNumcode = :countryNumcode";
				param.put("countryNumcode", dto.getCountryNumcode());
			}
			if (dto.getCountryPhonecode()!= null && dto.getCountryPhonecode() > 0) {
				req += " AND e.countryPhonecode = :countryPhonecode";
				param.put("countryPhonecode", dto.getCountryPhonecode());
			}
		}
		
		return req;
	}
}
