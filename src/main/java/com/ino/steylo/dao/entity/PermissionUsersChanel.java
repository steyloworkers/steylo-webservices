/*
 * Created on dec ( Time 18:46:57 )
 * Generated by Telosys Tools Generator ( version 2.1.1 )
 */
// This Bean has a basic Primary Key (not composite) 

package com.ino.steylo.dao.entity;

import java.io.Serializable;

//import javax.validation.constraints.* ;
//import org.hibernate.validator.constraints.* ;


import javax.persistence.*;

/**
 * Persistent class for entity stored in table "permission_users_chanel"
 *
 * @author ino
 *
 */

@Entity
@Table(name="permission_users_chanel" )
public class PermissionUsersChanel implements Serializable {

    private static final long serialVersionUID = 1L;

    //----------------------------------------------------------------------
    // ENTITY PRIMARY KEY ( BASED ON A SINGLE FIELD )
    //----------------------------------------------------------------------
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name="id", nullable=false)
    private Integer    id           ;


    //----------------------------------------------------------------------
    // ENTITY DATA FIELDS 
    //----------------------------------------------------------------------    
    @Column(name="ID_PERMISSION", nullable=false)
    private Integer    idPermission ;

    @Column(name="ID_CHANEL_USER", nullable=false)
    private Integer    idChanelUser ;



    //----------------------------------------------------------------------
    // ENTITY LINKS ( RELATIONSHIP )
    //----------------------------------------------------------------------

    //----------------------------------------------------------------------
    // CONSTRUCTOR(S)
    //----------------------------------------------------------------------
    public PermissionUsersChanel() {
		super();
    }
    
    //----------------------------------------------------------------------
    // GETTER & SETTER FOR THE KEY FIELD
    //----------------------------------------------------------------------
    public void setId( Integer id ) {
        this.id = id ;
    }
    public Integer getId() {
        return this.id;
    }

    //----------------------------------------------------------------------
    // GETTERS & SETTERS FOR FIELDS
    //----------------------------------------------------------------------
    //--- DATABASE MAPPING : ID_PERMISSION ( INT ) 
    public void setIdPermission( Integer idPermission ) {
        this.idPermission = idPermission;
    }
    public Integer getIdPermission() {
        return this.idPermission;
    }

    //--- DATABASE MAPPING : ID_CHANEL_USER ( INT ) 
    public void setIdChanelUser( Integer idChanelUser ) {
        this.idChanelUser = idChanelUser;
    }
    public Integer getIdChanelUser() {
        return this.idChanelUser;
    }


    //----------------------------------------------------------------------
    // GETTERS & SETTERS FOR LINKS
    //----------------------------------------------------------------------

    //----------------------------------------------------------------------
    // toString METHOD
    //----------------------------------------------------------------------
    public String toString() { 
        StringBuffer sb = new StringBuffer(); 
        sb.append("["); 
        sb.append(id);
        sb.append("]:"); 
        sb.append(idPermission);
        sb.append("|");
        sb.append(idChanelUser);
        return sb.toString(); 
    } 

}
