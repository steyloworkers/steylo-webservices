
/*
 * Java transformer for entity table sous_categorie_produit 
 * Created on dec ( Time 18:46:57 )
 * Generator tool : Telosys Tools Generator ( version 2.1.1 )
 */

package com.ino.steylo.business;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Date;
import java.util.Locale;
import java.util.Map;
import java.util.ArrayList;
import java.util.HashMap;

import javax.persistence.PersistenceContext;
import javax.persistence.EntityManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.dao.PermissionDeniedDataAccessException;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.ino.steylo.helper.dto.*;
import com.ino.steylo.helper.dto.transformer.*;
import com.ino.steylo.helper.contrat.*;
import com.ino.steylo.dao.entity.*;
import com.ino.steylo.helper.*;
import com.ino.steylo.dao.repository.*;

/**
BUSINESS for table "sous_categorie_produit"
 * 
 * @author ino
 *
 */
@Component
public class SousCategorieProduitBusiness implements IBasicBusiness<Request, Response> {


	private Response response;
	
	@Autowired
	private SousCategorieProduitRepository sousCategorieProduitRepository;
	@Autowired
	private CategorieProduitRepository categorieProduitRepository;
	
	@Autowired
	private FunctionalError functionalError;
	
	@Autowired
	private TechnicalError technicalError;

	@Autowired
	private ExceptionUtils exceptionUtils;
	
	@PersistenceContext
	private EntityManager em;
	
	private Logger slf4jLogger;

	private SimpleDateFormat dateFormat;

	public SousCategorieProduitBusiness() {
		dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
		slf4jLogger = LoggerFactory.getLogger(getClass());
	}

	
	/**
	 * create SousCategorieProduit by using SousCategorieProduitDto as object.
	 * 
	 * @param request
	 * @return response
	 * 
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	@Override
	public Response create(Request request, Locale locale)  {
		slf4jLogger.info("----begin create-----");
		
		response = new Response();
		
		try {
			List<SousCategorieProduit> items = new ArrayList<SousCategorieProduit>();
			
			for (SousCategorieProduitDto dto : request.getDatasSousCategorieProduit()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				fieldsToVerify.put("idCategorie", dto.getIdCategorie());
				fieldsToVerify.put("libelle", dto.getLibelle());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				// Verify if sousCategorieProduit to insert do not exist
				SousCategorieProduit existingEntity = null;
				//TODO: add/replace by the best method
				if (existingEntity != null) {
					response.setStatus(functionalError.DATA_EXIST("sousCategorieProduit -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}

				// Verify if categorieProduit exist
				CategorieProduit existingCategorieProduit = categorieProduitRepository.findOne(dto.getIdCategorie());
				if (existingCategorieProduit == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("categorieProduit -> " + dto.getIdCategorie(), locale));
					response.setHasError(true);
					return response;
				}
				SousCategorieProduit entityToSave = null;
				entityToSave = SousCategorieProduitTransformer.INSTANCE.toEntity(dto, existingCategorieProduit);
				items.add(entityToSave);
			}

			if (!items.isEmpty()) {
				List<SousCategorieProduit> itemsSaved = null;
				// inserer les donnees en base de donnees
				itemsSaved = sousCategorieProduitRepository.save((Iterable<SousCategorieProduit>) items);
				if (itemsSaved == null) {
					response.setStatus(functionalError.SAVE_FAIL("sousCategorieProduit", locale));
					response.setHasError(true);
					return response;
				}
				List<SousCategorieProduitDto> itemsDto = new ArrayList<SousCategorieProduitDto>();
				for (SousCategorieProduit entity : itemsSaved) {
					SousCategorieProduitDto dto = getFullInfos(entity, locale);
					if (dto == null) continue;
					itemsDto.add(dto);
				}
				response.setItemsSousCategorieProduit(itemsDto);
				response.setHasError(false);
			}

			slf4jLogger.info("----end-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}

	/**
	 * update SousCategorieProduit by using SousCategorieProduitDto as object.
	 * 
	 * @param request
	 * @return response
	 * 
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	@Override
	public Response update(Request request, Locale locale)  {
		slf4jLogger.info("----begin update-----");
		
		response = new Response();
		
		try {
			List<SousCategorieProduit> items = new ArrayList<SousCategorieProduit>();
			
			for (SousCategorieProduitDto dto : request.getDatasSousCategorieProduit()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				fieldsToVerify.put("id", dto.getId());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				// Verifier si la sousCategorieProduit existe
				SousCategorieProduit entityToSave = null;
				entityToSave = sousCategorieProduitRepository.findById(dto.getId());
				if (entityToSave == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("sousCategorieProduit -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}

				// Verify if categorieProduit exist
				if (dto.getIdCategorie() != null && dto.getIdCategorie() > 0){
					CategorieProduit existingCategorieProduit = categorieProduitRepository.findOne(dto.getIdCategorie());
					if (existingCategorieProduit == null) {
						response.setStatus(functionalError.DATA_NOT_EXIST("categorieProduit -> " + dto.getIdCategorie(), locale));
						response.setHasError(true);
						return response;
					}
					entityToSave.setCategorieProduit(existingCategorieProduit);
				}
				if (dto.getLibelle()!= null && !dto.getLibelle().isEmpty()) {
					entityToSave.setLibelle(dto.getLibelle());
				}
				items.add(entityToSave);
			}

			if (!items.isEmpty()) {
				List<SousCategorieProduit> itemsSaved = null;
				// maj les donnees en base
				itemsSaved = sousCategorieProduitRepository.save((Iterable<SousCategorieProduit>) items);
				if (itemsSaved == null) {
					response.setStatus(functionalError.SAVE_FAIL("sousCategorieProduit", locale));
					response.setHasError(true);
					return response;
				}
				List<SousCategorieProduitDto> itemsDto = new ArrayList<SousCategorieProduitDto>();
				for (SousCategorieProduit entity : itemsSaved) {
					SousCategorieProduitDto dto = getFullInfos(entity, locale);
					if (dto == null) continue;
					itemsDto.add(dto);
				}
				response.setItemsSousCategorieProduit(itemsDto);
				response.setHasError(false);
			}

			slf4jLogger.info("----end-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}

	/**
	 * delete SousCategorieProduit by using SousCategorieProduitDto as object.
	 * 
	 * @param request
	 * @return response
	 * 
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	@Override
	public Response delete(Request request, Locale locale)  {
		slf4jLogger.info("----begin delete-----");
		
		response = new Response();
		
		try {
			List<SousCategorieProduit> items = new ArrayList<SousCategorieProduit>();
			
			for (SousCategorieProduitDto dto : request.getDatasSousCategorieProduit()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				fieldsToVerify.put("id", dto.getId());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				// Verifier si la sousCategorieProduit existe
				SousCategorieProduit existingEntity = null;
				existingEntity = sousCategorieProduitRepository.findById(dto.getId());
				if (existingEntity == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("sousCategorieProduit -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}

				// --------------------------------------------------------------------------------------------
				// --------------------------------- CHECK IF DATA IS USED
				// --------------------------------------------------------------------------------------------



				items.add(existingEntity);
			}

			if (!items.isEmpty()) {
				// supprimer les donnees en base
				sousCategorieProduitRepository.delete((Iterable<SousCategorieProduit>) items);

				response.setHasError(false);
			}

			slf4jLogger.info("----end-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}

	/**
	 * get SousCategorieProduit by using SousCategorieProduitDto as object.
	 * 
	 * @param request
	 * @return response
	 * 
	 */
	@SuppressWarnings("unused")
	@Override
	public Response getByCriteria(Request request, Locale locale) {
		slf4jLogger.info("----begin-----");
		
		response = new Response();
		
		try {
			List<SousCategorieProduit> items = null;
			items = sousCategorieProduitRepository.getByCriteria(request, em);
			if (items != null && !items.isEmpty()) {
				List<SousCategorieProduitDto> itemsDto = new ArrayList<SousCategorieProduitDto>();
				for (SousCategorieProduit entity : items) {
					SousCategorieProduitDto dto = getFullInfos(entity, locale);
					if (dto == null) continue;
					itemsDto.add(dto);
				}
				response.setItemsSousCategorieProduit(itemsDto);
				response.setCount(sousCategorieProduitRepository.count(request, em));
				response.setHasError(false);
			} else {
				response.setStatus(functionalError.DATA_EMPTY("sousCategorieProduit", locale));
				response.setHasError(false);
				return response;
			}

			slf4jLogger.info("----end-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}

	/**
	 * get full SousCategorieProduitDto by using SousCategorieProduit as object.
	 * 
	 * @param entity, locale
	 * @return SousCategorieProduitDto
	 * 
	 */
	private SousCategorieProduitDto getFullInfos(SousCategorieProduit entity, Locale locale) throws Exception {
		SousCategorieProduitDto dto = SousCategorieProduitTransformer.INSTANCE.toDto(entity);
		if (dto == null){
			return null;
		}

		return dto;
	}
}
