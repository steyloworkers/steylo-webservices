
/*
 * Java transformer for entity table chanel_permissions 
 * Created on dec ( Time 18:46:54 )
 * Generator tool : Telosys Tools Generator ( version 2.1.1 )
 */

package com.ino.steylo.business;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Date;
import java.util.Locale;
import java.util.Map;
import java.util.ArrayList;
import java.util.HashMap;

import javax.persistence.PersistenceContext;
import javax.persistence.EntityManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.dao.PermissionDeniedDataAccessException;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.ino.steylo.helper.dto.*;
import com.ino.steylo.helper.dto.transformer.*;
import com.ino.steylo.helper.contrat.*;
import com.ino.steylo.dao.entity.*;
import com.ino.steylo.helper.*;
import com.ino.steylo.dao.repository.*;

/**
BUSINESS for table "chanel_permissions"
 * 
 * @author ino
 *
 */
@Component
public class ChanelPermissionsBusiness implements IBasicBusiness<Request, Response> {


	private Response response;
	
	@Autowired
	private ChanelPermissionsRepository chanelPermissionsRepository;
	@Autowired
	private ChanelUserPermissionsRepository chanelUserPermissionsRepository;
	@Autowired
	private PermissionActionsRepository permissionActionsRepository;
	
	@Autowired
	private FunctionalError functionalError;
	
	@Autowired
	private TechnicalError technicalError;

	@Autowired
	private ExceptionUtils exceptionUtils;
	
	@PersistenceContext
	private EntityManager em;
	
	private Logger slf4jLogger;

	private SimpleDateFormat dateFormat;

	public ChanelPermissionsBusiness() {
		dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
		slf4jLogger = LoggerFactory.getLogger(getClass());
	}

	
	/**
	 * create ChanelPermissions by using ChanelPermissionsDto as object.
	 * 
	 * @param request
	 * @return response
	 * 
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	@Override
	public Response create(Request request, Locale locale)  {
		slf4jLogger.info("----begin create-----");
		
		response = new Response();
		
		try {
			List<ChanelPermissions> items = new ArrayList<ChanelPermissions>();
			
			for (ChanelPermissionsDto dto : request.getDatasChanelPermissions()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				fieldsToVerify.put("code", dto.getCode());
				fieldsToVerify.put("libelle", dto.getLibelle());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				// Verify if chanelPermissions to insert do not exist
				ChanelPermissions existingEntity = null;
				//TODO: add/replace by the best method
				if (existingEntity != null) {
					response.setStatus(functionalError.DATA_EXIST("chanelPermissions -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}
				existingEntity = chanelPermissionsRepository.findByCode(dto.getCode());
				if (existingEntity != null) {
					response.setStatus(functionalError.DATA_EXIST("chanelPermissions -> " + dto.getCode(), locale));
					response.setHasError(true);
					return response;
				}

				ChanelPermissions entityToSave = null;
				entityToSave = ChanelPermissionsTransformer.INSTANCE.toEntity(dto);
				items.add(entityToSave);
			}

			if (!items.isEmpty()) {
				List<ChanelPermissions> itemsSaved = null;
				// inserer les donnees en base de donnees
				itemsSaved = chanelPermissionsRepository.save((Iterable<ChanelPermissions>) items);
				if (itemsSaved == null) {
					response.setStatus(functionalError.SAVE_FAIL("chanelPermissions", locale));
					response.setHasError(true);
					return response;
				}
				List<ChanelPermissionsDto> itemsDto = new ArrayList<ChanelPermissionsDto>();
				for (ChanelPermissions entity : itemsSaved) {
					ChanelPermissionsDto dto = getFullInfos(entity, locale);
					if (dto == null) continue;
					itemsDto.add(dto);
				}
				response.setItemsChanelPermissions(itemsDto);
				response.setHasError(false);
			}

			slf4jLogger.info("----end-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}

	/**
	 * update ChanelPermissions by using ChanelPermissionsDto as object.
	 * 
	 * @param request
	 * @return response
	 * 
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	@Override
	public Response update(Request request, Locale locale)  {
		slf4jLogger.info("----begin update-----");
		
		response = new Response();
		
		try {
			List<ChanelPermissions> items = new ArrayList<ChanelPermissions>();
			
			for (ChanelPermissionsDto dto : request.getDatasChanelPermissions()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				fieldsToVerify.put("id", dto.getId());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				// Verifier si la chanelPermissions existe
				ChanelPermissions entityToSave = null;
				entityToSave = chanelPermissionsRepository.findById(dto.getId());
				if (entityToSave == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("chanelPermissions -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}

				if (dto.getCode()!= null && dto.getCode() > 0) {
					entityToSave.setCode(dto.getCode());
				}
				if (dto.getLibelle()!= null && !dto.getLibelle().isEmpty()) {
					entityToSave.setLibelle(dto.getLibelle());
				}
				items.add(entityToSave);
			}

			if (!items.isEmpty()) {
				List<ChanelPermissions> itemsSaved = null;
				// maj les donnees en base
				itemsSaved = chanelPermissionsRepository.save((Iterable<ChanelPermissions>) items);
				if (itemsSaved == null) {
					response.setStatus(functionalError.SAVE_FAIL("chanelPermissions", locale));
					response.setHasError(true);
					return response;
				}
				List<ChanelPermissionsDto> itemsDto = new ArrayList<ChanelPermissionsDto>();
				for (ChanelPermissions entity : itemsSaved) {
					ChanelPermissionsDto dto = getFullInfos(entity, locale);
					if (dto == null) continue;
					itemsDto.add(dto);
				}
				response.setItemsChanelPermissions(itemsDto);
				response.setHasError(false);
			}

			slf4jLogger.info("----end-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}

	/**
	 * delete ChanelPermissions by using ChanelPermissionsDto as object.
	 * 
	 * @param request
	 * @return response
	 * 
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	@Override
	public Response delete(Request request, Locale locale)  {
		slf4jLogger.info("----begin delete-----");
		
		response = new Response();
		
		try {
			List<ChanelPermissions> items = new ArrayList<ChanelPermissions>();
			
			for (ChanelPermissionsDto dto : request.getDatasChanelPermissions()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				fieldsToVerify.put("id", dto.getId());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				// Verifier si la chanelPermissions existe
				ChanelPermissions existingEntity = null;
				existingEntity = chanelPermissionsRepository.findById(dto.getId());
				if (existingEntity == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("chanelPermissions -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}

				// --------------------------------------------------------------------------------------------
				// --------------------------------- CHECK IF DATA IS USED
				// --------------------------------------------------------------------------------------------

				// chanelUserPermissions
				List<ChanelUserPermissions> listOfChanelUserPermissions = chanelUserPermissionsRepository.findByIdPermission(existingEntity.getId());
				if (listOfChanelUserPermissions != null && !listOfChanelUserPermissions.isEmpty()){
					response.setStatus(functionalError.DATA_NOT_DELETABLE("(" + listOfChanelUserPermissions.size() + ")", locale));
					response.setHasError(true);
					return response;
				}
				// permissionActions
				List<PermissionActions> listOfPermissionActions = permissionActionsRepository.findByIdPermission(existingEntity.getId());
				if (listOfPermissionActions != null && !listOfPermissionActions.isEmpty()){
					response.setStatus(functionalError.DATA_NOT_DELETABLE("(" + listOfPermissionActions.size() + ")", locale));
					response.setHasError(true);
					return response;
				}


				items.add(existingEntity);
			}

			if (!items.isEmpty()) {
				// supprimer les donnees en base
				chanelPermissionsRepository.delete((Iterable<ChanelPermissions>) items);

				response.setHasError(false);
			}

			slf4jLogger.info("----end-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}

	/**
	 * get ChanelPermissions by using ChanelPermissionsDto as object.
	 * 
	 * @param request
	 * @return response
	 * 
	 */
	@SuppressWarnings("unused")
	@Override
	public Response getByCriteria(Request request, Locale locale) {
		slf4jLogger.info("----begin-----");
		
		response = new Response();
		
		try {
			List<ChanelPermissions> items = null;
			items = chanelPermissionsRepository.getByCriteria(request, em);
			if (items != null && !items.isEmpty()) {
				List<ChanelPermissionsDto> itemsDto = new ArrayList<ChanelPermissionsDto>();
				for (ChanelPermissions entity : items) {
					ChanelPermissionsDto dto = getFullInfos(entity, locale);
					if (dto == null) continue;
					itemsDto.add(dto);
				}
				response.setItemsChanelPermissions(itemsDto);
				response.setCount(chanelPermissionsRepository.count(request, em));
				response.setHasError(false);
			} else {
				response.setStatus(functionalError.DATA_EMPTY("chanelPermissions", locale));
				response.setHasError(false);
				return response;
			}

			slf4jLogger.info("----end-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}

	/**
	 * get full ChanelPermissionsDto by using ChanelPermissions as object.
	 * 
	 * @param entity, locale
	 * @return ChanelPermissionsDto
	 * 
	 */
	private ChanelPermissionsDto getFullInfos(ChanelPermissions entity, Locale locale) throws Exception {
		ChanelPermissionsDto dto = ChanelPermissionsTransformer.INSTANCE.toDto(entity);
		if (dto == null){
			return null;
		}

		return dto;
	}
}
