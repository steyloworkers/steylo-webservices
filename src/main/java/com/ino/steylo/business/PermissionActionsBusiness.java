
/*
 * Java transformer for entity table permission_actions 
 * Created on dec ( Time 18:46:56 )
 * Generator tool : Telosys Tools Generator ( version 2.1.1 )
 */

package com.ino.steylo.business;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Date;
import java.util.Locale;
import java.util.Map;
import java.util.ArrayList;
import java.util.HashMap;

import javax.persistence.PersistenceContext;
import javax.persistence.EntityManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.dao.PermissionDeniedDataAccessException;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.ino.steylo.helper.dto.*;
import com.ino.steylo.helper.dto.transformer.*;
import com.ino.steylo.helper.contrat.*;
import com.ino.steylo.dao.entity.*;
import com.ino.steylo.helper.*;
import com.ino.steylo.dao.repository.*;

/**
BUSINESS for table "permission_actions"
 * 
 * @author ino
 *
 */
@Component
public class PermissionActionsBusiness implements IBasicBusiness<Request, Response> {


	private Response response;
	
	@Autowired
	private PermissionActionsRepository permissionActionsRepository;
	@Autowired
	private ChanelPermissionsRepository chanelPermissionsRepository;
	
	@Autowired
	private FunctionalError functionalError;
	
	@Autowired
	private TechnicalError technicalError;

	@Autowired
	private ExceptionUtils exceptionUtils;
	
	@PersistenceContext
	private EntityManager em;
	
	private Logger slf4jLogger;

	private SimpleDateFormat dateFormat;

	public PermissionActionsBusiness() {
		dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
		slf4jLogger = LoggerFactory.getLogger(getClass());
	}

	
	/**
	 * create PermissionActions by using PermissionActionsDto as object.
	 * 
	 * @param request
	 * @return response
	 * 
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	@Override
	public Response create(Request request, Locale locale)  {
		slf4jLogger.info("----begin create-----");
		
		response = new Response();
		
		try {
			List<PermissionActions> items = new ArrayList<PermissionActions>();
			
			for (PermissionActionsDto dto : request.getDatasPermissionActions()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				fieldsToVerify.put("idPermission", dto.getIdPermission());
				fieldsToVerify.put("idActionProduit", dto.getIdActionProduit());
				fieldsToVerify.put("idActionAbonne", dto.getIdActionAbonne());
				fieldsToVerify.put("idActionParam", dto.getIdActionParam());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				// Verify if permissionActions to insert do not exist
				PermissionActions existingEntity = null;
				//TODO: add/replace by the best method
				if (existingEntity != null) {
					response.setStatus(functionalError.DATA_EXIST("permissionActions -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}

				// Verify if chanelPermissions exist
				ChanelPermissions existingChanelPermissions = chanelPermissionsRepository.findOne(dto.getIdPermission());
				if (existingChanelPermissions == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("chanelPermissions -> " + dto.getIdPermission(), locale));
					response.setHasError(true);
					return response;
				}
				PermissionActions entityToSave = null;
				entityToSave = PermissionActionsTransformer.INSTANCE.toEntity(dto, existingChanelPermissions);
				items.add(entityToSave);
			}

			if (!items.isEmpty()) {
				List<PermissionActions> itemsSaved = null;
				// inserer les donnees en base de donnees
				itemsSaved = permissionActionsRepository.save((Iterable<PermissionActions>) items);
				if (itemsSaved == null) {
					response.setStatus(functionalError.SAVE_FAIL("permissionActions", locale));
					response.setHasError(true);
					return response;
				}
				List<PermissionActionsDto> itemsDto = new ArrayList<PermissionActionsDto>();
				for (PermissionActions entity : itemsSaved) {
					PermissionActionsDto dto = getFullInfos(entity, locale);
					if (dto == null) continue;
					itemsDto.add(dto);
				}
				response.setItemsPermissionActions(itemsDto);
				response.setHasError(false);
			}

			slf4jLogger.info("----end-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}

	/**
	 * update PermissionActions by using PermissionActionsDto as object.
	 * 
	 * @param request
	 * @return response
	 * 
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	@Override
	public Response update(Request request, Locale locale)  {
		slf4jLogger.info("----begin update-----");
		
		response = new Response();
		
		try {
			List<PermissionActions> items = new ArrayList<PermissionActions>();
			
			for (PermissionActionsDto dto : request.getDatasPermissionActions()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				fieldsToVerify.put("id", dto.getId());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				// Verifier si la permissionActions existe
				PermissionActions entityToSave = null;
				entityToSave = permissionActionsRepository.findById(dto.getId());
				if (entityToSave == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("permissionActions -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}

				// Verify if chanelPermissions exist
				if (dto.getIdPermission() != null && dto.getIdPermission() > 0){
					ChanelPermissions existingChanelPermissions = chanelPermissionsRepository.findOne(dto.getIdPermission());
					if (existingChanelPermissions == null) {
						response.setStatus(functionalError.DATA_NOT_EXIST("chanelPermissions -> " + dto.getIdPermission(), locale));
						response.setHasError(true);
						return response;
					}
					entityToSave.setChanelPermissions(existingChanelPermissions);
				}
				if (dto.getIdActionProduit()!= null && dto.getIdActionProduit() > 0) {
					entityToSave.setIdActionProduit(dto.getIdActionProduit());
				}
				if (dto.getIdActionAbonne()!= null && dto.getIdActionAbonne() > 0) {
					entityToSave.setIdActionAbonne(dto.getIdActionAbonne());
				}
				if (dto.getIdActionParam()!= null && dto.getIdActionParam() > 0) {
					entityToSave.setIdActionParam(dto.getIdActionParam());
				}
				items.add(entityToSave);
			}

			if (!items.isEmpty()) {
				List<PermissionActions> itemsSaved = null;
				// maj les donnees en base
				itemsSaved = permissionActionsRepository.save((Iterable<PermissionActions>) items);
				if (itemsSaved == null) {
					response.setStatus(functionalError.SAVE_FAIL("permissionActions", locale));
					response.setHasError(true);
					return response;
				}
				List<PermissionActionsDto> itemsDto = new ArrayList<PermissionActionsDto>();
				for (PermissionActions entity : itemsSaved) {
					PermissionActionsDto dto = getFullInfos(entity, locale);
					if (dto == null) continue;
					itemsDto.add(dto);
				}
				response.setItemsPermissionActions(itemsDto);
				response.setHasError(false);
			}

			slf4jLogger.info("----end-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}

	/**
	 * delete PermissionActions by using PermissionActionsDto as object.
	 * 
	 * @param request
	 * @return response
	 * 
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	@Override
	public Response delete(Request request, Locale locale)  {
		slf4jLogger.info("----begin delete-----");
		
		response = new Response();
		
		try {
			List<PermissionActions> items = new ArrayList<PermissionActions>();
			
			for (PermissionActionsDto dto : request.getDatasPermissionActions()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				fieldsToVerify.put("id", dto.getId());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				// Verifier si la permissionActions existe
				PermissionActions existingEntity = null;
				existingEntity = permissionActionsRepository.findById(dto.getId());
				if (existingEntity == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("permissionActions -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}

				// --------------------------------------------------------------------------------------------
				// --------------------------------- CHECK IF DATA IS USED
				// --------------------------------------------------------------------------------------------



				items.add(existingEntity);
			}

			if (!items.isEmpty()) {
				// supprimer les donnees en base
				permissionActionsRepository.delete((Iterable<PermissionActions>) items);

				response.setHasError(false);
			}

			slf4jLogger.info("----end-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}

	/**
	 * get PermissionActions by using PermissionActionsDto as object.
	 * 
	 * @param request
	 * @return response
	 * 
	 */
	@SuppressWarnings("unused")
	@Override
	public Response getByCriteria(Request request, Locale locale) {
		slf4jLogger.info("----begin-----");
		
		response = new Response();
		
		try {
			List<PermissionActions> items = null;
			items = permissionActionsRepository.getByCriteria(request, em);
			if (items != null && !items.isEmpty()) {
				List<PermissionActionsDto> itemsDto = new ArrayList<PermissionActionsDto>();
				for (PermissionActions entity : items) {
					PermissionActionsDto dto = getFullInfos(entity, locale);
					if (dto == null) continue;
					itemsDto.add(dto);
				}
				response.setItemsPermissionActions(itemsDto);
				response.setCount(permissionActionsRepository.count(request, em));
				response.setHasError(false);
			} else {
				response.setStatus(functionalError.DATA_EMPTY("permissionActions", locale));
				response.setHasError(false);
				return response;
			}

			slf4jLogger.info("----end-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}

	/**
	 * get full PermissionActionsDto by using PermissionActions as object.
	 * 
	 * @param entity, locale
	 * @return PermissionActionsDto
	 * 
	 */
	private PermissionActionsDto getFullInfos(PermissionActions entity, Locale locale) throws Exception {
		PermissionActionsDto dto = PermissionActionsTransformer.INSTANCE.toDto(entity);
		if (dto == null){
			return null;
		}

		return dto;
	}
}
