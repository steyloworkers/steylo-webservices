
/*
 * Java transformer for entity table produit 
 * Created on dec ( Time 18:46:57 )
 * Generator tool : Telosys Tools Generator ( version 2.1.1 )
 */

package com.ino.steylo.business;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Date;
import java.util.Locale;
import java.util.Map;
import java.util.ArrayList;
import java.util.HashMap;

import javax.persistence.PersistenceContext;
import javax.persistence.EntityManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.dao.PermissionDeniedDataAccessException;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.ino.steylo.helper.dto.*;
import com.ino.steylo.helper.dto.transformer.*;
import com.ino.steylo.helper.contrat.*;
import com.ino.steylo.dao.entity.*;
import com.ino.steylo.helper.*;
import com.ino.steylo.dao.repository.*;

/**
BUSINESS for table "produit"
 * 
 * @author ino
 *
 */
@Component
public class ProduitBusiness implements IBasicBusiness<Request, Response> {


	private Response response;
	
	@Autowired
	private ProduitRepository produitRepository;
	@Autowired
	private ImageProduitRepository imageProduitRepository;
	@Autowired
	private ChanelRepository chanelRepository;
	@Autowired
	private CategorieProduitRepository categorieProduitRepository;
	
	@Autowired
	private FunctionalError functionalError;
	
	@Autowired
	private TechnicalError technicalError;

	@Autowired
	private ExceptionUtils exceptionUtils;
	
	@PersistenceContext
	private EntityManager em;
	
	private Logger slf4jLogger;

	private SimpleDateFormat dateFormat;

	public ProduitBusiness() {
		dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
		slf4jLogger = LoggerFactory.getLogger(getClass());
	}

	
	/**
	 * create Produit by using ProduitDto as object.
	 * 
	 * @param request
	 * @return response
	 * 
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	@Override
	public Response create(Request request, Locale locale)  {
		slf4jLogger.info("----begin create-----");
		
		response = new Response();
		
		try {
			List<Produit> itemsProduit = new ArrayList<>();
			List<ImageProduit> itemsImgProduit = new ArrayList<>();

			for (ProduitDto dto : request.getDatasProduit()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<>();
				fieldsToVerify.put("libelle", dto.getLibelle());
				fieldsToVerify.put("referenceProduit", dto.getReferenceProduit());
				fieldsToVerify.put("description", dto.getDescription());
				fieldsToVerify.put("prixMin", dto.getPrixMin());
				fieldsToVerify.put("prixMax", dto.getPrixMax());
				fieldsToVerify.put("prix", dto.getPrix());
				fieldsToVerify.put("quantite", dto.getQuantite());
				//fieldsToVerify.put("dateCreation", dto.getDateCreation());
				//fieldsToVerify.put("status", dto.getStatus());
				//fieldsToVerify.put("trash", dto.getTrash());
				fieldsToVerify.put("couleur", dto.getCouleur());
				fieldsToVerify.put("poids", dto.getPoids());
				fieldsToVerify.put("size", dto.getSize());
				fieldsToVerify.put("idCategorie", dto.getIdCategorie());
				fieldsToVerify.put("idChanel", dto.getIdChanel());
				fieldsToVerify.put("idSousCategorie", dto.getIdSousCategorie());
				//fieldsToVerify.put("lastUpdate", dto.getLastUpdate());
				fieldsToVerify.put("idMarque", dto.getIdMarque());
				fieldsToVerify.put("listImageProduit", dto.getListImageProduit());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				// Verify if produit to insert do not exist
				Produit existingEntity = produitRepository.findByReferenceProduit(dto.getReferenceProduit());
				if (existingEntity != null) {
					response.setStatus(functionalError.DATA_EXIST("Le produit avec la référence " + dto.getReferenceProduit() + " existe déjà !", locale));
					response.setHasError(true);
					return response;
				}

				// Verify if chanel exist
				Chanel existingChanel = chanelRepository.findById(dto.getIdChanel());
				if (existingChanel == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("L'entreprise n'est pas reconnue - Enregistrement impossible", locale));
					response.setHasError(true);
					return response;
				}
				// Verify if categorieProduit exist
				CategorieProduit existingCategorieProduit = categorieProduitRepository.findOne(dto.getIdCategorie());
				if (existingCategorieProduit == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("Le type de catégorie spécifié n'existe pas !", locale));
					response.setHasError(true);
					return response;
				}

				//on spécifie qu'il a effectué une nouvelle action dans chanel
				existingChanel.setRowDateUpdate(dateFormat.parse(Utilities.getCurrentLocalDateTimeStamp()));
				if (chanelRepository.save(existingChanel) == null){
					response.setStatus(functionalError.SAVE_FAIL("Un problème est survenu !", locale));
					response.setHasError(true);
					return response;
				}

				dto.setDateCreation(Utilities.getCurrentLocalDateTimeStamp());
				dto.setLastUpdate(Utilities.getCurrentLocalDateTimeStamp());
				dto.setStatus(1); //Par défaut activé
				dto.setTrash(0);  //Pas encore supprimer
				Produit produitToSave = ProduitTransformer.INSTANCE.toEntity(dto, existingChanel, existingCategorieProduit);
				itemsProduit.add(produitToSave);

				//On gère maintenant ses images-------------------------------------------------
				for (ImageProduitDto dtoImgProduit : dto.getListImageProduit()) {
					// Definir les parametres obligatoires
					Map<String, Object> fieldsImgProduitToVerify = new HashMap<>();
					fieldsToVerify.put("image", dtoImgProduit.getImage());
					fieldsToVerify.put("type", dtoImgProduit.getType());
					fieldsToVerify.put("extImg", dtoImgProduit.getExtImg());
					if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
						response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
						response.setHasError(true);
						return response;
					}

					ImageProduit imgProduitToSave = ImageProduitTransformer.INSTANCE.toEntity(dtoImgProduit, produitToSave);
					itemsImgProduit.add(imgProduitToSave);
				} //End for imgProduit
			}//End for Produit

			//Enregistrement du produit
			if (!itemsProduit.isEmpty()) {
				List<Produit> itemsProduitSaved = produitRepository.save(itemsProduit);
				if (itemsProduitSaved == null) {
					response.setStatus(functionalError.SAVE_FAIL("Impossible d'enregistrer le produit !", locale));
					response.setHasError(true);
					return response;
				}

				//Enregistrement des images
				if (!itemsImgProduit.isEmpty()) {
					List<ImageProduit> itemsImgProduitSaved = imageProduitRepository.save(itemsImgProduit);
					if (itemsImgProduitSaved == null) {
						response.setStatus(functionalError.SAVE_FAIL("Problème survenu avec les images du produit !", locale));
						response.setHasError(true);
						return response;
					}
				}

				List<ProduitDto> itemsDto = new ArrayList<>();
				for (Produit entity : itemsProduitSaved) {
					ProduitDto dto = getFullInfos(entity, locale);
					if (dto == null) continue;
					itemsDto.add(dto);
				}
				response.setItemsProduit(itemsDto);
				response.setHasError(false);
			}

			slf4jLogger.info("----end-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}

	/**
	 * update Produit by using ProduitDto as object.
	 * 
	 * @param request
	 * @return response
	 * 
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	@Override
	public Response update(Request request, Locale locale)  {
		slf4jLogger.info("----begin update-----");
		
		response = new Response();
		
		try {
			List<Produit> items = new ArrayList<>();
			Chanel existingChanel = new Chanel();
			
			for (ProduitDto dto : request.getDatasProduit()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<>();
				fieldsToVerify.put("id", dto.getId());
				fieldsToVerify.put("idChanel", dto.getId());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				// Verifier si le produit existe
				Produit entityToSave = null;
				entityToSave = produitRepository.findById(dto.getId());
				if (entityToSave == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("Le produit à modifier est inexistant ! " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}

				// Verify if chanel exist
				if (dto.getIdChanel() != null && dto.getIdChanel() > 0){
					existingChanel = chanelRepository.findById(dto.getIdChanel());
					if (existingChanel == null) {
						response.setStatus(functionalError.DATA_NOT_EXIST("L'entreprise rattachée n'existe pas !", locale));
						response.setHasError(true);
						return response;
					}
					entityToSave.setChanel(existingChanel);
				}
				// Verify if categorieProduit exist
				if (dto.getIdCategorie() != null && dto.getIdCategorie() > 0){
					CategorieProduit existingCategorieProduit = categorieProduitRepository.findOne(dto.getIdCategorie());
					if (existingCategorieProduit == null) {
						response.setStatus(functionalError.DATA_NOT_EXIST("La catégorie sélectionnée n'existe pas !", locale));
						response.setHasError(true);
						return response;
					}
					entityToSave.setCategorieProduit(existingCategorieProduit);
				}
				if (dto.getLibelle()!= null && !dto.getLibelle().isEmpty()) {
					entityToSave.setLibelle(dto.getLibelle());
				}
				if (dto.getReferenceProduit()!= null && !dto.getReferenceProduit().isEmpty()) {
					entityToSave.setReferenceProduit(dto.getReferenceProduit());
				}
				if (dto.getDescription()!= null && !dto.getDescription().isEmpty()) {
					entityToSave.setDescription(dto.getDescription());
				}
				if (dto.getPrixMin()!= null && dto.getPrixMin() > 0) {
					entityToSave.setPrixMin(dto.getPrixMin());
				}
				if (dto.getPrixMax()!= null && dto.getPrixMax() > 0) {
					entityToSave.setPrixMax(dto.getPrixMax());
				}
				if (dto.getPrix()!= null && dto.getPrix() > 0) {
					entityToSave.setPrix(dto.getPrix());
				}
				if (dto.getQuantite()!= null && dto.getQuantite() > 0) {
					entityToSave.setQuantite(dto.getQuantite());
				}
				if (dto.getDateCreation()!= null && !dto.getDateCreation().isEmpty()) {
					entityToSave.setDateCreation(dateFormat.parse(dto.getDateCreation()));
				}
				if (dto.getStatus()!= null && dto.getStatus() >= 0) {
					entityToSave.setStatus(dto.getStatus());
				}
				if (dto.getTrash()!= null && dto.getTrash() >= 0) {
					entityToSave.setTrash(dto.getTrash());
				}
				if (dto.getCouleur()!= null && !dto.getCouleur().isEmpty()) {
					entityToSave.setCouleur(dto.getCouleur());
				}
				if (dto.getPoids()!= null && !dto.getPoids().isEmpty()) {
					entityToSave.setPoids(dto.getPoids());
				}
				if (dto.getSize()!= null && !dto.getSize().isEmpty()) {
					entityToSave.setSize(dto.getSize());
				}
				if (dto.getIdSousCategorie()!= null && dto.getIdSousCategorie() > 0) {
					entityToSave.setIdSousCategorie(dto.getIdSousCategorie());
				}
				if (dto.getLastUpdate()!= null && !dto.getLastUpdate().isEmpty()) {
					entityToSave.setLastUpdate(dateFormat.parse(dto.getLastUpdate()));
				}
				if (dto.getIdMarque()!= null && dto.getIdMarque() > 0) {
					entityToSave.setIdMarque(dto.getIdMarque());
				}

				//On spécifie dans la table chanel qu'il a effectué une modif
				existingChanel.setRowDateUpdate(dateFormat.parse(Utilities.getCurrentLocalDateTimeStamp()));
				Chanel updateChanel = chanelRepository.save(existingChanel);
				if (updateChanel == null){
					response.setStatus(functionalError.SAVE_FAIL("Le produit n'a pas pu être modifié !", locale));
					response.setHasError(true);
					return response;
				}
				entityToSave.setLastUpdate(dateFormat.parse(Utilities.getCurrentLocalDateTimeStamp()));
				items.add(entityToSave);
			}

			if (!items.isEmpty()) {
				List<Produit> itemsSaved = null;
				// maj les donnees en base
				itemsSaved = produitRepository.save(items);
				if (itemsSaved == null) {
					response.setStatus(functionalError.SAVE_FAIL("Le produit n'a pas pu être modifié !", locale));
					response.setHasError(true);
					return response;
				}
				List<ProduitDto> itemsDto = new ArrayList<ProduitDto>();
				for (Produit entity : itemsSaved) {
					ProduitDto dto = getFullInfos(entity, locale);
					if (dto == null) continue;
					itemsDto.add(dto);
				}
				response.setItemsProduit(itemsDto);
				response.setHasError(false);
			}

			slf4jLogger.info("----end-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}

	/**
	 * delete Produit by using ProduitDto as object.
	 * 
	 * @param request
	 * @return response
	 * 
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	@Override
	public Response delete(Request request, Locale locale)  {
		slf4jLogger.info("----begin delete-----");
		
		response = new Response();
		
		try {
			List<Produit> items = new ArrayList<Produit>();
			
			for (ProduitDto dto : request.getDatasProduit()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				fieldsToVerify.put("id", dto.getId());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				// Verifier si la produit existe
				Produit existingEntity = null;
				existingEntity = produitRepository.findById(dto.getId());
				if (existingEntity == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("produit -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}

				// --------------------------------------------------------------------------------------------
				// --------------------------------- CHECK IF DATA IS USED
				// --------------------------------------------------------------------------------------------

				// imageProduit
				List<ImageProduit> listOfImageProduit = imageProduitRepository.findByIdProduit(existingEntity.getId());
				if (listOfImageProduit != null && !listOfImageProduit.isEmpty()){
					response.setStatus(functionalError.DATA_NOT_DELETABLE("(" + listOfImageProduit.size() + ")", locale));
					response.setHasError(true);
					return response;
				}


				items.add(existingEntity);
			}

			if (!items.isEmpty()) {
				// supprimer les donnees en base
				produitRepository.delete((Iterable<Produit>) items);

				response.setHasError(false);
			}

			slf4jLogger.info("----end-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}

	/**
	 * get Produit by using ProduitDto as object.
	 * 
	 * @param request
	 * @return response
	 * 
	 */
	@SuppressWarnings("unused")
	@Override
	public Response getByCriteria(Request request, Locale locale) {
		slf4jLogger.info("----begin-----");
		
		response = new Response();
		
		try {
			List<Produit> items = null;
			items = produitRepository.getByCriteria(request, em);
			if (items != null && !items.isEmpty()) {
				List<ProduitDto> itemsDto = new ArrayList<ProduitDto>();
				for (Produit entity : items) {
					ProduitDto dto = getFullInfos(entity, locale);
					if (dto == null) continue;
					itemsDto.add(dto);
				}
				response.setItemsProduit(itemsDto);
				response.setCount(produitRepository.count(request, em));
				response.setHasError(false);
			} else {
				response.setStatus(functionalError.DATA_EMPTY("produit", locale));
				response.setHasError(false);
				response.setCount(0L);
				return response;
			}

			slf4jLogger.info("----end-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}

	/**
	 * get full ProduitDto by using Produit as object.
	 * 
	 * @param entity, locale
	 * @return ProduitDto
	 * 
	 */
	private ProduitDto getLessInfos(Produit entity, Locale locale) throws Exception {
		ProduitDto dto = ProduitTransformer.INSTANCE.toDto(entity);
		if (dto == null){
			return null;
		}

		return dto;
	}

	/**
	 * get full ProduitDto by using Produit as object.
	 *
	 * @param produit, locale
	 * @return ProduitDto
	 *
	 */
	private ProduitDto getFullInfos(Produit produit, Locale locale) throws Exception {

		ProduitDto produitDto = ProduitTransformer.INSTANCE.toDto(produit);

		//On récupère les images du produit
		List<ImageProduit> listImageProduitTemp = imageProduitRepository.findByIdProduit(produitDto.getId());
		List<ImageProduit> listImageProduit = new ArrayList<>();
		if (listImageProduitTemp != null && !listImageProduitTemp.isEmpty()){
			for(ImageProduit imgp : listImageProduitTemp){
				ImageProduit img = new ImageProduit();
				//System.out.println("rrrrrrrrrrrrrrrrrrrrrrrrr"+ imgp.getImage());
				switch (imgp.getType()){
					case "png":
						img.setImage("data:image/png;base64," + imgp.getImage());
						break;
					case "jpeg":
						img.setImage("data:image/jpeg;base64," + imgp.getImage());
						break;
					case "jpg":
						img.setImage("data:image/jpg;base64," + imgp.getImage());
						break;
					case "gif":
						img.setImage("data:image/gif;base64," + imgp.getImage());
						break;
				}
				img.setExtImg(imgp.getExtImg());
				img.setType(imgp.getType());
				img.setId(imgp.getId());
				//img.setProduit(produit);
				listImageProduit.add(img);
				break;	//Parce que je veux récupérer une seule image dans cette liste. Si je veux toutes les images, alors je commente "break"
			}
		}
		produitDto.setListImageProduit(ImageProduitTransformer.INSTANCE.toDtos(listImageProduit));

		return produitDto;
	}
}
