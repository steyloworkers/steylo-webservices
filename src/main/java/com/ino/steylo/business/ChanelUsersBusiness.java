
/*
 * Java transformer for entity table chanel_users 
 * Created on dec ( Time 18:46:55 )
 * Generator tool : Telosys Tools Generator ( version 2.1.1 )
 */

package com.ino.steylo.business;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Date;
import java.util.Locale;
import java.util.Map;
import java.util.ArrayList;
import java.util.HashMap;

import javax.persistence.PersistenceContext;
import javax.persistence.EntityManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.dao.PermissionDeniedDataAccessException;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.ino.steylo.helper.dto.*;
import com.ino.steylo.helper.dto.transformer.*;
import com.ino.steylo.helper.contrat.*;
import com.ino.steylo.dao.entity.*;
import com.ino.steylo.helper.*;
import com.ino.steylo.dao.repository.*;

/**
BUSINESS for table "chanel_users"
 * 
 * @author ino
 *
 */
@Component
public class ChanelUsersBusiness implements IBasicBusiness<Request, Response> {


	private Response response;
	
	private final ChanelUsersRepository chanelUsersRepository;
	private final ChanelUserPermissionsRepository chanelUserPermissionsRepository;
	private final ChanelRepository chanelRepository;
	
	private final FunctionalError functionalError;
	
	private final TechnicalError technicalError;

	private final ExceptionUtils exceptionUtils;
	
	@PersistenceContext
	private EntityManager em;
	
	private Logger slf4jLogger;

	private SimpleDateFormat dateFormat;

	@Autowired
	public ChanelUsersBusiness(ChanelUsersRepository chanelUsersRepository, ChanelUserPermissionsRepository chanelUserPermissionsRepository, ChanelRepository chanelRepository, FunctionalError functionalError, TechnicalError technicalError, ExceptionUtils exceptionUtils) {
		dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
		slf4jLogger = LoggerFactory.getLogger(getClass());
		this.chanelUsersRepository = chanelUsersRepository;
		this.chanelUserPermissionsRepository = chanelUserPermissionsRepository;
		this.chanelRepository = chanelRepository;
		this.functionalError = functionalError;
		this.technicalError = technicalError;
		this.exceptionUtils = exceptionUtils;
	}

	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	public Response changePass(Request request, Locale locale){
		slf4jLogger.info("----begin changePass-----");

		response = new Response();

		try {
			ChanelUsers existingEntity = null;

			ChanelUsersDto dto = request.getDataChanelUsers();
			// Definir les parametres obligatoires
			Map<String, Object> fieldsToVerify = new HashMap<>();
			fieldsToVerify.put("id", dto.getLogin());
			fieldsToVerify.put("login", dto.getLogin());
			fieldsToVerify.put("password", dto.getPassword());
			fieldsToVerify.put("oldPassword", dto.getOldPassword());
			if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			// Verifier si la chanelUsers existe
			ChanelUsers entityToSave = null;
			entityToSave = chanelUsersRepository.findById(dto.getId());
			if (entityToSave == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur inexistant !", locale));
				response.setHasError(true);
				return response;
			}
			// Verify if utilisateur exist
			existingEntity = chanelUsersRepository.findChanelUsersByLoginAndPassword(dto.getLogin(), Utilities.generateHash(dto.getOldPassword()));
			if (existingEntity == null) {
				response.setStatus(functionalError.CHECK_USER_FAIL(locale));
				response.setHasError(true);
				return response;
			}

			//C'est le new password
			if (dto.getPassword() != null && !dto.getPassword().isEmpty()) {
				entityToSave.setPassword(Utilities.generateHash(dto.getPassword()));
			}

			ChanelUsers itemsSaved = chanelUsersRepository.save(entityToSave);
			if (itemsSaved == null) {
				response.setStatus(functionalError.SAVE_FAIL("La modification du mot de passe a échoué !", locale));
				response.setHasError(true);
				return response;
			}

			response.setHasError(false);

			slf4jLogger.info("----end-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				//throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage() );
			}
		}
		return response;
	}

	/**
	 * create ChanelUsers by using ChanelUsersDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	public Response connexion(Request request, Locale locale)  {
		slf4jLogger.info("----begin create-----");

		response = new Response();

		try {
			List<ChanelUsers> items = new ArrayList<>();

			ChanelUsersDto dto = request.getDataChanelUsers();
			// Definir les parametres obligatoires
			Map<String, Object> fieldsToVerify = new HashMap<>();
			fieldsToVerify.put("login", dto.getLogin());
			fieldsToVerify.put("password", dto.getPassword());
			if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			// Verify if chanelUsers to insert do not exist
			ChanelUsers existingEntity = null;

			// Verify if utilisateur exist
			existingEntity = chanelUsersRepository.findChanelUsersByLoginAndPassword(dto.getLogin(), Utilities.generateHash(dto.getPassword()));
			if (existingEntity == null) {
				response.setStatus(functionalError.LOGIN_FAIL(locale));
				response.setHasError(true);
				return response;
			}

			/*/
			//on verifie qu'un autre n'est pas deja connecté
			if (existingEntity.getStatut() == 1){
				response.setStatus(functionalError.DISALLOWED_OPERATION("Un autre utilisateur est déjà connecté", locale));
				response.setHasError(true);
				return response;
			}

			//-----------------------------------------------------
			//on spécifie qu'il est connecté si son statut est à 0
			//-----------------------------------------------------
			existingEntity.setStatut(1);
			//*/
			ChanelUsersDto chanelUsersDto = getFullInfos(existingEntity, locale);
			response.setItemChanelUsers(chanelUsersDto);
			response.setHasError(false);

			slf4jLogger.info("----end-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}

	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	public Response deconnexion(Request request, Locale locale){
		slf4jLogger.info("----begin deconnexion-----");

		response = new Response();

		try {
			ChanelUsers existingEntity = null;

			ChanelUsersDto dto = request.getDataChanelUsers();
			// Definir les parametres obligatoires
			Map<String, Object> fieldsToVerify = new HashMap<>();
			fieldsToVerify.put("id", dto.getId());
			if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			// Verifier si l'utilisateur existe
			existingEntity = chanelUsersRepository.findById(dto.getId());
			if (existingEntity == null) {
				response.setStatus(functionalError.DATA_NOT_EXIST("Utilisateur: " + dto.getId(), locale));
				response.setHasError(true);
				return response;
			}

			//on verifie qu'il n'est pas deja deconnecté
			/*/
			if (existingEntity.getStatut() == 0){
				response.setStatus(functionalError.DISALLOWED_OPERATION("Utilisateur déjà deconnecté", locale));
				response.setHasError(true);
				return response;
			}

			//------------------
			//on le deconnecte
			//------------------
			existingEntity.setStatut(0);
			//*/
			ChanelUsers chanelUsersUpdated = chanelUsersRepository.save(existingEntity);
			if (chanelUsersUpdated == null) {
				response.setStatus(functionalError.SAVE_FAIL("Impossible de mettre à jour les informations", locale));
				response.setHasError(true);
				return response;
			}

			response.setHasError(false);

			slf4jLogger.info("----end-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				//throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage() );
			}
		}
		return response;
	}

	/**
	 * create ChanelUsers by using ChanelUsersDto as object.
	 * 
	 * @param request
	 * @return response
	 * 
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	@Override
	public Response create(Request request, Locale locale)  {
		slf4jLogger.info("----begin create-----");
		
		response = new Response();
		
		try {
			List<ChanelUsers> items = new ArrayList<ChanelUsers>();
			
			for (ChanelUsersDto dto : request.getDatasChanelUsers()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<>();
				//fieldsToVerify.put("nom", dto.getNom());
				//fieldsToVerify.put("prenoms", dto.getPrenoms());
				fieldsToVerify.put("login", dto.getLogin());
				fieldsToVerify.put("password", dto.getPassword());
				fieldsToVerify.put("email", dto.getEmail());
				//fieldsToVerify.put("statut", dto.getStatut());
				//fieldsToVerify.put("dateCreation", dto.getDateCreation());
				fieldsToVerify.put("idChanel", dto.getIdChanel());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				// Verify if chanelUsers to insert do not exist
				ChanelUsers existingEntity = chanelUsersRepository.findChanelUsersByLoginAndChanel_Id(dto.getLogin(), dto.getIdChanel());
				if (existingEntity != null) {
					response.setStatus(functionalError.DATA_EXIST("L'identifiant est déjà utilisé !", locale));
					response.setHasError(true);
					return response;
				}

				// Verify if chanel exist
				Chanel existingChanel = chanelRepository.findById(dto.getIdChanel());
				if (existingChanel == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("chanel -> " + dto.getIdChanel(), locale));
					response.setHasError(true);
					return response;
				}
				ChanelUsers entityToSave = null;
				dto.setPassword(Utilities.generateHash(dto.getPassword()));
				dto.setDateCreation(Utilities.getCurrentLocalDateTimeStamp());
				dto.setUpdatedAt(Utilities.getCurrentLocalDateTimeStamp());
				dto.setStatut(0);
				entityToSave = ChanelUsersTransformer.INSTANCE.toEntity(dto, existingChanel);
				items.add(entityToSave);
			}

			if (!items.isEmpty()) {
				List<ChanelUsers> itemsSaved = null;
				// inserer les donnees en base de donnees
				itemsSaved = chanelUsersRepository.save(items);
				if (itemsSaved == null) {
					response.setStatus(functionalError.SAVE_FAIL("chanelUsers", locale));
					response.setHasError(true);
					return response;
				}
				List<ChanelUsersDto> itemsDto = new ArrayList<ChanelUsersDto>();
				for (ChanelUsers entity : itemsSaved) {
					ChanelUsersDto dto = getFullInfos(entity, locale);
					if (dto == null) continue;
					itemsDto.add(dto);
				}
				response.setItemsChanelUsers(itemsDto);
				response.setHasError(false);
			}

			slf4jLogger.info("----end-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}

	/**
	 * update ChanelUsers by using ChanelUsersDto as object.
	 * 
	 * @param request
	 * @return response
	 * 
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	@Override
	public Response update(Request request, Locale locale)  {
		slf4jLogger.info("----begin update-----");
		
		response = new Response();
		
		try {
			List<ChanelUsers> items = new ArrayList<ChanelUsers>();
			
			for (ChanelUsersDto dto : request.getDatasChanelUsers()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<>();
				fieldsToVerify.put("id", dto.getId());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				// Verifier si la chanelUsers existe
				ChanelUsers entityToSave = null;
				entityToSave = chanelUsersRepository.findById(dto.getId());
				if (entityToSave == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("chanelUsers -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}

				// Verify if chanel exist
				if (dto.getIdChanel() != null && dto.getIdChanel() > 0){
					Chanel existingChanel = chanelRepository.findOne(dto.getIdChanel());
					if (existingChanel == null) {
						response.setStatus(functionalError.DATA_NOT_EXIST("chanel -> " + dto.getIdChanel(), locale));
						response.setHasError(true);
						return response;
					}
					entityToSave.setChanel(existingChanel);
				}
				if (dto.getNom()!= null && !dto.getNom().isEmpty()) {
					entityToSave.setNom(dto.getNom());
				}
				if (dto.getPrenoms()!= null && !dto.getPrenoms().isEmpty()) {
					entityToSave.setPrenoms(dto.getPrenoms());
				}
				if (dto.getLogin()!= null && !dto.getLogin().isEmpty()) {
					entityToSave.setLogin(dto.getLogin());
				}
				if (dto.getPassword()!= null && !dto.getPassword().isEmpty()) {
					entityToSave.setPassword(Utilities.generateHash(dto.getPassword()));
				}
				if (dto.getEmail()!= null && !dto.getEmail().isEmpty()) {
					entityToSave.setEmail(dto.getEmail());
				}
				if (dto.getStatut()!= null && dto.getStatut() > 0) {
					entityToSave.setStatut(dto.getStatut());
				}
				if (dto.getDateCreation()!= null && !dto.getDateCreation().isEmpty()) {
					entityToSave.setDateCreation(dateFormat.parse(dto.getDateCreation()));
				}
				entityToSave.setUpdatedAt(dateFormat.parse(Utilities.getCurrentLocalDateTimeStamp()));
				items.add(entityToSave);
			}

			if (!items.isEmpty()) {
				List<ChanelUsers> itemsSaved = null;
				// maj les donnees en base
				itemsSaved = chanelUsersRepository.save(items);
				if (itemsSaved == null) {
					response.setStatus(functionalError.SAVE_FAIL("La mise à jour a échoué !", locale));
					response.setHasError(true);
					return response;
				}
				List<ChanelUsersDto> itemsDto = new ArrayList<>();
				for (ChanelUsers entity : itemsSaved) {
					ChanelUsersDto dto = getFullInfos(entity, locale);
					if (dto == null) continue;
					itemsDto.add(dto);
				}
				response.setItemsChanelUsers(itemsDto);
				response.setHasError(false);
			}

			slf4jLogger.info("----end-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}

	/**
	 * delete ChanelUsers by using ChanelUsersDto as object.
	 * 
	 * @param request
	 * @return response
	 * 
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	@Override
	public Response delete(Request request, Locale locale)  {
		slf4jLogger.info("----begin delete-----");
		
		response = new Response();
		
		try {
			List<ChanelUsers> items = new ArrayList<ChanelUsers>();
			
			for (ChanelUsersDto dto : request.getDatasChanelUsers()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				fieldsToVerify.put("id", dto.getId());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				// Verifier si la chanelUsers existe
				ChanelUsers existingEntity = null;
				existingEntity = chanelUsersRepository.findById(dto.getId());
				if (existingEntity == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("chanelUsers -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}

				// --------------------------------------------------------------------------------------------
				// --------------------------------- CHECK IF DATA IS USED
				// --------------------------------------------------------------------------------------------

				// chanelUserPermissions
				List<ChanelUserPermissions> listOfChanelUserPermissions = chanelUserPermissionsRepository.findByIdUser(existingEntity.getId());
				if (listOfChanelUserPermissions != null && !listOfChanelUserPermissions.isEmpty()){
					response.setStatus(functionalError.DATA_NOT_DELETABLE("(" + listOfChanelUserPermissions.size() + ")", locale));
					response.setHasError(true);
					return response;
				}


				items.add(existingEntity);
			}

			if (!items.isEmpty()) {
				// supprimer les donnees en base
				chanelUsersRepository.delete((Iterable<ChanelUsers>) items);

				response.setHasError(false);
			}

			slf4jLogger.info("----end-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}

	/**
	 * get ChanelUsers by using ChanelUsersDto as object.
	 * 
	 * @param request
	 * @return response
	 * 
	 */
	@SuppressWarnings("unused")
	@Override
	public Response getByCriteria(Request request, Locale locale) {
		slf4jLogger.info("----begin-----");
		
		response = new Response();
		
		try {
			List<ChanelUsers> items = null;
			items = chanelUsersRepository.getByCriteria(request, em);
			if (items != null && !items.isEmpty()) {
				List<ChanelUsersDto> itemsDto = new ArrayList<ChanelUsersDto>();
				for (ChanelUsers entity : items) {
					ChanelUsersDto dto = getFullInfos(entity, locale);
					if (dto == null) continue;
					itemsDto.add(dto);
				}
				response.setItemsChanelUsers(itemsDto);
				response.setCount(chanelUsersRepository.count(request, em));
				response.setHasError(false);
			} else {
				response.setStatus(functionalError.DATA_EMPTY("chanelUsers", locale));
				response.setHasError(false);
				return response;
			}

			slf4jLogger.info("----end-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}

	/**
	 * get full ChanelUsersDto by using ChanelUsers as object.
	 * 
	 * @param entity, locale
	 * @return ChanelUsersDto
	 * 
	 */
	private ChanelUsersDto getFullInfos(ChanelUsers entity, Locale locale) throws Exception {
		ChanelUsersDto dto = ChanelUsersTransformer.INSTANCE.toDto(entity);
		if (dto == null){
			return null;
		}

		return dto;
	}
}
