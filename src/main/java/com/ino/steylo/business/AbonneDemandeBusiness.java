
/*
 * Java transformer for entity table abonne_demande 
 * Created on dec ( Time 23:22:14 )
 * Generator tool : Telosys Tools Generator ( version 2.1.1 )
 */

package com.ino.steylo.business;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Date;
import java.util.Locale;
import java.util.Map;
import java.util.ArrayList;
import java.util.HashMap;

import javax.persistence.PersistenceContext;
import javax.persistence.EntityManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.dao.PermissionDeniedDataAccessException;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.ino.steylo.helper.dto.*;
import com.ino.steylo.helper.dto.transformer.*;
import com.ino.steylo.helper.contrat.*;
import com.ino.steylo.dao.entity.*;
import com.ino.steylo.helper.*;
import com.ino.steylo.dao.repository.*;

/**
BUSINESS for table "abonne_demande"
 * 
 * @author ino
 *
 */
@Component
public class AbonneDemandeBusiness implements IBasicBusiness<Request, Response> {


	private Response response;
	
	@Autowired
	private AbonneDemandeRepository abonneDemandeRepository;
	@Autowired
	private AbonneRepository abonneRepository;
	@Autowired
	private ChanelRepository chanelRepository;
	
	@Autowired
	private FunctionalError functionalError;
	
	@Autowired
	private TechnicalError technicalError;

	@Autowired
	private ExceptionUtils exceptionUtils;
	
	@PersistenceContext
	private EntityManager em;
	
	private Logger slf4jLogger;

	private SimpleDateFormat dateFormat;

	public AbonneDemandeBusiness() {
		dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
		slf4jLogger = LoggerFactory.getLogger(getClass());
	}

	
	/**
	 * create AbonneDemande by using AbonneDemandeDto as object.
	 * 
	 * @param request
	 * @return response
	 * 
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	@Override
	public Response create(Request request, Locale locale)  {
		slf4jLogger.info("----begin create-----");
		
		response = new Response();
		
		try {
			List<AbonneDemande> items = new ArrayList<AbonneDemande>();
			
			for (AbonneDemandeDto dto : request.getDatasAbonneDemande()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				fieldsToVerify.put("dateDemande", dto.getDateDemande());
				fieldsToVerify.put("idAbonne", dto.getIdAbonne());
				fieldsToVerify.put("idChannel", dto.getIdChannel());
				fieldsToVerify.put("statut", dto.getStatut());
				fieldsToVerify.put("commentaire", dto.getCommentaire());
				fieldsToVerify.put("dateDecision", dto.getDateDecision());
				fieldsToVerify.put("userDecisionId", dto.getUserDecisionId());
				fieldsToVerify.put("demandeState", dto.getDemandeState());
				fieldsToVerify.put("aDesouscrire", dto.getADesouscrire());
				fieldsToVerify.put("dateDesouscription", dto.getDateDesouscription());
				fieldsToVerify.put("motifRejet", dto.getMotifRejet());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				// Verify if abonneDemande to insert do not exist
				AbonneDemande existingEntity = null;
				//TODO: add/replace by the best method
				if (existingEntity != null) {
					response.setStatus(functionalError.DATA_EXIST("abonneDemande -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}

				// Verify if chanel exist
				Chanel existingChanel = chanelRepository.findOne(dto.getIdChannel());
				if (existingChanel == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("chanel -> " + dto.getIdChannel(), locale));
					response.setHasError(true);
					return response;
				}
				// Verify if abonne exist
				Abonne existingAbonne = abonneRepository.findOne(dto.getIdAbonne());
				if (existingAbonne == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("abonne -> " + dto.getIdAbonne(), locale));
					response.setHasError(true);
					return response;
				}
				AbonneDemande entityToSave = null;
				entityToSave = AbonneDemandeTransformer.INSTANCE.toEntity(dto, existingChanel, existingAbonne);
				items.add(entityToSave);
			}

			if (!items.isEmpty()) {
				List<AbonneDemande> itemsSaved = null;
				// inserer les donnees en base de donnees
				itemsSaved = abonneDemandeRepository.save((Iterable<AbonneDemande>) items);
				if (itemsSaved == null) {
					response.setStatus(functionalError.SAVE_FAIL("abonneDemande", locale));
					response.setHasError(true);
					return response;
				}
				List<AbonneDemandeDto> itemsDto = new ArrayList<AbonneDemandeDto>();
				for (AbonneDemande entity : itemsSaved) {
					AbonneDemandeDto dto = getFullInfos(entity, locale);
					if (dto == null) continue;
					itemsDto.add(dto);
				}
				response.setItemsAbonneDemande(itemsDto);
				response.setHasError(false);
			}

			slf4jLogger.info("----end-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}

	/**
	 * update AbonneDemande by using AbonneDemandeDto as object.
	 * 
	 * @param request
	 * @return response
	 * 
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	@Override
	public Response update(Request request, Locale locale)  {
		slf4jLogger.info("----begin update-----");
		
		response = new Response();
		
		try {
			List<AbonneDemande> items = new ArrayList<AbonneDemande>();
			
			for (AbonneDemandeDto dto : request.getDatasAbonneDemande()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				fieldsToVerify.put("id", dto.getId());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				// Verifier si la abonneDemande existe
				AbonneDemande entityToSave = null;
				entityToSave = abonneDemandeRepository.findById(dto.getId());
				if (entityToSave == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("abonneDemande -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}

				// Verify if chanel exist
				if (dto.getIdChannel() != null && dto.getIdChannel() > 0){
					Chanel existingChanel = chanelRepository.findOne(dto.getIdChannel());
					if (existingChanel == null) {
						response.setStatus(functionalError.DATA_NOT_EXIST("chanel -> " + dto.getIdChannel(), locale));
						response.setHasError(true);
						return response;
					}
					entityToSave.setChanel(existingChanel);
				}
				// Verify if abonne exist
				if (dto.getIdAbonne() != null && dto.getIdAbonne() > 0){
					Abonne existingAbonne = abonneRepository.findOne(dto.getIdAbonne());
					if (existingAbonne == null) {
						response.setStatus(functionalError.DATA_NOT_EXIST("abonne -> " + dto.getIdAbonne(), locale));
						response.setHasError(true);
						return response;
					}
					entityToSave.setAbonne(existingAbonne);
				}
				// Verify if chanel exist
				if (dto.getIdChannel() != null && dto.getIdChannel() > 0){
					Chanel existingChanel = chanelRepository.findOne(dto.getIdChannel());
					if (existingChanel == null) {
						response.setStatus(functionalError.DATA_NOT_EXIST("chanel -> " + dto.getIdChannel(), locale));
						response.setHasError(true);
						return response;
					}
					entityToSave.setChanel(existingChanel);
				}
				if (dto.getDateDemande()!= null && !dto.getDateDemande().isEmpty()) {
					entityToSave.setDateDemande(new SimpleDateFormat("dd-MM-yyyy HH:mm:ss").parse(dto.getDateDemande()));
				}
				if (dto.getStatut()!= null && dto.getStatut() > 0) {
					entityToSave.setStatut(dto.getStatut());
				}
				if (dto.getCommentaire()!= null && !dto.getCommentaire().isEmpty()) {
					entityToSave.setCommentaire(dto.getCommentaire());
				}
				if (dto.getDateDecision()!= null && !dto.getDateDecision().isEmpty()) {
					entityToSave.setDateDecision(new SimpleDateFormat("dd-MM-yyyy HH:mm:ss").parse(dto.getDateDecision()));
				}
				if (dto.getUserDecisionId()!= null && dto.getUserDecisionId() > 0) {
					entityToSave.setUserDecisionId(dto.getUserDecisionId());
				}
				if (dto.getDemandeState()!= null && dto.getDemandeState() > 0) {
					entityToSave.setDemandeState(dto.getDemandeState());
				}
				if (dto.getADesouscrire()!= null) {
					entityToSave.setADesouscrire(dto.getADesouscrire());
				}
				if (dto.getDateDesouscription()!= null && !dto.getDateDesouscription().isEmpty()) {
					entityToSave.setDateDesouscription(new SimpleDateFormat("dd-MM-yyyy HH:mm:ss").parse(dto.getDateDesouscription()));
				}
				if (dto.getMotifRejet()!= null && !dto.getMotifRejet().isEmpty()) {
					entityToSave.setMotifRejet(dto.getMotifRejet());
				}
				items.add(entityToSave);
			}

			if (!items.isEmpty()) {
				List<AbonneDemande> itemsSaved = null;
				// maj les donnees en base
				itemsSaved = abonneDemandeRepository.save((Iterable<AbonneDemande>) items);
				if (itemsSaved == null) {
					response.setStatus(functionalError.SAVE_FAIL("abonneDemande", locale));
					response.setHasError(true);
					return response;
				}
				List<AbonneDemandeDto> itemsDto = new ArrayList<AbonneDemandeDto>();
				for (AbonneDemande entity : itemsSaved) {
					AbonneDemandeDto dto = getFullInfos(entity, locale);
					if (dto == null) continue;
					itemsDto.add(dto);
				}
				response.setItemsAbonneDemande(itemsDto);
				response.setHasError(false);
			}

			slf4jLogger.info("----end-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}

	/**
	 * delete AbonneDemande by using AbonneDemandeDto as object.
	 * 
	 * @param request
	 * @return response
	 * 
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	@Override
	public Response delete(Request request, Locale locale)  {
		slf4jLogger.info("----begin delete-----");
		
		response = new Response();
		
		try {
			List<AbonneDemande> items = new ArrayList<AbonneDemande>();
			
			for (AbonneDemandeDto dto : request.getDatasAbonneDemande()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				fieldsToVerify.put("id", dto.getId());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				// Verifier si la abonneDemande existe
				AbonneDemande existingEntity = null;
				existingEntity = abonneDemandeRepository.findById(dto.getId());
				if (existingEntity == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("abonneDemande -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}

				// --------------------------------------------------------------------------------------------
				// --------------------------------- CHECK IF DATA IS USED
				// --------------------------------------------------------------------------------------------



				items.add(existingEntity);
			}

			if (!items.isEmpty()) {
				// supprimer les donnees en base
				abonneDemandeRepository.delete((Iterable<AbonneDemande>) items);

				response.setHasError(false);
			}

			slf4jLogger.info("----end-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}

	/**
	 * get AbonneDemande by using AbonneDemandeDto as object.
	 * 
	 * @param request
	 * @return response
	 * 
	 */
	@SuppressWarnings("unused")
	@Override
	public Response getByCriteria(Request request, Locale locale) {
		slf4jLogger.info("----begin-----");
		
		response = new Response();
		
		try {
			List<AbonneDemande> items = null;
			items = abonneDemandeRepository.getByCriteria(request, em);
			if (items != null && !items.isEmpty()) {
				List<AbonneDemandeDto> itemsDto = new ArrayList<AbonneDemandeDto>();
				for (AbonneDemande entity : items) {
					AbonneDemandeDto dto = getFullInfos(entity, locale);
					if (dto == null) continue;
					itemsDto.add(dto);
				}
				response.setItemsAbonneDemande(itemsDto);
				response.setCount(abonneDemandeRepository.count(request, em));
				response.setHasError(false);
			} else {
				response.setStatus(functionalError.DATA_EMPTY("abonneDemande", locale));
				response.setHasError(false);
				response.setCount(0L);
				return response;
			}

			slf4jLogger.info("----end-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}

	/**
	 * get full AbonneDemandeDto by using AbonneDemande as object.
	 * 
	 * @param entity, locale
	 * @return AbonneDemandeDto
	 * 
	 */
	private AbonneDemandeDto getFullInfos(AbonneDemande entity, Locale locale) throws Exception {
		AbonneDemandeDto dto = AbonneDemandeTransformer.INSTANCE.toDto(entity);
		if (dto == null){
			return null;
		}

		return dto;
	}
}
