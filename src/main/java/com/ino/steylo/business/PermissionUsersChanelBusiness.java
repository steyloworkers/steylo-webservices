
/*
 * Java transformer for entity table permission_users_chanel 
 * Created on dec ( Time 18:46:57 )
 * Generator tool : Telosys Tools Generator ( version 2.1.1 )
 */

package com.ino.steylo.business;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Date;
import java.util.Locale;
import java.util.Map;
import java.util.ArrayList;
import java.util.HashMap;

import javax.persistence.PersistenceContext;
import javax.persistence.EntityManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.dao.PermissionDeniedDataAccessException;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.ino.steylo.helper.dto.*;
import com.ino.steylo.helper.dto.transformer.*;
import com.ino.steylo.helper.contrat.*;
import com.ino.steylo.dao.entity.*;
import com.ino.steylo.helper.*;
import com.ino.steylo.dao.repository.*;

/**
BUSINESS for table "permission_users_chanel"
 * 
 * @author ino
 *
 */
@Component
public class PermissionUsersChanelBusiness implements IBasicBusiness<Request, Response> {


	private Response response;
	
	@Autowired
	private PermissionUsersChanelRepository permissionUsersChanelRepository;
	
	@Autowired
	private FunctionalError functionalError;
	
	@Autowired
	private TechnicalError technicalError;

	@Autowired
	private ExceptionUtils exceptionUtils;
	
	@PersistenceContext
	private EntityManager em;
	
	private Logger slf4jLogger;

	private SimpleDateFormat dateFormat;

	public PermissionUsersChanelBusiness() {
		dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
		slf4jLogger = LoggerFactory.getLogger(getClass());
	}

	
	/**
	 * create PermissionUsersChanel by using PermissionUsersChanelDto as object.
	 * 
	 * @param request
	 * @return response
	 * 
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	@Override
	public Response create(Request request, Locale locale)  {
		slf4jLogger.info("----begin create-----");
		
		response = new Response();
		
		try {
			List<PermissionUsersChanel> items = new ArrayList<PermissionUsersChanel>();
			
			for (PermissionUsersChanelDto dto : request.getDatasPermissionUsersChanel()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				fieldsToVerify.put("idPermission", dto.getIdPermission());
				fieldsToVerify.put("idChanelUser", dto.getIdChanelUser());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				// Verify if permissionUsersChanel to insert do not exist
				PermissionUsersChanel existingEntity = null;
				//TODO: add/replace by the best method
				if (existingEntity != null) {
					response.setStatus(functionalError.DATA_EXIST("permissionUsersChanel -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}

				PermissionUsersChanel entityToSave = null;
				entityToSave = PermissionUsersChanelTransformer.INSTANCE.toEntity(dto);
				items.add(entityToSave);
			}

			if (!items.isEmpty()) {
				List<PermissionUsersChanel> itemsSaved = null;
				// inserer les donnees en base de donnees
				itemsSaved = permissionUsersChanelRepository.save((Iterable<PermissionUsersChanel>) items);
				if (itemsSaved == null) {
					response.setStatus(functionalError.SAVE_FAIL("permissionUsersChanel", locale));
					response.setHasError(true);
					return response;
				}
				List<PermissionUsersChanelDto> itemsDto = new ArrayList<PermissionUsersChanelDto>();
				for (PermissionUsersChanel entity : itemsSaved) {
					PermissionUsersChanelDto dto = getFullInfos(entity, locale);
					if (dto == null) continue;
					itemsDto.add(dto);
				}
				response.setItemsPermissionUsersChanel(itemsDto);
				response.setHasError(false);
			}

			slf4jLogger.info("----end-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}

	/**
	 * update PermissionUsersChanel by using PermissionUsersChanelDto as object.
	 * 
	 * @param request
	 * @return response
	 * 
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	@Override
	public Response update(Request request, Locale locale)  {
		slf4jLogger.info("----begin update-----");
		
		response = new Response();
		
		try {
			List<PermissionUsersChanel> items = new ArrayList<PermissionUsersChanel>();
			
			for (PermissionUsersChanelDto dto : request.getDatasPermissionUsersChanel()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				fieldsToVerify.put("id", dto.getId());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				// Verifier si la permissionUsersChanel existe
				PermissionUsersChanel entityToSave = null;
				entityToSave = permissionUsersChanelRepository.findById(dto.getId());
				if (entityToSave == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("permissionUsersChanel -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}

				if (dto.getIdPermission()!= null && dto.getIdPermission() > 0) {
					entityToSave.setIdPermission(dto.getIdPermission());
				}
				if (dto.getIdChanelUser()!= null && dto.getIdChanelUser() > 0) {
					entityToSave.setIdChanelUser(dto.getIdChanelUser());
				}
				items.add(entityToSave);
			}

			if (!items.isEmpty()) {
				List<PermissionUsersChanel> itemsSaved = null;
				// maj les donnees en base
				itemsSaved = permissionUsersChanelRepository.save((Iterable<PermissionUsersChanel>) items);
				if (itemsSaved == null) {
					response.setStatus(functionalError.SAVE_FAIL("permissionUsersChanel", locale));
					response.setHasError(true);
					return response;
				}
				List<PermissionUsersChanelDto> itemsDto = new ArrayList<PermissionUsersChanelDto>();
				for (PermissionUsersChanel entity : itemsSaved) {
					PermissionUsersChanelDto dto = getFullInfos(entity, locale);
					if (dto == null) continue;
					itemsDto.add(dto);
				}
				response.setItemsPermissionUsersChanel(itemsDto);
				response.setHasError(false);
			}

			slf4jLogger.info("----end-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}

	/**
	 * delete PermissionUsersChanel by using PermissionUsersChanelDto as object.
	 * 
	 * @param request
	 * @return response
	 * 
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	@Override
	public Response delete(Request request, Locale locale)  {
		slf4jLogger.info("----begin delete-----");
		
		response = new Response();
		
		try {
			List<PermissionUsersChanel> items = new ArrayList<PermissionUsersChanel>();
			
			for (PermissionUsersChanelDto dto : request.getDatasPermissionUsersChanel()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				fieldsToVerify.put("id", dto.getId());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				// Verifier si la permissionUsersChanel existe
				PermissionUsersChanel existingEntity = null;
				existingEntity = permissionUsersChanelRepository.findById(dto.getId());
				if (existingEntity == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("permissionUsersChanel -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}

				// --------------------------------------------------------------------------------------------
				// --------------------------------- CHECK IF DATA IS USED
				// --------------------------------------------------------------------------------------------



				items.add(existingEntity);
			}

			if (!items.isEmpty()) {
				// supprimer les donnees en base
				permissionUsersChanelRepository.delete((Iterable<PermissionUsersChanel>) items);

				response.setHasError(false);
			}

			slf4jLogger.info("----end-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}

	/**
	 * get PermissionUsersChanel by using PermissionUsersChanelDto as object.
	 * 
	 * @param request
	 * @return response
	 * 
	 */
	@SuppressWarnings("unused")
	@Override
	public Response getByCriteria(Request request, Locale locale) {
		slf4jLogger.info("----begin-----");
		
		response = new Response();
		
		try {
			List<PermissionUsersChanel> items = null;
			items = permissionUsersChanelRepository.getByCriteria(request, em);
			if (items != null && !items.isEmpty()) {
				List<PermissionUsersChanelDto> itemsDto = new ArrayList<PermissionUsersChanelDto>();
				for (PermissionUsersChanel entity : items) {
					PermissionUsersChanelDto dto = getFullInfos(entity, locale);
					if (dto == null) continue;
					itemsDto.add(dto);
				}
				response.setItemsPermissionUsersChanel(itemsDto);
				response.setCount(permissionUsersChanelRepository.count(request, em));
				response.setHasError(false);
			} else {
				response.setStatus(functionalError.DATA_EMPTY("permissionUsersChanel", locale));
				response.setHasError(false);
				return response;
			}

			slf4jLogger.info("----end-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}

	/**
	 * get full PermissionUsersChanelDto by using PermissionUsersChanel as object.
	 * 
	 * @param entity, locale
	 * @return PermissionUsersChanelDto
	 * 
	 */
	private PermissionUsersChanelDto getFullInfos(PermissionUsersChanel entity, Locale locale) throws Exception {
		PermissionUsersChanelDto dto = PermissionUsersChanelTransformer.INSTANCE.toDto(entity);
		if (dto == null){
			return null;
		}

		return dto;
	}
}
