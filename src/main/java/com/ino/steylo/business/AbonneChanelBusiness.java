
/*
 * Java transformer for entity table abonne_chanel 
 * Created on dec ( Time 18:46:53 )
 * Generator tool : Telosys Tools Generator ( version 2.1.1 )
 */

package com.ino.steylo.business;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Date;
import java.util.Locale;
import java.util.Map;
import java.util.ArrayList;
import java.util.HashMap;

import javax.persistence.PersistenceContext;
import javax.persistence.EntityManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.dao.PermissionDeniedDataAccessException;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.ino.steylo.helper.dto.*;
import com.ino.steylo.helper.dto.transformer.*;
import com.ino.steylo.helper.contrat.*;
import com.ino.steylo.dao.entity.*;
import com.ino.steylo.helper.*;
import com.ino.steylo.dao.repository.*;

/**
BUSINESS for table "abonne_chanel"
 * 
 * @author ino
 *
 */
@Component
public class AbonneChanelBusiness implements IBasicBusiness<Request, Response> {


	private Response response;
	
	@Autowired
	private AbonneChanelRepository abonneChanelRepository;
	
	@Autowired
	private FunctionalError functionalError;
	
	@Autowired
	private TechnicalError technicalError;

	@Autowired
	private ExceptionUtils exceptionUtils;
	
	@PersistenceContext
	private EntityManager em;
	
	private Logger slf4jLogger;

	private SimpleDateFormat dateFormat;

	public AbonneChanelBusiness() {
		dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
		slf4jLogger = LoggerFactory.getLogger(getClass());
	}

	
	/**
	 * create AbonneChanel by using AbonneChanelDto as object.
	 * 
	 * @param request
	 * @return response
	 * 
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	@Override
	public Response create(Request request, Locale locale)  {
		slf4jLogger.info("----begin create-----");
		
		response = new Response();
		
		try {
			List<AbonneChanel> items = new ArrayList<AbonneChanel>();
			
			for (AbonneChanelDto dto : request.getDatasAbonneChanel()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<>();
				fieldsToVerify.put("idChanel", dto.getIdChanel());
				fieldsToVerify.put("idAbonne", dto.getIdAbonne());
				fieldsToVerify.put("dateDesouscription", dto.getDateDesouscription());
				fieldsToVerify.put("dateSouscription", dto.getDateSouscription());
				fieldsToVerify.put("metadata", dto.getMetadata());
				fieldsToVerify.put("isActive", dto.getIsActive());
				fieldsToVerify.put("isSeller", dto.getIsSeller());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				// Verify if abonneChanel to insert do not exist
				AbonneChanel existingEntity = null;
				//TODO: add/replace by the best method
				if (existingEntity != null) {
					response.setStatus(functionalError.DATA_EXIST("abonneChanel -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}

				AbonneChanel entityToSave = null;
				entityToSave = AbonneChanelTransformer.INSTANCE.toEntity(dto);
				items.add(entityToSave);
			}

			if (!items.isEmpty()) {
				List<AbonneChanel> itemsSaved = null;
				// inserer les donnees en base de donnees
				itemsSaved = abonneChanelRepository.save((Iterable<AbonneChanel>) items);
				if (itemsSaved == null) {
					response.setStatus(functionalError.SAVE_FAIL("abonneChanel", locale));
					response.setHasError(true);
					return response;
				}
				List<AbonneChanelDto> itemsDto = new ArrayList<AbonneChanelDto>();
				for (AbonneChanel entity : itemsSaved) {
					AbonneChanelDto dto = getFullInfos(entity, locale);
					if (dto == null) continue;
					itemsDto.add(dto);
				}
				response.setItemsAbonneChanel(itemsDto);
				response.setHasError(false);
			}

			slf4jLogger.info("----end-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}

	/**
	 * update AbonneChanel by using AbonneChanelDto as object.
	 * 
	 * @param request
	 * @return response
	 * 
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	@Override
	public Response update(Request request, Locale locale)  {
		slf4jLogger.info("----begin update-----");
		
		response = new Response();
		
		try {
			List<AbonneChanel> items = new ArrayList<AbonneChanel>();
			
			for (AbonneChanelDto dto : request.getDatasAbonneChanel()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				fieldsToVerify.put("id", dto.getId());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				// Verifier si la abonneChanel existe
				AbonneChanel entityToSave = null;
				entityToSave = abonneChanelRepository.findById(dto.getId());
				if (entityToSave == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("abonneChanel -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}

				if (dto.getIdChanel()!= null && dto.getIdChanel() > 0) {
					entityToSave.setIdChanel(dto.getIdChanel());
				}
				if (dto.getIdAbonne()!= null && dto.getIdAbonne() > 0) {
					entityToSave.setIdAbonne(dto.getIdAbonne());
				}
				if (dto.getDateDesouscription()!= null && !dto.getDateDesouscription().isEmpty()) {
					entityToSave.setDateDesouscription(new SimpleDateFormat("dd/MM/yyyy").parse(dto.getDateDesouscription()));
				}
				if (dto.getDateSouscription()!= null && !dto.getDateSouscription().isEmpty()) {
					entityToSave.setDateSouscription(new SimpleDateFormat("dd/MM/yyyy").parse(dto.getDateSouscription()));
				}
				if (dto.getMetadata()!= null && !dto.getMetadata().isEmpty()) {
					entityToSave.setMetadata(dto.getMetadata());
				}
				if (dto.getIsActive()!= null) {
					entityToSave.setIsActive(dto.getIsActive());
				}
				if (dto.getIsSeller()!= null) {
					entityToSave.setIsSeller(dto.getIsSeller());
				}
				items.add(entityToSave);
			}

			if (!items.isEmpty()) {
				List<AbonneChanel> itemsSaved = null;
				// maj les donnees en base
				itemsSaved = abonneChanelRepository.save((Iterable<AbonneChanel>) items);
				if (itemsSaved == null) {
					response.setStatus(functionalError.SAVE_FAIL("abonneChanel", locale));
					response.setHasError(true);
					return response;
				}
				List<AbonneChanelDto> itemsDto = new ArrayList<AbonneChanelDto>();
				for (AbonneChanel entity : itemsSaved) {
					AbonneChanelDto dto = getFullInfos(entity, locale);
					if (dto == null) continue;
					itemsDto.add(dto);
				}
				response.setItemsAbonneChanel(itemsDto);
				response.setHasError(false);
			}

			slf4jLogger.info("----end-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}

	/**
	 * delete AbonneChanel by using AbonneChanelDto as object.
	 * 
	 * @param request
	 * @return response
	 * 
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	@Override
	public Response delete(Request request, Locale locale)  {
		slf4jLogger.info("----begin delete-----");
		
		response = new Response();
		
		try {
			List<AbonneChanel> items = new ArrayList<AbonneChanel>();
			
			for (AbonneChanelDto dto : request.getDatasAbonneChanel()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				fieldsToVerify.put("id", dto.getId());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				// Verifier si la abonneChanel existe
				AbonneChanel existingEntity = null;
				existingEntity = abonneChanelRepository.findById(dto.getId());
				if (existingEntity == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("abonneChanel -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}

				// --------------------------------------------------------------------------------------------
				// --------------------------------- CHECK IF DATA IS USED
				// --------------------------------------------------------------------------------------------



				items.add(existingEntity);
			}

			if (!items.isEmpty()) {
				// supprimer les donnees en base
				abonneChanelRepository.delete((Iterable<AbonneChanel>) items);

				response.setHasError(false);
			}

			slf4jLogger.info("----end-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}

	/**
	 * get AbonneChanel by using AbonneChanelDto as object.
	 * 
	 * @param request
	 * @return response
	 * 
	 */
	@SuppressWarnings("unused")
	@Override
	public Response getByCriteria(Request request, Locale locale) {
		slf4jLogger.info("----begin-----");
		
		response = new Response();
		
		try {
			List<AbonneChanel> items = null;
			items = abonneChanelRepository.getByCriteria(request, em);
			if (items != null && !items.isEmpty()) {
				List<AbonneChanelDto> itemsDto = new ArrayList<AbonneChanelDto>();
				for (AbonneChanel entity : items) {
					AbonneChanelDto dto = getFullInfos(entity, locale);
					if (dto == null) continue;
					itemsDto.add(dto);
				}
				response.setItemsAbonneChanel(itemsDto);
				response.setCount(abonneChanelRepository.count(request, em));
				response.setHasError(false);
			} else {
				response.setStatus(functionalError.DATA_EMPTY("abonneChanel", locale));
				response.setHasError(false);
				return response;
			}

			slf4jLogger.info("----end-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}

	/**
	 * get full AbonneChanelDto by using AbonneChanel as object.
	 * 
	 * @param entity, locale
	 * @return AbonneChanelDto
	 * 
	 */
	private AbonneChanelDto getFullInfos(AbonneChanel entity, Locale locale) throws Exception {
		AbonneChanelDto dto = AbonneChanelTransformer.INSTANCE.toDto(entity);
		if (dto == null){
			return null;
		}

		return dto;
	}
}
