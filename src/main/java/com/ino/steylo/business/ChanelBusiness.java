
/*
 * Java transformer for entity table chanel 
 * Created on dec ( Time 10:56:38 )
 * Generator tool : Telosys Tools Generator ( version 2.1.1 )
 */

package com.ino.steylo.business;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Date;
import java.util.Locale;
import java.util.Map;
import java.util.ArrayList;
import java.util.HashMap;

import javax.persistence.PersistenceContext;
import javax.persistence.EntityManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.dao.PermissionDeniedDataAccessException;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.ino.steylo.helper.dto.*;
import com.ino.steylo.helper.dto.transformer.*;
import com.ino.steylo.helper.contrat.*;
import com.ino.steylo.dao.entity.*;
import com.ino.steylo.helper.*;
import com.ino.steylo.dao.repository.*;

/**
BUSINESS for table "chanel"
 * 
 * @author ino
 *
 */
@Component
public class ChanelBusiness implements IBasicBusiness<Request, Response> {


	private Response response;
	
	@Autowired
	private ChanelRepository chanelRepository;
	@Autowired
	private ChanelUsersRepository chanelUsersRepository;
	@Autowired
	private CountryRepository countryRepository;
	@Autowired
	private SecteurActiviteRepository secteurActiviteRepository;
	@Autowired
	private PermissionProfilRepository permissionProfilRepository;
	@Autowired
	private ProduitRepository produitRepository;
	
	@Autowired
	private FunctionalError functionalError;
	
	@Autowired
	private TechnicalError technicalError;

	@Autowired
	private ExceptionUtils exceptionUtils;
	
	@PersistenceContext
	private EntityManager em;
	
	private Logger slf4jLogger;

	private SimpleDateFormat dateFormat;

	public ChanelBusiness() {
		dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
		slf4jLogger = LoggerFactory.getLogger(getClass());
	}

	
	/**
	 * create Chanel by using ChanelDto as object.
	 * 
	 * @param request
	 * @return response
	 * 
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	@Override
	public Response create(Request request, Locale locale)  {
		slf4jLogger.info("----begin create-----");
		
		response = new Response();
		
		try {
			List<Chanel> itemsChanel = new ArrayList<>();
			List<ChanelUsers> itemsChanelUser = new ArrayList<>();

			for (ChanelDto dtoChanel : request.getDatasChanel()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsChanelToVerify = new HashMap<>();
				fieldsChanelToVerify.put("formeJuridique", dtoChanel.getFormeJuridique());
				fieldsChanelToVerify.put("nomChanel", dtoChanel.getNomChanel());
				fieldsChanelToVerify.put("sigle", dtoChanel.getSigle());
				fieldsChanelToVerify.put("numeroRegistre", dtoChanel.getNumeroRegistre());
				fieldsChanelToVerify.put("numeroImmatriculation", dtoChanel.getNumeroImmatriculation());
				fieldsChanelToVerify.put("idSecteurActivite", dtoChanel.getIdSecteurActivite());
				fieldsChanelToVerify.put("descriptionSecteur", dtoChanel.getDescriptionSecteur());
				fieldsChanelToVerify.put("siteWeb", dtoChanel.getSiteWeb());
				fieldsChanelToVerify.put("email", dtoChanel.getEmail());
				fieldsChanelToVerify.put("phoneNumber", dtoChanel.getPhoneNumber());
				fieldsChanelToVerify.put("fax", dtoChanel.getFax());
				fieldsChanelToVerify.put("idPays", dtoChanel.getIdPays());
				fieldsChanelToVerify.put("etat", dtoChanel.getEtat());
				fieldsChanelToVerify.put("ville", dtoChanel.getVille());
				fieldsChanelToVerify.put("codePostal", dtoChanel.getCodePostal());
				fieldsChanelToVerify.put("address", dtoChanel.getAddress());
				fieldsChanelToVerify.put("logo", dtoChanel.getLogo());
				fieldsChanelToVerify.put("extLogo", dtoChanel.getExtLogo());
				fieldsChanelToVerify.put("codeGuichet", dtoChanel.getCodeGuichet());
				fieldsChanelToVerify.put("codeBanque", dtoChanel.getCodeBanque());
				fieldsChanelToVerify.put("numeroCompte", dtoChanel.getNumeroCompte());
				fieldsChanelToVerify.put("swift", dtoChanel.getSwift());
				fieldsChanelToVerify.put("cleRib", dtoChanel.getCleRib());
				fieldsChanelToVerify.put("rib", dtoChanel.getRib());
				fieldsChanelToVerify.put("iban", dtoChanel.getIban());
				//fieldsToVerify.put("rowDateUpdate", dto.getRowDateUpdate());
				//fieldsToVerify.put("logoDateUpdate", dto.getLogoDateUpdate());
				if (!Validate.RequiredValue(fieldsChanelToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				// Verify if chanel to insert do not exist
				Chanel existingChanel = chanelRepository.findByNumeroRegistre(dtoChanel.getNumeroRegistre());

				if (existingChanel != null) {
					response.setStatus(functionalError.DATA_EXIST("chanel -> " + dtoChanel.getId(), locale));
					response.setHasError(true);
					return response;
				}

				existingChanel = chanelRepository.findByNumeroImmatriculation(dtoChanel.getNumeroImmatriculation());
				if (existingChanel != null) {
					response.setStatus(functionalError.DATA_EXIST("chanel -> " + dtoChanel.getId(), locale));
					response.setHasError(true);
					return response;
				}

				// Verify if pays exist
				Country existingPays = countryRepository.findById(dtoChanel.getIdPays());
				if (existingPays == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("Le pays sélectionné n'existe pas !" + dtoChanel.getIdPays(), locale));
					response.setHasError(true);
					return response;
				}
				// Verify if pays exist
				SecteurActivite existingSecteurActivite = secteurActiviteRepository.findById(dtoChanel.getIdSecteurActivite());
				if (existingSecteurActivite == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("Le secteur d'activité sélectionné n'existe pas !" + dtoChanel.getIdSecteurActivite(), locale));
					response.setHasError(true);
					return response;
				}

				dtoChanel.setLogoDateUpdate(Utilities.getCurrentLocalDateTimeStamp());
				dtoChanel.setRowDateUpdate(Utilities.getCurrentLocalDateTimeStamp());
				Chanel chanelToSave = ChanelTransformer.INSTANCE.toEntity(dtoChanel, existingPays, existingSecteurActivite);
				itemsChanel.add(chanelToSave);


				//Pour ChanelUsers
				ChanelUsersDto dtoChanelUsers = dtoChanel.getChanelUsers();
				System.out.println(dtoChanelUsers.toString());
				// Definir les parametres obligatoires
				Map<String, Object> fieldsChanelUsersToVerify = new HashMap<>();
				//fieldsChanelUsersToVerify.put("nom", dtoChanelUsers.getNom());
				//fieldsChanelUsersToVerify.put("prenoms", dtoChanelUsers.getPrenoms());
				fieldsChanelUsersToVerify.put("login", dtoChanelUsers.getLogin());
				fieldsChanelUsersToVerify.put("password", dtoChanelUsers.getPassword());
				fieldsChanelUsersToVerify.put("email", dtoChanelUsers.getEmail());
				//fieldsChanelUsersToVerify.put("statut", dtoChanelUsers.getStatut());
				//fieldsChanelUsersToVerify.put("dateCreation", dtoChanelUsers.getDateCreation());
				//fieldsChanelUsersToVerify.put("idChanel", dtoChanelUsers.getIdChanel());
				if (!Validate.RequiredValue(fieldsChanelUsersToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				// Verify if chanelUsers to insert do not exist
				ChanelUsers existingChanelUsers = chanelUsersRepository.findByLogin(dtoChanelUsers.getLogin());
				if (existingChanelUsers != null) {
					response.setStatus(functionalError.DATA_EXIST("chanelUsers -> " + dtoChanelUsers.getId(), locale));
					response.setHasError(true);
					return response;
				}

				dtoChanelUsers.setDateCreation(Utilities.getCurrentLocalDateTimeStamp());
				dtoChanelUsers.setStatut(0);
				dtoChanelUsers.setPassword(Utilities.generateHash(dtoChanelUsers.getPassword()));
				ChanelUsers chanelUsersToSave = ChanelUsersTransformer.INSTANCE.toEntity(dtoChanelUsers, chanelToSave);
				itemsChanelUser.add(chanelUsersToSave);

				//end chanelUser
			} //end for chanel

			if (!itemsChanel.isEmpty()) {
				List<Chanel> itemsChanelSaved = chanelRepository.save(itemsChanel);
				if (itemsChanelSaved == null) {
					response.setStatus(functionalError.SAVE_FAIL("La création du compte entreprise a échoué !", locale));
					response.setHasError(true);
					return response;
				}

				if (!itemsChanelUser.isEmpty()) {
					List<ChanelUsers> itemsChanelUserSaved = chanelUsersRepository.save(itemsChanelUser);
					if (itemsChanelUserSaved == null) {
						response.setStatus(functionalError.SAVE_FAIL("La création du com", locale));
						response.setHasError(true);
						return response;
					}
				}
				List<ChanelDto> itemsDto = new ArrayList<>();
				for (Chanel entity : itemsChanelSaved) {
					ChanelDto dto = getFullInfos(entity, locale);
					if (dto == null) continue;
					itemsDto.add(dto);
				}
				response.setItemsChanel(itemsDto);
				response.setHasError(false);
			}

			slf4jLogger.info("----end-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}

	/**
	 * update Chanel by using ChanelDto as object.
	 * 
	 * @param request
	 * @return response
	 * 
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	@Override
	public Response update(Request request, Locale locale)  {
		slf4jLogger.info("----begin update-----");
		
		response = new Response();
		
		try {
			List<Chanel> items = new ArrayList<Chanel>();
			
			for (ChanelDto dto : request.getDatasChanel()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				fieldsToVerify.put("id", dto.getId());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				// Verifier si la chanel existe
				Chanel entityToSave = null;
				entityToSave = chanelRepository.findById(dto.getId());
				if (entityToSave == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("chanel -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}

				// Verify if country exist
				if (dto.getIdPays() != null && dto.getIdPays() > 0){
					Country existingCountry = countryRepository.findOne(dto.getIdPays());
					if (existingCountry == null) {
						response.setStatus(functionalError.DATA_NOT_EXIST("country -> " + dto.getIdPays(), locale));
						response.setHasError(true);
						return response;
					}
					entityToSave.setCountry(existingCountry);
				}
				// Verify if secteurActivite exist
				if (dto.getIdSecteurActivite() != null && dto.getIdSecteurActivite() > 0){
					SecteurActivite existingSecteurActivite = secteurActiviteRepository.findOne(dto.getIdSecteurActivite());
					if (existingSecteurActivite == null) {
						response.setStatus(functionalError.DATA_NOT_EXIST("secteurActivite -> " + dto.getIdSecteurActivite(), locale));
						response.setHasError(true);
						return response;
					}
					entityToSave.setSecteurActivite(existingSecteurActivite);
				}
				if (dto.getFormeJuridique()!= null && !dto.getFormeJuridique().isEmpty()) {
					entityToSave.setFormeJuridique(dto.getFormeJuridique());
				}
				if (dto.getNomChanel()!= null && !dto.getNomChanel().isEmpty()) {
					entityToSave.setNomChanel(dto.getNomChanel());
				}
				if (dto.getSigle()!= null && !dto.getSigle().isEmpty()) {
					entityToSave.setSigle(dto.getSigle());
				}
				if (dto.getNumeroRegistre()!= null && !dto.getNumeroRegistre().isEmpty()) {
					entityToSave.setNumeroRegistre(dto.getNumeroRegistre());
				}
				if (dto.getNumeroImmatriculation()!= null && !dto.getNumeroImmatriculation().isEmpty()) {
					entityToSave.setNumeroImmatriculation(dto.getNumeroImmatriculation());
				}
				if (dto.getDescriptionSecteur()!= null && !dto.getDescriptionSecteur().isEmpty()) {
					entityToSave.setDescriptionSecteur(dto.getDescriptionSecteur());
				}
				if (dto.getSiteWeb()!= null && !dto.getSiteWeb().isEmpty()) {
					entityToSave.setSiteWeb(dto.getSiteWeb());
				}
				if (dto.getEmail()!= null && !dto.getEmail().isEmpty()) {
					entityToSave.setEmail(dto.getEmail());
				}
				if (dto.getPhoneNumber()!= null && !dto.getPhoneNumber().isEmpty()) {
					entityToSave.setPhoneNumber(dto.getPhoneNumber());
				}
				if (dto.getFax()!= null && !dto.getFax().isEmpty()) {
					entityToSave.setFax(dto.getFax());
				}
				if (dto.getEtat()!= null && !dto.getEtat().isEmpty()) {
					entityToSave.setEtat(dto.getEtat());
				}
				if (dto.getVille()!= null && !dto.getVille().isEmpty()) {
					entityToSave.setVille(dto.getVille());
				}
				if (dto.getCodePostal()!= null && !dto.getCodePostal().isEmpty()) {
					entityToSave.setCodePostal(dto.getCodePostal());
				}
				if (dto.getAddress()!= null && !dto.getAddress().isEmpty()) {
					entityToSave.setAddress(dto.getAddress());
				}
				if (dto.getLogo()!= null && !dto.getLogo().isEmpty()) {
					entityToSave.setLogo(dto.getLogo());
				}
				if (dto.getCodeGuichet()!= null && !dto.getCodeGuichet().isEmpty()) {
					entityToSave.setCodeGuichet(dto.getCodeGuichet());
				}
				if (dto.getCodeBanque()!= null && !dto.getCodeBanque().isEmpty()) {
					entityToSave.setCodeBanque(dto.getCodeBanque());
				}
				if (dto.getNumeroCompte()!= null && !dto.getNumeroCompte().isEmpty()) {
					entityToSave.setNumeroCompte(dto.getNumeroCompte());
				}
				if (dto.getSwift()!= null && !dto.getSwift().isEmpty()) {
					entityToSave.setSwift(dto.getSwift());
				}
				if (dto.getCleRib()!= null && !dto.getCleRib().isEmpty()) {
					entityToSave.setCleRib(dto.getCleRib());
				}
				if (dto.getRib()!= null && !dto.getRib().isEmpty()) {
					entityToSave.setRib(dto.getRib());
				}
				if (dto.getIban()!= null && !dto.getIban().isEmpty()) {
					entityToSave.setIban(dto.getIban());
				}
				if (dto.getRowDateUpdate()!= null && !dto.getRowDateUpdate().isEmpty()) {
					entityToSave.setRowDateUpdate(new SimpleDateFormat("dd-MM-yyyy HH:mm:ss").parse(dto.getRowDateUpdate()));
				}
				if (dto.getLogoDateUpdate()!= null && !dto.getLogoDateUpdate().isEmpty()) {
					entityToSave.setLogoDateUpdate(dateFormat.parse(dto.getLogoDateUpdate()));
				}

				entityToSave.setRowDateUpdate(dateFormat.parse(Utilities.getCurrentLocalDateTimeStamp()));
				items.add(entityToSave);
			}

			if (!items.isEmpty()) {
				List<Chanel> itemsSaved = null;
				// maj les donnees en base
				itemsSaved = chanelRepository.save(items);
				if (itemsSaved == null) {
					response.setStatus(functionalError.SAVE_FAIL("Impossible de modifier les informations de l'entreprise !", locale));
					response.setHasError(true);
					return response;
				}
				List<ChanelDto> itemsDto = new ArrayList<ChanelDto>();
				for (Chanel entity : itemsSaved) {
					ChanelDto dto = getFullInfos(entity, locale);
					if (dto == null) continue;
					itemsDto.add(dto);
				}
				response.setItemsChanel(itemsDto);
				response.setHasError(false);
			}

			slf4jLogger.info("----end-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}

	/**
	 * delete Chanel by using ChanelDto as object.
	 * 
	 * @param request
	 * @return response
	 * 
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	@Override
	public Response delete(Request request, Locale locale)  {
		slf4jLogger.info("----begin delete-----");
		
		response = new Response();
		
		try {
			List<Chanel> items = new ArrayList<Chanel>();
			
			for (ChanelDto dto : request.getDatasChanel()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				fieldsToVerify.put("id", dto.getId());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				// Verifier si la chanel existe
				Chanel existingEntity = null;
				existingEntity = chanelRepository.findById(dto.getId());
				if (existingEntity == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("chanel -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}

				// --------------------------------------------------------------------------------------------
				// --------------------------------- CHECK IF DATA IS USED
				// --------------------------------------------------------------------------------------------

				// chanelUsers
				List<ChanelUsers> listOfChanelUsers = chanelUsersRepository.findByIdChanel(existingEntity.getId());
				if (listOfChanelUsers != null && !listOfChanelUsers.isEmpty()){
					response.setStatus(functionalError.DATA_NOT_DELETABLE("(" + listOfChanelUsers.size() + ")", locale));
					response.setHasError(true);
					return response;
				}
				// permissionProfil
				List<PermissionProfil> listOfPermissionProfil = permissionProfilRepository.findByIdChanel(existingEntity.getId());
				if (listOfPermissionProfil != null && !listOfPermissionProfil.isEmpty()){
					response.setStatus(functionalError.DATA_NOT_DELETABLE("(" + listOfPermissionProfil.size() + ")", locale));
					response.setHasError(true);
					return response;
				}
				// produit
				List<Produit> listOfProduit = produitRepository.findByIdChanel(existingEntity.getId());
				if (listOfProduit != null && !listOfProduit.isEmpty()){
					response.setStatus(functionalError.DATA_NOT_DELETABLE("(" + listOfProduit.size() + ")", locale));
					response.setHasError(true);
					return response;
				}


				items.add(existingEntity);
			}

			if (!items.isEmpty()) {
				// supprimer les donnees en base
				chanelRepository.delete((Iterable<Chanel>) items);

				response.setHasError(false);
			}

			slf4jLogger.info("----end-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}

	/**
	 * get Chanel by using ChanelDto as object.
	 * 
	 * @param request
	 * @return response
	 * 
	 */
	@SuppressWarnings("unused")
	@Override
	public Response getByCriteria(Request request, Locale locale) {
		slf4jLogger.info("----begin-----");
		
		response = new Response();
		
		try {
			List<Chanel> items = null;
			items = chanelRepository.getByCriteria(request, em);
			if (items != null && !items.isEmpty()) {
				List<ChanelDto> itemsDto = new ArrayList<ChanelDto>();
				for (Chanel entity : items) {
					ChanelDto dto = getFullInfos(entity, locale);
					if (dto == null) continue;
					itemsDto.add(dto);
				}
				response.setItemsChanel(itemsDto);
				response.setCount(chanelRepository.count(request, em));
				response.setHasError(false);
			} else {
				response.setStatus(functionalError.DATA_EMPTY("chanel", locale));
				response.setHasError(false);
				return response;
			}

			slf4jLogger.info("----end-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}

	/**
	 * get full ChanelDto by using Chanel as object.
	 * 
	 * @param entity, locale
	 * @return ChanelDto
	 * 
	 */
	private ChanelDto getFullInfos(Chanel entity, Locale locale) throws Exception {
		ChanelDto dto = ChanelTransformer.INSTANCE.toDto(entity);
		if (dto == null){
			return null;
		}

		return dto;
	}
}
