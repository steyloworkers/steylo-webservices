
/*
 * Java transformer for entity table image_produit 
 * Created on dec ( Time 23:50:38 )
 * Generator tool : Telosys Tools Generator ( version 2.1.1 )
 */

package com.ino.steylo.business;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Date;
import java.util.Locale;
import java.util.Map;
import java.util.ArrayList;
import java.util.HashMap;

import javax.persistence.PersistenceContext;
import javax.persistence.EntityManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.dao.PermissionDeniedDataAccessException;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.ino.steylo.helper.dto.*;
import com.ino.steylo.helper.dto.transformer.*;
import com.ino.steylo.helper.contrat.*;
import com.ino.steylo.dao.entity.*;
import com.ino.steylo.helper.*;
import com.ino.steylo.dao.repository.*;

/**
BUSINESS for table "image_produit"
 * 
 * @author ino
 *
 */
@Component
public class ImageProduitBusiness implements IBasicBusiness<Request, Response> {


	private Response response;
	
	@Autowired
	private ImageProduitRepository imageProduitRepository;
	@Autowired
	private ProduitRepository produitRepository;
	
	@Autowired
	private FunctionalError functionalError;
	
	@Autowired
	private TechnicalError technicalError;

	@Autowired
	private ExceptionUtils exceptionUtils;
	
	@PersistenceContext
	private EntityManager em;
	
	private Logger slf4jLogger;

	private SimpleDateFormat dateFormat;

	public ImageProduitBusiness() {
		dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
		slf4jLogger = LoggerFactory.getLogger(getClass());
	}

	
	/**
	 * create ImageProduit by using ImageProduitDto as object.
	 * 
	 * @param request
	 * @return response
	 * 
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	@Override
	public Response create(Request request, Locale locale)  {
		slf4jLogger.info("----begin create-----");
		
		response = new Response();
		
		try {
			List<ImageProduit> items = new ArrayList<ImageProduit>();
			
			for (ImageProduitDto dto : request.getDatasImageProduit()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				fieldsToVerify.put("idProduit", dto.getIdProduit());
				fieldsToVerify.put("image", dto.getImage());
				fieldsToVerify.put("type", dto.getType());
				fieldsToVerify.put("extImg", dto.getExtImg());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				// Verify if imageProduit to insert do not exist
				ImageProduit existingEntity = null;
				//TODO: add/replace by the best method
				if (existingEntity != null) {
					response.setStatus(functionalError.DATA_EXIST("imageProduit -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}

				// Verify if produit exist
				Produit existingProduit = produitRepository.findOne(dto.getIdProduit());
				if (existingProduit == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("produit -> " + dto.getIdProduit(), locale));
					response.setHasError(true);
					return response;
				}
				ImageProduit entityToSave = null;
				entityToSave = ImageProduitTransformer.INSTANCE.toEntity(dto, existingProduit);
				items.add(entityToSave);
			}

			if (!items.isEmpty()) {
				List<ImageProduit> itemsSaved = null;
				// inserer les donnees en base de donnees
				itemsSaved = imageProduitRepository.save((Iterable<ImageProduit>) items);
				if (itemsSaved == null) {
					response.setStatus(functionalError.SAVE_FAIL("imageProduit", locale));
					response.setHasError(true);
					return response;
				}
				List<ImageProduitDto> itemsDto = new ArrayList<ImageProduitDto>();
				for (ImageProduit entity : itemsSaved) {
					ImageProduitDto dto = getFullInfos(entity, locale);
					if (dto == null) continue;
					itemsDto.add(dto);
				}
				response.setItemsImageProduit(itemsDto);
				response.setHasError(false);
			}

			slf4jLogger.info("----end-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}

	/**
	 * update ImageProduit by using ImageProduitDto as object.
	 * 
	 * @param request
	 * @return response
	 * 
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	@Override
	public Response update(Request request, Locale locale)  {
		slf4jLogger.info("----begin update-----");
		
		response = new Response();
		
		try {
			List<ImageProduit> items = new ArrayList<ImageProduit>();
			
			for (ImageProduitDto dto : request.getDatasImageProduit()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				fieldsToVerify.put("id", dto.getId());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				// Verifier si la imageProduit existe
				ImageProduit entityToSave = null;
				entityToSave = imageProduitRepository.findById(dto.getId());
				if (entityToSave == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("imageProduit -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}

				// Verify if produit exist
				if (dto.getIdProduit() != null && dto.getIdProduit() > 0){
					Produit existingProduit = produitRepository.findOne(dto.getIdProduit());
					if (existingProduit == null) {
						response.setStatus(functionalError.DATA_NOT_EXIST("produit -> " + dto.getIdProduit(), locale));
						response.setHasError(true);
						return response;
					}
					entityToSave.setProduit(existingProduit);
				}
				if (dto.getImage()!= null && !dto.getImage().isEmpty()) {
					entityToSave.setImage(dto.getImage());
				}
				if (dto.getType()!= null && !dto.getType().isEmpty()) {
					entityToSave.setType(dto.getType());
				}
				if (dto.getExtImg()!= null && !dto.getExtImg().isEmpty()) {
					entityToSave.setExtImg(dto.getExtImg());
				}
				items.add(entityToSave);
			}

			if (!items.isEmpty()) {
				List<ImageProduit> itemsSaved = null;
				// maj les donnees en base
				itemsSaved = imageProduitRepository.save((Iterable<ImageProduit>) items);
				if (itemsSaved == null) {
					response.setStatus(functionalError.SAVE_FAIL("imageProduit", locale));
					response.setHasError(true);
					return response;
				}
				List<ImageProduitDto> itemsDto = new ArrayList<ImageProduitDto>();
				for (ImageProduit entity : itemsSaved) {
					ImageProduitDto dto = getFullInfos(entity, locale);
					if (dto == null) continue;
					itemsDto.add(dto);
				}
				response.setItemsImageProduit(itemsDto);
				response.setHasError(false);
			}

			slf4jLogger.info("----end-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}

	/**
	 * delete ImageProduit by using ImageProduitDto as object.
	 * 
	 * @param request
	 * @return response
	 * 
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	@Override
	public Response delete(Request request, Locale locale)  {
		slf4jLogger.info("----begin delete-----");
		
		response = new Response();
		
		try {
			List<ImageProduit> items = new ArrayList<ImageProduit>();
			
			for (ImageProduitDto dto : request.getDatasImageProduit()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				fieldsToVerify.put("id", dto.getId());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				// Verifier si la imageProduit existe
				ImageProduit existingEntity = null;
				existingEntity = imageProduitRepository.findById(dto.getId());
				if (existingEntity == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("imageProduit -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}

				// --------------------------------------------------------------------------------------------
				// --------------------------------- CHECK IF DATA IS USED
				// --------------------------------------------------------------------------------------------



				items.add(existingEntity);
			}

			if (!items.isEmpty()) {
				// supprimer les donnees en base
				imageProduitRepository.delete((Iterable<ImageProduit>) items);

				response.setHasError(false);
			}

			slf4jLogger.info("----end-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}

	/**
	 * get ImageProduit by using ImageProduitDto as object.
	 * 
	 * @param request
	 * @return response
	 * 
	 */
	@SuppressWarnings("unused")
	@Override
	public Response getByCriteria(Request request, Locale locale) {
		slf4jLogger.info("----begin-----");
		
		response = new Response();
		
		try {
			List<ImageProduit> items = null;
			items = imageProduitRepository.getByCriteria(request, em);
			if (items != null && !items.isEmpty()) {
				List<ImageProduitDto> itemsDto = new ArrayList<ImageProduitDto>();
				for (ImageProduit entity : items) {
					ImageProduitDto dto = getFullInfos(entity, locale);
					if (dto == null) continue;
					itemsDto.add(dto);
				}
				response.setItemsImageProduit(itemsDto);
				response.setCount(imageProduitRepository.count(request, em));
				response.setHasError(false);
			} else {
				response.setStatus(functionalError.DATA_EMPTY("imageProduit", locale));
				response.setHasError(false);
				return response;
			}

			slf4jLogger.info("----end-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}

	/**
	 * get full ImageProduitDto by using ImageProduit as object.
	 * 
	 * @param entity, locale
	 * @return ImageProduitDto
	 * 
	 */
	private ImageProduitDto getFullInfos(ImageProduit entity, Locale locale) throws Exception {
		ImageProduitDto dto = ImageProduitTransformer.INSTANCE.toDto(entity);
		if (dto == null){
			return null;
		}

		switch (dto.getType()){
			case "png":
				dto.setImage("data:image/png;base64," + dto.getImage());
				break;
			case "jpeg":
				dto.setImage("data:image/jpeg;base64," + dto.getImage());
				break;
			case "jpg":
				dto.setImage("data:image/jpg;base64," + dto.getImage());
				break;
			case "gif":
				dto.setImage("data:image/gif;base64," + dto.getImage());
				break;
		}

		return dto;
	}
}
