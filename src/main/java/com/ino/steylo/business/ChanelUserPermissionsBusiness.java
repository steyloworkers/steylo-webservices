
/*
 * Java transformer for entity table chanel_user_permissions 
 * Created on dec ( Time 18:46:54 )
 * Generator tool : Telosys Tools Generator ( version 2.1.1 )
 */

package com.ino.steylo.business;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Date;
import java.util.Locale;
import java.util.Map;
import java.util.ArrayList;
import java.util.HashMap;

import javax.persistence.PersistenceContext;
import javax.persistence.EntityManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.dao.PermissionDeniedDataAccessException;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.ino.steylo.helper.dto.*;
import com.ino.steylo.helper.dto.transformer.*;
import com.ino.steylo.helper.contrat.*;
import com.ino.steylo.dao.entity.*;
import com.ino.steylo.helper.*;
import com.ino.steylo.dao.repository.*;

/**
BUSINESS for table "chanel_user_permissions"
 * 
 * @author ino
 *
 */
@Component
public class ChanelUserPermissionsBusiness implements IBasicBusiness<Request, Response> {


	private Response response;
	
	@Autowired
	private ChanelUserPermissionsRepository chanelUserPermissionsRepository;
	@Autowired
	private ChanelPermissionsRepository chanelPermissionsRepository;
	@Autowired
	private ChanelUsersRepository chanelUsersRepository;
	
	@Autowired
	private FunctionalError functionalError;
	
	@Autowired
	private TechnicalError technicalError;

	@Autowired
	private ExceptionUtils exceptionUtils;
	
	@PersistenceContext
	private EntityManager em;
	
	private Logger slf4jLogger;

	private SimpleDateFormat dateFormat;

	public ChanelUserPermissionsBusiness() {
		dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
		slf4jLogger = LoggerFactory.getLogger(getClass());
	}

	
	/**
	 * create ChanelUserPermissions by using ChanelUserPermissionsDto as object.
	 * 
	 * @param request
	 * @return response
	 * 
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	@Override
	public Response create(Request request, Locale locale)  {
		slf4jLogger.info("----begin create-----");
		
		response = new Response();
		
		try {
			List<ChanelUserPermissions> items = new ArrayList<ChanelUserPermissions>();
			
			for (ChanelUserPermissionsDto dto : request.getDatasChanelUserPermissions()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				fieldsToVerify.put("idUser", dto.getIdUser());
				fieldsToVerify.put("idPermission", dto.getIdPermission());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				// Verify if chanelUserPermissions to insert do not exist
				ChanelUserPermissions existingEntity = null;
				//TODO: add/replace by the best method
				if (existingEntity != null) {
					response.setStatus(functionalError.DATA_EXIST("chanelUserPermissions -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}

				// Verify if chanelPermissions exist
				ChanelPermissions existingChanelPermissions = chanelPermissionsRepository.findOne(dto.getIdPermission());
				if (existingChanelPermissions == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("chanelPermissions -> " + dto.getIdPermission(), locale));
					response.setHasError(true);
					return response;
				}
				// Verify if chanelUsers exist
				ChanelUsers existingChanelUsers = chanelUsersRepository.findOne(dto.getIdUser());
				if (existingChanelUsers == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("chanelUsers -> " + dto.getIdUser(), locale));
					response.setHasError(true);
					return response;
				}
				ChanelUserPermissions entityToSave = null;
				entityToSave = ChanelUserPermissionsTransformer.INSTANCE.toEntity(dto, existingChanelPermissions, existingChanelUsers);
				items.add(entityToSave);
			}

			if (!items.isEmpty()) {
				List<ChanelUserPermissions> itemsSaved = null;
				// inserer les donnees en base de donnees
				itemsSaved = chanelUserPermissionsRepository.save((Iterable<ChanelUserPermissions>) items);
				if (itemsSaved == null) {
					response.setStatus(functionalError.SAVE_FAIL("chanelUserPermissions", locale));
					response.setHasError(true);
					return response;
				}
				List<ChanelUserPermissionsDto> itemsDto = new ArrayList<ChanelUserPermissionsDto>();
				for (ChanelUserPermissions entity : itemsSaved) {
					ChanelUserPermissionsDto dto = getFullInfos(entity, locale);
					if (dto == null) continue;
					itemsDto.add(dto);
				}
				response.setItemsChanelUserPermissions(itemsDto);
				response.setHasError(false);
			}

			slf4jLogger.info("----end-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}

	/**
	 * update ChanelUserPermissions by using ChanelUserPermissionsDto as object.
	 * 
	 * @param request
	 * @return response
	 * 
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	@Override
	public Response update(Request request, Locale locale)  {
		slf4jLogger.info("----begin update-----");
		
		response = new Response();
		
		try {
			List<ChanelUserPermissions> items = new ArrayList<ChanelUserPermissions>();
			
			for (ChanelUserPermissionsDto dto : request.getDatasChanelUserPermissions()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				fieldsToVerify.put("id", dto.getId());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				// Verifier si la chanelUserPermissions existe
				ChanelUserPermissions entityToSave = null;
				entityToSave = chanelUserPermissionsRepository.findById(dto.getId());
				if (entityToSave == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("chanelUserPermissions -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}

				// Verify if chanelPermissions exist
				if (dto.getIdPermission() != null && dto.getIdPermission() > 0){
					ChanelPermissions existingChanelPermissions = chanelPermissionsRepository.findOne(dto.getIdPermission());
					if (existingChanelPermissions == null) {
						response.setStatus(functionalError.DATA_NOT_EXIST("chanelPermissions -> " + dto.getIdPermission(), locale));
						response.setHasError(true);
						return response;
					}
					entityToSave.setChanelPermissions(existingChanelPermissions);
				}
				// Verify if chanelUsers exist
				if (dto.getIdUser() != null && dto.getIdUser() > 0){
					ChanelUsers existingChanelUsers = chanelUsersRepository.findOne(dto.getIdUser());
					if (existingChanelUsers == null) {
						response.setStatus(functionalError.DATA_NOT_EXIST("chanelUsers -> " + dto.getIdUser(), locale));
						response.setHasError(true);
						return response;
					}
					entityToSave.setChanelUsers(existingChanelUsers);
				}
				items.add(entityToSave);
			}

			if (!items.isEmpty()) {
				List<ChanelUserPermissions> itemsSaved = null;
				// maj les donnees en base
				itemsSaved = chanelUserPermissionsRepository.save((Iterable<ChanelUserPermissions>) items);
				if (itemsSaved == null) {
					response.setStatus(functionalError.SAVE_FAIL("chanelUserPermissions", locale));
					response.setHasError(true);
					return response;
				}
				List<ChanelUserPermissionsDto> itemsDto = new ArrayList<ChanelUserPermissionsDto>();
				for (ChanelUserPermissions entity : itemsSaved) {
					ChanelUserPermissionsDto dto = getFullInfos(entity, locale);
					if (dto == null) continue;
					itemsDto.add(dto);
				}
				response.setItemsChanelUserPermissions(itemsDto);
				response.setHasError(false);
			}

			slf4jLogger.info("----end-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}

	/**
	 * delete ChanelUserPermissions by using ChanelUserPermissionsDto as object.
	 * 
	 * @param request
	 * @return response
	 * 
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	@Override
	public Response delete(Request request, Locale locale)  {
		slf4jLogger.info("----begin delete-----");
		
		response = new Response();
		
		try {
			List<ChanelUserPermissions> items = new ArrayList<ChanelUserPermissions>();
			
			for (ChanelUserPermissionsDto dto : request.getDatasChanelUserPermissions()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				fieldsToVerify.put("id", dto.getId());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				// Verifier si la chanelUserPermissions existe
				ChanelUserPermissions existingEntity = null;
				existingEntity = chanelUserPermissionsRepository.findById(dto.getId());
				if (existingEntity == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("chanelUserPermissions -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}

				// --------------------------------------------------------------------------------------------
				// --------------------------------- CHECK IF DATA IS USED
				// --------------------------------------------------------------------------------------------



				items.add(existingEntity);
			}

			if (!items.isEmpty()) {
				// supprimer les donnees en base
				chanelUserPermissionsRepository.delete((Iterable<ChanelUserPermissions>) items);

				response.setHasError(false);
			}

			slf4jLogger.info("----end-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}

	/**
	 * get ChanelUserPermissions by using ChanelUserPermissionsDto as object.
	 * 
	 * @param request
	 * @return response
	 * 
	 */
	@SuppressWarnings("unused")
	@Override
	public Response getByCriteria(Request request, Locale locale) {
		slf4jLogger.info("----begin-----");
		
		response = new Response();
		
		try {
			List<ChanelUserPermissions> items = null;
			items = chanelUserPermissionsRepository.getByCriteria(request, em);
			if (items != null && !items.isEmpty()) {
				List<ChanelUserPermissionsDto> itemsDto = new ArrayList<ChanelUserPermissionsDto>();
				for (ChanelUserPermissions entity : items) {
					ChanelUserPermissionsDto dto = getFullInfos(entity, locale);
					if (dto == null) continue;
					itemsDto.add(dto);
				}
				response.setItemsChanelUserPermissions(itemsDto);
				response.setCount(chanelUserPermissionsRepository.count(request, em));
				response.setHasError(false);
			} else {
				response.setStatus(functionalError.DATA_EMPTY("chanelUserPermissions", locale));
				response.setHasError(false);
				return response;
			}

			slf4jLogger.info("----end-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}

	/**
	 * get full ChanelUserPermissionsDto by using ChanelUserPermissions as object.
	 * 
	 * @param entity, locale
	 * @return ChanelUserPermissionsDto
	 * 
	 */
	private ChanelUserPermissionsDto getFullInfos(ChanelUserPermissions entity, Locale locale) throws Exception {
		ChanelUserPermissionsDto dto = ChanelUserPermissionsTransformer.INSTANCE.toDto(entity);
		if (dto == null){
			return null;
		}

		return dto;
	}
}
