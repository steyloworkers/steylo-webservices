
/*
 * Java transformer for entity table abonne 
 * Created on dec ( Time 21:51:06 )
 * Generator tool : Telosys Tools Generator ( version 2.1.1 )
 */

package com.ino.steylo.business;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Date;
import java.util.Locale;
import java.util.Map;
import java.util.ArrayList;
import java.util.HashMap;

import javax.persistence.PersistenceContext;
import javax.persistence.EntityManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.dao.PermissionDeniedDataAccessException;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.ino.steylo.helper.dto.*;
import com.ino.steylo.helper.dto.transformer.*;
import com.ino.steylo.helper.contrat.*;
import com.ino.steylo.dao.entity.*;
import com.ino.steylo.helper.*;
import com.ino.steylo.dao.repository.*;

/**
BUSINESS for table "abonne"
 * 
 * @author ino
 *
 */
@Component
public class AbonneBusiness implements IBasicBusiness<Request, Response> {


	private Response response;
	
	@Autowired
	private AbonneRepository abonneRepository;
	@Autowired
	private AbonneDemandeRepository abonneDemandeRepository;
	@Autowired
	private CountryRepository countryRepository;
	
	@Autowired
	private FunctionalError functionalError;
	
	@Autowired
	private TechnicalError technicalError;

	@Autowired
	private ExceptionUtils exceptionUtils;
	
	@PersistenceContext
	private EntityManager em;
	
	private Logger slf4jLogger;

	private SimpleDateFormat dateFormat;

	public AbonneBusiness() {
		dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
		slf4jLogger = LoggerFactory.getLogger(getClass());
	}

	
	/**
	 * create Abonne by using AbonneDto as object.
	 * 
	 * @param request
	 * @return response
	 * 
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	@Override
	public Response create(Request request, Locale locale)  {
		slf4jLogger.info("----begin create-----");
		
		response = new Response();
		
		try {
			List<Abonne> items = new ArrayList<Abonne>();
			
			for (AbonneDto dto : request.getDatasAbonne()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				fieldsToVerify.put("dateEnreg", dto.getDateEnreg());
				fieldsToVerify.put("nom", dto.getNom());
				fieldsToVerify.put("prenoms", dto.getPrenoms());
				fieldsToVerify.put("sexe", dto.getSexe());
				fieldsToVerify.put("dateNaissance", dto.getDateNaissance());
				fieldsToVerify.put("profession", dto.getProfession());
				fieldsToVerify.put("mobileNumber", dto.getMobileNumber());
				fieldsToVerify.put("address", dto.getAddress());
				fieldsToVerify.put("pays", dto.getPays());
				fieldsToVerify.put("ville", dto.getVille());
				fieldsToVerify.put("nationalite", dto.getNationalite());
				fieldsToVerify.put("idTypeUtilisateur", dto.getIdTypeUtilisateur());
				fieldsToVerify.put("metadata", dto.getMetadata());
				fieldsToVerify.put("login", dto.getLogin());
				fieldsToVerify.put("password", dto.getPassword());
				fieldsToVerify.put("email", dto.getEmail());
				fieldsToVerify.put("isActive", dto.getIsActive());
				fieldsToVerify.put("longitude", dto.getLongitude());
				fieldsToVerify.put("latitude", dto.getLatitude());
				fieldsToVerify.put("photoIdentite", dto.getPhotoIdentite());
				fieldsToVerify.put("photoPieceRecto", dto.getPhotoPieceRecto());
				fieldsToVerify.put("photoPieceVerso", dto.getPhotoPieceVerso());
				fieldsToVerify.put("communaute", dto.getCommunaute());
				fieldsToVerify.put("extPhotoIdentite", dto.getExtPhotoIdentite());
				fieldsToVerify.put("extPhotoRecto", dto.getExtPhotoRecto());
				fieldsToVerify.put("extPhotoVerso", dto.getExtPhotoVerso());
				fieldsToVerify.put("verifKey", dto.getVerifKey());
				fieldsToVerify.put("seen", dto.getSeen());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				// Verify if abonne to insert do not exist
				Abonne existingEntity = null;
				//TODO: add/replace by the best method
				if (existingEntity != null) {
					response.setStatus(functionalError.DATA_EXIST("abonne -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}

				// Verify if country exist
				Country existingCountry = countryRepository.findOne(dto.getPays());
				if (existingCountry == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("country -> " + dto.getPays(), locale));
					response.setHasError(true);
					return response;
				}
				Abonne entityToSave = null;
				entityToSave = AbonneTransformer.INSTANCE.toEntity(dto, existingCountry);
				items.add(entityToSave);
			}

			if (!items.isEmpty()) {
				List<Abonne> itemsSaved = null;
				// inserer les donnees en base de donnees
				itemsSaved = abonneRepository.save((Iterable<Abonne>) items);
				if (itemsSaved == null) {
					response.setStatus(functionalError.SAVE_FAIL("abonne", locale));
					response.setHasError(true);
					return response;
				}
				List<AbonneDto> itemsDto = new ArrayList<AbonneDto>();
				for (Abonne entity : itemsSaved) {
					AbonneDto dto = getFullInfos(entity, locale);
					if (dto == null) continue;
					itemsDto.add(dto);
				}
				response.setItemsAbonne(itemsDto);
				response.setHasError(false);
			}

			slf4jLogger.info("----end-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}

	/**
	 * update Abonne by using AbonneDto as object.
	 * 
	 * @param request
	 * @return response
	 * 
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	@Override
	public Response update(Request request, Locale locale)  {
		slf4jLogger.info("----begin update-----");
		
		response = new Response();
		
		try {
			List<Abonne> items = new ArrayList<Abonne>();
			
			for (AbonneDto dto : request.getDatasAbonne()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				fieldsToVerify.put("id", dto.getId());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				// Verifier si la abonne existe
				Abonne entityToSave = null;
				entityToSave = abonneRepository.findById(dto.getId());
				if (entityToSave == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("abonne -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}

				// Verify if country exist
				if (dto.getPays() != null && dto.getPays() > 0){
					Country existingCountry = countryRepository.findOne(dto.getPays());
					if (existingCountry == null) {
						response.setStatus(functionalError.DATA_NOT_EXIST("country -> " + dto.getPays(), locale));
						response.setHasError(true);
						return response;
					}
					entityToSave.setCountry(existingCountry);
				}
				if (dto.getDateEnreg()!= null && !dto.getDateEnreg().isEmpty()) {
					entityToSave.setDateEnreg(new SimpleDateFormat("dd/MM/yyyy").parse(dto.getDateEnreg()));
				}
				if (dto.getNom()!= null && !dto.getNom().isEmpty()) {
					entityToSave.setNom(dto.getNom());
				}
				if (dto.getPrenoms()!= null && !dto.getPrenoms().isEmpty()) {
					entityToSave.setPrenoms(dto.getPrenoms());
				}
				if (dto.getSexe()!= null && !dto.getSexe().isEmpty()) {
					entityToSave.setSexe(dto.getSexe());
				}
				if (dto.getDateNaissance()!= null && !dto.getDateNaissance().isEmpty()) {
					entityToSave.setDateNaissance(dto.getDateNaissance());
				}
				if (dto.getProfession()!= null && !dto.getProfession().isEmpty()) {
					entityToSave.setProfession(dto.getProfession());
				}
				if (dto.getMobileNumber()!= null && !dto.getMobileNumber().isEmpty()) {
					entityToSave.setMobileNumber(dto.getMobileNumber());
				}
				if (dto.getAddress()!= null && !dto.getAddress().isEmpty()) {
					entityToSave.setAddress(dto.getAddress());
				}
				if (dto.getVille()!= null && !dto.getVille().isEmpty()) {
					entityToSave.setVille(dto.getVille());
				}
				if (dto.getNationalite()!= null && !dto.getNationalite().isEmpty()) {
					entityToSave.setNationalite(dto.getNationalite());
				}
				if (dto.getIdTypeUtilisateur()!= null && dto.getIdTypeUtilisateur() > 0) {
					entityToSave.setIdTypeUtilisateur(dto.getIdTypeUtilisateur());
				}
				if (dto.getMetadata()!= null && !dto.getMetadata().isEmpty()) {
					entityToSave.setMetadata(dto.getMetadata());
				}
				if (dto.getLogin()!= null && !dto.getLogin().isEmpty()) {
					entityToSave.setLogin(dto.getLogin());
				}
				if (dto.getPassword()!= null && !dto.getPassword().isEmpty()) {
					entityToSave.setPassword(dto.getPassword());
				}
				if (dto.getEmail()!= null && !dto.getEmail().isEmpty()) {
					entityToSave.setEmail(dto.getEmail());
				}
				if (dto.getIsActive()!= null) {
					entityToSave.setIsActive(dto.getIsActive());
				}
				if (dto.getLongitude()!= null && dto.getLongitude() > 0) {
					entityToSave.setLongitude(dto.getLongitude());
				}
				if (dto.getLatitude()!= null && dto.getLatitude() > 0) {
					entityToSave.setLatitude(dto.getLatitude());
				}
				if (dto.getPhotoIdentite()!= null && !dto.getPhotoIdentite().isEmpty()) {
					entityToSave.setPhotoIdentite(dto.getPhotoIdentite());
				}
				if (dto.getPhotoPieceRecto()!= null && !dto.getPhotoPieceRecto().isEmpty()) {
					entityToSave.setPhotoPieceRecto(dto.getPhotoPieceRecto());
				}
				if (dto.getPhotoPieceVerso()!= null && !dto.getPhotoPieceVerso().isEmpty()) {
					entityToSave.setPhotoPieceVerso(dto.getPhotoPieceVerso());
				}
				if (dto.getCommunaute()!= null && dto.getCommunaute() > 0) {
					entityToSave.setCommunaute(dto.getCommunaute());
				}
				if (dto.getExtPhotoIdentite()!= null && !dto.getExtPhotoIdentite().isEmpty()) {
					entityToSave.setExtPhotoIdentite(dto.getExtPhotoIdentite());
				}
				if (dto.getExtPhotoRecto()!= null && !dto.getExtPhotoRecto().isEmpty()) {
					entityToSave.setExtPhotoRecto(dto.getExtPhotoRecto());
				}
				if (dto.getExtPhotoVerso()!= null && !dto.getExtPhotoVerso().isEmpty()) {
					entityToSave.setExtPhotoVerso(dto.getExtPhotoVerso());
				}
				if (dto.getVerifKey()!= null && !dto.getVerifKey().isEmpty()) {
					entityToSave.setVerifKey(dto.getVerifKey());
				}
				if (dto.getSeen()!= null && !dto.getSeen().isEmpty()) {
					entityToSave.setSeen(new SimpleDateFormat("dd/MM/yyyy").parse(dto.getSeen()));
				}
				items.add(entityToSave);
			}

			if (!items.isEmpty()) {
				List<Abonne> itemsSaved = null;
				// maj les donnees en base
				itemsSaved = abonneRepository.save((Iterable<Abonne>) items);
				if (itemsSaved == null) {
					response.setStatus(functionalError.SAVE_FAIL("abonne", locale));
					response.setHasError(true);
					return response;
				}
				List<AbonneDto> itemsDto = new ArrayList<AbonneDto>();
				for (Abonne entity : itemsSaved) {
					AbonneDto dto = getFullInfos(entity, locale);
					if (dto == null) continue;
					itemsDto.add(dto);
				}
				response.setItemsAbonne(itemsDto);
				response.setHasError(false);
			}

			slf4jLogger.info("----end-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}

	/**
	 * delete Abonne by using AbonneDto as object.
	 * 
	 * @param request
	 * @return response
	 * 
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	@Override
	public Response delete(Request request, Locale locale)  {
		slf4jLogger.info("----begin delete-----");
		
		response = new Response();
		
		try {
			List<Abonne> items = new ArrayList<Abonne>();
			
			for (AbonneDto dto : request.getDatasAbonne()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				fieldsToVerify.put("id", dto.getId());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				// Verifier si la abonne existe
				Abonne existingEntity = null;
				existingEntity = abonneRepository.findById(dto.getId());
				if (existingEntity == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("abonne -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}

				// --------------------------------------------------------------------------------------------
				// --------------------------------- CHECK IF DATA IS USED
				// --------------------------------------------------------------------------------------------

				// abonneDemande
				List<AbonneDemande> listOfAbonneDemande = abonneDemandeRepository.findByIdAbonne(existingEntity.getId());
				if (listOfAbonneDemande != null && !listOfAbonneDemande.isEmpty()){
					response.setStatus(functionalError.DATA_NOT_DELETABLE("(" + listOfAbonneDemande.size() + ")", locale));
					response.setHasError(true);
					return response;
				}


				items.add(existingEntity);
			}

			if (!items.isEmpty()) {
				// supprimer les donnees en base
				abonneRepository.delete((Iterable<Abonne>) items);

				response.setHasError(false);
			}

			slf4jLogger.info("----end-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}

	/**
	 * get Abonne by using AbonneDto as object.
	 * 
	 * @param request
	 * @return response
	 * 
	 */
	@SuppressWarnings("unused")
	@Override
	public Response getByCriteria(Request request, Locale locale) {
		slf4jLogger.info("----begin-----");
		
		response = new Response();
		
		try {
			List<Abonne> items = null;
			items = abonneRepository.getByCriteria(request, em);
			if (items != null && !items.isEmpty()) {
				List<AbonneDto> itemsDto = new ArrayList<AbonneDto>();
				for (Abonne entity : items) {
					AbonneDto dto = getFullInfos(entity, locale);
					if (dto == null) continue;
					itemsDto.add(dto);
				}
				response.setItemsAbonne(itemsDto);
				response.setCount(abonneRepository.count(request, em));
				response.setHasError(false);
			} else {
				response.setStatus(functionalError.DATA_EMPTY("abonne", locale));
				response.setHasError(false);
				return response;
			}

			slf4jLogger.info("----end-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}

	/**
	 * get full AbonneDto by using Abonne as object.
	 * 
	 * @param entity, locale
	 * @return AbonneDto
	 * 
	 */
	private AbonneDto getFullInfos(Abonne entity, Locale locale) throws Exception {
		AbonneDto dto = AbonneTransformer.INSTANCE.toDto(entity);
		if (dto == null){
			return null;
		}

		return dto;
	}
}
