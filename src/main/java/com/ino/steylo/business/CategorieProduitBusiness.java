
/*
 * Java transformer for entity table categorie_produit 
 * Created on dec ( Time 18:46:53 )
 * Generator tool : Telosys Tools Generator ( version 2.1.1 )
 */

package com.ino.steylo.business;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Date;
import java.util.Locale;
import java.util.Map;
import java.util.ArrayList;
import java.util.HashMap;

import javax.persistence.PersistenceContext;
import javax.persistence.EntityManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.dao.PermissionDeniedDataAccessException;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.ino.steylo.helper.dto.*;
import com.ino.steylo.helper.dto.transformer.*;
import com.ino.steylo.helper.contrat.*;
import com.ino.steylo.dao.entity.*;
import com.ino.steylo.helper.*;
import com.ino.steylo.dao.repository.*;

/**
BUSINESS for table "categorie_produit"
 * 
 * @author ino
 *
 */
@Component
public class CategorieProduitBusiness implements IBasicBusiness<Request, Response> {


	private Response response;
	
	@Autowired
	private CategorieProduitRepository categorieProduitRepository;
	@Autowired
	private ProduitRepository produitRepository;
	@Autowired
	private SousCategorieProduitRepository sousCategorieProduitRepository;
	
	@Autowired
	private FunctionalError functionalError;
	
	@Autowired
	private TechnicalError technicalError;

	@Autowired
	private ExceptionUtils exceptionUtils;
	
	@PersistenceContext
	private EntityManager em;
	
	private Logger slf4jLogger;

	private SimpleDateFormat dateFormat;

	public CategorieProduitBusiness() {
		dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
		slf4jLogger = LoggerFactory.getLogger(getClass());
	}

	
	/**
	 * create CategorieProduit by using CategorieProduitDto as object.
	 * 
	 * @param request
	 * @return response
	 * 
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	@Override
	public Response create(Request request, Locale locale)  {
		slf4jLogger.info("----begin create-----");
		
		response = new Response();
		
		try {
			List<CategorieProduit> items = new ArrayList<CategorieProduit>();
			
			for (CategorieProduitDto dto : request.getDatasCategorieProduit()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				fieldsToVerify.put("libelle", dto.getLibelle());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				// Verify if categorieProduit to insert do not exist
				CategorieProduit existingEntity = null;
				//TODO: add/replace by the best method
				if (existingEntity != null) {
					response.setStatus(functionalError.DATA_EXIST("categorieProduit -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}

				CategorieProduit entityToSave = null;
				entityToSave = CategorieProduitTransformer.INSTANCE.toEntity(dto);
				items.add(entityToSave);
			}

			if (!items.isEmpty()) {
				List<CategorieProduit> itemsSaved = null;
				// inserer les donnees en base de donnees
				itemsSaved = categorieProduitRepository.save((Iterable<CategorieProduit>) items);
				if (itemsSaved == null) {
					response.setStatus(functionalError.SAVE_FAIL("categorieProduit", locale));
					response.setHasError(true);
					return response;
				}
				List<CategorieProduitDto> itemsDto = new ArrayList<CategorieProduitDto>();
				for (CategorieProduit entity : itemsSaved) {
					CategorieProduitDto dto = getFullInfos(entity, locale);
					if (dto == null) continue;
					itemsDto.add(dto);
				}
				response.setItemsCategorieProduit(itemsDto);
				response.setHasError(false);
			}

			slf4jLogger.info("----end-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}

	/**
	 * update CategorieProduit by using CategorieProduitDto as object.
	 * 
	 * @param request
	 * @return response
	 * 
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	@Override
	public Response update(Request request, Locale locale)  {
		slf4jLogger.info("----begin update-----");
		
		response = new Response();
		
		try {
			List<CategorieProduit> items = new ArrayList<CategorieProduit>();
			
			for (CategorieProduitDto dto : request.getDatasCategorieProduit()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				fieldsToVerify.put("id", dto.getId());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				// Verifier si la categorieProduit existe
				CategorieProduit entityToSave = null;
				entityToSave = categorieProduitRepository.findById(dto.getId());
				if (entityToSave == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("categorieProduit -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}

				if (dto.getLibelle()!= null && !dto.getLibelle().isEmpty()) {
					entityToSave.setLibelle(dto.getLibelle());
				}
				items.add(entityToSave);
			}

			if (!items.isEmpty()) {
				List<CategorieProduit> itemsSaved = null;
				// maj les donnees en base
				itemsSaved = categorieProduitRepository.save((Iterable<CategorieProduit>) items);
				if (itemsSaved == null) {
					response.setStatus(functionalError.SAVE_FAIL("categorieProduit", locale));
					response.setHasError(true);
					return response;
				}
				List<CategorieProduitDto> itemsDto = new ArrayList<CategorieProduitDto>();
				for (CategorieProduit entity : itemsSaved) {
					CategorieProduitDto dto = getFullInfos(entity, locale);
					if (dto == null) continue;
					itemsDto.add(dto);
				}
				response.setItemsCategorieProduit(itemsDto);
				response.setHasError(false);
			}

			slf4jLogger.info("----end-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}

	/**
	 * delete CategorieProduit by using CategorieProduitDto as object.
	 * 
	 * @param request
	 * @return response
	 * 
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	@Override
	public Response delete(Request request, Locale locale)  {
		slf4jLogger.info("----begin delete-----");
		
		response = new Response();
		
		try {
			List<CategorieProduit> items = new ArrayList<CategorieProduit>();
			
			for (CategorieProduitDto dto : request.getDatasCategorieProduit()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				fieldsToVerify.put("id", dto.getId());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				// Verifier si la categorieProduit existe
				CategorieProduit existingEntity = null;
				existingEntity = categorieProduitRepository.findById(dto.getId());
				if (existingEntity == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("categorieProduit -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}

				// --------------------------------------------------------------------------------------------
				// --------------------------------- CHECK IF DATA IS USED
				// --------------------------------------------------------------------------------------------

				// produit
				List<Produit> listOfProduit = produitRepository.findByIdCategorie(existingEntity.getId());
				if (listOfProduit != null && !listOfProduit.isEmpty()){
					response.setStatus(functionalError.DATA_NOT_DELETABLE("(" + listOfProduit.size() + ")", locale));
					response.setHasError(true);
					return response;
				}
				// sousCategorieProduit
				List<SousCategorieProduit> listOfSousCategorieProduit = sousCategorieProduitRepository.findByIdCategorie(existingEntity.getId());
				if (listOfSousCategorieProduit != null && !listOfSousCategorieProduit.isEmpty()){
					response.setStatus(functionalError.DATA_NOT_DELETABLE("(" + listOfSousCategorieProduit.size() + ")", locale));
					response.setHasError(true);
					return response;
				}


				items.add(existingEntity);
			}

			if (!items.isEmpty()) {
				// supprimer les donnees en base
				categorieProduitRepository.delete((Iterable<CategorieProduit>) items);

				response.setHasError(false);
			}

			slf4jLogger.info("----end-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}

	/**
	 * get CategorieProduit by using CategorieProduitDto as object.
	 * 
	 * @param request
	 * @return response
	 * 
	 */
	@SuppressWarnings("unused")
	@Override
	public Response getByCriteria(Request request, Locale locale) {
		slf4jLogger.info("----begin-----");
		
		response = new Response();
		
		try {
			List<CategorieProduit> items = null;
			items = categorieProduitRepository.getByCriteria(request, em);
			if (items != null && !items.isEmpty()) {
				List<CategorieProduitDto> itemsDto = new ArrayList<CategorieProduitDto>();
				for (CategorieProduit entity : items) {
					CategorieProduitDto dto = getFullInfos(entity, locale);
					if (dto == null) continue;
					itemsDto.add(dto);
				}
				response.setItemsCategorieProduit(itemsDto);
				response.setCount(categorieProduitRepository.count(request, em));
				response.setHasError(false);
			} else {
				response.setStatus(functionalError.DATA_EMPTY("categorieProduit", locale));
				response.setHasError(false);
				return response;
			}

			slf4jLogger.info("----end-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}

	/**
	 * get CategorieProduit by using CategorieProduitDto as object.
	 *
	 * @param request
	 * @return response
	 *
	 */
	@SuppressWarnings("unused")
	public Response getCategorieByMarque(Request request, Locale locale) {
		slf4jLogger.info("----begin-----");

		response = new Response();

		try {
			CategorieProduitDto categorieProduitDto = request.getDataCategorieProduit();
			List<CategorieProduit> items = categorieProduitRepository.findByIdMarque(categorieProduitDto.getIdMarque());
			if (items != null && !items.isEmpty()) {
				List<CategorieProduitDto> itemsDto = new ArrayList<>();
				for (CategorieProduit entity : items) {
					CategorieProduitDto dto = getFullInfos(entity, locale);
					if (dto == null) continue;
					itemsDto.add(dto);
				}
				response.setItemsCategorieProduit(itemsDto);
				response.setCount(categorieProduitRepository.count(request, em));
				response.setHasError(false);
			} else {
				response.setStatus(functionalError.DATA_EMPTY("categorieProduit", locale));
				response.setCount(0L);
				response.setHasError(false);
				return response;
			}

			slf4jLogger.info("----end-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}

	/**
	 * get full CategorieProduitDto by using CategorieProduit as object.
	 * 
	 * @param entity, locale
	 * @return CategorieProduitDto
	 * 
	 */
	private CategorieProduitDto getFullInfos(CategorieProduit entity, Locale locale) throws Exception {
		CategorieProduitDto dto = CategorieProduitTransformer.INSTANCE.toDto(entity);
		if (dto == null){
			return null;
		}

		return dto;
	}
}
