
/*
 * Java transformer for entity table secteur_activite 
 * Created on dec ( Time 10:56:06 )
 * Generator tool : Telosys Tools Generator ( version 2.1.1 )
 */

package com.ino.steylo.business;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Date;
import java.util.Locale;
import java.util.Map;
import java.util.ArrayList;
import java.util.HashMap;

import javax.persistence.PersistenceContext;
import javax.persistence.EntityManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.dao.PermissionDeniedDataAccessException;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.ino.steylo.helper.dto.*;
import com.ino.steylo.helper.dto.transformer.*;
import com.ino.steylo.helper.contrat.*;
import com.ino.steylo.dao.entity.*;
import com.ino.steylo.helper.*;
import com.ino.steylo.dao.repository.*;

/**
BUSINESS for table "secteur_activite"
 * 
 * @author ino
 *
 */
@Component
public class SecteurActiviteBusiness implements IBasicBusiness<Request, Response> {


	private Response response;
	
	@Autowired
	private SecteurActiviteRepository secteurActiviteRepository;
	@Autowired
	private ChanelRepository chanelRepository;
	
	@Autowired
	private FunctionalError functionalError;
	
	@Autowired
	private TechnicalError technicalError;

	@Autowired
	private ExceptionUtils exceptionUtils;
	
	@PersistenceContext
	private EntityManager em;
	
	private Logger slf4jLogger;

	private SimpleDateFormat dateFormat;

	public SecteurActiviteBusiness() {
		dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
		slf4jLogger = LoggerFactory.getLogger(getClass());
	}

	
	/**
	 * create SecteurActivite by using SecteurActiviteDto as object.
	 * 
	 * @param request
	 * @return response
	 * 
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	@Override
	public Response create(Request request, Locale locale)  {
		slf4jLogger.info("----begin create-----");
		
		response = new Response();
		
		try {
			List<SecteurActivite> items = new ArrayList<SecteurActivite>();
			
			for (SecteurActiviteDto dto : request.getDatasSecteurActivite()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				fieldsToVerify.put("libelle", dto.getLibelle());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				// Verify if secteurActivite to insert do not exist
				SecteurActivite existingEntity = null;
				//TODO: add/replace by the best method
				if (existingEntity != null) {
					response.setStatus(functionalError.DATA_EXIST("secteurActivite -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}

				SecteurActivite entityToSave = null;
				entityToSave = SecteurActiviteTransformer.INSTANCE.toEntity(dto);
				items.add(entityToSave);
			}

			if (!items.isEmpty()) {
				List<SecteurActivite> itemsSaved = null;
				// inserer les donnees en base de donnees
				itemsSaved = secteurActiviteRepository.save((Iterable<SecteurActivite>) items);
				if (itemsSaved == null) {
					response.setStatus(functionalError.SAVE_FAIL("secteurActivite", locale));
					response.setHasError(true);
					return response;
				}
				List<SecteurActiviteDto> itemsDto = new ArrayList<SecteurActiviteDto>();
				for (SecteurActivite entity : itemsSaved) {
					SecteurActiviteDto dto = getFullInfos(entity, locale);
					if (dto == null) continue;
					itemsDto.add(dto);
				}
				response.setItemsSecteurActivite(itemsDto);
				response.setHasError(false);
			}

			slf4jLogger.info("----end-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}

	/**
	 * update SecteurActivite by using SecteurActiviteDto as object.
	 * 
	 * @param request
	 * @return response
	 * 
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	@Override
	public Response update(Request request, Locale locale)  {
		slf4jLogger.info("----begin update-----");
		
		response = new Response();
		
		try {
			List<SecteurActivite> items = new ArrayList<SecteurActivite>();
			
			for (SecteurActiviteDto dto : request.getDatasSecteurActivite()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				fieldsToVerify.put("id", dto.getId());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				// Verifier si la secteurActivite existe
				SecteurActivite entityToSave = null;
				entityToSave = secteurActiviteRepository.findById(dto.getId());
				if (entityToSave == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("secteurActivite -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}

				if (dto.getLibelle()!= null && !dto.getLibelle().isEmpty()) {
					entityToSave.setLibelle(dto.getLibelle());
				}
				items.add(entityToSave);
			}

			if (!items.isEmpty()) {
				List<SecteurActivite> itemsSaved = null;
				// maj les donnees en base
				itemsSaved = secteurActiviteRepository.save((Iterable<SecteurActivite>) items);
				if (itemsSaved == null) {
					response.setStatus(functionalError.SAVE_FAIL("secteurActivite", locale));
					response.setHasError(true);
					return response;
				}
				List<SecteurActiviteDto> itemsDto = new ArrayList<SecteurActiviteDto>();
				for (SecteurActivite entity : itemsSaved) {
					SecteurActiviteDto dto = getFullInfos(entity, locale);
					if (dto == null) continue;
					itemsDto.add(dto);
				}
				response.setItemsSecteurActivite(itemsDto);
				response.setHasError(false);
			}

			slf4jLogger.info("----end-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}

	/**
	 * delete SecteurActivite by using SecteurActiviteDto as object.
	 * 
	 * @param request
	 * @return response
	 * 
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	@Override
	public Response delete(Request request, Locale locale)  {
		slf4jLogger.info("----begin delete-----");
		
		response = new Response();
		
		try {
			List<SecteurActivite> items = new ArrayList<SecteurActivite>();
			
			for (SecteurActiviteDto dto : request.getDatasSecteurActivite()) {
				// Definir les parametres obligatoires
				Map<String, Object> fieldsToVerify = new HashMap<String, Object>();
				fieldsToVerify.put("id", dto.getId());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				// Verifier si la secteurActivite existe
				SecteurActivite existingEntity = null;
				existingEntity = secteurActiviteRepository.findById(dto.getId());
				if (existingEntity == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("secteurActivite -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}

				// --------------------------------------------------------------------------------------------
				// --------------------------------- CHECK IF DATA IS USED
				// --------------------------------------------------------------------------------------------

				// chanel
				List<Chanel> listOfChanel = chanelRepository.findByIdSecteurActivite(existingEntity.getId());
				if (listOfChanel != null && !listOfChanel.isEmpty()){
					response.setStatus(functionalError.DATA_NOT_DELETABLE("(" + listOfChanel.size() + ")", locale));
					response.setHasError(true);
					return response;
				}


				items.add(existingEntity);
			}

			if (!items.isEmpty()) {
				// supprimer les donnees en base
				secteurActiviteRepository.delete((Iterable<SecteurActivite>) items);

				response.setHasError(false);
			}

			slf4jLogger.info("----end-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}

	/**
	 * get SecteurActivite by using SecteurActiviteDto as object.
	 * 
	 * @param request
	 * @return response
	 * 
	 */
	@SuppressWarnings("unused")
	@Override
	public Response getByCriteria(Request request, Locale locale) {
		slf4jLogger.info("----begin-----");
		
		response = new Response();
		
		try {
			List<SecteurActivite> items = null;
			items = secteurActiviteRepository.getByCriteria(request, em);
			if (items != null && !items.isEmpty()) {
				List<SecteurActiviteDto> itemsDto = new ArrayList<SecteurActiviteDto>();
				for (SecteurActivite entity : items) {
					SecteurActiviteDto dto = getFullInfos(entity, locale);
					if (dto == null) continue;
					itemsDto.add(dto);
				}
				response.setItemsSecteurActivite(itemsDto);
				response.setCount(secteurActiviteRepository.count(request, em));
				response.setHasError(false);
			} else {
				response.setStatus(functionalError.DATA_EMPTY("secteurActivite", locale));
				response.setHasError(false);
				return response;
			}

			slf4jLogger.info("----end-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage());
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}

	/**
	 * get full SecteurActiviteDto by using SecteurActivite as object.
	 * 
	 * @param entity, locale
	 * @return SecteurActiviteDto
	 * 
	 */
	private SecteurActiviteDto getFullInfos(SecteurActivite entity, Locale locale) throws Exception {
		SecteurActiviteDto dto = SecteurActiviteTransformer.INSTANCE.toDto(entity);
		if (dto == null){
			return null;
		}

		return dto;
	}
}
