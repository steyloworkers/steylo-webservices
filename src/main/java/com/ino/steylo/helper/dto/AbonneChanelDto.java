
/*
 * Java dto for entity table abonne_chanel 
 * Created on dec ( Time 18:46:52 )
 * Generator tool : Telosys Tools Generator ( version 2.1.1 )
 */

package com.ino.steylo.helper.dto;

import java.util.Date;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;



/**
 * DTO for table "abonne_chanel"
 * 
 * @author ino
 *
 */
@JsonInclude(Include.NON_NULL)
public class AbonneChanelDto {

    private Long       id                   ; // Primary Key

    private Integer    idChanel             ;
    private Integer    idAbonne             ;
	private String     dateDesouscription   ;
	private String     dateSouscription     ;
    private String     metadata             ;
    private Boolean    isActive             ;
    private Boolean    isSeller             ;

    //----------------------------------------------------------------------
    // ENTITY LINKS FIELD ( RELATIONSHIP )
    //----------------------------------------------------------------------



    /**
     * Default constructor
     */
    public AbonneChanelDto()
    {
        super();
    }
    
    //----------------------------------------------------------------------
    // GETTER(S) & SETTER(S) FOR THE PRIMARY KEY 
    //----------------------------------------------------------------------
    /**
     * Set the "id" field value
     * @param id
     */
    public void setId( Long id ){
        this.id = id;
    }
    /**
     * Get the "id" field value
     * @return the field value
     */
    public Long getId(){
        return this.id;
    }


    //----------------------------------------------------------------------
    // GETTER(S) & SETTER(S) FOR DATA FIELDS
    //----------------------------------------------------------------------
    /**
     * Set the "idChanel" field value
     * @param idChanel
     */
	public void setIdChanel( Integer idChanel )
    {
        this.idChanel = idChanel ;
    }
    /**
     * Get the "idChanel" field value
     * @return the field value
     */
	public Integer getIdChanel()
    {
        return this.idChanel;
    }

    /**
     * Set the "idAbonne" field value
     * @param idAbonne
     */
	public void setIdAbonne( Integer idAbonne )
    {
        this.idAbonne = idAbonne ;
    }
    /**
     * Get the "idAbonne" field value
     * @return the field value
     */
	public Integer getIdAbonne()
    {
        return this.idAbonne;
    }

    /**
     * Set the "dateDesouscription" field value
     * @param dateDesouscription
     */
	public void setDateDesouscription( String dateDesouscription )
    {
        this.dateDesouscription = dateDesouscription ;
    }
    /**
     * Get the "dateDesouscription" field value
     * @return the field value
     */
	public String getDateDesouscription()
    {
        return this.dateDesouscription;
    }

    /**
     * Set the "dateSouscription" field value
     * @param dateSouscription
     */
	public void setDateSouscription( String dateSouscription )
    {
        this.dateSouscription = dateSouscription ;
    }
    /**
     * Get the "dateSouscription" field value
     * @return the field value
     */
	public String getDateSouscription()
    {
        return this.dateSouscription;
    }

    /**
     * Set the "metadata" field value
     * @param metadata
     */
	public void setMetadata( String metadata )
    {
        this.metadata = metadata ;
    }
    /**
     * Get the "metadata" field value
     * @return the field value
     */
	public String getMetadata()
    {
        return this.metadata;
    }

    /**
     * Set the "isActive" field value
     * @param isActive
     */
	public void setIsActive( Boolean isActive )
    {
        this.isActive = isActive ;
    }
    /**
     * Get the "isActive" field value
     * @return the field value
     */
	public Boolean getIsActive()
    {
        return this.isActive;
    }

    /**
     * Set the "isSeller" field value
     * @param isSeller
     */
	public void setIsSeller( Boolean isSeller )
    {
        this.isSeller = isSeller ;
    }
    /**
     * Get the "isSeller" field value
     * @return the field value
     */
	public Boolean getIsSeller()
    {
        return this.isSeller;
    }


    //----------------------------------------------------------------------
    // GETTERS & SETTERS FOR LINKS FIELD ( RELATIONSHIP )
    //----------------------------------------------------------------------


	//----------------------------------------------------------------------
    // equals METHOD
    //----------------------------------------------------------------------
	public boolean equals(Object obj) { 
		if ( this == obj ) return true ; 
		if ( obj == null ) return false ;
		if ( this.getClass() != obj.getClass() ) return false ; 
		AbonneChanelDto other = (AbonneChanelDto) obj; 
		//--- Attribute id
		if ( id == null ) { 
			if ( other.id != null ) 
				return false ; 
		} else if ( ! id.equals(other.id) ) 
			return false ; 
		return true; 
	} 


    //----------------------------------------------------------------------
    // toString METHOD
    //----------------------------------------------------------------------
    public String toString() {
		StringBuffer sb = new StringBuffer(); 
		sb.append(id); 
		sb.append("|"); 
		sb.append(idChanel); 
		sb.append("|"); 
		sb.append(idAbonne); 
		sb.append("|"); 
		sb.append(dateDesouscription); 
		sb.append("|"); 
		sb.append(dateSouscription); 
		sb.append("|"); 
		sb.append(metadata); 
		sb.append("|"); 
		sb.append(isActive); 
		sb.append("|"); 
		sb.append(isSeller); 
        return sb.toString();
    }
}
