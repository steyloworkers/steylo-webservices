
/*
 * Java dto for entity table permission_profil 
 * Created on dec ( Time 18:46:56 )
 * Generator tool : Telosys Tools Generator ( version 2.1.1 )
 */

package com.ino.steylo.helper.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;



/**
 * DTO for table "permission_profil"
 * 
 * @author ino
 *
 */
@JsonInclude(Include.NON_NULL)
public class PermissionProfilDto {

    private Integer    id                   ; // Primary Key

    private String     libelle              ;
    private Integer    idChanel             ;

    //----------------------------------------------------------------------
    // ENTITY LINKS FIELD ( RELATIONSHIP )
    //----------------------------------------------------------------------



    /**
     * Default constructor
     */
    public PermissionProfilDto()
    {
        super();
    }
    
    //----------------------------------------------------------------------
    // GETTER(S) & SETTER(S) FOR THE PRIMARY KEY 
    //----------------------------------------------------------------------
    /**
     * Set the "id" field value
     * @param id
     */
    public void setId( Integer id ){
        this.id = id;
    }
    /**
     * Get the "id" field value
     * @return the field value
     */
    public Integer getId(){
        return this.id;
    }


    //----------------------------------------------------------------------
    // GETTER(S) & SETTER(S) FOR DATA FIELDS
    //----------------------------------------------------------------------
    /**
     * Set the "libelle" field value
     * @param libelle
     */
	public void setLibelle( String libelle )
    {
        this.libelle = libelle ;
    }
    /**
     * Get the "libelle" field value
     * @return the field value
     */
	public String getLibelle()
    {
        return this.libelle;
    }

    /**
     * Set the "idChanel" field value
     * @param idChanel
     */
	public void setIdChanel( Integer idChanel )
    {
        this.idChanel = idChanel ;
    }
    /**
     * Get the "idChanel" field value
     * @return the field value
     */
	public Integer getIdChanel()
    {
        return this.idChanel;
    }


    //----------------------------------------------------------------------
    // GETTERS & SETTERS FOR LINKS FIELD ( RELATIONSHIP )
    //----------------------------------------------------------------------


	//----------------------------------------------------------------------
    // equals METHOD
    //----------------------------------------------------------------------
	public boolean equals(Object obj) { 
		if ( this == obj ) return true ; 
		if ( obj == null ) return false ;
		if ( this.getClass() != obj.getClass() ) return false ; 
		PermissionProfilDto other = (PermissionProfilDto) obj; 
		//--- Attribute id
		if ( id == null ) { 
			if ( other.id != null ) 
				return false ; 
		} else if ( ! id.equals(other.id) ) 
			return false ; 
		return true; 
	} 


    //----------------------------------------------------------------------
    // toString METHOD
    //----------------------------------------------------------------------
    public String toString() {
		StringBuffer sb = new StringBuffer(); 
		sb.append(id); 
		sb.append("|"); 
		sb.append(libelle); 
        return sb.toString();
    }
}
