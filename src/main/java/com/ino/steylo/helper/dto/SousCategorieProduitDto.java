
/*
 * Java dto for entity table sous_categorie_produit 
 * Created on dec ( Time 18:46:57 )
 * Generator tool : Telosys Tools Generator ( version 2.1.1 )
 */

package com.ino.steylo.helper.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;



/**
 * DTO for table "sous_categorie_produit"
 * 
 * @author ino
 *
 */
@JsonInclude(Include.NON_NULL)
public class SousCategorieProduitDto {

    private Integer    id                   ; // Primary Key

    private Integer    idCategorie          ;
    private String     libelle              ;

    //----------------------------------------------------------------------
    // ENTITY LINKS FIELD ( RELATIONSHIP )
    //----------------------------------------------------------------------



    /**
     * Default constructor
     */
    public SousCategorieProduitDto()
    {
        super();
    }
    
    //----------------------------------------------------------------------
    // GETTER(S) & SETTER(S) FOR THE PRIMARY KEY 
    //----------------------------------------------------------------------
    /**
     * Set the "id" field value
     * @param id
     */
    public void setId( Integer id ){
        this.id = id;
    }
    /**
     * Get the "id" field value
     * @return the field value
     */
    public Integer getId(){
        return this.id;
    }


    //----------------------------------------------------------------------
    // GETTER(S) & SETTER(S) FOR DATA FIELDS
    //----------------------------------------------------------------------
    /**
     * Set the "idCategorie" field value
     * @param idCategorie
     */
	public void setIdCategorie( Integer idCategorie )
    {
        this.idCategorie = idCategorie ;
    }
    /**
     * Get the "idCategorie" field value
     * @return the field value
     */
	public Integer getIdCategorie()
    {
        return this.idCategorie;
    }

    /**
     * Set the "libelle" field value
     * @param libelle
     */
	public void setLibelle( String libelle )
    {
        this.libelle = libelle ;
    }
    /**
     * Get the "libelle" field value
     * @return the field value
     */
	public String getLibelle()
    {
        return this.libelle;
    }


    //----------------------------------------------------------------------
    // GETTERS & SETTERS FOR LINKS FIELD ( RELATIONSHIP )
    //----------------------------------------------------------------------


	//----------------------------------------------------------------------
    // equals METHOD
    //----------------------------------------------------------------------
	public boolean equals(Object obj) { 
		if ( this == obj ) return true ; 
		if ( obj == null ) return false ;
		if ( this.getClass() != obj.getClass() ) return false ; 
		SousCategorieProduitDto other = (SousCategorieProduitDto) obj; 
		//--- Attribute id
		if ( id == null ) { 
			if ( other.id != null ) 
				return false ; 
		} else if ( ! id.equals(other.id) ) 
			return false ; 
		return true; 
	} 


    //----------------------------------------------------------------------
    // toString METHOD
    //----------------------------------------------------------------------
    public String toString() {
		StringBuffer sb = new StringBuffer(); 
		sb.append(id); 
		sb.append("|"); 
		sb.append(libelle); 
        return sb.toString();
    }
}
