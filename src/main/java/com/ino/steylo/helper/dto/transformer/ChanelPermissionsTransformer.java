

/*
 * Java transformer for entity table chanel_permissions 
 * Created on dec ( Time 18:46:54 )
 * Generator tool : Telosys Tools Generator ( version 2.1.1 )
 */

package com.ino.steylo.helper.dto.transformer;

import java.text.ParseException;
import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import com.ino.steylo.helper.dto.*;
import com.ino.steylo.dao.entity.*;


/**
TRANSFORMER for table "chanel_permissions"
 * 
 * @author ino
 *
 */
@Mapper
public interface ChanelPermissionsTransformer {

	ChanelPermissionsTransformer INSTANCE = Mappers.getMapper(ChanelPermissionsTransformer.class);

	@Mappings({
	})
	ChanelPermissionsDto toDto(ChanelPermissions entity) throws ParseException;

    List<ChanelPermissionsDto> toDtos(List<ChanelPermissions> entities) throws ParseException;

	@Mappings({
		@Mapping(source="dto.id", target="id"),
		@Mapping(source="dto.code", target="code"),
		@Mapping(source="dto.libelle", target="libelle"),
	})
    ChanelPermissions toEntity(ChanelPermissionsDto dto) throws ParseException;

    //List<ChanelPermissions> toEntities(List<ChanelPermissionsDto> dtos) throws ParseException;

}
