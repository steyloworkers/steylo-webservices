
/*
 * Java dto for entity table image_produit 
 * Created on dec ( Time 23:50:38 )
 * Generator tool : Telosys Tools Generator ( version 2.1.1 )
 */

package com.ino.steylo.helper.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;



/**
 * DTO for table "image_produit"
 * 
 * @author ino
 *
 */
@JsonInclude(Include.NON_NULL)
public class ImageProduitDto {

    private Integer    id                   ; // Primary Key

    private Integer    idProduit            ;
    private String     image                ;
    private String     type                 ;
    private String     extImg               ;

    //----------------------------------------------------------------------
    // ENTITY LINKS FIELD ( RELATIONSHIP )
    //----------------------------------------------------------------------



    /**
     * Default constructor
     */
    public ImageProduitDto()
    {
        super();
    }
    
    //----------------------------------------------------------------------
    // GETTER(S) & SETTER(S) FOR THE PRIMARY KEY 
    //----------------------------------------------------------------------
    /**
     * Set the "id" field value
     * @param id
     */
    public void setId( Integer id ){
        this.id = id;
    }
    /**
     * Get the "id" field value
     * @return the field value
     */
    public Integer getId(){
        return this.id;
    }


    //----------------------------------------------------------------------
    // GETTER(S) & SETTER(S) FOR DATA FIELDS
    //----------------------------------------------------------------------
    /**
     * Set the "idProduit" field value
     * @param idProduit
     */
	public void setIdProduit( Integer idProduit )
    {
        this.idProduit = idProduit ;
    }
    /**
     * Get the "idProduit" field value
     * @return the field value
     */
	public Integer getIdProduit()
    {
        return this.idProduit;
    }

    /**
     * Set the "image" field value
     * @param image
     */
	public void setImage( String image )
    {
        this.image = image ;
    }
    /**
     * Get the "image" field value
     * @return the field value
     */
	public String getImage()
    {
        return this.image;
    }

    /**
     * Set the "type" field value
     * @param type
     */
	public void setType( String type )
    {
        this.type = type ;
    }
    /**
     * Get the "type" field value
     * @return the field value
     */
	public String getType()
    {
        return this.type;
    }

    /**
     * Set the "extImg" field value
     * @param extImg
     */
	public void setExtImg( String extImg )
    {
        this.extImg = extImg ;
    }
    /**
     * Get the "extImg" field value
     * @return the field value
     */
	public String getExtImg()
    {
        return this.extImg;
    }


    //----------------------------------------------------------------------
    // GETTERS & SETTERS FOR LINKS FIELD ( RELATIONSHIP )
    //----------------------------------------------------------------------


	//----------------------------------------------------------------------
    // equals METHOD
    //----------------------------------------------------------------------
	public boolean equals(Object obj) { 
		if ( this == obj ) return true ; 
		if ( obj == null ) return false ;
		if ( this.getClass() != obj.getClass() ) return false ; 
		ImageProduitDto other = (ImageProduitDto) obj; 
		//--- Attribute id
		if ( id == null ) { 
			if ( other.id != null ) 
				return false ; 
		} else if ( ! id.equals(other.id) ) 
			return false ; 
		return true; 
	} 


    //----------------------------------------------------------------------
    // toString METHOD
    //----------------------------------------------------------------------
    public String toString() {
		StringBuffer sb = new StringBuffer(); 
		sb.append(id); 
		sb.append("|"); 
		sb.append(image); 
		sb.append("|"); 
		sb.append(type); 
		sb.append("|"); 
		sb.append(extImg); 
        return sb.toString();
    }
}
