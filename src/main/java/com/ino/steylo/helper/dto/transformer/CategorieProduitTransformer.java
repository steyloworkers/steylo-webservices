

/*
 * Java transformer for entity table categorie_produit 
 * Created on dec ( Time 18:46:53 )
 * Generator tool : Telosys Tools Generator ( version 2.1.1 )
 */

package com.ino.steylo.helper.dto.transformer;

import java.text.ParseException;
import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import com.ino.steylo.helper.dto.*;
import com.ino.steylo.dao.entity.*;


/**
TRANSFORMER for table "categorie_produit"
 * 
 * @author ino
 *
 */
@Mapper
public interface CategorieProduitTransformer {

	CategorieProduitTransformer INSTANCE = Mappers.getMapper(CategorieProduitTransformer.class);

	@Mappings({
	})
	CategorieProduitDto toDto(CategorieProduit entity) throws ParseException;

    List<CategorieProduitDto> toDtos(List<CategorieProduit> entities) throws ParseException;

	@Mappings({
		@Mapping(source="dto.id", target="id"),
		@Mapping(source="dto.libelle", target="libelle"),
	})
    CategorieProduit toEntity(CategorieProduitDto dto) throws ParseException;

    //List<CategorieProduit> toEntities(List<CategorieProduitDto> dtos) throws ParseException;

}
