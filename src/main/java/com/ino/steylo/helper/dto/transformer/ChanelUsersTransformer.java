

/*
 * Java transformer for entity table chanel_users 
 * Created on dec ( Time 01:34:01 )
 * Generator tool : Telosys Tools Generator ( version 2.1.1 )
 */

package com.ino.steylo.helper.dto.transformer;

import java.text.ParseException;
import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import com.ino.steylo.helper.dto.*;
import com.ino.steylo.dao.entity.*;


/**
TRANSFORMER for table "chanel_users"
 * 
 * @author ino
 *
 */
@Mapper
public interface ChanelUsersTransformer {

	ChanelUsersTransformer INSTANCE = Mappers.getMapper(ChanelUsersTransformer.class);

	@Mappings({
		@Mapping(source="entity.dateCreation", dateFormat="dd-MM-yyyy HH:mm:ss",target="dateCreation"),
		@Mapping(source="entity.updatedAt", dateFormat="dd-MM-yyyy HH:mm:ss",target="updatedAt"),
		@Mapping(source="entity.deleteAt", dateFormat="dd-MM-yyyy HH:mm:ss",target="deleteAt"),
		@Mapping(source="entity.chanelUsers3.id", target="updatedBy"),
		@Mapping(source="entity.chanelUsers2.id", target="deleteBy"),
		@Mapping(source="entity.chanel.id", target="idChanel"),
		@Mapping(source="entity.chanelUsers.id", target="createdBy"),
        @Mapping(source="entity.chanel.nomChanel", target="nomChanel"),
        @Mapping(source="entity.chanel.logo", target="logoChanel"),
        @Mapping(source="entity.chanel.extLogo", target="extLogoChanel"),
    })
	ChanelUsersDto toDto(ChanelUsers entity) throws ParseException;

    List<ChanelUsersDto> toDtos(List<ChanelUsers> entities) throws ParseException;

	@Mappings({
		@Mapping(source="dto.id", target="id"),
		@Mapping(source="dto.nom", target="nom"),
		@Mapping(source="dto.prenoms", target="prenoms"),
		@Mapping(source="dto.login", target="login"),
		@Mapping(source="dto.password", target="password"),
		@Mapping(source="dto.email", target="email"),
		@Mapping(source="dto.statut", target="statut"),
		@Mapping(source="dto.dateCreation", dateFormat="dd-MM-yyyy HH:mm:ss",target="dateCreation"),
		@Mapping(source="dto.updatedAt", dateFormat="dd-MM-yyyy HH:mm:ss",target="updatedAt"),
		@Mapping(source="dto.deleteAt", dateFormat="dd-MM-yyyy HH:mm:ss",target="deleteAt"),
		@Mapping(source="chanel", target="chanel"),
	})
    ChanelUsers toEntity(ChanelUsersDto dto, Chanel chanel) throws ParseException;

    //List<ChanelUsers> toEntities(List<ChanelUsersDto> dtos) throws ParseException;

}
