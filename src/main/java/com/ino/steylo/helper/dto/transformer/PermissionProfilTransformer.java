

/*
 * Java transformer for entity table permission_profil 
 * Created on dec ( Time 18:46:56 )
 * Generator tool : Telosys Tools Generator ( version 2.1.1 )
 */

package com.ino.steylo.helper.dto.transformer;

import java.text.ParseException;
import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import com.ino.steylo.helper.dto.*;
import com.ino.steylo.dao.entity.*;


/**
TRANSFORMER for table "permission_profil"
 * 
 * @author ino
 *
 */
@Mapper
public interface PermissionProfilTransformer {

	PermissionProfilTransformer INSTANCE = Mappers.getMapper(PermissionProfilTransformer.class);

	@Mappings({
		@Mapping(source="entity.chanel.id", target="idChanel"),
	})
	PermissionProfilDto toDto(PermissionProfil entity) throws ParseException;

    List<PermissionProfilDto> toDtos(List<PermissionProfil> entities) throws ParseException;

	@Mappings({
		@Mapping(source="dto.id", target="id"),
		@Mapping(source="dto.libelle", target="libelle"),
		@Mapping(source="chanel", target="chanel"),
	})
    PermissionProfil toEntity(PermissionProfilDto dto, Chanel chanel) throws ParseException;

    //List<PermissionProfil> toEntities(List<PermissionProfilDto> dtos) throws ParseException;

}
