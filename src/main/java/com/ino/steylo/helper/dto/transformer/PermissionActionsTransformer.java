

/*
 * Java transformer for entity table permission_actions 
 * Created on dec ( Time 18:46:56 )
 * Generator tool : Telosys Tools Generator ( version 2.1.1 )
 */

package com.ino.steylo.helper.dto.transformer;

import java.text.ParseException;
import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import com.ino.steylo.helper.dto.*;
import com.ino.steylo.dao.entity.*;


/**
TRANSFORMER for table "permission_actions"
 * 
 * @author ino
 *
 */
@Mapper
public interface PermissionActionsTransformer {

	PermissionActionsTransformer INSTANCE = Mappers.getMapper(PermissionActionsTransformer.class);

	@Mappings({
		@Mapping(source="entity.chanelPermissions.id", target="idPermission"),
		@Mapping(source="entity.chanelPermissions.code", target="chanelPermissionsCode"),
	})
	PermissionActionsDto toDto(PermissionActions entity) throws ParseException;

    List<PermissionActionsDto> toDtos(List<PermissionActions> entities) throws ParseException;

	@Mappings({
		@Mapping(source="dto.id", target="id"),
		@Mapping(source="dto.idActionProduit", target="idActionProduit"),
		@Mapping(source="dto.idActionAbonne", target="idActionAbonne"),
		@Mapping(source="dto.idActionParam", target="idActionParam"),
		@Mapping(source="chanelPermissions", target="chanelPermissions"),
	})
    PermissionActions toEntity(PermissionActionsDto dto, ChanelPermissions chanelPermissions) throws ParseException;

    //List<PermissionActions> toEntities(List<PermissionActionsDto> dtos) throws ParseException;

}
