
/*
 * Java dto for entity table chanel_permissions 
 * Created on dec ( Time 18:46:54 )
 * Generator tool : Telosys Tools Generator ( version 2.1.1 )
 */

package com.ino.steylo.helper.dto;

import java.util.List;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;



/**
 * DTO for table "chanel_permissions"
 * 
 * @author ino
 *
 */
@JsonInclude(Include.NON_NULL)
public class ChanelPermissionsDto {

    private Integer    id                   ; // Primary Key

    private Integer    code                 ;
    private String     libelle              ;

    //----------------------------------------------------------------------
    // ENTITY LINKS FIELD ( RELATIONSHIP )
    //----------------------------------------------------------------------



    /**
     * Default constructor
     */
    public ChanelPermissionsDto()
    {
        super();
    }
    
    //----------------------------------------------------------------------
    // GETTER(S) & SETTER(S) FOR THE PRIMARY KEY 
    //----------------------------------------------------------------------
    /**
     * Set the "id" field value
     * @param id
     */
    public void setId( Integer id ){
        this.id = id;
    }
    /**
     * Get the "id" field value
     * @return the field value
     */
    public Integer getId(){
        return this.id;
    }


    //----------------------------------------------------------------------
    // GETTER(S) & SETTER(S) FOR DATA FIELDS
    //----------------------------------------------------------------------
    /**
     * Set the "code" field value
     * @param code
     */
	public void setCode( Integer code )
    {
        this.code = code ;
    }
    /**
     * Get the "code" field value
     * @return the field value
     */
	public Integer getCode()
    {
        return this.code;
    }

    /**
     * Set the "libelle" field value
     * @param libelle
     */
	public void setLibelle( String libelle )
    {
        this.libelle = libelle ;
    }
    /**
     * Get the "libelle" field value
     * @return the field value
     */
	public String getLibelle()
    {
        return this.libelle;
    }


    //----------------------------------------------------------------------
    // GETTERS & SETTERS FOR LINKS FIELD ( RELATIONSHIP )
    //----------------------------------------------------------------------


	//----------------------------------------------------------------------
    // equals METHOD
    //----------------------------------------------------------------------
	public boolean equals(Object obj) { 
		if ( this == obj ) return true ; 
		if ( obj == null ) return false ;
		if ( this.getClass() != obj.getClass() ) return false ; 
		ChanelPermissionsDto other = (ChanelPermissionsDto) obj; 
		//--- Attribute id
		if ( id == null ) { 
			if ( other.id != null ) 
				return false ; 
		} else if ( ! id.equals(other.id) ) 
			return false ; 
		return true; 
	} 


    //----------------------------------------------------------------------
    // toString METHOD
    //----------------------------------------------------------------------
    public String toString() {
		StringBuffer sb = new StringBuffer(); 
		sb.append(id); 
		sb.append("|"); 
		sb.append(code); 
		sb.append("|"); 
		sb.append(libelle); 
        return sb.toString();
    }
}
