
/*
 * Java dto for entity table produit 
 * Created on dec ( Time 18:46:57 )
 * Generator tool : Telosys Tools Generator ( version 2.1.1 )
 */

package com.ino.steylo.helper.dto;

import java.util.Date;
import java.util.List;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;



/**
 * DTO for table "produit"
 * 
 * @author ino
 *
 */
@JsonInclude(Include.NON_NULL)
public class ProduitDto {

    private Integer    id                   ; // Primary Key

    private String     libelle              ;
    private String     referenceProduit     ;
    private String     description          ;
    private Integer    prixMin              ;
    private Integer    prixMax              ;
    private Integer    prix                 ;
    private Integer    quantite             ;
	private String     dateCreation         ;
    private Integer    status               ;
    private Integer    trash                ;
    private String     couleur              ;
    private String     poids                ;
    private String     size                 ;
    private Integer    idCategorie          ;
    private Integer    idChanel             ;
    private Integer    idSousCategorie      ;
	private String     lastUpdate           ;
    private Integer    idMarque             ;

    private List<ImageProduitDto>  listImageProduit;

    //----------------------------------------------------------------------
    // ENTITY LINKS FIELD ( RELATIONSHIP )
    //----------------------------------------------------------------------



    /**
     * Default constructor
     */
    public ProduitDto()
    {
        super();
    }
    
    //----------------------------------------------------------------------
    // GETTER(S) & SETTER(S) FOR THE PRIMARY KEY 
    //----------------------------------------------------------------------
    /**
     * Set the "id" field value
     * @param id
     */
    public void setId( Integer id ){
        this.id = id;
    }
    /**
     * Get the "id" field value
     * @return the field value
     */
    public Integer getId(){
        return this.id;
    }


    //----------------------------------------------------------------------
    // GETTER(S) & SETTER(S) FOR DATA FIELDS
    //----------------------------------------------------------------------
    /**
     * Set the "libelle" field value
     * @param libelle
     */
	public void setLibelle( String libelle )
    {
        this.libelle = libelle ;
    }
    /**
     * Get the "libelle" field value
     * @return the field value
     */
	public String getLibelle()
    {
        return this.libelle;
    }

    /**
     * Set the "referenceProduit" field value
     * @param referenceProduit
     */
	public void setReferenceProduit( String referenceProduit )
    {
        this.referenceProduit = referenceProduit ;
    }
    /**
     * Get the "referenceProduit" field value
     * @return the field value
     */
	public String getReferenceProduit()
    {
        return this.referenceProduit;
    }

    /**
     * Set the "description" field value
     * @param description
     */
	public void setDescription( String description )
    {
        this.description = description ;
    }
    /**
     * Get the "description" field value
     * @return the field value
     */
	public String getDescription()
    {
        return this.description;
    }

    /**
     * Set the "prixMin" field value
     * @param prixMin
     */
	public void setPrixMin( Integer prixMin )
    {
        this.prixMin = prixMin ;
    }
    /**
     * Get the "prixMin" field value
     * @return the field value
     */
	public Integer getPrixMin()
    {
        return this.prixMin;
    }

    /**
     * Set the "prixMax" field value
     * @param prixMax
     */
	public void setPrixMax( Integer prixMax )
    {
        this.prixMax = prixMax ;
    }
    /**
     * Get the "prixMax" field value
     * @return the field value
     */
	public Integer getPrixMax()
    {
        return this.prixMax;
    }

    /**
     * Set the "prix" field value
     * @param prix
     */
	public void setPrix( Integer prix )
    {
        this.prix = prix ;
    }
    /**
     * Get the "prix" field value
     * @return the field value
     */
	public Integer getPrix()
    {
        return this.prix;
    }

    /**
     * Set the "quantite" field value
     * @param quantite
     */
	public void setQuantite( Integer quantite )
    {
        this.quantite = quantite ;
    }
    /**
     * Get the "quantite" field value
     * @return the field value
     */
	public Integer getQuantite()
    {
        return this.quantite;
    }

    /**
     * Set the "dateCreation" field value
     * @param dateCreation
     */
	public void setDateCreation( String dateCreation )
    {
        this.dateCreation = dateCreation ;
    }
    /**
     * Get the "dateCreation" field value
     * @return the field value
     */
	public String getDateCreation()
    {
        return this.dateCreation;
    }

    /**
     * Set the "status" field value
     * @param status
     */
	public void setStatus( Integer status )
    {
        this.status = status ;
    }
    /**
     * Get the "status" field value
     * @return the field value
     */
	public Integer getStatus()
    {
        return this.status;
    }

    /**
     * Set the "trash" field value
     * @param trash
     */
	public void setTrash( Integer trash )
    {
        this.trash = trash ;
    }
    /**
     * Get the "trash" field value
     * @return the field value
     */
	public Integer getTrash()
    {
        return this.trash;
    }

    /**
     * Set the "couleur" field value
     * @param couleur
     */
	public void setCouleur( String couleur )
    {
        this.couleur = couleur ;
    }
    /**
     * Get the "couleur" field value
     * @return the field value
     */
	public String getCouleur()
    {
        return this.couleur;
    }

    /**
     * Set the "poids" field value
     * @param poids
     */
	public void setPoids( String poids )
    {
        this.poids = poids ;
    }
    /**
     * Get the "poids" field value
     * @return the field value
     */
	public String getPoids()
    {
        return this.poids;
    }

    /**
     * Set the "size" field value
     * @param size
     */
	public void setSize( String size )
    {
        this.size = size ;
    }
    /**
     * Get the "size" field value
     * @return the field value
     */
	public String getSize()
    {
        return this.size;
    }

    /**
     * Set the "idCategorie" field value
     * @param idCategorie
     */
	public void setIdCategorie( Integer idCategorie )
    {
        this.idCategorie = idCategorie ;
    }
    /**
     * Get the "idCategorie" field value
     * @return the field value
     */
	public Integer getIdCategorie()
    {
        return this.idCategorie;
    }

    /**
     * Set the "idChanel" field value
     * @param idChanel
     */
	public void setIdChanel( Integer idChanel )
    {
        this.idChanel = idChanel ;
    }
    /**
     * Get the "idChanel" field value
     * @return the field value
     */
	public Integer getIdChanel()
    {
        return this.idChanel;
    }

    /**
     * Set the "idSousCategorie" field value
     * @param idSousCategorie
     */
	public void setIdSousCategorie( Integer idSousCategorie )
    {
        this.idSousCategorie = idSousCategorie ;
    }
    /**
     * Get the "idSousCategorie" field value
     * @return the field value
     */
	public Integer getIdSousCategorie()
    {
        return this.idSousCategorie;
    }

    /**
     * Set the "lastUpdate" field value
     * @param lastUpdate
     */
	public void setLastUpdate( String lastUpdate )
    {
        this.lastUpdate = lastUpdate ;
    }
    /**
     * Get the "lastUpdate" field value
     * @return the field value
     */
	public String getLastUpdate()
    {
        return this.lastUpdate;
    }

    /**
     * Set the "idMarque" field value
     * @param idMarque
     */
	public void setIdMarque( Integer idMarque )
    {
        this.idMarque = idMarque ;
    }
    /**
     * Get the "idMarque" field value
     * @return the field value
     */
	public Integer getIdMarque()
    {
        return this.idMarque;
    }

    public List<ImageProduitDto> getListImageProduit() {
        return listImageProduit;
    }

    public void setListImageProduit(List<ImageProduitDto> listImageProduit) {
        this.listImageProduit = listImageProduit;
    }

    //----------------------------------------------------------------------
    // GETTERS & SETTERS FOR LINKS FIELD ( RELATIONSHIP )
    //----------------------------------------------------------------------


	//----------------------------------------------------------------------
    // equals METHOD
    //----------------------------------------------------------------------
	public boolean equals(Object obj) { 
		if ( this == obj ) return true ; 
		if ( obj == null ) return false ;
		if ( this.getClass() != obj.getClass() ) return false ; 
		ProduitDto other = (ProduitDto) obj; 
		//--- Attribute id
		if ( id == null ) { 
			if ( other.id != null ) 
				return false ; 
		} else if ( ! id.equals(other.id) ) 
			return false ; 
		return true; 
	} 


    //----------------------------------------------------------------------
    // toString METHOD
    //----------------------------------------------------------------------
    public String toString() {
		StringBuffer sb = new StringBuffer(); 
		sb.append(id); 
		sb.append("|"); 
		sb.append(libelle); 
		sb.append("|"); 
		sb.append(referenceProduit); 
		sb.append("|"); 
		sb.append(description); 
		sb.append("|"); 
		sb.append(prixMin); 
		sb.append("|"); 
		sb.append(prixMax); 
		sb.append("|"); 
		sb.append(prix); 
		sb.append("|"); 
		sb.append(quantite); 
		sb.append("|"); 
		sb.append(dateCreation); 
		sb.append("|"); 
		sb.append(status); 
		sb.append("|"); 
		sb.append(trash); 
		sb.append("|"); 
		sb.append(couleur); 
		sb.append("|"); 
		sb.append(poids); 
		sb.append("|"); 
		sb.append(size); 
		sb.append("|"); 
		sb.append(idSousCategorie); 
		sb.append("|"); 
		sb.append(lastUpdate); 
		sb.append("|"); 
		sb.append(idMarque); 
        return sb.toString();
    }
}
