

/*
 * Java transformer for entity table abonne_demande 
 * Created on dec ( Time 23:22:14 )
 * Generator tool : Telosys Tools Generator ( version 2.1.1 )
 */

package com.ino.steylo.helper.dto.transformer;

import java.text.ParseException;
import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import com.ino.steylo.helper.dto.*;
import com.ino.steylo.dao.entity.*;


/**
TRANSFORMER for table "abonne_demande"
 * 
 * @author ino
 *
 */
@Mapper
public interface AbonneDemandeTransformer {

	AbonneDemandeTransformer INSTANCE = Mappers.getMapper(AbonneDemandeTransformer.class);

	@Mappings({
		@Mapping(source="entity.dateDemande", dateFormat="dd-MM-yyyy HH:mm:ss",target="dateDemande"),
		@Mapping(source="entity.dateDecision", dateFormat="dd-MM-yyyy HH:mm:ss",target="dateDecision"),
		@Mapping(source="entity.dateDesouscription", dateFormat="dd-MM-yyyy HH:mm:ss",target="dateDesouscription"),
		@Mapping(source="entity.chanel.id", target="idChannel"),
		@Mapping(source="entity.abonne.id", target="idAbonne"),
		@Mapping(source="entity.abonne.nom", target="nomAbonne"),
		@Mapping(source="entity.abonne.prenoms", target="prenomAbonne"),
		@Mapping(source="entity.abonne.longitude", target="longitude"),
		@Mapping(source="entity.abonne.latitude", target="latitude"),
		@Mapping(source="entity.abonne.communaute", target="sesContactsAbonne"),
		@Mapping(source="entity.abonne.country.countryNicename", target="paysAbonne"),
		@Mapping(source="entity.abonne.photoIdentite", target="photoAbonne"),
		@Mapping(source="entity.abonne.extPhotoIdentite", target="extPhotoAbonne"),
		@Mapping(source="entity.abonne.dateEnreg", dateFormat="dd-MM-yyyy HH:mm:ss", target="dateEnregAbonne"),
	})
	AbonneDemandeDto toDto(AbonneDemande entity) throws ParseException;

    List<AbonneDemandeDto> toDtos(List<AbonneDemande> entities) throws ParseException;

	@Mappings({
		@Mapping(source="dto.id", target="id"),
		@Mapping(source="dto.dateDemande", dateFormat="dd-MM-yyyy HH:mm:ss",target="dateDemande"),
		@Mapping(source="dto.statut", target="statut"),
		@Mapping(source="dto.commentaire", target="commentaire"),
		@Mapping(source="dto.dateDecision", dateFormat="dd-MM-yyyy HH:mm:ss",target="dateDecision"),
		@Mapping(source="dto.userDecisionId", target="userDecisionId"),
		@Mapping(source="dto.demandeState", target="demandeState"),
		//@Mapping(source="dto.aDesouscrire", target="aDesouscrire"),
		@Mapping(source="dto.dateDesouscription", dateFormat="dd-MM-yyyy HH:mm:ss",target="dateDesouscription"),
		@Mapping(source="dto.motifRejet", target="motifRejet"),
		@Mapping(source="abonne", target="abonne"),
		@Mapping(source="chanel", target="chanel"),
	})
    AbonneDemande toEntity(AbonneDemandeDto dto, Chanel chanel, Abonne abonne) throws ParseException;

    //List<AbonneDemande> toEntities(List<AbonneDemandeDto> dtos) throws ParseException;

}
