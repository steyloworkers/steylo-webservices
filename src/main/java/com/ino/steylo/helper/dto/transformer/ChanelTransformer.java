

/*
 * Java transformer for entity table chanel 
 * Created on dec ( Time 10:56:38 )
 * Generator tool : Telosys Tools Generator ( version 2.1.1 )
 */

package com.ino.steylo.helper.dto.transformer;

import java.text.ParseException;
import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import com.ino.steylo.helper.dto.*;
import com.ino.steylo.dao.entity.*;


/**
TRANSFORMER for table "chanel"
 * 
 * @author ino
 *
 */
@Mapper
public interface ChanelTransformer {

	ChanelTransformer INSTANCE = Mappers.getMapper(ChanelTransformer.class);

	@Mappings({
		@Mapping(source="entity.rowDateUpdate", dateFormat="dd-MM-yyyy HH:mm:ss",target="rowDateUpdate"),
		@Mapping(source="entity.logoDateUpdate", dateFormat="dd-MM-yyyy HH:mm:ss",target="logoDateUpdate"),
		@Mapping(source="entity.country.id", target="idPays"),
		@Mapping(source="entity.secteurActivite.id", target="idSecteurActivite"),
	})
	ChanelDto toDto(Chanel entity) throws ParseException;

    List<ChanelDto> toDtos(List<Chanel> entities) throws ParseException;

	@Mappings({
		@Mapping(source="dto.id", target="id"),
		@Mapping(source="dto.formeJuridique", target="formeJuridique"),
		@Mapping(source="dto.nomChanel", target="nomChanel"),
		@Mapping(source="dto.sigle", target="sigle"),
		@Mapping(source="dto.numeroRegistre", target="numeroRegistre"),
		@Mapping(source="dto.numeroImmatriculation", target="numeroImmatriculation"),
		@Mapping(source="dto.descriptionSecteur", target="descriptionSecteur"),
		@Mapping(source="dto.siteWeb", target="siteWeb"),
		@Mapping(source="dto.email", target="email"),
		@Mapping(source="dto.phoneNumber", target="phoneNumber"),
		@Mapping(source="dto.fax", target="fax"),
		@Mapping(source="dto.etat", target="etat"),
		@Mapping(source="dto.ville", target="ville"),
		@Mapping(source="dto.codePostal", target="codePostal"),
		@Mapping(source="dto.address", target="address"),
		@Mapping(source="dto.logo", target="logo"),
		@Mapping(source="dto.codeGuichet", target="codeGuichet"),
		@Mapping(source="dto.codeBanque", target="codeBanque"),
		@Mapping(source="dto.numeroCompte", target="numeroCompte"),
		@Mapping(source="dto.swift", target="swift"),
		@Mapping(source="dto.cleRib", target="cleRib"),
		@Mapping(source="dto.rib", target="rib"),
		@Mapping(source="dto.iban", target="iban"),
		@Mapping(source="dto.rowDateUpdate", dateFormat="dd-MM-yyyy HH:mm:ss",target="rowDateUpdate"),
		@Mapping(source="dto.logoDateUpdate", dateFormat="dd-MM-yyyy HH:mm:ss",target="logoDateUpdate"),
		@Mapping(source="country", target="country"),
		@Mapping(source="secteurActivite", target="secteurActivite"),
	})
    Chanel toEntity(ChanelDto dto, Country country, SecteurActivite secteurActivite) throws ParseException;

    //List<Chanel> toEntities(List<ChanelDto> dtos) throws ParseException;

}
