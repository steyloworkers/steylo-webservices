

/*
 * Java transformer for entity table image_produit 
 * Created on dec ( Time 23:50:38 )
 * Generator tool : Telosys Tools Generator ( version 2.1.1 )
 */

package com.ino.steylo.helper.dto.transformer;

import java.text.ParseException;
import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import com.ino.steylo.helper.dto.*;
import com.ino.steylo.dao.entity.*;


/**
TRANSFORMER for table "image_produit"
 * 
 * @author ino
 *
 */
@Mapper
public interface ImageProduitTransformer {

	ImageProduitTransformer INSTANCE = Mappers.getMapper(ImageProduitTransformer.class);

	@Mappings({
		@Mapping(source="entity.produit.id", target="idProduit"),
	})
	ImageProduitDto toDto(ImageProduit entity) throws ParseException;

    List<ImageProduitDto> toDtos(List<ImageProduit> entities) throws ParseException;

	@Mappings({
		@Mapping(source="dto.id", target="id"),
		@Mapping(source="dto.image", target="image"),
		@Mapping(source="dto.type", target="type"),
		@Mapping(source="dto.extImg", target="extImg"),
		@Mapping(source="produit", target="produit"),
	})
    ImageProduit toEntity(ImageProduitDto dto, Produit produit) throws ParseException;

    //List<ImageProduit> toEntities(List<ImageProduitDto> dtos) throws ParseException;

}
