
/*
 * Java dto for entity table country 
 * Created on dec ( Time 10:54:12 )
 * Generator tool : Telosys Tools Generator ( version 2.1.1 )
 */

package com.ino.steylo.helper.dto;

import java.util.List;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;



/**
 * DTO for table "country"
 * 
 * @author ino
 *
 */
@JsonInclude(Include.NON_NULL)
public class CountryDto {

    private Integer    id                   ; // Primary Key

    private String     countryIso           ;
    private String     countryName          ;
    private String     countryNicename      ;
    private String     countryIso3          ;
    private Short      countryNumcode       ;
    private Integer    countryPhonecode     ;

    //----------------------------------------------------------------------
    // ENTITY LINKS FIELD ( RELATIONSHIP )
    //----------------------------------------------------------------------



    /**
     * Default constructor
     */
    public CountryDto()
    {
        super();
    }
    
    //----------------------------------------------------------------------
    // GETTER(S) & SETTER(S) FOR THE PRIMARY KEY 
    //----------------------------------------------------------------------
    /**
     * Set the "id" field value
     * @param id
     */
    public void setId( Integer id ){
        this.id = id;
    }
    /**
     * Get the "id" field value
     * @return the field value
     */
    public Integer getId(){
        return this.id;
    }


    //----------------------------------------------------------------------
    // GETTER(S) & SETTER(S) FOR DATA FIELDS
    //----------------------------------------------------------------------
    /**
     * Set the "countryIso" field value
     * @param countryIso
     */
	public void setCountryIso( String countryIso )
    {
        this.countryIso = countryIso ;
    }
    /**
     * Get the "countryIso" field value
     * @return the field value
     */
	public String getCountryIso()
    {
        return this.countryIso;
    }

    /**
     * Set the "countryName" field value
     * @param countryName
     */
	public void setCountryName( String countryName )
    {
        this.countryName = countryName ;
    }
    /**
     * Get the "countryName" field value
     * @return the field value
     */
	public String getCountryName()
    {
        return this.countryName;
    }

    /**
     * Set the "countryNicename" field value
     * @param countryNicename
     */
	public void setCountryNicename( String countryNicename )
    {
        this.countryNicename = countryNicename ;
    }
    /**
     * Get the "countryNicename" field value
     * @return the field value
     */
	public String getCountryNicename()
    {
        return this.countryNicename;
    }

    /**
     * Set the "countryIso3" field value
     * @param countryIso3
     */
	public void setCountryIso3( String countryIso3 )
    {
        this.countryIso3 = countryIso3 ;
    }
    /**
     * Get the "countryIso3" field value
     * @return the field value
     */
	public String getCountryIso3()
    {
        return this.countryIso3;
    }

    /**
     * Set the "countryNumcode" field value
     * @param countryNumcode
     */
	public void setCountryNumcode( Short countryNumcode )
    {
        this.countryNumcode = countryNumcode ;
    }
    /**
     * Get the "countryNumcode" field value
     * @return the field value
     */
	public Short getCountryNumcode()
    {
        return this.countryNumcode;
    }

    /**
     * Set the "countryPhonecode" field value
     * @param countryPhonecode
     */
	public void setCountryPhonecode( Integer countryPhonecode )
    {
        this.countryPhonecode = countryPhonecode ;
    }
    /**
     * Get the "countryPhonecode" field value
     * @return the field value
     */
	public Integer getCountryPhonecode()
    {
        return this.countryPhonecode;
    }


    //----------------------------------------------------------------------
    // GETTERS & SETTERS FOR LINKS FIELD ( RELATIONSHIP )
    //----------------------------------------------------------------------


	//----------------------------------------------------------------------
    // equals METHOD
    //----------------------------------------------------------------------
	public boolean equals(Object obj) { 
		if ( this == obj ) return true ; 
		if ( obj == null ) return false ;
		if ( this.getClass() != obj.getClass() ) return false ; 
		CountryDto other = (CountryDto) obj; 
		//--- Attribute id
		if ( id == null ) { 
			if ( other.id != null ) 
				return false ; 
		} else if ( ! id.equals(other.id) ) 
			return false ; 
		return true; 
	} 


    //----------------------------------------------------------------------
    // toString METHOD
    //----------------------------------------------------------------------
    public String toString() {
		StringBuffer sb = new StringBuffer(); 
		sb.append(id); 
		sb.append("|"); 
		sb.append(countryIso); 
		sb.append("|"); 
		sb.append(countryName); 
		sb.append("|"); 
		sb.append(countryNicename); 
		sb.append("|"); 
		sb.append(countryIso3); 
		sb.append("|"); 
		sb.append(countryNumcode); 
		sb.append("|"); 
		sb.append(countryPhonecode); 
        return sb.toString();
    }
}
