

/*
 * Java transformer for entity table secteur_activite 
 * Created on dec ( Time 10:56:06 )
 * Generator tool : Telosys Tools Generator ( version 2.1.1 )
 */

package com.ino.steylo.helper.dto.transformer;

import java.text.ParseException;
import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import com.ino.steylo.helper.dto.*;
import com.ino.steylo.dao.entity.*;


/**
TRANSFORMER for table "secteur_activite"
 * 
 * @author ino
 *
 */
@Mapper
public interface SecteurActiviteTransformer {

	SecteurActiviteTransformer INSTANCE = Mappers.getMapper(SecteurActiviteTransformer.class);

	@Mappings({
	})
	SecteurActiviteDto toDto(SecteurActivite entity) throws ParseException;

    List<SecteurActiviteDto> toDtos(List<SecteurActivite> entities) throws ParseException;

	@Mappings({
		@Mapping(source="dto.id", target="id"),
		@Mapping(source="dto.libelle", target="libelle"),
	})
    SecteurActivite toEntity(SecteurActiviteDto dto) throws ParseException;

    //List<SecteurActivite> toEntities(List<SecteurActiviteDto> dtos) throws ParseException;

}
