
/*
 * Java dto for entity table marque 
 * Created on dec ( Time 18:46:56 )
 * Generator tool : Telosys Tools Generator ( version 2.1.1 )
 */

package com.ino.steylo.helper.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;



/**
 * DTO for table "marque"
 * 
 * @author ino
 *
 */
@JsonInclude(Include.NON_NULL)
public class MarqueDto {

    private Integer    id                   ; // Primary Key

    private String     libelle              ;
    private String     slogan               ;
    private Boolean    statut               ;

    //----------------------------------------------------------------------
    // ENTITY LINKS FIELD ( RELATIONSHIP )
    //----------------------------------------------------------------------



    /**
     * Default constructor
     */
    public MarqueDto()
    {
        super();
    }
    
    //----------------------------------------------------------------------
    // GETTER(S) & SETTER(S) FOR THE PRIMARY KEY 
    //----------------------------------------------------------------------
    /**
     * Set the "id" field value
     * @param id
     */
    public void setId( Integer id ){
        this.id = id;
    }
    /**
     * Get the "id" field value
     * @return the field value
     */
    public Integer getId(){
        return this.id;
    }


    //----------------------------------------------------------------------
    // GETTER(S) & SETTER(S) FOR DATA FIELDS
    //----------------------------------------------------------------------
    /**
     * Set the "libelle" field value
     * @param libelle
     */
	public void setLibelle( String libelle )
    {
        this.libelle = libelle ;
    }
    /**
     * Get the "libelle" field value
     * @return the field value
     */
	public String getLibelle()
    {
        return this.libelle;
    }

    /**
     * Set the "slogan" field value
     * @param slogan
     */
	public void setSlogan( String slogan )
    {
        this.slogan = slogan ;
    }
    /**
     * Get the "slogan" field value
     * @return the field value
     */
	public String getSlogan()
    {
        return this.slogan;
    }

    /**
     * Set the "statut" field value
     * @param statut
     */
	public void setStatut( Boolean statut )
    {
        this.statut = statut ;
    }
    /**
     * Get the "statut" field value
     * @return the field value
     */
	public Boolean getStatut()
    {
        return this.statut;
    }


    //----------------------------------------------------------------------
    // GETTERS & SETTERS FOR LINKS FIELD ( RELATIONSHIP )
    //----------------------------------------------------------------------


	//----------------------------------------------------------------------
    // equals METHOD
    //----------------------------------------------------------------------
	public boolean equals(Object obj) { 
		if ( this == obj ) return true ; 
		if ( obj == null ) return false ;
		if ( this.getClass() != obj.getClass() ) return false ; 
		MarqueDto other = (MarqueDto) obj; 
		//--- Attribute id
		if ( id == null ) { 
			if ( other.id != null ) 
				return false ; 
		} else if ( ! id.equals(other.id) ) 
			return false ; 
		return true; 
	} 


    //----------------------------------------------------------------------
    // toString METHOD
    //----------------------------------------------------------------------
    public String toString() {
		StringBuffer sb = new StringBuffer(); 
		sb.append(id); 
		sb.append("|"); 
		sb.append(libelle); 
		sb.append("|"); 
		sb.append(slogan); 
		sb.append("|"); 
		sb.append(statut); 
        return sb.toString();
    }
}
