

/*
 * Java transformer for entity table produit 
 * Created on dec ( Time 18:46:57 )
 * Generator tool : Telosys Tools Generator ( version 2.1.1 )
 */

package com.ino.steylo.helper.dto.transformer;

import java.text.ParseException;
import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import com.ino.steylo.helper.dto.*;
import com.ino.steylo.dao.entity.*;


/**
TRANSFORMER for table "produit"
 * 
 * @author ino
 *
 */
@Mapper
public interface ProduitTransformer {

	ProduitTransformer INSTANCE = Mappers.getMapper(ProduitTransformer.class);

	@Mappings({
		@Mapping(source="entity.dateCreation", dateFormat="dd-MM-yyyy HH:mm:ss",target="dateCreation"),
		@Mapping(source="entity.lastUpdate", dateFormat="dd-MM-yyyy HH:mm:ss",target="lastUpdate"),
		@Mapping(source="entity.chanel.id", target="idChanel"),
		@Mapping(source="entity.categorieProduit.id", target="idCategorie"),
	})
	ProduitDto toDto(Produit entity) throws ParseException;

    List<ProduitDto> toDtos(List<Produit> entities) throws ParseException;

	@Mappings({
		@Mapping(source="dto.id", target="id"),
		@Mapping(source="dto.libelle", target="libelle"),
		@Mapping(source="dto.referenceProduit", target="referenceProduit"),
		@Mapping(source="dto.description", target="description"),
		@Mapping(source="dto.prixMin", target="prixMin"),
		@Mapping(source="dto.prixMax", target="prixMax"),
		@Mapping(source="dto.prix", target="prix"),
		@Mapping(source="dto.quantite", target="quantite"),
		@Mapping(source="dto.dateCreation", dateFormat="dd-MM-yyyy HH:mm:ss",target="dateCreation"),
		@Mapping(source="dto.status", target="status"),
		@Mapping(source="dto.trash", target="trash"),
		@Mapping(source="dto.couleur", target="couleur"),
		@Mapping(source="dto.poids", target="poids"),
		@Mapping(source="dto.size", target="size"),
		@Mapping(source="dto.idSousCategorie", target="idSousCategorie"),
		@Mapping(source="dto.lastUpdate", dateFormat="dd-MM-yyyy HH:mm:ss",target="lastUpdate"),
		@Mapping(source="dto.idMarque", target="idMarque"),
		@Mapping(source="chanel", target="chanel"),
		@Mapping(source="categorieProduit", target="categorieProduit"),
	})
    Produit toEntity(ProduitDto dto, Chanel chanel, CategorieProduit categorieProduit) throws ParseException;

    //List<Produit> toEntities(List<ProduitDto> dtos) throws ParseException;

}
