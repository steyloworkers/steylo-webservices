

/*
 * Java transformer for entity table country 
 * Created on dec ( Time 10:54:12 )
 * Generator tool : Telosys Tools Generator ( version 2.1.1 )
 */

package com.ino.steylo.helper.dto.transformer;

import java.text.ParseException;
import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import com.ino.steylo.helper.dto.*;
import com.ino.steylo.dao.entity.*;


/**
TRANSFORMER for table "country"
 * 
 * @author ino
 *
 */
@Mapper
public interface CountryTransformer {

	CountryTransformer INSTANCE = Mappers.getMapper(CountryTransformer.class);

	@Mappings({
	})
	CountryDto toDto(Country entity) throws ParseException;

    List<CountryDto> toDtos(List<Country> entities) throws ParseException;

	@Mappings({
		@Mapping(source="dto.id", target="id"),
		@Mapping(source="dto.countryIso", target="countryIso"),
		@Mapping(source="dto.countryName", target="countryName"),
		@Mapping(source="dto.countryNicename", target="countryNicename"),
		@Mapping(source="dto.countryIso3", target="countryIso3"),
		@Mapping(source="dto.countryNumcode", target="countryNumcode"),
		@Mapping(source="dto.countryPhonecode", target="countryPhonecode"),
	})
    Country toEntity(CountryDto dto) throws ParseException;

    //List<Country> toEntities(List<CountryDto> dtos) throws ParseException;

}
