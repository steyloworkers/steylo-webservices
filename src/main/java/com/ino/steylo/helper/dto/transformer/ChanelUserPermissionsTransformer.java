

/*
 * Java transformer for entity table chanel_user_permissions 
 * Created on dec ( Time 18:46:54 )
 * Generator tool : Telosys Tools Generator ( version 2.1.1 )
 */

package com.ino.steylo.helper.dto.transformer;

import java.text.ParseException;
import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import com.ino.steylo.helper.dto.*;
import com.ino.steylo.dao.entity.*;


/**
TRANSFORMER for table "chanel_user_permissions"
 * 
 * @author ino
 *
 */
@Mapper
public interface ChanelUserPermissionsTransformer {

	ChanelUserPermissionsTransformer INSTANCE = Mappers.getMapper(ChanelUserPermissionsTransformer.class);

	@Mappings({
		@Mapping(source="entity.chanelPermissions.id", target="idPermission"),
		@Mapping(source="entity.chanelPermissions.code", target="chanelPermissionsCode"),
		@Mapping(source="entity.chanelUsers.id", target="idUser"),
	})
	ChanelUserPermissionsDto toDto(ChanelUserPermissions entity) throws ParseException;

    List<ChanelUserPermissionsDto> toDtos(List<ChanelUserPermissions> entities) throws ParseException;

	@Mappings({
		@Mapping(source="dto.id", target="id"),
		@Mapping(source="chanelPermissions", target="chanelPermissions"),
		@Mapping(source="chanelUsers", target="chanelUsers"),
	})
    ChanelUserPermissions toEntity(ChanelUserPermissionsDto dto, ChanelPermissions chanelPermissions, ChanelUsers chanelUsers) throws ParseException;

    //List<ChanelUserPermissions> toEntities(List<ChanelUserPermissionsDto> dtos) throws ParseException;

}
