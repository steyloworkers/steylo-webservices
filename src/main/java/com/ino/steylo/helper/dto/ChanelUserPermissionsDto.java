
/*
 * Java dto for entity table chanel_user_permissions 
 * Created on dec ( Time 18:46:54 )
 * Generator tool : Telosys Tools Generator ( version 2.1.1 )
 */

package com.ino.steylo.helper.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;



/**
 * DTO for table "chanel_user_permissions"
 * 
 * @author ino
 *
 */
@JsonInclude(Include.NON_NULL)
public class ChanelUserPermissionsDto {

    private Integer    id                   ; // Primary Key

    private Integer    idUser               ;
    private Integer    idPermission         ;

    //----------------------------------------------------------------------
    // ENTITY LINKS FIELD ( RELATIONSHIP )
    //----------------------------------------------------------------------
	private String chanelPermissionsCode;



    /**
     * Default constructor
     */
    public ChanelUserPermissionsDto()
    {
        super();
    }
    
    //----------------------------------------------------------------------
    // GETTER(S) & SETTER(S) FOR THE PRIMARY KEY 
    //----------------------------------------------------------------------
    /**
     * Set the "id" field value
     * @param id
     */
    public void setId( Integer id ){
        this.id = id;
    }
    /**
     * Get the "id" field value
     * @return the field value
     */
    public Integer getId(){
        return this.id;
    }


    //----------------------------------------------------------------------
    // GETTER(S) & SETTER(S) FOR DATA FIELDS
    //----------------------------------------------------------------------
    /**
     * Set the "idUser" field value
     * @param idUser
     */
	public void setIdUser( Integer idUser )
    {
        this.idUser = idUser ;
    }
    /**
     * Get the "idUser" field value
     * @return the field value
     */
	public Integer getIdUser()
    {
        return this.idUser;
    }

    /**
     * Set the "idPermission" field value
     * @param idPermission
     */
	public void setIdPermission( Integer idPermission )
    {
        this.idPermission = idPermission ;
    }
    /**
     * Get the "idPermission" field value
     * @return the field value
     */
	public Integer getIdPermission()
    {
        return this.idPermission;
    }


    //----------------------------------------------------------------------
    // GETTERS & SETTERS FOR LINKS FIELD ( RELATIONSHIP )
    //----------------------------------------------------------------------
	public String getChanelPermissionsCode()
    {
        return this.chanelPermissionsCode;
    }
	public void setChanelPermissionsCode(String chanelPermissionsCode)
    {
        this.chanelPermissionsCode = chanelPermissionsCode;
    }



	//----------------------------------------------------------------------
    // equals METHOD
    //----------------------------------------------------------------------
	public boolean equals(Object obj) { 
		if ( this == obj ) return true ; 
		if ( obj == null ) return false ;
		if ( this.getClass() != obj.getClass() ) return false ; 
		ChanelUserPermissionsDto other = (ChanelUserPermissionsDto) obj; 
		//--- Attribute id
		if ( id == null ) { 
			if ( other.id != null ) 
				return false ; 
		} else if ( ! id.equals(other.id) ) 
			return false ; 
		return true; 
	} 


    //----------------------------------------------------------------------
    // toString METHOD
    //----------------------------------------------------------------------
    public String toString() {
		StringBuffer sb = new StringBuffer(); 
		sb.append(id); 
        return sb.toString();
    }
}
