

/*
 * Java transformer for entity table abonne_chanel 
 * Created on dec ( Time 18:46:52 )
 * Generator tool : Telosys Tools Generator ( version 2.1.1 )
 */

package com.ino.steylo.helper.dto.transformer;

import java.text.ParseException;
import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import com.ino.steylo.helper.dto.*;
import com.ino.steylo.dao.entity.*;


/**
TRANSFORMER for table "abonne_chanel"
 * 
 * @author ino
 *
 */
@Mapper
public interface AbonneChanelTransformer {

	AbonneChanelTransformer INSTANCE = Mappers.getMapper(AbonneChanelTransformer.class);

	@Mappings({
		@Mapping(source="entity.dateDesouscription", dateFormat="dd-MM-yyyy",target="dateDesouscription"),
		@Mapping(source="entity.dateSouscription", dateFormat="dd-MM-yyyy",target="dateSouscription"),
	})
	AbonneChanelDto toDto(AbonneChanel entity) throws ParseException;

    List<AbonneChanelDto> toDtos(List<AbonneChanel> entities) throws ParseException;

	@Mappings({
		@Mapping(source="dto.id", target="id"),
		@Mapping(source="dto.idChanel", target="idChanel"),
		@Mapping(source="dto.idAbonne", target="idAbonne"),
		@Mapping(source="dto.dateDesouscription", dateFormat="dd-MM-yyyy",target="dateDesouscription"),
		@Mapping(source="dto.dateSouscription", dateFormat="dd-MM-yyyy",target="dateSouscription"),
		@Mapping(source="dto.metadata", target="metadata"),
		@Mapping(source="dto.isActive", target="isActive"),
		@Mapping(source="dto.isSeller", target="isSeller"),
	})
    AbonneChanel toEntity(AbonneChanelDto dto) throws ParseException;

    //List<AbonneChanel> toEntities(List<AbonneChanelDto> dtos) throws ParseException;

}
