
/*
 * Java dto for entity table permission_users_chanel 
 * Created on dec ( Time 18:46:57 )
 * Generator tool : Telosys Tools Generator ( version 2.1.1 )
 */

package com.ino.steylo.helper.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;



/**
 * DTO for table "permission_users_chanel"
 * 
 * @author ino
 *
 */
@JsonInclude(Include.NON_NULL)
public class PermissionUsersChanelDto {

    private Integer    id                   ; // Primary Key

    private Integer    idPermission         ;
    private Integer    idChanelUser         ;

    //----------------------------------------------------------------------
    // ENTITY LINKS FIELD ( RELATIONSHIP )
    //----------------------------------------------------------------------



    /**
     * Default constructor
     */
    public PermissionUsersChanelDto()
    {
        super();
    }
    
    //----------------------------------------------------------------------
    // GETTER(S) & SETTER(S) FOR THE PRIMARY KEY 
    //----------------------------------------------------------------------
    /**
     * Set the "id" field value
     * @param id
     */
    public void setId( Integer id ){
        this.id = id;
    }
    /**
     * Get the "id" field value
     * @return the field value
     */
    public Integer getId(){
        return this.id;
    }


    //----------------------------------------------------------------------
    // GETTER(S) & SETTER(S) FOR DATA FIELDS
    //----------------------------------------------------------------------
    /**
     * Set the "idPermission" field value
     * @param idPermission
     */
	public void setIdPermission( Integer idPermission )
    {
        this.idPermission = idPermission ;
    }
    /**
     * Get the "idPermission" field value
     * @return the field value
     */
	public Integer getIdPermission()
    {
        return this.idPermission;
    }

    /**
     * Set the "idChanelUser" field value
     * @param idChanelUser
     */
	public void setIdChanelUser( Integer idChanelUser )
    {
        this.idChanelUser = idChanelUser ;
    }
    /**
     * Get the "idChanelUser" field value
     * @return the field value
     */
	public Integer getIdChanelUser()
    {
        return this.idChanelUser;
    }


    //----------------------------------------------------------------------
    // GETTERS & SETTERS FOR LINKS FIELD ( RELATIONSHIP )
    //----------------------------------------------------------------------


	//----------------------------------------------------------------------
    // equals METHOD
    //----------------------------------------------------------------------
	public boolean equals(Object obj) { 
		if ( this == obj ) return true ; 
		if ( obj == null ) return false ;
		if ( this.getClass() != obj.getClass() ) return false ; 
		PermissionUsersChanelDto other = (PermissionUsersChanelDto) obj; 
		//--- Attribute id
		if ( id == null ) { 
			if ( other.id != null ) 
				return false ; 
		} else if ( ! id.equals(other.id) ) 
			return false ; 
		return true; 
	} 


    //----------------------------------------------------------------------
    // toString METHOD
    //----------------------------------------------------------------------
    public String toString() {
		StringBuffer sb = new StringBuffer(); 
		sb.append(id); 
		sb.append("|"); 
		sb.append(idPermission); 
		sb.append("|"); 
		sb.append(idChanelUser); 
        return sb.toString();
    }
}
