

/*
 * Java transformer for entity table abonne 
 * Created on dec ( Time 21:48:44 )
 * Generator tool : Telosys Tools Generator ( version 2.1.1 )
 */

package com.ino.steylo.helper.dto.transformer;

import java.text.ParseException;
import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import com.ino.steylo.helper.dto.*;
import com.ino.steylo.dao.entity.*;


/**
TRANSFORMER for table "abonne"
 * 
 * @author ino
 *
 */
@Mapper
public interface AbonneTransformer {

	AbonneTransformer INSTANCE = Mappers.getMapper(AbonneTransformer.class);

	@Mappings({
		@Mapping(source="entity.dateEnreg", dateFormat="dd-MM-yyyy",target="dateEnreg"),
		@Mapping(source="entity.seen", dateFormat="dd-MM-yyyy",target="seen"),
		@Mapping(source="entity.country.id", target="pays"),
	})
	AbonneDto toDto(Abonne entity) throws ParseException;

    List<AbonneDto> toDtos(List<Abonne> entities) throws ParseException;

	@Mappings({
		@Mapping(source="dto.id", target="id"),
		@Mapping(source="dto.dateEnreg", dateFormat="dd-MM-yyyy",target="dateEnreg"),
		@Mapping(source="dto.nom", target="nom"),
		@Mapping(source="dto.prenoms", target="prenoms"),
		@Mapping(source="dto.sexe", target="sexe"),
		@Mapping(source="dto.dateNaissance", target="dateNaissance"),
		@Mapping(source="dto.profession", target="profession"),
		@Mapping(source="dto.mobileNumber", target="mobileNumber"),
		@Mapping(source="dto.address", target="address"),
		@Mapping(source="dto.ville", target="ville"),
		@Mapping(source="dto.nationalite", target="nationalite"),
		@Mapping(source="dto.idTypeUtilisateur", target="idTypeUtilisateur"),
		@Mapping(source="dto.metadata", target="metadata"),
		@Mapping(source="dto.login", target="login"),
		@Mapping(source="dto.password", target="password"),
		@Mapping(source="dto.email", target="email"),
		@Mapping(source="dto.isActive", target="isActive"),
		@Mapping(source="dto.longitude", target="longitude"),
		@Mapping(source="dto.latitude", target="latitude"),
		@Mapping(source="dto.photoIdentite", target="photoIdentite"),
		@Mapping(source="dto.photoPieceRecto", target="photoPieceRecto"),
		@Mapping(source="dto.photoPieceVerso", target="photoPieceVerso"),
		@Mapping(source="dto.communaute", target="communaute"),
		@Mapping(source="dto.extPhotoIdentite", target="extPhotoIdentite"),
		@Mapping(source="dto.extPhotoRecto", target="extPhotoRecto"),
		@Mapping(source="dto.extPhotoVerso", target="extPhotoVerso"),
		@Mapping(source="dto.verifKey", target="verifKey"),
		@Mapping(source="dto.seen", dateFormat="dd-MM-yyyy",target="seen"),
		@Mapping(source="country", target="country"),
	})
    Abonne toEntity(AbonneDto dto, Country country) throws ParseException;

    //List<Abonne> toEntities(List<AbonneDto> dtos) throws ParseException;

}
