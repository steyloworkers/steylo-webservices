
/*
 * Java dto for entity table permission_actions 
 * Created on dec ( Time 18:46:56 )
 * Generator tool : Telosys Tools Generator ( version 2.1.1 )
 */

package com.ino.steylo.helper.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;



/**
 * DTO for table "permission_actions"
 * 
 * @author ino
 *
 */
@JsonInclude(Include.NON_NULL)
public class PermissionActionsDto {

    private Integer    id                   ; // Primary Key

    private Integer    idPermission         ;
    private Integer    idActionProduit      ;
    private Integer    idActionAbonne       ;
    private Integer    idActionParam        ;

    //----------------------------------------------------------------------
    // ENTITY LINKS FIELD ( RELATIONSHIP )
    //----------------------------------------------------------------------
	private String chanelPermissionsCode;



    /**
     * Default constructor
     */
    public PermissionActionsDto()
    {
        super();
    }
    
    //----------------------------------------------------------------------
    // GETTER(S) & SETTER(S) FOR THE PRIMARY KEY 
    //----------------------------------------------------------------------
    /**
     * Set the "id" field value
     * @param id
     */
    public void setId( Integer id ){
        this.id = id;
    }
    /**
     * Get the "id" field value
     * @return the field value
     */
    public Integer getId(){
        return this.id;
    }


    //----------------------------------------------------------------------
    // GETTER(S) & SETTER(S) FOR DATA FIELDS
    //----------------------------------------------------------------------
    /**
     * Set the "idPermission" field value
     * @param idPermission
     */
	public void setIdPermission( Integer idPermission )
    {
        this.idPermission = idPermission ;
    }
    /**
     * Get the "idPermission" field value
     * @return the field value
     */
	public Integer getIdPermission()
    {
        return this.idPermission;
    }

    /**
     * Set the "idActionProduit" field value
     * @param idActionProduit
     */
	public void setIdActionProduit( Integer idActionProduit )
    {
        this.idActionProduit = idActionProduit ;
    }
    /**
     * Get the "idActionProduit" field value
     * @return the field value
     */
	public Integer getIdActionProduit()
    {
        return this.idActionProduit;
    }

    /**
     * Set the "idActionAbonne" field value
     * @param idActionAbonne
     */
	public void setIdActionAbonne( Integer idActionAbonne )
    {
        this.idActionAbonne = idActionAbonne ;
    }
    /**
     * Get the "idActionAbonne" field value
     * @return the field value
     */
	public Integer getIdActionAbonne()
    {
        return this.idActionAbonne;
    }

    /**
     * Set the "idActionParam" field value
     * @param idActionParam
     */
	public void setIdActionParam( Integer idActionParam )
    {
        this.idActionParam = idActionParam ;
    }
    /**
     * Get the "idActionParam" field value
     * @return the field value
     */
	public Integer getIdActionParam()
    {
        return this.idActionParam;
    }


    //----------------------------------------------------------------------
    // GETTERS & SETTERS FOR LINKS FIELD ( RELATIONSHIP )
    //----------------------------------------------------------------------
	public String getChanelPermissionsCode()
    {
        return this.chanelPermissionsCode;
    }
	public void setChanelPermissionsCode(String chanelPermissionsCode)
    {
        this.chanelPermissionsCode = chanelPermissionsCode;
    }



	//----------------------------------------------------------------------
    // equals METHOD
    //----------------------------------------------------------------------
	public boolean equals(Object obj) { 
		if ( this == obj ) return true ; 
		if ( obj == null ) return false ;
		if ( this.getClass() != obj.getClass() ) return false ; 
		PermissionActionsDto other = (PermissionActionsDto) obj; 
		//--- Attribute id
		if ( id == null ) { 
			if ( other.id != null ) 
				return false ; 
		} else if ( ! id.equals(other.id) ) 
			return false ; 
		return true; 
	} 


    //----------------------------------------------------------------------
    // toString METHOD
    //----------------------------------------------------------------------
    public String toString() {
		StringBuffer sb = new StringBuffer(); 
		sb.append(id); 
		sb.append("|"); 
		sb.append(idActionProduit); 
		sb.append("|"); 
		sb.append(idActionAbonne); 
		sb.append("|"); 
		sb.append(idActionParam); 
        return sb.toString();
    }
}
