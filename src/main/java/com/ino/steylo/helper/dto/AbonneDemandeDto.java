
/*
 * Java dto for entity table abonne_demande 
 * Created on dec ( Time 23:22:14 )
 * Generator tool : Telosys Tools Generator ( version 2.1.1 )
 */

package com.ino.steylo.helper.dto;

import java.util.Date;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;



/**
 * DTO for table "abonne_demande"
 * 
 * @author ino
 *
 */
@JsonInclude(Include.NON_NULL)
public class AbonneDemandeDto {

    private Integer    id                   ; // Primary Key

	private String     dateDemande          ;
    private Long       idAbonne             ;
    private String     nomAbonne            ;
    private String     prenomAbonne         ;
    private float      longitude            ;
    private float      latitude             ;
    private String     paysAbonne     	    ;
    private String     sesContactsAbonne    ;
    private String     dateEnregAbonne      ;
    private String     photoAbonne          ;
    private String     extPhotoAbonne       ;
    private Integer    idChannel            ;
    private Integer    statut               ;
    private String     commentaire          ;
	private String     dateDecision         ;
    private Integer    userDecisionId       ;
    private Integer    demandeState         ;
    private Boolean    aDesouscrire         ;
	private String     dateDesouscription   ;
    private String     motifRejet           ;

    //----------------------------------------------------------------------
    // ENTITY LINKS FIELD ( RELATIONSHIP )
    //----------------------------------------------------------------------



    /**
     * Default constructor
     */
    public AbonneDemandeDto()
    {
        super();
    }
    
    //----------------------------------------------------------------------
    // GETTER(S) & SETTER(S) FOR THE PRIMARY KEY 
    //----------------------------------------------------------------------
    /**
     * Set the "id" field value
     * @param id
     */
    public void setId( Integer id ){
        this.id = id;
    }
    /**
     * Get the "id" field value
     * @return the field value
     */
    public Integer getId(){
        return this.id;
    }


    //----------------------------------------------------------------------
    // GETTER(S) & SETTER(S) FOR DATA FIELDS
    //----------------------------------------------------------------------
    /**
     * Set the "dateDemande" field value
     * @param dateDemande
     */
	public void setDateDemande( String dateDemande )
    {
        this.dateDemande = dateDemande ;
    }
    /**
     * Get the "dateDemande" field value
     * @return the field value
     */
	public String getDateDemande()
    {
        return this.dateDemande;
    }

    /**
     * Set the "idAbonne" field value
     * @param idAbonne
     */
	public void setIdAbonne( Long idAbonne )
    {
        this.idAbonne = idAbonne ;
    }
    /**
     * Get the "idAbonne" field value
     * @return the field value
     */
	public Long getIdAbonne()
    {
        return this.idAbonne;
    }

    /**
     * Set the "idChannel" field value
     * @param idChannel
     */
	public void setIdChannel( Integer idChannel )
    {
        this.idChannel = idChannel ;
    }
    /**
     * Get the "idChannel" field value
     * @return the field value
     */
	public Integer getIdChannel()
    {
        return this.idChannel;
    }

    /**
     * Set the "statut" field value
     * @param statut
     */
	public void setStatut( Integer statut )
    {
        this.statut = statut ;
    }
    /**
     * Get the "statut" field value
     * @return the field value
     */
	public Integer getStatut()
    {
        return this.statut;
    }

    /**
     * Set the "commentaire" field value
     * @param commentaire
     */
	public void setCommentaire( String commentaire )
    {
        this.commentaire = commentaire ;
    }
    /**
     * Get the "commentaire" field value
     * @return the field value
     */
	public String getCommentaire()
    {
        return this.commentaire;
    }

    /**
     * Set the "dateDecision" field value
     * @param dateDecision
     */
	public void setDateDecision( String dateDecision )
    {
        this.dateDecision = dateDecision ;
    }
    /**
     * Get the "dateDecision" field value
     * @return the field value
     */
	public String getDateDecision()
    {
        return this.dateDecision;
    }

    /**
     * Set the "userDecisionId" field value
     * @param userDecisionId
     */
	public void setUserDecisionId( Integer userDecisionId )
    {
        this.userDecisionId = userDecisionId ;
    }
    /**
     * Get the "userDecisionId" field value
     * @return the field value
     */
	public Integer getUserDecisionId()
    {
        return this.userDecisionId;
    }

    /**
     * Set the "demandeState" field value
     * @param demandeState
     */
	public void setDemandeState( Integer demandeState )
    {
        this.demandeState = demandeState ;
    }
    /**
     * Get the "demandeState" field value
     * @return the field value
     */
	public Integer getDemandeState()
    {
        return this.demandeState;
    }

    /**
     * Set the "aDesouscrire" field value
     * @param aDesouscrire
     */
	public void setADesouscrire( Boolean aDesouscrire )
    {
        this.aDesouscrire = aDesouscrire ;
    }
    /**
     * Get the "aDesouscrire" field value
     * @return the field value
     */
	public Boolean getADesouscrire()
    {
        return this.aDesouscrire;
    }

    /**
     * Set the "dateDesouscription" field value
     * @param dateDesouscription
     */
	public void setDateDesouscription( String dateDesouscription )
    {
        this.dateDesouscription = dateDesouscription ;
    }
    /**
     * Get the "dateDesouscription" field value
     * @return the field value
     */
	public String getDateDesouscription()
    {
        return this.dateDesouscription;
    }

    public String getNomAbonne() {
        return nomAbonne;
    }

    public void setNomAbonne(String nomAbonne) {
        this.nomAbonne = nomAbonne;
    }

    public String getPrenomAbonne() {
        return prenomAbonne;
    }

    public void setPrenomAbonne(String prenomAbonne) {
        this.prenomAbonne = prenomAbonne;
    }

    public String getPhotoAbonne() {
        return photoAbonne;
    }

    public void setPhotoAbonne(String photoAbonne) {
        this.photoAbonne = photoAbonne;
    }

    public String getDateEnregAbonne() {
        return dateEnregAbonne;
    }

    public void setDateEnregAbonne(String dateEnregAbonne) {
        this.dateEnregAbonne = dateEnregAbonne;
    }

    public String getPaysAbonne() {
        return paysAbonne;
    }

    public void setPaysAbonne(String paysAbonne) {
        this.paysAbonne = paysAbonne;
    }

    public String getExtPhotoAbonne() {
        return extPhotoAbonne;
    }

    public void setExtPhotoAbonne(String extPhotoAbonne) {
        this.extPhotoAbonne = extPhotoAbonne;
    }

    public String getSesContactsAbonne() {
        return sesContactsAbonne;
    }

    public void setSesContactsAbonne(String sesContactsAbonne) {
        this.sesContactsAbonne = sesContactsAbonne;
    }
    /**
     * Set the "motifRejet" field value
     * @param motifRejet
     */
    public void setMotifRejet( String motifRejet )
    {
        this.motifRejet = motifRejet ;
    }
    /**
     * Get the "motifRejet" field value
     * @return the field value
     */
    public String getMotifRejet()
    {
        return this.motifRejet;
    }

    public float getLongitude() {
        return longitude;
    }

    public void setLongitude(float longitude) {
        this.longitude = longitude;
    }

    public float getLatitude() {
        return latitude;
    }

    public void setLatitude(float latitude) {
        this.latitude = latitude;
    }

    //----------------------------------------------------------------------
    // GETTERS & SETTERS FOR LINKS FIELD ( RELATIONSHIP )
    //----------------------------------------------------------------------


	//----------------------------------------------------------------------
    // equals METHOD
    //----------------------------------------------------------------------
	public boolean equals(Object obj) { 
		if ( this == obj ) return true ; 
		if ( obj == null ) return false ;
		if ( this.getClass() != obj.getClass() ) return false ; 
		AbonneDemandeDto other = (AbonneDemandeDto) obj; 
		//--- Attribute id
		if ( id == null ) { 
			if ( other.id != null ) 
				return false ; 
		} else if ( ! id.equals(other.id) ) 
			return false ; 
		return true; 
	} 


    //----------------------------------------------------------------------
    // toString METHOD
    //----------------------------------------------------------------------
    public String toString() {
		StringBuffer sb = new StringBuffer(); 
		sb.append(id); 
		sb.append("|"); 
		sb.append(dateDemande); 
		sb.append("|"); 
		sb.append(statut); 
		sb.append("|"); 
		sb.append(commentaire); 
		sb.append("|"); 
		sb.append(dateDecision); 
		sb.append("|"); 
		sb.append(userDecisionId); 
		sb.append("|"); 
		sb.append(demandeState); 
		sb.append("|"); 
		sb.append(aDesouscrire); 
		sb.append("|"); 
		sb.append(dateDesouscription); 
		sb.append("|"); 
		sb.append(motifRejet); 
        return sb.toString();
    }
}
