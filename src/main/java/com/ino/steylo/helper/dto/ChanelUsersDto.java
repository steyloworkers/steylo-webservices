
/*
 * Java dto for entity table chanel_users 
 * Created on dec ( Time 01:34:01 )
 * Generator tool : Telosys Tools Generator ( version 2.1.1 )
 */

package com.ino.steylo.helper.dto;

import java.util.Date;
import java.util.List;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;



/**
 * DTO for table "chanel_users"
 * 
 * @author ino
 *
 */
@JsonInclude(Include.NON_NULL)
public class ChanelUsersDto {

    private Integer    id                   ; // Primary Key

    private String     nom                  ;
    private String     prenoms              ;
    private String     login                ;
    private String     password             ;
    private String     oldPassword          ;
    private String     email                ;
    private Integer    statut               ;
	private String     dateCreation         ;
    private Integer    idChanel             ;
    private String     nomChanel            ;
    private String     logoChanel           ;
    private String     extLogoChanel        ;
    private Integer    createdBy            ;
	private String     updatedAt            ;
    private Integer    updatedBy            ;
	private String     deleteAt             ;
    private Integer    deleteBy             ;

    //----------------------------------------------------------------------
    // ENTITY LINKS FIELD ( RELATIONSHIP )
    //----------------------------------------------------------------------



    /**
     * Default constructor
     */
    public ChanelUsersDto()
    {
        super();
    }
    
    //----------------------------------------------------------------------
    // GETTER(S) & SETTER(S) FOR THE PRIMARY KEY 
    //----------------------------------------------------------------------
    /**
     * Set the "id" field value
     * @param id
     */
    public void setId( Integer id ){
        this.id = id;
    }
    /**
     * Get the "id" field value
     * @return the field value
     */
    public Integer getId(){
        return this.id;
    }


    //----------------------------------------------------------------------
    // GETTER(S) & SETTER(S) FOR DATA FIELDS
    //----------------------------------------------------------------------
    /**
     * Set the "nom" field value
     * @param nom
     */
	public void setNom( String nom )
    {
        this.nom = nom ;
    }
    /**
     * Get the "nom" field value
     * @return the field value
     */
	public String getNom()
    {
        return this.nom;
    }

    /**
     * Set the "prenoms" field value
     * @param prenoms
     */
	public void setPrenoms( String prenoms )
    {
        this.prenoms = prenoms ;
    }
    /**
     * Get the "prenoms" field value
     * @return the field value
     */
	public String getPrenoms()
    {
        return this.prenoms;
    }

    /**
     * Set the "login" field value
     * @param login
     */
	public void setLogin( String login )
    {
        this.login = login ;
    }
    /**
     * Get the "login" field value
     * @return the field value
     */
	public String getLogin()
    {
        return this.login;
    }

    /**
     * Set the "password" field value
     * @param password
     */
	public void setPassword( String password )
    {
        this.password = password ;
    }
    /**
     * Get the "password" field value
     * @return the field value
     */
	public String getPassword()
    {
        return this.password;
    }

    /**
     * Set the "email" field value
     * @param email
     */
	public void setEmail( String email )
    {
        this.email = email ;
    }
    /**
     * Get the "email" field value
     * @return the field value
     */
	public String getEmail()
    {
        return this.email;
    }

    /**
     * Set the "statut" field value
     * @param statut
     */
	public void setStatut( Integer statut )
    {
        this.statut = statut ;
    }
    /**
     * Get the "statut" field value
     * @return the field value
     */
	public Integer getStatut()
    {
        return this.statut;
    }

    /**
     * Set the "dateCreation" field value
     * @param dateCreation
     */
	public void setDateCreation( String dateCreation )
    {
        this.dateCreation = dateCreation ;
    }
    /**
     * Get the "dateCreation" field value
     * @return the field value
     */
	public String getDateCreation()
    {
        return this.dateCreation;
    }

    /**
     * Set the "idChanel" field value
     * @param idChanel
     */
	public void setIdChanel( Integer idChanel )
    {
        this.idChanel = idChanel ;
    }
    /**
     * Get the "idChanel" field value
     * @return the field value
     */
	public Integer getIdChanel()
    {
        return this.idChanel;
    }

    /**
     * Set the "createdBy" field value
     * @param createdBy
     */
	public void setCreatedBy( Integer createdBy )
    {
        this.createdBy = createdBy ;
    }
    /**
     * Get the "createdBy" field value
     * @return the field value
     */
	public Integer getCreatedBy()
    {
        return this.createdBy;
    }

    /**
     * Set the "updatedAt" field value
     * @param updatedAt
     */
	public void setUpdatedAt( String updatedAt )
    {
        this.updatedAt = updatedAt ;
    }
    /**
     * Get the "updatedAt" field value
     * @return the field value
     */
	public String getUpdatedAt()
    {
        return this.updatedAt;
    }

    /**
     * Set the "updatedBy" field value
     * @param updatedBy
     */
	public void setUpdatedBy( Integer updatedBy )
    {
        this.updatedBy = updatedBy ;
    }
    /**
     * Get the "updatedBy" field value
     * @return the field value
     */
	public Integer getUpdatedBy()
    {
        return this.updatedBy;
    }

    /**
     * Set the "deleteAt" field value
     * @param deleteAt
     */
	public void setDeleteAt( String deleteAt )
    {
        this.deleteAt = deleteAt ;
    }
    /**
     * Get the "deleteAt" field value
     * @return the field value
     */
	public String getDeleteAt()
    {
        return this.deleteAt;
    }

    /**
     * Set the "deleteBy" field value
     * @param deleteBy
     */
	public void setDeleteBy( Integer deleteBy )
    {
        this.deleteBy = deleteBy ;
    }
    /**
     * Get the "deleteBy" field value
     * @return the field value
     */
	public Integer getDeleteBy()
    {
        return this.deleteBy;
    }

    public String getOldPassword() {
        return oldPassword;
    }

    public void setOldPassword(String oldPassword) {
        this.oldPassword = oldPassword;
    }

    public String getNomChanel() {
        return nomChanel;
    }

    public void setNomChanel(String nomChanel) {
        this.nomChanel = nomChanel;
    }

    public String getLogoChanel() {
        return logoChanel;
    }

    public void setLogoChanel(String logoChanel) {
        this.logoChanel = logoChanel;
    }

    public String getExtLogoChanel() {
        return extLogoChanel;
    }

    public void setExtLogoChanel(String extLogoChanel) {
        this.extLogoChanel = extLogoChanel;
    }

    //----------------------------------------------------------------------
    // GETTERS & SETTERS FOR LINKS FIELD ( RELATIONSHIP )
    //----------------------------------------------------------------------


	//----------------------------------------------------------------------
    // equals METHOD
    //----------------------------------------------------------------------
	public boolean equals(Object obj) { 
		if ( this == obj ) return true ; 
		if ( obj == null ) return false ;
		if ( this.getClass() != obj.getClass() ) return false ; 
		ChanelUsersDto other = (ChanelUsersDto) obj; 
		//--- Attribute id
		if ( id == null ) { 
			if ( other.id != null ) 
				return false ; 
		} else if ( ! id.equals(other.id) ) 
			return false ; 
		return true; 
	} 


    //----------------------------------------------------------------------
    // toString METHOD
    //----------------------------------------------------------------------
    public String toString() {
		StringBuffer sb = new StringBuffer(); 
		sb.append(id); 
		sb.append("|"); 
		sb.append(nom); 
		sb.append("|"); 
		sb.append(prenoms); 
		sb.append("|"); 
		sb.append(login); 
		sb.append("|"); 
		sb.append(password); 
		sb.append("|"); 
		sb.append(email); 
		sb.append("|"); 
		sb.append(statut); 
		sb.append("|"); 
		sb.append(dateCreation); 
		sb.append("|"); 
		sb.append(updatedAt); 
		sb.append("|"); 
		sb.append(deleteAt); 
        return sb.toString();
    }
}
