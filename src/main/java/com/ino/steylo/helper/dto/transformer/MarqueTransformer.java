

/*
 * Java transformer for entity table marque 
 * Created on dec ( Time 18:46:56 )
 * Generator tool : Telosys Tools Generator ( version 2.1.1 )
 */

package com.ino.steylo.helper.dto.transformer;

import java.text.ParseException;
import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import com.ino.steylo.helper.dto.*;
import com.ino.steylo.dao.entity.*;


/**
TRANSFORMER for table "marque"
 * 
 * @author ino
 *
 */
@Mapper
public interface MarqueTransformer {

	MarqueTransformer INSTANCE = Mappers.getMapper(MarqueTransformer.class);

	@Mappings({
	})
	MarqueDto toDto(Marque entity) throws ParseException;

    List<MarqueDto> toDtos(List<Marque> entities) throws ParseException;

	@Mappings({
		@Mapping(source="dto.id", target="id"),
		@Mapping(source="dto.libelle", target="libelle"),
		@Mapping(source="dto.slogan", target="slogan"),
		@Mapping(source="dto.statut", target="statut"),
	})
    Marque toEntity(MarqueDto dto) throws ParseException;

    //List<Marque> toEntities(List<MarqueDto> dtos) throws ParseException;

}
