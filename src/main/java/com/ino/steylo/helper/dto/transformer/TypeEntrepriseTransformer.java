

/*
 * Java transformer for entity table type_entreprise 
 * Created on dec ( Time 18:46:58 )
 * Generator tool : Telosys Tools Generator ( version 2.1.1 )
 */

package com.ino.steylo.helper.dto.transformer;

import java.text.ParseException;
import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import com.ino.steylo.helper.dto.*;
import com.ino.steylo.dao.entity.*;


/**
TRANSFORMER for table "type_entreprise"
 * 
 * @author ino
 *
 */
@Mapper
public interface TypeEntrepriseTransformer {

	TypeEntrepriseTransformer INSTANCE = Mappers.getMapper(TypeEntrepriseTransformer.class);

	@Mappings({
	})
	TypeEntrepriseDto toDto(TypeEntreprise entity) throws ParseException;

    List<TypeEntrepriseDto> toDtos(List<TypeEntreprise> entities) throws ParseException;

	@Mappings({
		@Mapping(source="dto.id", target="id"),
		@Mapping(source="dto.libelle", target="libelle"),
	})
    TypeEntreprise toEntity(TypeEntrepriseDto dto) throws ParseException;

    //List<TypeEntreprise> toEntities(List<TypeEntrepriseDto> dtos) throws ParseException;

}
