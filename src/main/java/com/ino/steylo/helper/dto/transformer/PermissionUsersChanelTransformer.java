

/*
 * Java transformer for entity table permission_users_chanel 
 * Created on dec ( Time 18:46:57 )
 * Generator tool : Telosys Tools Generator ( version 2.1.1 )
 */

package com.ino.steylo.helper.dto.transformer;

import java.text.ParseException;
import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import com.ino.steylo.helper.dto.*;
import com.ino.steylo.dao.entity.*;


/**
TRANSFORMER for table "permission_users_chanel"
 * 
 * @author ino
 *
 */
@Mapper
public interface PermissionUsersChanelTransformer {

	PermissionUsersChanelTransformer INSTANCE = Mappers.getMapper(PermissionUsersChanelTransformer.class);

	@Mappings({
	})
	PermissionUsersChanelDto toDto(PermissionUsersChanel entity) throws ParseException;

    List<PermissionUsersChanelDto> toDtos(List<PermissionUsersChanel> entities) throws ParseException;

	@Mappings({
		@Mapping(source="dto.id", target="id"),
		@Mapping(source="dto.idPermission", target="idPermission"),
		@Mapping(source="dto.idChanelUser", target="idChanelUser"),
	})
    PermissionUsersChanel toEntity(PermissionUsersChanelDto dto) throws ParseException;

    //List<PermissionUsersChanel> toEntities(List<PermissionUsersChanelDto> dtos) throws ParseException;

}
