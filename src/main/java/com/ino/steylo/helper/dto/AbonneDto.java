
/*
 * Java dto for entity table abonne 
 * Created on dec ( Time 21:51:33 )
 * Generator tool : Telosys Tools Generator ( version 2.1.1 )
 */

package com.ino.steylo.helper.dto;

import java.util.Date;
import java.util.List;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;



/**
 * DTO for table "abonne"
 * 
 * @author ino
 *
 */
@JsonInclude(Include.NON_NULL)
public class AbonneDto {

    private Long       id                   ; // Primary Key

	private String     dateEnreg            ;
    private String     nom                  ;
    private String     prenoms              ;
    private String     sexe                 ;
    private String     dateNaissance        ;
    private String     profession           ;
    private String     mobileNumber         ;
    private String     address              ;
    private Integer     pays                 ;
    private String     ville                ;
    private String     nationalite          ;
    private Integer    idTypeUtilisateur    ;
    private String     metadata             ;
    private String     login                ;
    private String     password             ;
    private String     email                ;
    private Boolean    isActive             ;
    private Float      longitude            ;
    private Float      latitude             ;
    private String     photoIdentite        ;
    private String     photoPieceRecto      ;
    private String     photoPieceVerso      ;
    private Integer    communaute           ;
    private String     extPhotoIdentite     ;
    private String     extPhotoRecto        ;
    private String     extPhotoVerso        ;
    private String     verifKey             ;
	private String     seen                 ;

    //----------------------------------------------------------------------
    // ENTITY LINKS FIELD ( RELATIONSHIP )
    //----------------------------------------------------------------------



    /**
     * Default constructor
     */
    public AbonneDto()
    {
        super();
    }
    
    //----------------------------------------------------------------------
    // GETTER(S) & SETTER(S) FOR THE PRIMARY KEY 
    //----------------------------------------------------------------------
    /**
     * Set the "id" field value
     * @param id
     */
    public void setId( Long id ){
        this.id = id;
    }
    /**
     * Get the "id" field value
     * @return the field value
     */
    public Long getId(){
        return this.id;
    }


    //----------------------------------------------------------------------
    // GETTER(S) & SETTER(S) FOR DATA FIELDS
    //----------------------------------------------------------------------
    /**
     * Set the "dateEnreg" field value
     * @param dateEnreg
     */
	public void setDateEnreg( String dateEnreg )
    {
        this.dateEnreg = dateEnreg ;
    }
    /**
     * Get the "dateEnreg" field value
     * @return the field value
     */
	public String getDateEnreg()
    {
        return this.dateEnreg;
    }

    /**
     * Set the "nom" field value
     * @param nom
     */
	public void setNom( String nom )
    {
        this.nom = nom ;
    }
    /**
     * Get the "nom" field value
     * @return the field value
     */
	public String getNom()
    {
        return this.nom;
    }

    /**
     * Set the "prenoms" field value
     * @param prenoms
     */
	public void setPrenoms( String prenoms )
    {
        this.prenoms = prenoms ;
    }
    /**
     * Get the "prenoms" field value
     * @return the field value
     */
	public String getPrenoms()
    {
        return this.prenoms;
    }

    /**
     * Set the "sexe" field value
     * @param sexe
     */
	public void setSexe( String sexe )
    {
        this.sexe = sexe ;
    }
    /**
     * Get the "sexe" field value
     * @return the field value
     */
	public String getSexe()
    {
        return this.sexe;
    }

    /**
     * Set the "dateNaissance" field value
     * @param dateNaissance
     */
	public void setDateNaissance( String dateNaissance )
    {
        this.dateNaissance = dateNaissance ;
    }
    /**
     * Get the "dateNaissance" field value
     * @return the field value
     */
	public String getDateNaissance()
    {
        return this.dateNaissance;
    }

    /**
     * Set the "profession" field value
     * @param profession
     */
	public void setProfession( String profession )
    {
        this.profession = profession ;
    }
    /**
     * Get the "profession" field value
     * @return the field value
     */
	public String getProfession()
    {
        return this.profession;
    }

    /**
     * Set the "mobileNumber" field value
     * @param mobileNumber
     */
	public void setMobileNumber( String mobileNumber )
    {
        this.mobileNumber = mobileNumber ;
    }
    /**
     * Get the "mobileNumber" field value
     * @return the field value
     */
	public String getMobileNumber()
    {
        return this.mobileNumber;
    }

    /**
     * Set the "address" field value
     * @param address
     */
	public void setAddress( String address )
    {
        this.address = address ;
    }
    /**
     * Get the "address" field value
     * @return the field value
     */
	public String getAddress()
    {
        return this.address;
    }

    /**
     * Set the "pays" field value
     * @param pays
     */
	public void setPays( Integer pays )
    {
        this.pays = pays ;
    }
    /**
     * Get the "pays" field value
     * @return the field value
     */
	public Integer getPays()
    {
        return this.pays;
    }

    /**
     * Set the "ville" field value
     * @param ville
     */
	public void setVille( String ville )
    {
        this.ville = ville ;
    }
    /**
     * Get the "ville" field value
     * @return the field value
     */
	public String getVille()
    {
        return this.ville;
    }

    /**
     * Set the "nationalite" field value
     * @param nationalite
     */
	public void setNationalite( String nationalite )
    {
        this.nationalite = nationalite ;
    }
    /**
     * Get the "nationalite" field value
     * @return the field value
     */
	public String getNationalite()
    {
        return this.nationalite;
    }

    /**
     * Set the "idTypeUtilisateur" field value
     * @param idTypeUtilisateur
     */
	public void setIdTypeUtilisateur( Integer idTypeUtilisateur )
    {
        this.idTypeUtilisateur = idTypeUtilisateur ;
    }
    /**
     * Get the "idTypeUtilisateur" field value
     * @return the field value
     */
	public Integer getIdTypeUtilisateur()
    {
        return this.idTypeUtilisateur;
    }

    /**
     * Set the "metadata" field value
     * @param metadata
     */
	public void setMetadata( String metadata )
    {
        this.metadata = metadata ;
    }
    /**
     * Get the "metadata" field value
     * @return the field value
     */
	public String getMetadata()
    {
        return this.metadata;
    }

    /**
     * Set the "login" field value
     * @param login
     */
	public void setLogin( String login )
    {
        this.login = login ;
    }
    /**
     * Get the "login" field value
     * @return the field value
     */
	public String getLogin()
    {
        return this.login;
    }

    /**
     * Set the "password" field value
     * @param password
     */
	public void setPassword( String password )
    {
        this.password = password ;
    }
    /**
     * Get the "password" field value
     * @return the field value
     */
	public String getPassword()
    {
        return this.password;
    }

    /**
     * Set the "email" field value
     * @param email
     */
	public void setEmail( String email )
    {
        this.email = email ;
    }
    /**
     * Get the "email" field value
     * @return the field value
     */
	public String getEmail()
    {
        return this.email;
    }

    /**
     * Set the "isActive" field value
     * @param isActive
     */
	public void setIsActive( Boolean isActive )
    {
        this.isActive = isActive ;
    }
    /**
     * Get the "isActive" field value
     * @return the field value
     */
	public Boolean getIsActive()
    {
        return this.isActive;
    }

    /**
     * Set the "longitude" field value
     * @param longitude
     */
	public void setLongitude( Float longitude )
    {
        this.longitude = longitude ;
    }
    /**
     * Get the "longitude" field value
     * @return the field value
     */
	public Float getLongitude()
    {
        return this.longitude;
    }

    /**
     * Set the "latitude" field value
     * @param latitude
     */
	public void setLatitude( Float latitude )
    {
        this.latitude = latitude ;
    }
    /**
     * Get the "latitude" field value
     * @return the field value
     */
	public Float getLatitude()
    {
        return this.latitude;
    }

    /**
     * Set the "photoIdentite" field value
     * @param photoIdentite
     */
	public void setPhotoIdentite( String photoIdentite )
    {
        this.photoIdentite = photoIdentite ;
    }
    /**
     * Get the "photoIdentite" field value
     * @return the field value
     */
	public String getPhotoIdentite()
    {
        return this.photoIdentite;
    }

    /**
     * Set the "photoPieceRecto" field value
     * @param photoPieceRecto
     */
	public void setPhotoPieceRecto( String photoPieceRecto )
    {
        this.photoPieceRecto = photoPieceRecto ;
    }
    /**
     * Get the "photoPieceRecto" field value
     * @return the field value
     */
	public String getPhotoPieceRecto()
    {
        return this.photoPieceRecto;
    }

    /**
     * Set the "photoPieceVerso" field value
     * @param photoPieceVerso
     */
	public void setPhotoPieceVerso( String photoPieceVerso )
    {
        this.photoPieceVerso = photoPieceVerso ;
    }
    /**
     * Get the "photoPieceVerso" field value
     * @return the field value
     */
	public String getPhotoPieceVerso()
    {
        return this.photoPieceVerso;
    }

    /**
     * Set the "communaute" field value
     * @param communaute
     */
	public void setCommunaute( Integer communaute )
    {
        this.communaute = communaute ;
    }
    /**
     * Get the "communaute" field value
     * @return the field value
     */
	public Integer getCommunaute()
    {
        return this.communaute;
    }

    /**
     * Set the "extPhotoIdentite" field value
     * @param extPhotoIdentite
     */
	public void setExtPhotoIdentite( String extPhotoIdentite )
    {
        this.extPhotoIdentite = extPhotoIdentite ;
    }
    /**
     * Get the "extPhotoIdentite" field value
     * @return the field value
     */
	public String getExtPhotoIdentite()
    {
        return this.extPhotoIdentite;
    }

    /**
     * Set the "extPhotoRecto" field value
     * @param extPhotoRecto
     */
	public void setExtPhotoRecto( String extPhotoRecto )
    {
        this.extPhotoRecto = extPhotoRecto ;
    }
    /**
     * Get the "extPhotoRecto" field value
     * @return the field value
     */
	public String getExtPhotoRecto()
    {
        return this.extPhotoRecto;
    }

    /**
     * Set the "extPhotoVerso" field value
     * @param extPhotoVerso
     */
	public void setExtPhotoVerso( String extPhotoVerso )
    {
        this.extPhotoVerso = extPhotoVerso ;
    }
    /**
     * Get the "extPhotoVerso" field value
     * @return the field value
     */
	public String getExtPhotoVerso()
    {
        return this.extPhotoVerso;
    }

    /**
     * Set the "verifKey" field value
     * @param verifKey
     */
	public void setVerifKey( String verifKey )
    {
        this.verifKey = verifKey ;
    }
    /**
     * Get the "verifKey" field value
     * @return the field value
     */
	public String getVerifKey()
    {
        return this.verifKey;
    }

    /**
     * Set the "seen" field value
     * @param seen
     */
	public void setSeen( String seen )
    {
        this.seen = seen ;
    }
    /**
     * Get the "seen" field value
     * @return the field value
     */
	public String getSeen()
    {
        return this.seen;
    }


    //----------------------------------------------------------------------
    // GETTERS & SETTERS FOR LINKS FIELD ( RELATIONSHIP )
    //----------------------------------------------------------------------


	//----------------------------------------------------------------------
    // equals METHOD
    //----------------------------------------------------------------------
	public boolean equals(Object obj) { 
		if ( this == obj ) return true ; 
		if ( obj == null ) return false ;
		if ( this.getClass() != obj.getClass() ) return false ; 
		AbonneDto other = (AbonneDto) obj; 
		//--- Attribute id
		if ( id == null ) { 
			if ( other.id != null ) 
				return false ; 
		} else if ( ! id.equals(other.id) ) 
			return false ; 
		return true; 
	} 


    //----------------------------------------------------------------------
    // toString METHOD
    //----------------------------------------------------------------------
    public String toString() {
		StringBuffer sb = new StringBuffer(); 
		sb.append(id); 
		sb.append("|"); 
		sb.append(dateEnreg); 
		sb.append("|"); 
		sb.append(nom); 
		sb.append("|"); 
		sb.append(prenoms); 
		sb.append("|"); 
		sb.append(sexe); 
		sb.append("|"); 
		sb.append(dateNaissance); 
		sb.append("|"); 
		sb.append(profession); 
		sb.append("|"); 
		sb.append(mobileNumber); 
		sb.append("|"); 
		sb.append(address); 
		sb.append("|"); 
		sb.append(ville); 
		sb.append("|"); 
		sb.append(nationalite); 
		sb.append("|"); 
		sb.append(idTypeUtilisateur); 
		sb.append("|"); 
		sb.append(metadata); 
		sb.append("|"); 
		sb.append(login); 
		sb.append("|"); 
		sb.append(password); 
		sb.append("|"); 
		sb.append(email); 
		sb.append("|"); 
		sb.append(isActive); 
		sb.append("|"); 
		sb.append(longitude); 
		sb.append("|"); 
		sb.append(latitude); 
		sb.append("|"); 
		sb.append(photoIdentite); 
		sb.append("|"); 
		sb.append(photoPieceRecto); 
		sb.append("|"); 
		sb.append(photoPieceVerso); 
		sb.append("|"); 
		sb.append(communaute); 
		sb.append("|"); 
		sb.append(extPhotoIdentite); 
		sb.append("|"); 
		sb.append(extPhotoRecto); 
		sb.append("|"); 
		sb.append(extPhotoVerso); 
		sb.append("|"); 
		sb.append(verifKey); 
		sb.append("|"); 
		sb.append(seen); 
        return sb.toString();
    }
}
