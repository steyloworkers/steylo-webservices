
/*
 * Java dto for entity table chanel 
 * Created on dec ( Time 21:32:52 )
 * Generator tool : Telosys Tools Generator ( version 2.1.1 )
 */

package com.ino.steylo.helper.dto;

import java.util.Date;
import java.util.List;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;



/**
 * DTO for table "chanel"
 * 
 * @author ino
 *
 */
@JsonInclude(Include.NON_NULL)
public class ChanelDto {

    private Integer    id                   ; // Primary Key

    private String     formeJuridique       ;
    private String     nomChanel            ;
    private String     sigle                ;
    private String     numeroRegistre       ;
    private String     numeroImmatriculation ;
    private Integer    idSecteurActivite    ;
    private String     descriptionSecteur   ;
    private String     siteWeb              ;
    private String     email                ;
    private String     phoneNumber          ;
    private String     fax                  ;
    private Integer    idPays               ;
    private String     etat                 ;
    private String     ville                ;
    private String     codePostal           ;
    private String     address              ;
    private String     logo                 ;
    private String     extLogo              ;
    private String     codeGuichet          ;
    private String     codeBanque           ;
    private String     numeroCompte         ;
    private String     swift                ;
    private String     cleRib               ;
    private String     rib                  ;
    private String     iban                 ;
	private String     rowDateUpdate        ;
	private String     logoDateUpdate       ;

	private ChanelUsersDto chanelUsers      ;

    //----------------------------------------------------------------------
    // ENTITY LINKS FIELD ( RELATIONSHIP )
    //----------------------------------------------------------------------



    /**
     * Default constructor
     */
    public ChanelDto()
    {
        super();
    }
    
    //----------------------------------------------------------------------
    // GETTER(S) & SETTER(S) FOR THE PRIMARY KEY 
    //----------------------------------------------------------------------
    /**
     * Set the "id" field value
     * @param id
     */
    public void setId( Integer id ){
        this.id = id;
    }
    /**
     * Get the "id" field value
     * @return the field value
     */
    public Integer getId(){
        return this.id;
    }


    //----------------------------------------------------------------------
    // GETTER(S) & SETTER(S) FOR DATA FIELDS
    //----------------------------------------------------------------------
    /**
     * Set the "formeJuridique" field value
     * @param formeJuridique
     */
	public void setFormeJuridique( String formeJuridique )
    {
        this.formeJuridique = formeJuridique ;
    }
    /**
     * Get the "formeJuridique" field value
     * @return the field value
     */
	public String getFormeJuridique()
    {
        return this.formeJuridique;
    }

    /**
     * Set the "nomChanel" field value
     * @param nomChanel
     */
	public void setNomChanel( String nomChanel )
    {
        this.nomChanel = nomChanel ;
    }
    /**
     * Get the "nomChanel" field value
     * @return the field value
     */
	public String getNomChanel()
    {
        return this.nomChanel;
    }

    /**
     * Set the "sigle" field value
     * @param sigle
     */
	public void setSigle( String sigle )
    {
        this.sigle = sigle ;
    }
    /**
     * Get the "sigle" field value
     * @return the field value
     */
	public String getSigle()
    {
        return this.sigle;
    }

    /**
     * Set the "numeroRegistre" field value
     * @param numeroRegistre
     */
	public void setNumeroRegistre( String numeroRegistre )
    {
        this.numeroRegistre = numeroRegistre ;
    }
    /**
     * Get the "numeroRegistre" field value
     * @return the field value
     */
	public String getNumeroRegistre()
    {
        return this.numeroRegistre;
    }

    /**
     * Set the "numeroImmatriculation" field value
     * @param numeroImmatriculation
     */
	public void setNumeroImmatriculation( String numeroImmatriculation )
    {
        this.numeroImmatriculation = numeroImmatriculation ;
    }
    /**
     * Get the "numeroImmatriculation" field value
     * @return the field value
     */
	public String getNumeroImmatriculation()
    {
        return this.numeroImmatriculation;
    }

    /**
     * Set the "idSecteurActivite" field value
     * @param idSecteurActivite
     */
	public void setIdSecteurActivite( Integer idSecteurActivite )
    {
        this.idSecteurActivite = idSecteurActivite ;
    }
    /**
     * Get the "idSecteurActivite" field value
     * @return the field value
     */
	public Integer getIdSecteurActivite()
    {
        return this.idSecteurActivite;
    }

    /**
     * Set the "descriptionSecteur" field value
     * @param descriptionSecteur
     */
	public void setDescriptionSecteur( String descriptionSecteur )
    {
        this.descriptionSecteur = descriptionSecteur ;
    }
    /**
     * Get the "descriptionSecteur" field value
     * @return the field value
     */
	public String getDescriptionSecteur()
    {
        return this.descriptionSecteur;
    }

    /**
     * Set the "siteWeb" field value
     * @param siteWeb
     */
	public void setSiteWeb( String siteWeb )
    {
        this.siteWeb = siteWeb ;
    }
    /**
     * Get the "siteWeb" field value
     * @return the field value
     */
	public String getSiteWeb()
    {
        return this.siteWeb;
    }

    /**
     * Set the "email" field value
     * @param email
     */
	public void setEmail( String email )
    {
        this.email = email ;
    }
    /**
     * Get the "email" field value
     * @return the field value
     */
	public String getEmail()
    {
        return this.email;
    }

    /**
     * Set the "phoneNumber" field value
     * @param phoneNumber
     */
	public void setPhoneNumber( String phoneNumber )
    {
        this.phoneNumber = phoneNumber ;
    }
    /**
     * Get the "phoneNumber" field value
     * @return the field value
     */
	public String getPhoneNumber()
    {
        return this.phoneNumber;
    }

    /**
     * Set the "fax" field value
     * @param fax
     */
	public void setFax( String fax )
    {
        this.fax = fax ;
    }
    /**
     * Get the "fax" field value
     * @return the field value
     */
	public String getFax()
    {
        return this.fax;
    }

    /**
     * Set the "idPays" field value
     * @param idPays
     */
	public void setIdPays( Integer idPays )
    {
        this.idPays = idPays ;
    }
    /**
     * Get the "idPays" field value
     * @return the field value
     */
	public Integer getIdPays()
    {
        return this.idPays;
    }

    /**
     * Set the "etat" field value
     * @param etat
     */
	public void setEtat( String etat )
    {
        this.etat = etat ;
    }
    /**
     * Get the "etat" field value
     * @return the field value
     */
	public String getEtat()
    {
        return this.etat;
    }

    /**
     * Set the "ville" field value
     * @param ville
     */
	public void setVille( String ville )
    {
        this.ville = ville ;
    }
    /**
     * Get the "ville" field value
     * @return the field value
     */
	public String getVille()
    {
        return this.ville;
    }

    /**
     * Set the "codePostal" field value
     * @param codePostal
     */
	public void setCodePostal( String codePostal )
    {
        this.codePostal = codePostal ;
    }
    /**
     * Get the "codePostal" field value
     * @return the field value
     */
	public String getCodePostal()
    {
        return this.codePostal;
    }

    /**
     * Set the "address" field value
     * @param address
     */
	public void setAddress( String address )
    {
        this.address = address ;
    }
    /**
     * Get the "address" field value
     * @return the field value
     */
	public String getAddress()
    {
        return this.address;
    }

    /**
     * Set the "logo" field value
     * @param logo
     */
	public void setLogo( String logo )
    {
        this.logo = logo ;
    }
    /**
     * Get the "logo" field value
     * @return the field value
     */
	public String getLogo()
    {
        return this.logo;
    }

    /**
     * Set the "extLogo" field value
     * @param extLogo
     */
	public void setExtLogo( String extLogo )
    {
        this.extLogo = extLogo ;
    }
    /**
     * Get the "extLogo" field value
     * @return the field value
     */
	public String getExtLogo()
    {
        return this.extLogo;
    }

    /**
     * Set the "codeGuichet" field value
     * @param codeGuichet
     */
	public void setCodeGuichet( String codeGuichet )
    {
        this.codeGuichet = codeGuichet ;
    }
    /**
     * Get the "codeGuichet" field value
     * @return the field value
     */
	public String getCodeGuichet()
    {
        return this.codeGuichet;
    }

    /**
     * Set the "codeBanque" field value
     * @param codeBanque
     */
	public void setCodeBanque( String codeBanque )
    {
        this.codeBanque = codeBanque ;
    }
    /**
     * Get the "codeBanque" field value
     * @return the field value
     */
	public String getCodeBanque()
    {
        return this.codeBanque;
    }

    /**
     * Set the "numeroCompte" field value
     * @param numeroCompte
     */
	public void setNumeroCompte( String numeroCompte )
    {
        this.numeroCompte = numeroCompte ;
    }
    /**
     * Get the "numeroCompte" field value
     * @return the field value
     */
	public String getNumeroCompte()
    {
        return this.numeroCompte;
    }

    /**
     * Set the "swift" field value
     * @param swift
     */
	public void setSwift( String swift )
    {
        this.swift = swift ;
    }
    /**
     * Get the "swift" field value
     * @return the field value
     */
	public String getSwift()
    {
        return this.swift;
    }

    /**
     * Set the "cleRib" field value
     * @param cleRib
     */
	public void setCleRib( String cleRib )
    {
        this.cleRib = cleRib ;
    }
    /**
     * Get the "cleRib" field value
     * @return the field value
     */
	public String getCleRib()
    {
        return this.cleRib;
    }

    /**
     * Set the "rib" field value
     * @param rib
     */
	public void setRib( String rib )
    {
        this.rib = rib ;
    }
    /**
     * Get the "rib" field value
     * @return the field value
     */
	public String getRib()
    {
        return this.rib;
    }

    /**
     * Set the "iban" field value
     * @param iban
     */
	public void setIban( String iban )
    {
        this.iban = iban ;
    }
    /**
     * Get the "iban" field value
     * @return the field value
     */
	public String getIban()
    {
        return this.iban;
    }

    /**
     * Set the "rowDateUpdate" field value
     * @param rowDateUpdate
     */
	public void setRowDateUpdate( String rowDateUpdate )
    {
        this.rowDateUpdate = rowDateUpdate ;
    }
    /**
     * Get the "rowDateUpdate" field value
     * @return the field value
     */
	public String getRowDateUpdate()
    {
        return this.rowDateUpdate;
    }

    /**
     * Set the "logoDateUpdate" field value
     * @param logoDateUpdate
     */
	public void setLogoDateUpdate( String logoDateUpdate )
    {
        this.logoDateUpdate = logoDateUpdate ;
    }
    /**
     * Get the "logoDateUpdate" field value
     * @return the field value
     */
	public String getLogoDateUpdate()
    {
        return this.logoDateUpdate;
    }

    public ChanelUsersDto getChanelUsers() {
        return chanelUsers;
    }

    public void setChanelUsers(ChanelUsersDto chanelUsers) {
        this.chanelUsers = chanelUsers;
    }

    //----------------------------------------------------------------------
    // GETTERS & SETTERS FOR LINKS FIELD ( RELATIONSHIP )
    //----------------------------------------------------------------------


	//----------------------------------------------------------------------
    // equals METHOD
    //----------------------------------------------------------------------
	public boolean equals(Object obj) { 
		if ( this == obj ) return true ; 
		if ( obj == null ) return false ;
		if ( this.getClass() != obj.getClass() ) return false ; 
		ChanelDto other = (ChanelDto) obj; 
		//--- Attribute id
		if ( id == null ) { 
			if ( other.id != null ) 
				return false ; 
		} else if ( ! id.equals(other.id) ) 
			return false ; 
		return true; 
	} 


    //----------------------------------------------------------------------
    // toString METHOD
    //----------------------------------------------------------------------
    public String toString() {
		StringBuffer sb = new StringBuffer(); 
		sb.append(id); 
		sb.append("|"); 
		sb.append(formeJuridique); 
		sb.append("|"); 
		sb.append(nomChanel); 
		sb.append("|"); 
		sb.append(sigle); 
		sb.append("|"); 
		sb.append(numeroRegistre); 
		sb.append("|"); 
		sb.append(numeroImmatriculation); 
		sb.append("|"); 
		sb.append(descriptionSecteur); 
		sb.append("|"); 
		sb.append(siteWeb); 
		sb.append("|"); 
		sb.append(email); 
		sb.append("|"); 
		sb.append(phoneNumber); 
		sb.append("|"); 
		sb.append(fax); 
		sb.append("|"); 
		sb.append(etat); 
		sb.append("|"); 
		sb.append(ville); 
		sb.append("|"); 
		sb.append(codePostal); 
		sb.append("|"); 
		sb.append(address); 
		sb.append("|"); 
		sb.append(logo); 
		sb.append("|"); 
		sb.append(extLogo); 
		sb.append("|"); 
		sb.append(codeGuichet); 
		sb.append("|"); 
		sb.append(codeBanque); 
		sb.append("|"); 
		sb.append(numeroCompte); 
		sb.append("|"); 
		sb.append(swift); 
		sb.append("|"); 
		sb.append(cleRib); 
		sb.append("|"); 
		sb.append(rib); 
		sb.append("|"); 
		sb.append(iban); 
		sb.append("|"); 
		sb.append(rowDateUpdate); 
		sb.append("|"); 
		sb.append(logoDateUpdate); 
        return sb.toString();
    }
}
