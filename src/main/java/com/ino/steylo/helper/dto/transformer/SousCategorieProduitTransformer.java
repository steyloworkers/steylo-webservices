

/*
 * Java transformer for entity table sous_categorie_produit 
 * Created on dec ( Time 18:46:57 )
 * Generator tool : Telosys Tools Generator ( version 2.1.1 )
 */

package com.ino.steylo.helper.dto.transformer;

import java.text.ParseException;
import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import com.ino.steylo.helper.dto.*;
import com.ino.steylo.dao.entity.*;


/**
TRANSFORMER for table "sous_categorie_produit"
 * 
 * @author ino
 *
 */
@Mapper
public interface SousCategorieProduitTransformer {

	SousCategorieProduitTransformer INSTANCE = Mappers.getMapper(SousCategorieProduitTransformer.class);

	@Mappings({
		@Mapping(source="entity.categorieProduit.id", target="idCategorie"),
	})
	SousCategorieProduitDto toDto(SousCategorieProduit entity) throws ParseException;

    List<SousCategorieProduitDto> toDtos(List<SousCategorieProduit> entities) throws ParseException;

	@Mappings({
		@Mapping(source="dto.id", target="id"),
		@Mapping(source="dto.libelle", target="libelle"),
		@Mapping(source="categorieProduit", target="categorieProduit"),
	})
    SousCategorieProduit toEntity(SousCategorieProduitDto dto, CategorieProduit categorieProduit) throws ParseException;

    //List<SousCategorieProduit> toEntities(List<SousCategorieProduitDto> dtos) throws ParseException;

}
