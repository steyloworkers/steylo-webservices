
package com.ino.steylo.helper.contrat;

import java.util.List;
import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.ino.steylo.helper.dto.*;

@XmlRootElement
@JsonInclude(Include.NON_NULL)
public class Request extends RequestBase {
	
	protected AbonneDto                                              dataAbonne                    ;                     
	protected List<AbonneDto>                                        datasAbonne                   ;                     
	protected AbonneChanelDto                                        dataAbonneChanel              ;                     
	protected List<AbonneChanelDto>                                  datasAbonneChanel             ;                     
	protected AbonneDemandeDto                                       dataAbonneDemande             ;                     
	protected List<AbonneDemandeDto>                                 datasAbonneDemande            ;                     
	protected CategorieProduitDto                                    dataCategorieProduit          ;                     
	protected List<CategorieProduitDto>                              datasCategorieProduit         ;                     
	protected ChanelDto                                              dataChanel                    ;                     
	protected List<ChanelDto>                                        datasChanel                   ;                     
	protected ChanelPermissionsDto                                   dataChanelPermissions         ;                     
	protected List<ChanelPermissionsDto>                             datasChanelPermissions        ;                     
	protected ChanelUserPermissionsDto                               dataChanelUserPermissions     ;                     
	protected List<ChanelUserPermissionsDto>                         datasChanelUserPermissions    ;                     
	protected ChanelUsersDto                                         dataChanelUsers               ;                     
	protected List<ChanelUsersDto>                                   datasChanelUsers              ;                     
	protected CountryDto                                             dataCountry                   ;                     
	protected List<CountryDto>                                       datasCountry                  ;                     
	protected ImageProduitDto                                        dataImageProduit              ;                     
	protected List<ImageProduitDto>                                  datasImageProduit             ;                     
	protected MarqueDto                                              dataMarque                    ;                     
	protected List<MarqueDto>                                        datasMarque                   ;
	protected PermissionActionsDto                                   dataPermissionActions         ;                     
	protected List<PermissionActionsDto>                             datasPermissionActions        ;                     
	protected PermissionProfilDto                                    dataPermissionProfil          ;                     
	protected List<PermissionProfilDto>                              datasPermissionProfil         ;                     
	protected PermissionUsersChanelDto                               dataPermissionUsersChanel     ;                     
	protected List<PermissionUsersChanelDto>                         datasPermissionUsersChanel    ;                     
	protected ProduitDto                                             dataProduit                   ;                     
	protected List<ProduitDto>                                       datasProduit                  ;                     
	protected SecteurActiviteDto                                     dataSecteurActivite           ;                     
	protected List<SecteurActiviteDto>                               datasSecteurActivite          ;                     
	protected SousCategorieProduitDto                                dataSousCategorieProduit      ;                     
	protected List<SousCategorieProduitDto>                          datasSousCategorieProduit     ;                     
	protected TypeEntrepriseDto                                      dataTypeEntreprise            ;                     
	protected List<TypeEntrepriseDto>                                datasTypeEntreprise           ;                     

	//----------------------------------------------------------------------
    // GETTER(S) & SETTER(S) FOR DATA FIELDS
    //----------------------------------------------------------------------

	/**
     * Get the "dataAbonne" field value
     * @return the field value
     */
	public AbonneDto getDataAbonne() {
		return dataAbonne;
	}
	/**
     * Set the "dataAbonne" field value
     * @param dataAbonne
     */
	public void setDataAbonne(AbonneDto dataAbonne) {
		this.dataAbonne = dataAbonne;
	}
	/**
     * Get the "datasAbonne" field value
     * @return the field value
     */
	public List<AbonneDto> getDatasAbonne() {
		return datasAbonne;
	}
	/**
     * Set the "datasAbonne" field value
     * @param datasAbonne
     */
	public void setDatasAbonne(List<AbonneDto> datasAbonne) {
		this.datasAbonne = datasAbonne;
	} 

	/**
     * Get the "dataAbonneChanel" field value
     * @return the field value
     */
	public AbonneChanelDto getDataAbonneChanel() {
		return dataAbonneChanel;
	}
	/**
     * Set the "dataAbonneChanel" field value
     * @param dataAbonneChanel
     */
	public void setDataAbonneChanel(AbonneChanelDto dataAbonneChanel) {
		this.dataAbonneChanel = dataAbonneChanel;
	}
	/**
     * Get the "datasAbonneChanel" field value
     * @return the field value
     */
	public List<AbonneChanelDto> getDatasAbonneChanel() {
		return datasAbonneChanel;
	}
	/**
     * Set the "datasAbonneChanel" field value
     * @param datasAbonneChanel
     */
	public void setDatasAbonneChanel(List<AbonneChanelDto> datasAbonneChanel) {
		this.datasAbonneChanel = datasAbonneChanel;
	} 

	/**
     * Get the "dataAbonneDemande" field value
     * @return the field value
     */
	public AbonneDemandeDto getDataAbonneDemande() {
		return dataAbonneDemande;
	}
	/**
     * Set the "dataAbonneDemande" field value
     * @param dataAbonneDemande
     */
	public void setDataAbonneDemande(AbonneDemandeDto dataAbonneDemande) {
		this.dataAbonneDemande = dataAbonneDemande;
	}
	/**
     * Get the "datasAbonneDemande" field value
     * @return the field value
     */
	public List<AbonneDemandeDto> getDatasAbonneDemande() {
		return datasAbonneDemande;
	}
	/**
     * Set the "datasAbonneDemande" field value
     * @param datasAbonneDemande
     */
	public void setDatasAbonneDemande(List<AbonneDemandeDto> datasAbonneDemande) {
		this.datasAbonneDemande = datasAbonneDemande;
	} 

	/**
     * Get the "dataCategorieProduit" field value
     * @return the field value
     */
	public CategorieProduitDto getDataCategorieProduit() {
		return dataCategorieProduit;
	}
	/**
     * Set the "dataCategorieProduit" field value
     * @param dataCategorieProduit
     */
	public void setDataCategorieProduit(CategorieProduitDto dataCategorieProduit) {
		this.dataCategorieProduit = dataCategorieProduit;
	}
	/**
     * Get the "datasCategorieProduit" field value
     * @return the field value
     */
	public List<CategorieProduitDto> getDatasCategorieProduit() {
		return datasCategorieProduit;
	}
	/**
     * Set the "datasCategorieProduit" field value
     * @param datasCategorieProduit
     */
	public void setDatasCategorieProduit(List<CategorieProduitDto> datasCategorieProduit) {
		this.datasCategorieProduit = datasCategorieProduit;
	} 

	/**
     * Get the "dataChanel" field value
     * @return the field value
     */
	public ChanelDto getDataChanel() {
		return dataChanel;
	}
	/**
     * Set the "dataChanel" field value
     * @param dataChanel
     */
	public void setDataChanel(ChanelDto dataChanel) {
		this.dataChanel = dataChanel;
	}
	/**
     * Get the "datasChanel" field value
     * @return the field value
     */
	public List<ChanelDto> getDatasChanel() {
		return datasChanel;
	}
	/**
     * Set the "datasChanel" field value
     * @param datasChanel
     */
	public void setDatasChanel(List<ChanelDto> datasChanel) {
		this.datasChanel = datasChanel;
	} 

	/**
     * Get the "dataChanelPermissions" field value
     * @return the field value
     */
	public ChanelPermissionsDto getDataChanelPermissions() {
		return dataChanelPermissions;
	}
	/**
     * Set the "dataChanelPermissions" field value
     * @param dataChanelPermissions
     */
	public void setDataChanelPermissions(ChanelPermissionsDto dataChanelPermissions) {
		this.dataChanelPermissions = dataChanelPermissions;
	}
	/**
     * Get the "datasChanelPermissions" field value
     * @return the field value
     */
	public List<ChanelPermissionsDto> getDatasChanelPermissions() {
		return datasChanelPermissions;
	}
	/**
     * Set the "datasChanelPermissions" field value
     * @param datasChanelPermissions
     */
	public void setDatasChanelPermissions(List<ChanelPermissionsDto> datasChanelPermissions) {
		this.datasChanelPermissions = datasChanelPermissions;
	} 

	/**
     * Get the "dataChanelUserPermissions" field value
     * @return the field value
     */
	public ChanelUserPermissionsDto getDataChanelUserPermissions() {
		return dataChanelUserPermissions;
	}
	/**
     * Set the "dataChanelUserPermissions" field value
     * @param dataChanelUserPermissions
     */
	public void setDataChanelUserPermissions(ChanelUserPermissionsDto dataChanelUserPermissions) {
		this.dataChanelUserPermissions = dataChanelUserPermissions;
	}
	/**
     * Get the "datasChanelUserPermissions" field value
     * @return the field value
     */
	public List<ChanelUserPermissionsDto> getDatasChanelUserPermissions() {
		return datasChanelUserPermissions;
	}
	/**
     * Set the "datasChanelUserPermissions" field value
     * @param datasChanelUserPermissions
     */
	public void setDatasChanelUserPermissions(List<ChanelUserPermissionsDto> datasChanelUserPermissions) {
		this.datasChanelUserPermissions = datasChanelUserPermissions;
	} 

	/**
     * Get the "dataChanelUsers" field value
     * @return the field value
     */
	public ChanelUsersDto getDataChanelUsers() {
		return dataChanelUsers;
	}
	/**
     * Set the "dataChanelUsers" field value
     * @param dataChanelUsers
     */
	public void setDataChanelUsers(ChanelUsersDto dataChanelUsers) {
		this.dataChanelUsers = dataChanelUsers;
	}
	/**
     * Get the "datasChanelUsers" field value
     * @return the field value
     */
	public List<ChanelUsersDto> getDatasChanelUsers() {
		return datasChanelUsers;
	}
	/**
     * Set the "datasChanelUsers" field value
     * @param datasChanelUsers
     */
	public void setDatasChanelUsers(List<ChanelUsersDto> datasChanelUsers) {
		this.datasChanelUsers = datasChanelUsers;
	} 

	/**
     * Get the "dataCountry" field value
     * @return the field value
     */
	public CountryDto getDataCountry() {
		return dataCountry;
	}
	/**
     * Set the "dataCountry" field value
     * @param dataCountry
     */
	public void setDataCountry(CountryDto dataCountry) {
		this.dataCountry = dataCountry;
	}
	/**
     * Get the "datasCountry" field value
     * @return the field value
     */
	public List<CountryDto> getDatasCountry() {
		return datasCountry;
	}
	/**
     * Set the "datasCountry" field value
     * @param datasCountry
     */
	public void setDatasCountry(List<CountryDto> datasCountry) {
		this.datasCountry = datasCountry;
	} 

	/**
     * Get the "dataImageProduit" field value
     * @return the field value
     */
	public ImageProduitDto getDataImageProduit() {
		return dataImageProduit;
	}
	/**
     * Set the "dataImageProduit" field value
     * @param dataImageProduit
     */
	public void setDataImageProduit(ImageProduitDto dataImageProduit) {
		this.dataImageProduit = dataImageProduit;
	}
	/**
     * Get the "datasImageProduit" field value
     * @return the field value
     */
	public List<ImageProduitDto> getDatasImageProduit() {
		return datasImageProduit;
	}
	/**
     * Set the "datasImageProduit" field value
     * @param datasImageProduit
     */
	public void setDatasImageProduit(List<ImageProduitDto> datasImageProduit) {
		this.datasImageProduit = datasImageProduit;
	} 

	/**
     * Get the "dataMarque" field value
     * @return the field value
     */
	public MarqueDto getDataMarque() {
		return dataMarque;
	}
	/**
     * Set the "dataMarque" field value
     * @param dataMarque
     */
	public void setDataMarque(MarqueDto dataMarque) {
		this.dataMarque = dataMarque;
	}
	/**
     * Get the "datasMarque" field value
     * @return the field value
     */
	public List<MarqueDto> getDatasMarque() {
		return datasMarque;
	}
	/**
     * Set the "datasMarque" field value
     * @param datasMarque
     */
	public void setDatasMarque(List<MarqueDto> datasMarque) {
		this.datasMarque = datasMarque;
	}

	/**
     * Get the "dataPermissionActions" field value
     * @return the field value
     */
	public PermissionActionsDto getDataPermissionActions() {
		return dataPermissionActions;
	}
	/**
     * Set the "dataPermissionActions" field value
     * @param dataPermissionActions
     */
	public void setDataPermissionActions(PermissionActionsDto dataPermissionActions) {
		this.dataPermissionActions = dataPermissionActions;
	}
	/**
     * Get the "datasPermissionActions" field value
     * @return the field value
     */
	public List<PermissionActionsDto> getDatasPermissionActions() {
		return datasPermissionActions;
	}
	/**
     * Set the "datasPermissionActions" field value
     * @param datasPermissionActions
     */
	public void setDatasPermissionActions(List<PermissionActionsDto> datasPermissionActions) {
		this.datasPermissionActions = datasPermissionActions;
	} 

	/**
     * Get the "dataPermissionProfil" field value
     * @return the field value
     */
	public PermissionProfilDto getDataPermissionProfil() {
		return dataPermissionProfil;
	}
	/**
     * Set the "dataPermissionProfil" field value
     * @param dataPermissionProfil
     */
	public void setDataPermissionProfil(PermissionProfilDto dataPermissionProfil) {
		this.dataPermissionProfil = dataPermissionProfil;
	}
	/**
     * Get the "datasPermissionProfil" field value
     * @return the field value
     */
	public List<PermissionProfilDto> getDatasPermissionProfil() {
		return datasPermissionProfil;
	}
	/**
     * Set the "datasPermissionProfil" field value
     * @param datasPermissionProfil
     */
	public void setDatasPermissionProfil(List<PermissionProfilDto> datasPermissionProfil) {
		this.datasPermissionProfil = datasPermissionProfil;
	} 

	/**
     * Get the "dataPermissionUsersChanel" field value
     * @return the field value
     */
	public PermissionUsersChanelDto getDataPermissionUsersChanel() {
		return dataPermissionUsersChanel;
	}
	/**
     * Set the "dataPermissionUsersChanel" field value
     * @param dataPermissionUsersChanel
     */
	public void setDataPermissionUsersChanel(PermissionUsersChanelDto dataPermissionUsersChanel) {
		this.dataPermissionUsersChanel = dataPermissionUsersChanel;
	}
	/**
     * Get the "datasPermissionUsersChanel" field value
     * @return the field value
     */
	public List<PermissionUsersChanelDto> getDatasPermissionUsersChanel() {
		return datasPermissionUsersChanel;
	}
	/**
     * Set the "datasPermissionUsersChanel" field value
     * @param datasPermissionUsersChanel
     */
	public void setDatasPermissionUsersChanel(List<PermissionUsersChanelDto> datasPermissionUsersChanel) {
		this.datasPermissionUsersChanel = datasPermissionUsersChanel;
	} 

	/**
     * Get the "dataProduit" field value
     * @return the field value
     */
	public ProduitDto getDataProduit() {
		return dataProduit;
	}
	/**
     * Set the "dataProduit" field value
     * @param dataProduit
     */
	public void setDataProduit(ProduitDto dataProduit) {
		this.dataProduit = dataProduit;
	}
	/**
     * Get the "datasProduit" field value
     * @return the field value
     */
	public List<ProduitDto> getDatasProduit() {
		return datasProduit;
	}
	/**
     * Set the "datasProduit" field value
     * @param datasProduit
     */
	public void setDatasProduit(List<ProduitDto> datasProduit) {
		this.datasProduit = datasProduit;
	} 

	/**
     * Get the "dataSecteurActivite" field value
     * @return the field value
     */
	public SecteurActiviteDto getDataSecteurActivite() {
		return dataSecteurActivite;
	}
	/**
     * Set the "dataSecteurActivite" field value
     * @param dataSecteurActivite
     */
	public void setDataSecteurActivite(SecteurActiviteDto dataSecteurActivite) {
		this.dataSecteurActivite = dataSecteurActivite;
	}
	/**
     * Get the "datasSecteurActivite" field value
     * @return the field value
     */
	public List<SecteurActiviteDto> getDatasSecteurActivite() {
		return datasSecteurActivite;
	}
	/**
     * Set the "datasSecteurActivite" field value
     * @param datasSecteurActivite
     */
	public void setDatasSecteurActivite(List<SecteurActiviteDto> datasSecteurActivite) {
		this.datasSecteurActivite = datasSecteurActivite;
	} 

	/**
     * Get the "dataSousCategorieProduit" field value
     * @return the field value
     */
	public SousCategorieProduitDto getDataSousCategorieProduit() {
		return dataSousCategorieProduit;
	}
	/**
     * Set the "dataSousCategorieProduit" field value
     * @param dataSousCategorieProduit
     */
	public void setDataSousCategorieProduit(SousCategorieProduitDto dataSousCategorieProduit) {
		this.dataSousCategorieProduit = dataSousCategorieProduit;
	}
	/**
     * Get the "datasSousCategorieProduit" field value
     * @return the field value
     */
	public List<SousCategorieProduitDto> getDatasSousCategorieProduit() {
		return datasSousCategorieProduit;
	}
	/**
     * Set the "datasSousCategorieProduit" field value
     * @param datasSousCategorieProduit
     */
	public void setDatasSousCategorieProduit(List<SousCategorieProduitDto> datasSousCategorieProduit) {
		this.datasSousCategorieProduit = datasSousCategorieProduit;
	} 

	/**
     * Get the "dataTypeEntreprise" field value
     * @return the field value
     */
	public TypeEntrepriseDto getDataTypeEntreprise() {
		return dataTypeEntreprise;
	}
	/**
     * Set the "dataTypeEntreprise" field value
     * @param dataTypeEntreprise
     */
	public void setDataTypeEntreprise(TypeEntrepriseDto dataTypeEntreprise) {
		this.dataTypeEntreprise = dataTypeEntreprise;
	}
	/**
     * Get the "datasTypeEntreprise" field value
     * @return the field value
     */
	public List<TypeEntrepriseDto> getDatasTypeEntreprise() {
		return datasTypeEntreprise;
	}
	/**
     * Set the "datasTypeEntreprise" field value
     * @param datasTypeEntreprise
     */
	public void setDatasTypeEntreprise(List<TypeEntrepriseDto> datasTypeEntreprise) {
		this.datasTypeEntreprise = datasTypeEntreprise;
	} 


}