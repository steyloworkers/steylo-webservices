/*
 * To change this license header, choose License Headers in Project Properties.

 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ino.steylo.helper.contrat;

/**
 *
 * @author
 */
public class RequestBase {
	protected String	sessionUser;
	protected Integer	size;
	protected Integer	index;
	protected String	lang;
	protected String	businessLineCode;
	protected String	caseEngine;

	/**
	 * @return the businessLineCode
	 */
	public String getBusinessLineCode() {
		return businessLineCode;
	}

	/**
	 * @param businessLineCode
	 *            the businessLineCode to set
	 */
	public void setBusinessLineCode(String businessLineCode) {
		this.businessLineCode = businessLineCode;
	}

	/**
	 * @return the caseEngine
	 */
	public String getCaseEngine() {
		return caseEngine;
	}

	/**
	 * @param caseEngine
	 *            the caseEngine to set
	 */
	public void setCaseEngine(String caseEngine) {
		this.caseEngine = caseEngine;
	}

	public String getSessionUser() {
		return sessionUser;
	}

	public void setSessionUser(String sessionUser) {
		this.sessionUser = sessionUser;
	}

	public Integer getSize() {
		return size;
	}

	public void setSize(Integer size) {
		this.size = size;
	}

	public Integer getIndex() {
		return index;
	}

	public void setIndex(Integer index) {
		this.index = index;
	}

	public String getLang() {
		return lang;
	}

	public void setLang(String lang) {
		this.lang = lang;
	}

}
