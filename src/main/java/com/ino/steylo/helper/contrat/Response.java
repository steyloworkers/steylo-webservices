
package com.ino.steylo.helper.contrat;

import java.util.List;
import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.ino.steylo.helper.dto.*;

@XmlRootElement
@JsonInclude(Include.NON_NULL)
public class Response extends ResponseBase {
	
	protected List<AbonneDto>                    itemsAbonne                   ;                     
	protected List<AbonneChanelDto>              itemsAbonneChanel             ;                     
	protected List<AbonneDemandeDto>             itemsAbonneDemande            ;                     
	protected List<CategorieProduitDto>          itemsCategorieProduit         ;                     
	protected List<ChanelDto>                    itemsChanel                   ;                     
	protected List<ChanelPermissionsDto>         itemsChanelPermissions        ;                     
	protected List<ChanelUserPermissionsDto>     itemsChanelUserPermissions    ;                     
	protected ChanelUsersDto		             itemChanelUsers               ;
	protected List<ChanelUsersDto>               itemsChanelUsers              ;
	protected List<CountryDto>                   itemsCountry                  ;
	protected List<ImageProduitDto>              itemsImageProduit             ;                     
	protected List<MarqueDto>                    itemsMarque                   ;                     
	protected List<PermissionActionsDto>         itemsPermissionActions        ;
	protected List<PermissionProfilDto>          itemsPermissionProfil         ;                     
	protected List<PermissionUsersChanelDto>     itemsPermissionUsersChanel    ;                     
	protected List<ProduitDto>                   itemsProduit                  ;                     
	protected List<SecteurActiviteDto>           itemsSecteurActivite          ;                     
	protected List<SousCategorieProduitDto>      itemsSousCategorieProduit     ;                     
	protected List<TypeEntrepriseDto>            itemsTypeEntreprise           ;                     

	//----------------------------------------------------------------------
    // GETTER(S) & SETTER(S) FOR DATA FIELDS
    //----------------------------------------------------------------------

	/**
     * Get the "itemsAbonne" field value
     * @return the field value
     */
	public List<AbonneDto> getItemsAbonne() {
		return itemsAbonne;
	}
	/**
     * Set the "itemsAbonne" field value
     * @param itemsAbonne
     */
	public void setItemsAbonne(List<AbonneDto> itemsAbonne) {
		this.itemsAbonne = itemsAbonne;
	} 

	/**
     * Get the "itemsAbonneChanel" field value
     * @return the field value
     */
	public List<AbonneChanelDto> getItemsAbonneChanel() {
		return itemsAbonneChanel;
	}
	/**
     * Set the "itemsAbonneChanel" field value
     * @param itemsAbonneChanel
     */
	public void setItemsAbonneChanel(List<AbonneChanelDto> itemsAbonneChanel) {
		this.itemsAbonneChanel = itemsAbonneChanel;
	} 

	/**
     * Get the "itemsAbonneDemande" field value
     * @return the field value
     */
	public List<AbonneDemandeDto> getItemsAbonneDemande() {
		return itemsAbonneDemande;
	}
	/**
     * Set the "itemsAbonneDemande" field value
     * @param itemsAbonneDemande
     */
	public void setItemsAbonneDemande(List<AbonneDemandeDto> itemsAbonneDemande) {
		this.itemsAbonneDemande = itemsAbonneDemande;
	} 

	/**
     * Get the "itemsCategorieProduit" field value
     * @return the field value
     */
	public List<CategorieProduitDto> getItemsCategorieProduit() {
		return itemsCategorieProduit;
	}
	/**
     * Set the "itemsCategorieProduit" field value
     * @param itemsCategorieProduit
     */
	public void setItemsCategorieProduit(List<CategorieProduitDto> itemsCategorieProduit) {
		this.itemsCategorieProduit = itemsCategorieProduit;
	} 

	/**
     * Get the "itemsChanel" field value
     * @return the field value
     */
	public List<ChanelDto> getItemsChanel() {
		return itemsChanel;
	}
	/**
     * Set the "itemsChanel" field value
     * @param itemsChanel
     */
	public void setItemsChanel(List<ChanelDto> itemsChanel) {
		this.itemsChanel = itemsChanel;
	} 

	/**
     * Get the "itemsChanelPermissions" field value
     * @return the field value
     */
	public List<ChanelPermissionsDto> getItemsChanelPermissions() {
		return itemsChanelPermissions;
	}
	/**
     * Set the "itemsChanelPermissions" field value
     * @param itemsChanelPermissions
     */
	public void setItemsChanelPermissions(List<ChanelPermissionsDto> itemsChanelPermissions) {
		this.itemsChanelPermissions = itemsChanelPermissions;
	} 

	/**
     * Get the "itemsChanelUserPermissions" field value
     * @return the field value
     */
	public List<ChanelUserPermissionsDto> getItemsChanelUserPermissions() {
		return itemsChanelUserPermissions;
	}
	/**
     * Set the "itemsChanelUserPermissions" field value
     * @param itemsChanelUserPermissions
     */
	public void setItemsChanelUserPermissions(List<ChanelUserPermissionsDto> itemsChanelUserPermissions) {
		this.itemsChanelUserPermissions = itemsChanelUserPermissions;
	} 

	/**
     * Get the "itemsChanelUsers" field value
     * @return the field value
     */
	public List<ChanelUsersDto> getItemsChanelUsers() {
		return itemsChanelUsers;
	}
	/**
     * Set the "itemsChanelUsers" field value
     * @param itemsChanelUsers
     */
	public void setItemsChanelUsers(List<ChanelUsersDto> itemsChanelUsers) {
		this.itemsChanelUsers = itemsChanelUsers;
	} 

	/**
     * Get the "itemsCountry" field value
     * @return the field value
     */
	public List<CountryDto> getItemsCountry() {
		return itemsCountry;
	}
	/**
     * Set the "itemsCountry" field value
     * @param itemsCountry
     */
	public void setItemsCountry(List<CountryDto> itemsCountry) {
		this.itemsCountry = itemsCountry;
	} 

	/**
     * Get the "itemsImageProduit" field value
     * @return the field value
     */
	public List<ImageProduitDto> getItemsImageProduit() {
		return itemsImageProduit;
	}
	/**
     * Set the "itemsImageProduit" field value
     * @param itemsImageProduit
     */
	public void setItemsImageProduit(List<ImageProduitDto> itemsImageProduit) {
		this.itemsImageProduit = itemsImageProduit;
	} 

	/**
     * Get the "itemsMarque" field value
     * @return the field value
     */
	public List<MarqueDto> getItemsMarque() {
		return itemsMarque;
	}
	/**
     * Set the "itemsMarque" field value
     * @param itemsMarque
     */
	public void setItemsMarque(List<MarqueDto> itemsMarque) {
		this.itemsMarque = itemsMarque;
	}

	/**
     * Get the "itemsPermissionActions" field value
     * @return the field value
     */
	public List<PermissionActionsDto> getItemsPermissionActions() {
		return itemsPermissionActions;
	}
	/**
     * Set the "itemsPermissionActions" field value
     * @param itemsPermissionActions
     */
	public void setItemsPermissionActions(List<PermissionActionsDto> itemsPermissionActions) {
		this.itemsPermissionActions = itemsPermissionActions;
	} 

	/**
     * Get the "itemsPermissionProfil" field value
     * @return the field value
     */
	public List<PermissionProfilDto> getItemsPermissionProfil() {
		return itemsPermissionProfil;
	}
	/**
     * Set the "itemsPermissionProfil" field value
     * @param itemsPermissionProfil
     */
	public void setItemsPermissionProfil(List<PermissionProfilDto> itemsPermissionProfil) {
		this.itemsPermissionProfil = itemsPermissionProfil;
	} 

	/**
     * Get the "itemsPermissionUsersChanel" field value
     * @return the field value
     */
	public List<PermissionUsersChanelDto> getItemsPermissionUsersChanel() {
		return itemsPermissionUsersChanel;
	}
	/**
     * Set the "itemsPermissionUsersChanel" field value
     * @param itemsPermissionUsersChanel
     */
	public void setItemsPermissionUsersChanel(List<PermissionUsersChanelDto> itemsPermissionUsersChanel) {
		this.itemsPermissionUsersChanel = itemsPermissionUsersChanel;
	} 

	/**
     * Get the "itemsProduit" field value
     * @return the field value
     */
	public List<ProduitDto> getItemsProduit() {
		return itemsProduit;
	}
	/**
     * Set the "itemsProduit" field value
     * @param itemsProduit
     */
	public void setItemsProduit(List<ProduitDto> itemsProduit) {
		this.itemsProduit = itemsProduit;
	} 

	/**
     * Get the "itemsSecteurActivite" field value
     * @return the field value
     */
	public List<SecteurActiviteDto> getItemsSecteurActivite() {
		return itemsSecteurActivite;
	}
	/**
     * Set the "itemsSecteurActivite" field value
     * @param itemsSecteurActivite
     */
	public void setItemsSecteurActivite(List<SecteurActiviteDto> itemsSecteurActivite) {
		this.itemsSecteurActivite = itemsSecteurActivite;
	} 

	/**
     * Get the "itemsSousCategorieProduit" field value
     * @return the field value
     */
	public List<SousCategorieProduitDto> getItemsSousCategorieProduit() {
		return itemsSousCategorieProduit;
	}
	/**
     * Set the "itemsSousCategorieProduit" field value
     * @param itemsSousCategorieProduit
     */
	public void setItemsSousCategorieProduit(List<SousCategorieProduitDto> itemsSousCategorieProduit) {
		this.itemsSousCategorieProduit = itemsSousCategorieProduit;
	} 

	/**
     * Get the "itemsTypeEntreprise" field value
     * @return the field value
     */
	public List<TypeEntrepriseDto> getItemsTypeEntreprise() {
		return itemsTypeEntreprise;
	}
	/**
     * Set the "itemsTypeEntreprise" field value
     * @param itemsTypeEntreprise
     */
	public void setItemsTypeEntreprise(List<TypeEntrepriseDto> itemsTypeEntreprise) {
		this.itemsTypeEntreprise = itemsTypeEntreprise;
	}

	public ChanelUsersDto getItemChanelUsers() {
		return itemChanelUsers;
	}

	public void setItemChanelUsers(ChanelUsersDto itemChanelUsers) {
		this.itemChanelUsers = itemChanelUsers;
	}
}