/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ino.steylo.helper.contrat;

import java.util.Locale;

/**
 *
 * @author ino
 */

public interface IBasicBusiness<T, K> {

	/**
	 * create Object by using T as object.
	 * 
	 * @param request, locale
	 * @return K
	 * 
	 */
	public abstract K create(T request, Locale locale);

	/**
	 * update Object by using T as object.
	 * 
	 * @param request, locale
	 * @return K
	 * 
	 */
	public abstract K update(T request, Locale locale);

	/**
	 * delete Object by using T as object.
	 * 
	 * @param request, locale
	 * @return K
	 * 
	 */
	public abstract K delete(T request, Locale locale);

	/**
	 * get a List of Object by using T as criteria object.
	 * 
	 * @param request, locale
	 * @return K
	 * 
	 */
	public abstract K getByCriteria(T request, Locale locale);
}
